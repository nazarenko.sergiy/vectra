//#define F_CPU 1000000UL  // 1 MHz
// https://www.electronics-lab.com/project/programming-attiny10-platform-io-ide/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>


#define PIN_ODROID PB1
#define PIN_MOSFET PB2
#define PIN_CHECK  PB0


#define STATE_WAIT 0
#define STATE_XU4_SIGNAL 1
#define STATE_WATCH 2

volatile uint8_t state = 0;
volatile uint16_t ovf_counter = 0;


// https://www.shermluge.com/electronics/attiny10/attiny10-reset-as-mode-button

#define EXT_RESET (1 << EXTRF)

volatile unsigned char count __attribute__((section(".noinit"))); // Not cleared on RESET

void ioinit(void)
{
    DDRB = 0b00000110;
    DDRB &= ~(1 << PIN_CHECK); // input
    //PUEB = 0b00000001;

    //EIMSK = 0x01; // turn on INT0
    //EICRA |= 0x01; //detect falling edge ext int on ISC0 0 and 1
    //PCMSK = 0b00000001; // PC0 interrupt is enabled
    PCICR |= (1 << PCIE0);
    PCMSK |= (1 << PCINT0);

    //ADMUX = (1 << PIN_CHECK);
    //ADCSRA = (1<<ADEN) | (1<<ADPS1) | (1<<ADPS0);
    //ADCSRA = (1<<ADEN)|(1<<ADPS1)|(1<<ADPS1)|(1<<ADPS0);
    //PORTB |= 1 << PIN_CHECK; // pull up
    //PORTB &= ~(1 << PIN_CHECK); // pull up
    //DDRB = (1<<PIN_ODROID)|(1<<PIN_MOSFET);

    PORTB &=~ (1 << PIN_ODROID);
    PORTB |= (1 << PIN_MOSFET); // close MOSFET
     
    // //set timer1 interrupt at 1Hz
    TCCR0A = 0;// set entire TCCR1A register to 0
    TCCR0B = (1<<1)|(1<<WGM02);// same for TCCR1B
    
    OCR0A = 15; // 1 000 000 / 64 //This flag is set in the timer clock cycle after the counter 
    TIMSK0 |= (1<<OCIE0A);
    TCCR0B |= (1 << CS00) | (1 << CS01); // prescaler 1:64
    // 1 / 1000000 / 64 / 256 = 164 ms
    // 
    // 100.16025641025641 Hz (1000000/((155+1)*64))
    // 976.5625 Hz (1000000/((15+1)*64))
    sei();
}


int main(void)
{
    static uint8_t state_previous = 0;
    state = state_previous = STATE_WATCH;

    if ((RSTFLR & EXT_RESET) != 0) 
    {
      // If External RESET then increment count and output to pins
      RSTFLR = RSTFLR & ~EXT_RESET;
      PORTB = ++count;
    } 
    else 
    {
      PORTB = count = 0;
    }

    ioinit();

    if (count > 0)
    {
      PORTB |= (1<<PIN_MOSFET); ///close MOSFET
      count = 0;
    }

    uint8_t val = 0;

    while(1)
    {
      switch(state)
      {
        case STATE_WATCH:
          // val = (PINB & (1 << PIN_CHECK)) >> PIN_CHECK;
          // if( val == 0 )
          // {
          //   PORTB |= (1<<PIN_ODROID); // odroid setup
          //   state_previous = state;
          //   state = STATE_XU4_SIGNAL;
          // }
          ovf_counter=0;
          break;
        case STATE_XU4_SIGNAL:
          if (ovf_counter >= 500)
          {
            if (state_previous == STATE_WAIT)
            {
              state = STATE_WATCH;
            }
            if (state_previous == STATE_WATCH)
            {
              state = STATE_WAIT;
            }
            PORTB &= ~(1 << PIN_ODROID); // 500 ms -> goes low
            ovf_counter = 0;
          }
          break;
        case STATE_WAIT:
          // val = (PINB & (1 << PIN_CHECK)) >> PIN_CHECK;
          // if( val == 1 )
          // {
          //   PORTB |= (1<<PIN_ODROID); // odroid setup
          //   state_previous = state;
          //   state = STATE_XU4_SIGNAL;
          // }
          ovf_counter = 0;
          break;
        default:
          break;
      }
    }
}

ISR(TIM0_COMPA_vect)
{
  ovf_counter++;
}

ISR(PCINT0_vect)
{
  PORTB |= (1<<PIN_ODROID);
  state = STATE_XU4_SIGNAL;
  ovf_counter = 0;
}

