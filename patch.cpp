#include <stdio.h>
#include <stdlib.h>     /* atoi */

int main(int argc, char *argv[]) 
{
    int i = strtol(argv[1], NULL, 16);
    FILE *f = fopen("/home/odroid/test_can", "r+b");
    unsigned char opcode = i;
    fseek(f, 0x3d50, SEEK_SET);
    fwrite(&opcode, sizeof(opcode), 1, f);
    fclose(f);
}