#include "vectra.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>


#define CAN_HELP 1
#define CAN_DRAIN  2
#define CSR_HOME "\33[H"
#define CLR_SCREEN "\33[2J"

void usage(const char * s) {
    printf("Usage: %s <drain>\n", s);
}
// https://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit
int main (int argc, char ** argv) 
{

    int mode = 0;
    if ( argc > 1 ) 
    {
        if (!strcmp(argv[1], "help")) 
        {
            usage(argv[0]);
            return 1;
        }
        if (!strcmp(argv[1], "drain"))
            mode = CAN_DRAIN;
    }

    //printf("\033[H\033[J");
    //printf("\e[2J\e[H"); 
    printf("%s%s", CLR_SCREEN, CSR_HOME);
    printf("%s", CSR_HOME);
    std::vector<int> data_can;
    data_can.reserve(CAN_MEM_ARR_SIZE);
    data_can.assign(CAN_MEM_ARR_SIZE, -1);

    struct ifreq ifr;
    struct sockaddr_can addr;
 
    struct can_frame frame;
    int s;

    memset(&ifr, 0x0, sizeof(ifr));
    memset(&addr, 0x0, sizeof(addr));
    memset(&frame, 0x0, sizeof(frame));

    /* open CAN_RAW socket */
    s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    /* convert interface sting "can0" into interface index */ 
    strcpy(ifr.ifr_name, "can2");
    ioctl(s, SIOCGIFINDEX, &ifr);

    /* setup address for bind */
    addr.can_ifindex = ifr.ifr_ifindex;
    addr.can_family = PF_CAN;

    /* bind socket to the can0 interface */
    bind(s, (struct sockaddr *)&addr, sizeof(addr));

    int n = 0;
    int ret;

    frame.can_id = 321;
    for(k = 0 ; k <= 7 ; k++) {
        frame.data[k] = 0x00;
    }
    frame.can_dlc = 8;

    data_can = arr_read_can();
    data_can[BLOCKM_CALL_A] = 0;
    data_can[BLOCKM_CALL_B] = 0;
    arr_write_can(data_can);

    while( true )
    {
        data_can = arr_read_can();

        frame.data[0] = (data_can[BLOCKM_CALL_A]&0xFF);         //extract first byte
        frame.data[1] = ((data_can[BLOCKM_CALL_A]>>8)&0xFF);    //extract second byte
        frame.data[2] = ((data_can[BLOCKM_CALL_A]>>16)&0xFF);   //extract third byte
        frame.data[3] = ((data_can[BLOCKM_CALL_A]>>24)&0xFF);   //extract fourth byte

        frame.data[4] = (data_can[BLOCKM_CALL_B]&0xFF);         //extract first byte
        frame.data[5] = ((data_can[BLOCKM_CALL_B]>>8)&0xFF);    //extract second byte
        frame.data[6] = ((data_can[BLOCKM_CALL_B]>>16)&0xFF);   //extract third byte
        frame.data[7] = ((data_can[BLOCKM_CALL_B]>>24)&0xFF);   //extract fourth byte

        ret = write(s, &frame, sizeof(frame));
        if(ret < 0 ) {
            perror("can write");
            sleep(40);
        }
        sleep(1);
    }

    return 0;
}

