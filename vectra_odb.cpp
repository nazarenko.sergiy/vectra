#include <stdint.h>
#include <cstdlib>
#include <pthread.h>
#include <linux/version.h>
#include <linux/input.h>
#include <linux/reboot.h>
#include <sys/reboot.h>
#include <grp.h>
#include <pwd.h>
#include <error.h>
#include <errno.h>
#include <signal.h>
 #include <iostream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "yaml-cpp/yaml.h"
#include "vectra_timer.h"
#include "vectra.h"

#include <time.h>

// https://bdebyl.net/post/stm32-part1/

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  ((byte) & 0x80 ? '1' : '0'), \
  ((byte) & 0x40 ? '1' : '0'), \
  ((byte) & 0x20 ? '1' : '0'), \
  ((byte) & 0x10 ? '1' : '0'), \
  ((byte) & 0x08 ? '1' : '0'), \
  ((byte) & 0x04 ? '1' : '0'), \
  ((byte) & 0x02 ? '1' : '0'), \
  ((byte) & 0x01 ? '1' : '0') 
  

// https://linuxhint.com/pthread-cond-wait-function-c/

// 00 - 99999 | 7E8 | 06 41 00 BE 3F B8 11 AA .A..?...  1   0   1   1   1   1   1   0   0   0   1   1   1   1   1   1   1   0   1   1   1   0   0   0   0   0   0   1   0   0   0   1
//                                                      01	02	03	04	05	06	07	08	09	0A	0B	0C	0D	0E	0F	10	11	12	13	14	15	16	17	18	19	1A	1B	1C	1D	1E	1F	20
// 20 - 85760 | 7E8 | 06 41 20 80 00 00 00 AA .A .....  1000 0000
// auto 02184 | 23A | 00000100 00000000 00000000
// 0    39254 | 23A | 00000000 00000000 00000000
// gabarity 34179 | 23A | 00000000 00100000 00000000
// onn 31801 | 23A | 00000000 10100000 00000000

std::vector<fptr> canfunctions;

struct Odb2
{
    std::string name;
    int service;
    int period_ms;
    int progress_ms;
};

static std::vector<Odb2> odb_services;

void operator >> (const YAML::Node& node, Odb2& monster) 
{
   monster.name = node["name"].as<std::string>();
   monster.service = node["service"].as<int>();
   monster.period_ms = monster.progress_ms = node["period_ms"].as<int>();
}

pthread_mutex_t mutexService ;
pthread_cond_t condSend;    // creating the condition variable.

pthread_mutex_t mutexTime ;
pthread_cond_t condTime;    // creating the condition variable.


typedef enum {
	SHOW_ODB2 = 0x00,
	SHOW_DTC,
	SHOW_VERSIONS,
	SHOW_BOOTLOADER_VERSION,
    SHOW_TIMESTAMP,
    SHOW_GIT_TIMESTAMP,
    SHOW_BOOTLOADER_GIT_TIMESTAMP,
    SHOW_BOOTLOADER_TIMESTAMP,
	SHOW_NC
} TypeQueries_t;

static TypeQueries_t show_current_data;

static int service_id;

void time_handler2(size_t timer_id, void * user_data)
{
        pthread_mutex_unlock(&mutexTime) ;
        pthread_cond_signal(&condTime) ;
}
/*
void* fuelling(void* arg) 
{
    for (int i = 0; i < 5; i++) 
    {
        pthread_mutex_lock(&mutexFuel);
        fuel += 15 ;
        printf ( " Got fuel... %d \n " ,  fuel ) ;
        pthread_mutex_unlock(&mutexFuel) ;
        pthread_cond_signal(&condFuel) ;
        sleep(1);
    }
}

void* vehicle(void* arg) 
{
pthread_mutex_lock(&mutexFuel);
    while (fuel < 40) 
    {
        printf ( " Zero fuel. Waiting... \n " ) ;
pthread_cond_wait(&condFuel, &mutexFuel);
        // Equivalent to:
        // pthread_mutex_unlock(&mutexFuel);
        // wait for signal on condFuel
        // pthread_mutex_lock(&mutexFuel) ;
    }
    fuel -= 40 ;
    printf ( " fuel collected Now left: %d \n " , fuel ) ;
pthread_mutex_unlock(&mutexFuel);
}
*/

#define MODE_BOOTLOADER_GIT_TS 0x01
#define MODE_FIRMWARE_GIT_TS 0x02
#define MODE_BOOTLOADER_GIT_HASH 0x03
#define MODE_FIRMWARE_GIT_HASH 0x04
#define MODE_FIRMWARE_BUILD_TS 0x05


// Some definitions for calculation
#define SEC_PER_MIN             60UL
#define SEC_PER_HOUR            3600UL
#define SEC_PER_DAY             86400UL
#define SEC_PER_YEAR            (SEC_PER_DAY*365)

// extracts 1..4 characters from a string and interprets it as a decimal value
#define CONV_STR2DEC_1(str, i)  (str[i]>'0'?str[i]-'0':0)
#define CONV_STR2DEC_2(str, i)  (CONV_STR2DEC_1(str, i)*10 + str[i+1]-'0')
#define CONV_STR2DEC_3(str, i)  (CONV_STR2DEC_2(str, i)*10 + str[i+2]-'0')
#define CONV_STR2DEC_4(str, i)  (CONV_STR2DEC_3(str, i)*10 + str[i+3]-'0')

// Custom "glue logic" to convert the month name to a usable number
#define GET_MONTH(str, i)      (str[i]=='J' && str[i+1]=='a' && str[i+2]=='n' ? 1 :     \
                                str[i]=='F' && str[i+1]=='e' && str[i+2]=='b' ? 2 :     \
                                str[i]=='M' && str[i+1]=='a' && str[i+2]=='r' ? 3 :     \
                                str[i]=='A' && str[i+1]=='p' && str[i+2]=='r' ? 4 :     \
                                str[i]=='M' && str[i+1]=='a' && str[i+2]=='y' ? 5 :     \
                                str[i]=='J' && str[i+1]=='u' && str[i+2]=='n' ? 6 :     \
                                str[i]=='J' && str[i+1]=='u' && str[i+2]=='l' ? 7 :     \
                                str[i]=='A' && str[i+1]=='u' && str[i+2]=='g' ? 8 :     \
                                str[i]=='S' && str[i+1]=='e' && str[i+2]=='p' ? 9 :     \
                                str[i]=='O' && str[i+1]=='c' && str[i+2]=='t' ? 10 :    \
                                str[i]=='N' && str[i+1]=='o' && str[i+2]=='v' ? 11 :    \
                                str[i]=='D' && str[i+1]=='e' && str[i+2]=='c' ? 12 : 0)

// extract the information from the time string given by __TIME__ and __DATE__
#define __TIME_SECONDS__        CONV_STR2DEC_2(__TIME__, 6)
#define __TIME_MINUTES__        CONV_STR2DEC_2(__TIME__, 3)
#define __TIME_HOURS__          CONV_STR2DEC_2(__TIME__, 0)
#define __TIME_DAYS__           CONV_STR2DEC_2(__DATE__, 4)
#define __TIME_MONTH__          GET_MONTH(__DATE__, 0)
#define __TIME_YEARS__          CONV_STR2DEC_4(__DATE__, 7)

// Days in February
#define _UNIX_TIMESTAMP_FDAY(year) \
    (((year) % 400) == 0UL ? 29UL : \
        (((year) % 100) == 0UL ? 28UL : \
            (((year) % 4) == 0UL ? 29UL : \
                28UL)))

// Days in the year
#define _UNIX_TIMESTAMP_YDAY(year, month, day) \
    ( \
        /* January */    day \
        /* February */ + (month >=  2 ? 31UL : 0UL) \
        /* March */    + (month >=  3 ? _UNIX_TIMESTAMP_FDAY(year) : 0UL) \
        /* April */    + (month >=  4 ? 31UL : 0UL) \
        /* May */      + (month >=  5 ? 30UL : 0UL) \
        /* June */     + (month >=  6 ? 31UL : 0UL) \
        /* July */     + (month >=  7 ? 30UL : 0UL) \
        /* August */   + (month >=  8 ? 31UL : 0UL) \
        /* September */+ (month >=  9 ? 31UL : 0UL) \
        /* October */  + (month >= 10 ? 30UL : 0UL) \
        /* November */ + (month >= 11 ? 31UL : 0UL) \
        /* December */ + (month >= 12 ? 30UL : 0UL) \
    )

// get the UNIX timestamp from a digits representation
#define _UNIX_TIMESTAMP(year, month, day, hour, minute, second) \
    ( /* time */ second \
                + minute * SEC_PER_MIN \
                + hour * SEC_PER_HOUR \
    + /* year day (month + day) */ (_UNIX_TIMESTAMP_YDAY(year, month, day) - 1) * SEC_PER_DAY \
    + /* year */ (year - 1970UL) * SEC_PER_YEAR \
                + ((year - 1969UL) / 4UL) * SEC_PER_DAY \
                - ((year - 1901UL) / 100UL) * SEC_PER_DAY \
                + ((year - 1601UL) / 400UL) * SEC_PER_DAY \
    )

// the UNIX timestamp
#define UNIX_TIMESTAMP (_UNIX_TIMESTAMP(__TIME_YEARS__, __TIME_MONTH__, __TIME_DAYS__, __TIME_HOURS__, __TIME_MINUTES__, __TIME_SECONDS__))



union stm32_epoch_t {
  uint64_t big;
  uint8_t chunks[8];
} ;



void* get_request_odb(void* parm)
{
    int error_idx_a = 1;
    int error_idx_b = 2;
    int *param = (int *)parm;
    int cansock = param[0];
    struct can_frame rxmsg;     
    int n;
    bool got_response = false;
    can_ret can_r;
    char trans[ sizeof( double ) ] = { 0 };
    int rt;
    pthread_mutex_lock(&mutexService);
    while(1)
    {
        pthread_cond_wait(&condSend, &mutexService);
       // printf("message received service id 0x%02X\n", service_id); 
       // pthread_mutex_lock(&mutexService);
    //}
        while( (n = read(cansock, &rxmsg, sizeof(rxmsg)) > 0))
        {
            switch (rxmsg.can_id)
            {
                case 0x7F9: // STM32 versions
                {
                    got_response = true;
                    printf("m: "BYTE_TO_BINARY_PATTERN" = ", BYTE_TO_BINARY(rxmsg.data[0]));
                    uint8_t d = rxmsg.data[0] >> 5;
                    switch(d)
                    {
                        case MODE_BOOTLOADER_GIT_TS:
                        {
                            union stm32_epoch_t epoch;
                            epoch.chunks[0] = rxmsg.data[2];
                            epoch.chunks[1] = rxmsg.data[3];
                            epoch.chunks[2] = rxmsg.data[4];
                            epoch.chunks[3] = rxmsg.data[5];
                            epoch.chunks[4] = rxmsg.data[6];
                            epoch.chunks[5] = rxmsg.data[7];
                            epoch.chunks[6] = 0;
                            epoch.chunks[7] = 0;

                            time_t raw_time = epoch.big;
                            printf("BL build timestamp %02X %llu %s",
                                (rxmsg.data[0] & 0x1F), epoch.big, ctime(&raw_time)  ); 
                            break;
                        }
                        case MODE_FIRMWARE_GIT_TS:
                        {
                            union stm32_epoch_t epoch;
                            epoch.chunks[0] = rxmsg.data[2];
                            epoch.chunks[1] = rxmsg.data[3];
                            epoch.chunks[2] = rxmsg.data[4];
                            epoch.chunks[3] = rxmsg.data[5];
                            epoch.chunks[4] = rxmsg.data[6];
                            epoch.chunks[5] = rxmsg.data[7];
                            epoch.chunks[6] = 0;
                            epoch.chunks[7] = 0;

                            time_t raw_time = epoch.big;
                            printf("FW git timestamp %02X %llu %s",
                                (rxmsg.data[0] & 0x1F), epoch.big, ctime(&raw_time)  ); 
                            break;
                        }
                        case MODE_FIRMWARE_BUILD_TS: //  0x10 0xBC 0xB2 0x65 0x1F 0x40 0x84 0x6B
                        {
                            union stm32_epoch_t epoch;
                            epoch.chunks[0] = rxmsg.data[2];
                            epoch.chunks[1] = rxmsg.data[3];
                            epoch.chunks[2] = rxmsg.data[4];
                            epoch.chunks[3] = rxmsg.data[5];
                            epoch.chunks[4] = rxmsg.data[6];
                            epoch.chunks[5] = rxmsg.data[7];
                            epoch.chunks[6] = 0;
                            epoch.chunks[7] = 0;

                            time_t raw_time = epoch.big;

                            printf("FW build timestamp %02X %llu %s",
                                (rxmsg.data[0] & 0x1F), epoch.big, ctime(&raw_time) ); 
                            break;
                        }
                        case MODE_BOOTLOADER_GIT_HASH: // A78E8A792ED26D
                        {
                            printf("BL version %02X %02X%02X%02X%02X%02X%02X%02X\n",  
                                (rxmsg.data[0] & 0x1F), rxmsg.data[1], rxmsg.data[2], rxmsg.data[3], rxmsg.data[4], rxmsg.data[5],
                                rxmsg.data[6], rxmsg.data[7]); 
                            break;
                        }
                        case MODE_FIRMWARE_GIT_HASH:
                        {
                            printf("FW version %02X %02X%02X%02X%02X%02X%02X%02X\n",  
                                (rxmsg.data[0] & 0x1F), rxmsg.data[1], rxmsg.data[2], rxmsg.data[3], rxmsg.data[4], rxmsg.data[5],
                                rxmsg.data[6], rxmsg.data[7]); 
                            break;
                        }
                        default:
                        {
                            printf("message 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X\n",  
                                rxmsg.data[0], rxmsg.data[1], rxmsg.data[2], rxmsg.data[3], rxmsg.data[4], rxmsg.data[5],
                                rxmsg.data[6], rxmsg.data[7]); 
                            break;
                        }
                    }
                    break;
                }
                case 0x7E8:
                { 
                    int frame_len = rxmsg.data[0];
                    int mode = rxmsg.data[1] ;
                    switch(mode) 
                    {
                        case 0x41:
                        {
                            got_response = true;
                            int service_id_from_frame = rxmsg.data[frame_len-1];
                            // printf("message %02X (%02X) 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X\n",  
                            //             service_id, service_id_from_frame,
                            //             rxmsg.data[0], rxmsg.data[1], rxmsg.data[2], rxmsg.data[3], rxmsg.data[4], rxmsg.data[5],
                            //             rxmsg.data[6], rxmsg.data[7]); 
                            canfunctions[service_id_from_frame](can_r, rxmsg.data);
                            
                            memcpy( trans , &can_r.ret , sizeof( double ) );
                            memcpy( &rt , trans , sizeof( int ) );
                            int data[4] = {ReadCan::REVERSE_TEST_CANID, service_id_from_frame, ReadCan::REVERSE_TEST_VALUE, rt};
                            arr_set_can(data, 4);
                            break;
                        }
                        case 0x43:
                        {
                            int a = int((unsigned char)(rxmsg.data[7]) << 24 |
                                        (unsigned char)(rxmsg.data[6]) << 16 |
                                        (unsigned char)(rxmsg.data[5]) << 8 |
                                        (unsigned char)(rxmsg.data[4]));
                            int b = int((unsigned char)(rxmsg.data[3]) << 24 |
                                        (unsigned char)(rxmsg.data[2]) << 16 |
                                        (unsigned char)(rxmsg.data[1]) << 8 |
                                        (unsigned char)(rxmsg.data[0]));
                            int data[4] = { error_idx_a, a, error_idx_b, b  };
                            error_idx_a += 2;
                            error_idx_b += 2;
                            arr_set_nrf(data, 4);
                            break;
                        }
                        default:
                            break;
                    }

                    break;
                }
                default:
                    break;
            }
            if (got_response == true)
            {
                got_response = false;
                pthread_cond_wait(&condSend, &mutexService);
            }
         }
    }  
}

void* send_query_odb(void* parm)
{
    int *param = (int *)parm;
    int cansock = param[0];

    struct can_frame txmsg;   

    txmsg.can_id = 0x7DF;
    txmsg.can_dlc = 8;
    txmsg.data[0] = 0x02;
    txmsg.data[1] = 0x01;
    txmsg.data[2] = 0xAA;
    txmsg.data[3] = 0xAA;
    txmsg.data[4] = 0xAA;
    txmsg.data[5] = 0xAA;
    txmsg.data[6] = 0xAA;
    txmsg.data[7] = 0xAA;

    
    struct can_frame txmsg_dtc;   

    txmsg_dtc.can_id = 0x7DF;
    txmsg_dtc.can_dlc = 2;
    txmsg_dtc.data[0] = 0x01;
    txmsg_dtc.data[1] = 0x03;

    
    struct can_frame txmsg_version; 

    txmsg_version.can_id = 0x7FF; //0x7DF;
    txmsg_version.can_dlc = 8;
    txmsg_version.data[0] = 0x80;
    txmsg_version.data[1] = 0x04;
    txmsg_version.data[2] = 0x00;
    txmsg_version.data[3] = 0x00;
    txmsg_version.data[4] = 0x00;
    txmsg_version.data[5] = 0x00;
    txmsg_version.data[6] = 0x00;
    txmsg_version.data[7] = 0x00;

pthread_mutex_lock(&mutexTime);
    while(1)
    {
pthread_cond_wait(&condTime, &mutexTime);

        switch(show_current_data)
        {
            case SHOW_DTC:
            {
                show_current_data = SHOW_ODB2;
                write(cansock, &txmsg_dtc, sizeof(txmsg_dtc));
                break;
            }
            case SHOW_ODB2:
            {
                for (int i = 0; i < odb_services.size() ; i++)
                {
                    usleep(5000);
                    odb_services[i].progress_ms -= 10;
                    if (odb_services[i].progress_ms < 0)
                    {
                        odb_services[i].progress_ms = odb_services[i].period_ms;
                        txmsg.data[2] = service_id = odb_services[i].service;
                        write(cansock, &txmsg, sizeof(txmsg));
                        //printf("Send service: 0x%02X\n", odb_services[i].service); 
                        pthread_mutex_unlock(&mutexService) ;
                        pthread_cond_signal(&condSend) ;
                    }
                }
                break;
            }
            case SHOW_VERSIONS:
            {
                usleep(5000);

                show_current_data = SHOW_BOOTLOADER_VERSION;
                write(cansock, &txmsg_version, sizeof(txmsg_version));
                break;
            }
            case SHOW_BOOTLOADER_VERSION:
            {
                usleep(5000);
                show_current_data = SHOW_BOOTLOADER_GIT_TIMESTAMP;     
                txmsg_version.data[1] = 0x08;
                write(cansock, &txmsg_version, sizeof(txmsg_version));
                break;
            }
            case SHOW_BOOTLOADER_GIT_TIMESTAMP:
            {
                usleep(5000);
                show_current_data = SHOW_GIT_TIMESTAMP;     
                txmsg_version.data[1] = 0x18;
                write(cansock, &txmsg_version, sizeof(txmsg_version));
                break;
            }
            case SHOW_GIT_TIMESTAMP:
            {
                usleep(5000);
                show_current_data = SHOW_TIMESTAMP;     
                txmsg_version.data[1] = 0x14;
                write(cansock, &txmsg_version, sizeof(txmsg_version));
                break;
            }
            case SHOW_TIMESTAMP:
            {
                usleep(1000);
                show_current_data = SHOW_ODB2;       
                txmsg_version.data[1] = 0x20;
                write(cansock, &txmsg_version, sizeof(txmsg_version));
                break;
            }
            default:
            {
                break;
            }
        }
pthread_mutex_lock(&mutexTime);
    }
pthread_mutex_unlock(&mutexTime);
}

void signal_handler(int signal)
{
    switch (signal)
    {
        case SIGUSR1:
        {
            //printf("get DTC errors\n");
            show_current_data = SHOW_DTC;
            break;
        }
        case SIGUSR2:
        {
            //printf("get DTC errors\n");
            show_current_data = SHOW_VERSIONS;
            break;
        }
        default:
        {
            printf("signal_handler: signal = %d\n", signal); 

            pthread_mutex_destroy(&mutexService) ;
            pthread_cond_destroy(&condSend) ;   // destroying the threads.
            pthread_mutex_destroy(&mutexTime) ;
            pthread_cond_destroy(&condTime) ;   // destroying the threads.

            exit(0);
        }
    }
}

int main(int argc, char* argv[]) 
{ 
    union stm32_epoch_t epoch;
    epoch.big = UNIX_TIMESTAMP;

    printf("Compile time: %d%d%d%d%d%d%d%d %llu\n", epoch.chunks[0],
        epoch.chunks[1], epoch.chunks[2], epoch.chunks[3], epoch.chunks[4],
        epoch.chunks[5], epoch.chunks[6], epoch.chunks[7], epoch.big);

    struct sigaction sa;
    sa.sa_flags = 0;
    // Setup the signal handler
    sa.sa_handler = signal_handler;
    // Block every signal during the handler
    sigfillset(&sa.sa_mask);  
    if (sigaction(SIGINT, &sa, NULL) == -1) 
    {
        printf("Error: cannot handle SIGINT\n"); 
    }
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
    {
        printf("Error: cannot handle SIGUSR1\n"); 
    }
    if (sigaction(SIGUSR2, &sa, NULL) == -1)
    {
        printf("Error: cannot handle SIGUSR2\n"); 
    }

    canfunctions.reserve(200);
    //std::fill(canfunctions.begin(), canfunctions.end(), &dummy_service);
    // for (int i = 0; i < canfunctions.size(); i++)
    //     canfunctions[i] =  &coolant_temperature;
    canfunctions[0x10] = &MAF_air_flow_rate;
    canfunctions[0x0D] = &vehicle_speed;
    canfunctions[0x0F] = &intake_air_temperature;

    int fd, rd, i, j, k;
	struct input_event ev[64];
	int version;
	unsigned short id[4];
	char name[256] = "Unknown";
    char can0_str[]="can0";

    struct ifreq ifr;
    struct sockaddr_can addr;
    int required_mtu;
    struct can_frame frame;
    int cansock;

    memset(&ifr, 0x0, sizeof(ifr));
    memset(&addr, 0x0, sizeof(addr));
    memset(&frame, 0x0, sizeof(frame));

    /* open CAN_RAW socket */
    cansock = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    /* convert interface sting "can0" into interface index */ 
    strcpy(ifr.ifr_name, can0_str);
    ioctl(cansock, SIOCGIFINDEX, &ifr);

    /* setup address for bind */
    addr.can_ifindex = ifr.ifr_ifindex;
    addr.can_family = PF_CAN;

    /* bind socket to the can0 interface */
    bind(cansock, (struct sockaddr *)&addr, sizeof(addr));
    timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 10000;
    setsockopt(cansock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
        
    YAML::Node config = YAML::LoadFile("/home/odroid/vectra/odb2.yaml");
    std::cout << config.size() << std::endl;
    for(int i = 0; i < config.size();i++) 
    {
        Odb2 o;
        config[i] >> o;
        std::cout << o.name << " " << o.service << std::endl;
        odb_services.push_back(o);
    }

    pthread_t a[2] ;

    pthread_mutex_init(&mutexService, NULL) ;
    pthread_cond_init(&condSend, NULL) ;

    pthread_mutex_init(&mutexTime, NULL) ;
    pthread_cond_init(&condTime, NULL) ;

    
pthread_mutex_lock(&mutexTime);

    int *param = (int *)malloc(1 * sizeof(int));
    param[0] = cansock;

    for ( int i = 0 ; i < 2 ; i++ ) 
    {
        if (i == 1) 
        {
            if (pthread_create(&a[i], NULL, &send_query_odb, param) != 0) 
            {
                perror("Failed to create thread");
            }
        } 
        else 
        {
            if (pthread_create(&a[i], NULL, &get_request_odb, param) != 0) 
            {
                perror("Failed to create thread");
            }
        }
    }

    
    size_t timer2;

    initialize();

    //  timer1 = start_timer(200, time_handler1, TIMER_SINGLE_SHOT, NULL);
    timer2 = start_timer(10, time_handler2, TIMER_PERIODIC, NULL);

    for ( int i = 0 ; i < 2 ; i++ ) 
    {
        if (pthread_join(a[i], NULL) != 0) 
        {
            perror("Failed to join thread") ;
        }
    }
    
    pthread_mutex_destroy(&mutexService) ;
    pthread_cond_destroy(&condSend) ;   // destroying the threads.
    pthread_mutex_destroy(&mutexTime) ;
    pthread_cond_destroy(&condTime) ;   // destroying the threads.

    return 0 ;
}