ifeq ($(OS),Windows_NT)
    uname_S := Windows
else
    uname_S := $(shell uname -s)
endif

ifeq ($(uname_S), Darwin)
	PYTHON_HEADERS=/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.7/include/python3.7m/
	GUI_CPPFLAGS=-D_GLIBCXX_USE_CXX11_ABI=0 -Dexternal_module_EXPORTS -I$(HOME)/usr/include -I$(PYTHON_HEADERS) -Os -DNDEBUG -fPIC -fvisibility=hidden -std=c++14 -flto 
	PYTHON_LDFLAGS=-L/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.7/lib/ -lpython3.7
	GUI_LINKFLAGS=-flto -lpthread -pthread $(PYTHON_LDFLAGS)
	CPPFLAGS=-std=c++14 -D_GLIBCXX_USE_CXX11_ABI=0
	LINKFLAGS=-lstdc++  -pthread -lpthread 
	LDFLAGS= -flto -lpthread -pthread 
	STRIP_CMD=echo
endif
ifeq ($(uname_S), Linux)
    PYTHON_HEADERS=/usr/include/python2.7
    GUI_CPPFLAGS=-D_GLIBCXX_USE_CXX11_ABI=0 -Dexternal_module_EXPORTS -I$(HOME)/usr/include -I$(PYTHON_HEADERS) -Os -DNDEBUG -fPIC -fvisibility=hidden -std=c++14 -flto -fno-fat-lto-objects
	CPPFLAGS=-std=c++14 -D_GLIBCXX_USE_CXX11_ABI=0
	PYTHON_LDFLAGS=
	GUI_LINKFLAGS=-flto -lpthread -pthread -lrt $(PYTHON_LDFLAGS)
	LINKFLAGS=-lstdc++ -lwiringPi -lwiringPiDev -pthread -lpthread -lrt -lm -lcrypt 
	LDFLAGS=-lwiringPi -lwiringPiDev  -flto -lpthread -pthread -lrt

	STRIP_CMD=strip
endif

#
CC=g++



vectra.o: vectra.h vectra.cpp
	g++ -c ${CPPFLAGS} -fPIC vectra.cpp -o build/vectra.o

vectra_nrf: vectra_nrf.o vectra.o
	g++ -o build/bin/vectra_nrf build/vectra.o build/vectra_nrf.o ${LINKFLAGS} 

vectra_nrf.o: vectra_nrf.cpp vectra.o
	g++ -c ${CPPFLAGS} vectra_nrf.cpp -o build/vectra_nrf.o

test_can: test_can.o vectra.o
	gcc -o build/bin/test_can build/vectra.o build/test_can.o ${LINKFLAGS}

test_can.o: test_can.cpp
	gcc -c ${CPPFLAGS} test_can.cpp -o build/test_can.o

vectra_can: vectra_can.o vectra.o
	gcc -o build/bin/vectra_can build/vectra.o build/vectra_can.o ${LINKFLAGS}

vectra_can.o: vectra_nrf.cpp
	gcc -c ${CPPFLAGS} vectra_can.cpp -o build/vectra_can.o

vectra_can2: vectra_can2.o vectra.o
	gcc -o build/bin/vectra_can2 build/vectra.o build/vectra_can2.o ${LINKFLAGS}


vectra_xu4: vectra_xu4.o vectra.o
	gcc -o build/bin/vectra_xu4 build/vectra.o build/vectra_xu4.o ${LINKFLAGS}


vectra_can2.o: vectra_can2.cpp
	gcc -c ${CPPFLAGS} vectra_can2.cpp -o build/vectra_can2.o

vectra_can3: vectra_can3.o vectra.o
	gcc -o build/bin/vectra_can3 build/vectra.o build/vectra_can3.o ${LINKFLAGS}

vectra_can3.o: vectra_can3.cpp
	gcc -c ${CPPFLAGS} vectra_can3.cpp -o build/vectra_can3.o


vectra_xu4.o: vectra_can_xu4.cpp
	gcc -c ${CPPFLAGS} vectra_can_xu4.cpp -o build/vectra_xu4.o


vectra_blockm: vectra_blockm.o vectra.o
	gcc -o build/bin/vectra_blockm build/vectra.o build/vectra_blockm.o ${LINKFLAGS}

vectra_blockm.o: vectra_blockm.cpp
	gcc -c ${CPPFLAGS} vectra_blockm.cpp -o build/vectra_blockm.o


poweroff: poweroff.o
	gcc -o build/bin/odroid-poweroff build/poweroff.o ${LINKFLAGS} 

poweroff.o: odroid-poweroff.c
	gcc -c odroid-poweroff.c -o build/poweroff.o


vectra_gui.o: vectra_gui.cpp vectra.o
	${CC} ${GUI_CPPFLAGS} -o build/vectra_gui.o -c vectra_gui.cpp

vectra_gui: vectra_gui.o vectra.o
	${CC} -fPIC -Os -DNDEBUG -shared -o build/lib/vectra_gui.so build/vectra.o build/vectra_gui.o $(GUI_LINKFLAGS)
	$(STRIP_CMD) ./build/lib/vectra_gui.so

test_arr: test_arr.cpp vectra.o
	gcc test_arr.cpp -o /tmp/arr  build/vectra.o -lpthread -lstdc++ -flto -lrt

cdbm: cd4051bm.o
	${CC} -fPIC -Os -DNDEBUG -shared -o build/lib/cd4051bm.so build/cd4051bm.o ${LDFLAGS} $(PYTHON_LDFLAGS)
	$(STRIP_CMD) ./build/lib/cd4051bm.so

cd4051bm.o: cd4051bm.cpp
	${CC} ${GUI_CPPFLAGS} -o build/cd4051bm.o -c cd4051bm.cpp

all: vectra_blockm vectra_can vectra_can2 vectra_can3 vectra_gui cdbm

.PHONY: ld_candump

ld_candump:  ld_candump/ld_custom_file_name_canlog.c 
	gcc $(CFLAGS) -shared -fPIC ld_candump/ld_custom_file_name_canlog.c -o build/ld_candump.so -ldl

install_gui: vectra_gui
	cp build/lib/vectra_gui.so /home/odroid/vectra/

install_can3: vectra_can3
	-systemctl stop vectra_can3
	cp build/bin/vectra_can3 ~/vectra/
	cp .vectra_can3.conf ~/vectra/
	systemctl start vectra_can3

install_can2: vectra_can2
	-systemctl stop vectra_can2
	cp build/bin/vectra_can2 ~/vectra/
	cp .vectra_can.conf ~/vectra/
	systemctl start vectra_can2

install_menu: vectra_gui
	cp menu.py ~/vectra/ 

install_lc: ld_candump
	cp build/ld_candump.so ~/vectra/
	cp xtermcan.sh ~/vectra/
	chmod 777 ~/vectra/xtermcan.sh

vevent: vevent.c
	gcc vevent.c -o build/vevent

install_ve: vevent
	cp build/vevent ~/vectra/


install_test: test_arr
	echo "Install /tmp/arr" 

pwr_dog: pwr_dog.c
	avr-gcc -mmcu=attiny10 -g -Os pwr_dog.c -o build/pwr_dog.bin
	avr-objcopy -j .text -j .data -O ihex build/pwr_dog.bin build/pwr_dog.hex

install_services: vectra_can2.service vectra_can3.service vectra_can.service vectra_blockm.service 
	cp vectra_can2.service vectra_can3.service vectra_can.service vectra_blockm.service /lib/systemd/system/
	cp *.conf ~/vectra/

install_xu4: vectra_xu4 vectra_can_xu4.service
	sudo systemctl stop vectra_can_xu4
	cp build/bin/vectra_xu4 ~/vectra
	sudo cp vectra_can_xu4.service /lib/systemd/system/
	sudo systemctl daemon-reload
	sudo systemctl start vectra_can_xu4

	

install_pwr_dog: pwr_dog
	avrdude -p attiny10 -c usbasp -U flash:w:build/pwr_dog.hex:i -F -P usb

install: poweroff install_gui install_can3 install_can2 install_menu install_lc install_ve
	cp {dump_hex,dump_log,sniffer_binary,sniffer_hex,play_vcan,test_can}.sh /home/odroid/vectra/
	cp {windshield,gas,turn_signals_left,battery}.png /home/odroid/vectra/
	cp build/bin/vectra_can /home/odroid/vectra/
	cp build/lib/cd4051bm.so /home/odroid/vectra/
	cp build/bin/vectra_blockm /home/odroid/vectra/
	cp build/bin/odroid-poweroff /home/odroid/vectra/
	cp odroid-poweroff-now /home/odroid/vectra/


