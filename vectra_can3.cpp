#include "vectra.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <csignal>
#include<fstream>


#define CAN_HELP 1
#define CAN_DRAIN  2
#define CSR_HOME "\33[H"
#define CLR_SCREEN "\33[2J"


#define N (256)


typedef struct  {
    int in = 0, out = 0;
    int summa;
    int b_summa;
    int buffer[N];
} TC_Data;

// http://astra-3.pl/porada-astra-h-test-mode-gid-cid
static TC_Data tc_data;

static std::string tc_data_filepath;


void signal_callback( int signum ) {
    std::ofstream wf(tc_data_filepath, std::ios::out | std::ios::binary);
    if(!wf) {
        printf("Can't write...");
    } else {
        wf.write((char *) &tc_data, sizeof(TC_Data));
        wf.close();
        /*printf("IN: %d\nOUT: %d\n", tc_data.in, tc_data.out);
        printf("SUMM: %d\nb_summa: %d\n", tc_data.summa, tc_data.b_summa);
        for(int i=0; i < N; i++) {
            printf("BUFFER[%d]: %d\n", i, tc_data.buffer[i]);
        }*/
    }

    exit(signum);  
}

// https://github.com/angrave/SystemProgramming/wiki/Synchronization,-Part-8:-Ring-Buffer-Example
int enqueue(int value) { 
    int k = (tc_data.in++) & (N-1);
    int all = ((tc_data.b_summa == 1) ? N : ((k == 0) ? 1 : k)); 

    tc_data.summa -= tc_data.buffer[ k ];
    tc_data.buffer[ k ] = value;
    
    if (tc_data.in == N) 
         tc_data.b_summa = 1;
 
    tc_data.summa += value;
    return tc_data.summa/all;
}

void usage(const char * s) {
    printf("Usage: %s <drain>\n", s);
}


int  tc_inst[] = {600,600,600,600,600,600,600,600,600
    ,600,600,600,600,600,600,600,600,600,600,600
    ,600,600,600,600,600,600,600,600,600,600,600
    ,600,600,600,600,600,600,600,600,600,600,560
    ,520,514,508,502,496,490,485,479,473,467,461
    ,455,450,444,439,433,428,422,417,412,406,401
    ,395,390,385,379,344,368,363,357,352,347,341
    ,336,330,325,320,313,307,301,295,289,283,277
    ,271,265,258,252,246,240,234,228,222,216,210
    ,204,198,192,186,180,175,169,163,157,151,145
    ,140,133,127,120,114,107,101,95,88,82,75,69
    ,62,56,50,42,35,28,21,14,7};


int main (int argc, char ** argv) 
{
    signal(SIGINT, signal_callback);
    signal(SIGTERM, signal_callback);

    unsigned char tc_time = 0;
    int mode = 0;
    char can0_str[]="can3";

    if ( argc > 1 ) 
    {
        if (!strcmp(argv[1], "help")) 
        {
            usage(argv[0]);
            return 1;
        }
        if (!strcmp(argv[1], "drain"))
            mode = CAN_DRAIN;
        else
            strcpy (can0_str,argv[1]);
    }

    if ( argc > 2 ) {
        tc_data_filepath.assign(  argv[2] );
        std::ifstream rf(tc_data_filepath, std::ios::out | std::ios::binary);
        if(!rf) {
          printf("Cannot open file!\n");
        }
        rf.read((char *) &tc_data, sizeof(TC_Data));
    }
    else 
    {
        tc_data.b_summa = 0;
        for (int j = 0 ; j < N; ++j)
            tc_data.buffer[j] = 0;
    }

    //printf("\033[H\033[J");
    //printf("\e[2J\e[H"); 
    printf("%s%s", CLR_SCREEN, CSR_HOME);
    printf("%s", CSR_HOME);
    std::vector<int> data_can;
    data_can.reserve(CAN_MEM_ARR_SIZE);
    data_can.assign(CAN_MEM_ARR_SIZE, 0);

    if (mode == CAN_DRAIN)
    {
        while(true)
        {
            std::vector<int> data_nrf = arr_read_nrf();
            std::vector<int> data_gui = arr_read_gui();

            int nrf0 = data_nrf[0];
            if (nrf0 != -1)
                printf("CAN READ: Data[0] from nrf == %d\n", nrf0);

            int gui0 = data_gui[0];
            if (gui0 != -1)
                printf("CAN READ: Data[0] from gui == %d\n", gui0);

            int data[2] = {3, rand() % 800};
            arr_set_can(data, 2);

            usleep(5000);
        }
    }
    else
    {
        struct ifreq ifr;
        struct sockaddr_can addr;
     
        struct can_frame frame;
        int s;

        memset(&ifr, 0x0, sizeof(ifr));
        memset(&addr, 0x0, sizeof(addr));
        memset(&frame, 0x0, sizeof(frame));

        /* open CAN_RAW socket */
        s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

        /* convert interface sting "can0" into interface index */ 
        strcpy(ifr.ifr_name, can0_str);
        ioctl(s, SIOCGIFINDEX, &ifr);

        /* setup address for bind */
        addr.can_ifindex = ifr.ifr_ifindex;
        addr.can_family = PF_CAN;

        /* bind socket to the can0 interface */
        bind(s, (struct sockaddr *)&addr, sizeof(addr));

        int n = 0;

        while( (n = read(s, &frame, sizeof(frame)) > 0))
        {
            switch (frame.can_id)
            {
                case 0x108:
                {
                    int a = int((unsigned char)(frame.data[5]) << 8 |
                                (unsigned char)(frame.data[4]));
                    int b = int((unsigned char)(frame.data[6]) << 8 |
                                (unsigned char)(frame.data[7]));
                    int data[4] = { ReadCan::SPEED , a
                                    ,ReadCan::SPEED_MS, b  }; 
                    arr_set_can(data, 4);
                    break;
                }
                case 0x350:
                { 
                    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
                    if ( (frame.data[0] >> 2) & 1 )
                        data_can[ReadCan::LIGHT_SIGNALS] |= (1<<0);
                    else
                        data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<0);
                    
                    int data[2] = {ReadCan::LIGHT_SIGNALS
                                    , data_can[ReadCan::LIGHT_SIGNALS]};
                    arr_set_can(data, 2);
                    break;
                }
                case 0x145:
                {
                    int data[2] = { ReadCan::COOLANT 
                                    , frame.data[3] }; // -40
                    arr_set_can(data, 2);
                    break;       
                }
                case 0x130:
                {
                    int val = 0;
                    unsigned char i = frame.data[3];
                    if (i < 0x81) val = tc_inst[ i ];

                    int val2 = enqueue(val);
                    
                    int data[4] = { ReadCan::TC_INST, val 
                                    , ReadCan::TC_TIME, val2};
                    arr_set_can(data, 4);   
                    break;
                }
                case 0x260:
                { 
                    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);

                    data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<1);
                    data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<2);
                    data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<3);
                    if ( frame.data[0] == 0b00100101 )
                        data_can[ReadCan::LIGHT_SIGNALS] |= (1<<1);
                    if ( frame.data[0] == 0b00111010 )
                        data_can[ReadCan::LIGHT_SIGNALS] |= (1<<2);
                    if ( frame.data[0] == 0b00011111 )
                        data_can[ReadCan::LIGHT_SIGNALS] |= (1<<3);
                    
                    arr_write_can(data_can);
                    break;
                }
            }
            usleep(1000);
        }
    }
    return 0;
}

