#!/bin/bash

i=0
j=40000000
j_increment=100000
while true; do 
		currentS=$(date +%s)
		currentN=$(date +%N)
		curr=$((10#${currentN//[^0-9]} + 0))
		if (( curr > 100000000  && curr < 300000000 && $i == 0 ))
			then
				hex=`printf "%08X" $j`
				((j=j+j_increment))
				echo $i $curr $currentS can3 130#00${hex}
				((i=1))

		fi

		if (( curr > 600000000 && curr < 800000000 && $i == 1 ))
			then
				hex=`printf "%08X" $j`
				((j=j+j_increment))				
				echo $i $curr  $currentS can3 130#00${hex}
				((i=0))
		fi
done