# -*- coding: utf-8 -*-
import os
import pygame
#import pygame.gfxdraw
from PIL import Image, ImageDraw
from math import sin, cos, pi, sqrt
# --- constants ---

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE  = (  0,   0, 255)
GREEN = (  0, 255,   0)
RED   = (255,   0,   0)
GREY  = (128, 128, 128)
font2_name = 'dejavusans'
#PI = 3.1415
import sys
# --- main ----
MENUDIR = '/home/odroid/vectra'
sys.path.append(MENUDIR)




os.environ['VECTRA_CAN'] = 'gray2'
os.environ['CAN1_SW'] = 'gray2'
os.environ['ODROID_CAN'] = 'can0' 

CMDDUMPH = MENUDIR + os.sep + 'dump_hex.sh'
CMDDUMPL = MENUDIR + os.sep + 'dump_log.sh'
CMDSNIFB = MENUDIR + os.sep + 'sniffer_binary.sh'
CMDSNIFH = MENUDIR + os.sep + 'sniffer_hex.sh'
CMDPLAYV = MENUDIR + os.sep + 'play_vcan.sh'

CMD500 = MENUDIR + os.sep + 'can500kbps.sh'
CMD95 = MENUDIR + os.sep + 'can95kbps.sh'
CMD33 = MENUDIR + os.sep + 'can33kbps.sh'

IMG_WINDSHIELD = MENUDIR + os.sep + 'windshield.png'
IMG_TURN_SIG = MENUDIR + os.sep + 'turn_signals_left.png'
IMG_BATTERY_TEMP = MENUDIR + os.sep + 'battery.png'


# colors    R    G    B
white    = (255, 255, 255)
tron_whi = (189, 254, 255)
red      = (255,   0,   0)
green    = (  0, 255,   0)
blue     = (  0,   0, 255)
tron_blu = (  0, 219, 232)
black    = (  0,   0,   0)
cyan     = ( 50, 255, 255)
magenta  = (255,   0, 255)
yellow   = (255, 255,   0)
tron_yel = (255, 140, 0)
orange   = (255, 127,   0)
tron_ora = (255, 202,   0)

# Tron theme orange
tron_regular = tron_ora
tron_light   = tron_yel
tron_inverse = black


pygame.init()
screen = pygame.display.set_mode((800,480))

# - generate PIL image with transparent background -


class Screen(object):
    def __init__(self):
        self._objects = []


    def attach(self, *objs):
        for obj in objs:
            self._objects += [obj]


    def move(self, deltax, deltay):
        for n in self._objects:
            n.move(deltax, deltay)


    def render(self,v):
        for n in self._objects:
            n.render(v)


    def on_touch_down(self, touch_pos):
        for n in self._objects: 
            if touch_pos in n:
                n.set_pressed()
                return

    def on_touch_up(self, touch_pos):
        for n in self._objects: 
            if (touch_pos in n) and n.is_pressed():
                n.click()
            n.set_released()    

class LBattery(object):
    def __init__(self, pos, colors=(tron_light, black)
            , font=pygame.font.SysFont(font2_name,22)):

        self._font = font
        self._colors = colors
        self.x, self.y = pos

        src = pygame.image.load(IMG_BATTERY_TEMP).convert_alpha()
        self._img_surf = pygame.transform.scale(src
            , (src.get_width()//17, src.get_height()//17))

        self._img_surf_invert = self._img_surf.copy()
        self._img_surf_invert.fill(colors[1], special_flags=pygame.BLEND_ADD)
        self._img_rect = self._img_surf.get_rect(center=(self.x-5, self.y+7))


    def render(self, inverse_color, *args, **kwargs):
        v = '20.345' #str(float(globalcan.getData()[ int(vectra_gui.TMPR_BATTERY) ])*0.0625)
        label=self._font.render(u'%sᵒC' % v.rjust(6), 1, self._colors[inverse_color])
        img = self._img_surf if inverse_color == 1 else self._img_surf_invert
        screen.blit(img, self._img_rect)
        screen.blit(label, (self.x + 20, self.y))


class ProgressCircle(object):
    def __init__(self):   
        self._pil_size = 70
        src = pygame.image.load(IMG_WINDSHIELD).convert_alpha()
        src.fill(pygame.colordict.THECOLORS['tan3'], special_flags=pygame.BLEND_ADD)
        
        self._dog_surf = pygame.transform.scale(src, (src.get_width()//4, 
                                          src.get_height()//4))
        self._dog_x = self._dog_surf.get_width() // 2
        self._dog_y = self._dog_surf.get_height() // 2
        self._pil_image = Image.new("RGBA", (self._pil_size+1, self._pil_size+1))
        self._pil_draw = ImageDraw.Draw(self._pil_image)
        self._pil_half_size = self._pil_size // 2
        self._mode = self._pil_image.mode
        self._size = self._pil_image.size
        self._font=pygame.font.Font(None,21)
        
    def render(self, color_index):
        #pil_draw.arc((0, 0, pil_size-1, pil_size-1), 0, 270, fill=RED)
        i = 1010
        #0     - 3600
        #63000 - 1
        #12500 - x
        k=0.005714
        consump=360.0-(k*i)
        self._pil_draw.pieslice((0, 0, self._pil_size, self._pil_size), consump, 0, fill=GREY)
        self._pil_draw.pieslice((0, 0, self._pil_size, self._pil_size), 360, consump, fill=(255, 202,   0))

        # - convert into PyGame image -
        data = self._pil_image.tobytes()
        self._image = pygame.image.fromstring(data, self._size, self._mode)
        self._image_rect = self._image.get_rect(center=screen.get_rect().center)
    
        label=self._font.render(str(i), 1, (  0, 219, 232))
        x = label.get_width() // 2
        y = label.get_height() // 2
        screen.blit(self._image, self._image_rect) # <- display image
        screen.blit(self._dog_surf,
                             (self._image_rect[0] + self._pil_half_size - self._dog_x
                            , self._image_rect[1] + self._pil_half_size - self._dog_y))
        screen.blit(label,
                             (self._image_rect[0] + self._pil_half_size - x
                            , self._image_rect[1] + self._pil_size))
    def set_pressed(self):
        pass
    def is_pressed(self):
        return False
    def set_released(self):
        pass
    def __contains__(self, touch_pos):
        return False
# - mainloop -


class HexaButton(object):
    def __init__(self, pos, label, radius, colors=(RED, GREY, GREEN)):
        self._label = label
        self._colors = colors
        self._radius = radius
        self._vertex_count = 6
        self._color_index = 0
        self.x,self.y = pos

        n, r = self._vertex_count, self._radius
        x, y = self.x,self.y

        inner = sqrt(2)*r
        #print inner

        rect = pygame.draw.polygon(screen, self._colors[0], [
            (x + r * cos(2 * pi * i / n), y + r * sin(2 * pi * i / n))
            for i in range(n)
        ])
        
        self._range = [
            (x + r * cos(2 * pi * i / n), y + r * sin(2 * pi * i / n))
            for i in range(n)
        ]
        self.x, self.y, self.h, self.w = rect
        self.x_min, self.x_max, self.y_min, self.y_max = \
                            self.x, self.x + self.w, self.y, self.y + self.h
        self._pressed = False

    def render(self, inverse_color):
        n, r = self._vertex_count, self._radius
        x, y = self.x,self.y
        
        self._color_index = inverse_color
        if self._pressed == True:
            self._color_index = 2

        rect = pygame.draw.polygon(screen, self._colors[self._color_index], self._range)
        pygame.draw.rect(screen, self._colors[inverse_color], rect,5)
        
        # font=pygame.font.Font(None,42)
        # label=font.render(str(self._label), 1, (self._colors[inverse_color]))
        # screen.blit(label,(self.x,self.y))
    def click(self):
        print("click")
    
    def set_pressed(self):
        self._pressed = True
    
    def is_pressed(self):
        return self._pressed 

    def set_released(self):
        self._pressed = False

    def __contains__(self, touch_pos):
        return (self.x_min <= touch_pos[0] <= self.x_max and self.y_min <= touch_pos[1] <= self.y_max)


class Arrow(object):
    def __init__(self):

        center_x = 200
        src_l = pygame.image.load(MENUDIR + '/turn_signals_left.png').convert_alpha()

        self._left_surf = pygame.transform.scale(src_l, (src_l.get_width()//4, 
                                          src_l.get_height()//4))

        src_r = pygame.image.load(MENUDIR+'/turn_signals_left.png').convert_alpha()

        src_rr = pygame.transform.scale(src_r, (src_r.get_width()//4, 
                                          src_r.get_height()//4))

        self._right_surf = pygame.transform.rotate(src_rr, 180)

        #pixels = pygame.surfarray.pixels_green(self._dog_surf)
        #pixels ^= 2 ** 32 - 1
        # self._dog_surf.set_colorkey((255, 255, 255))
        self._left_rect = self._left_surf.get_rect(center=(center_x-20, 150))
        self._right_rect = self._right_surf.get_rect(center=(center_x+20, 150))

    def render(self, color_index):
        self._left_surf.fill((0, 255, 10, 0), special_flags=pygame.BLEND_ADD)
        self._right_surf.fill((0, 255, 10, 0), special_flags=pygame.BLEND_ADD)
        screen.blit(self._left_surf, self._left_rect)
        screen.blit(self._right_surf, self._right_rect)

    def set_pressed(self):
        pass

    def is_pressed(self):
        pass

    def set_released(self):
        pass

    def __contains__(self, touch_pos):
        return False


clock = pygame.time.Clock()
running = True
p = ProgressCircle()
a = Arrow()
h = HexaButton((200,330),"koko",50)


s1 = Screen()
s1.attach( p,a,h, LBattery((240,50)) )
screens = [s1]

while running:
    clock.tick(10)
    for event in pygame.event.get():

        if event.type == pygame.MOUSEBUTTONDOWN:
            pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])
            screens[0].on_touch_down(pos)
        if event.type == pygame.MOUSEBUTTONUP:
            pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])
            screens[0].on_touch_up(pos)

        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False

    screen.fill(WHITE)
    #pygame.draw.arc(screen, BLACK, (300, 200, 200, 200), 0, PI/2, 1)
    #pygame.gfxdraw.pie(screen, 400, 300, 100, 0, 90, RED)
    #pygame.gfxdraw.arc(screen, 400, 300, 100, 90, 180, GREEN)
    # draw_regular_polygon(screen, (0,255,0), 6,  50, (200, 200))
    
    screens[0].render(0)
    
    pygame.display.flip()
