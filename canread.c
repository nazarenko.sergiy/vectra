#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>

void bin(unsigned n)
{
    unsigned i;
    for(i = 1 << 31; i > 0; i = i / 2)
        (n & i) ? printf("1") : printf("0");
}

int main(int argc, char **argv) 
{
	struct ifreq ifr;
	struct sockaddr_can addr;
 
	struct can_frame frame;
 	int s;

	memset(&ifr, 0x0, sizeof(ifr));
 	memset(&addr, 0x0, sizeof(addr));
 	memset(&frame, 0x0, sizeof(frame));

	/* open CAN_RAW socket */
	s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

	/* convert interface sting "can0" into interface index */ strcpy(ifr.ifr_name, "vcan0");

	ioctl(s, SIOCGIFINDEX, &ifr);

	/* setup address for bind */ 
	addr.can_ifindex = ifr.ifr_ifindex;
 	addr.can_family = PF_CAN;

	/* bind socket to the can0 interface */
	bind(s, (struct sockaddr *)&addr, sizeof(addr));

	int n = 0;
	while( (n = read(s, &frame, sizeof(frame)) > 0))
	{
        if (frame.can_id == 0x450){	
		  if (frame.data[3]==0x47){
            printf("can_id %X [%d] %02X %02X ", frame.can_id, frame.can_dlc, frame.data[2], frame.data[3]);
            bin(frame.data[3]);
            printf("\n");
            }
        }
	}

	return 0;
/* first fill, then send the CAN frame */ frame.can_id = 0x23;

strcpy((char *)frame.data, "hello");
 frame.can_dlc = 5;

write(s, &frame, sizeof(frame));

/* first fill, then send the CAN frame */ 
frame.can_id = 0x23;

strcpy((char *)frame.data, "iCC2012");
 frame.can_dlc = 7;

write(s, &frame, sizeof(frame));
 close(s);

return 0;
}
