#!/bin/bash

#TC: Tank 
#TC-current / TC-Inst: actual fuel level
#TC-Time: average fuel level
#TC-Dist: average fuel level, filtered

#RC: Range-Consum 
#RC-Time: average consumption (last minute ?)
#RC-Dist: average consumption (distance 50km ? )
#RC-Inst: instantenous

#Fuel-Cut-Off: FCO
#Residual-Fuel-Cut: RFC
#Cold-Start-Dist: CSD

#Inj-RC: Inj Range-Consum [ml/sec]( * 3,6 = l/h)
#TC-Dist / RC-Dist = Range reachable distance 


#11 00 - 34
#15 00 - 42
#15 FF - 43
#30 00 - 96
#30 46 - 96
#30 96 - 97
#30 8C - 97
#30 78 - 97


while :
do

for i in {0..255..2}
do
	hex=`printf "%02X" $i`
	for j in {0..255..5}
	do
		kex=`printf "%02X" $j`
		#c="108#11${hex}${kex}00${hex}${kex}BED2"
		c="108#11000000${hex}${kex}00${kex}"
		cansend can2 $c
        	#echo "108#11${hex}${kex}000000BED2"
		sleep 0.1
		#sleep 0.485
      	#	c="130#00${hex}${kex}${hex}00" # 65 -20 68 -16  58 - 25.8
        #cansend can2 $c
		echo $c
	done
done


done

