#!/bin/bash
# check candump-2020-02-01_125834.log can3
#array=(. can33.txt)

#OIFS="$IFS"; IFS=$'\n'; array=($(<can33.txt)); IFS="$OIFS"
array_c=( `cat $1 | cut -d'#' -f1 | cut -d' ' -f3 | sort | uniq -c | sort -nr | awk '{ printf " \"%s\" \"%s - %s\" ON ",$2,$2,$1 }'` ) 

#array=("108" "108 - 2448" ON "621" "621 - 2447" ON "500" "500 - 1721" ON "130" "130 - 493" ON "155" "155 - 319" ON "400" "400 - 263" ON "445" "445 - 245" ON "440" "440 - 245" ON "330" "330 - 245" ON "228" "228 - 245" ON "220" "220 - 245" ON "530" "530 - 244" ON "090" "090 - 244" ON "190" "190 - 224" ON "11A" "11A - 214" ON "260" "260 - 195" ON "360" "360 - 193" ON "115" "115 - 192" ON "145" "145 - 189" ON "305" "305 - 173" ON "430" "430 - 163" ON "426" "426 - 163" ON "420" "420 - 163" ON "405" "405 - 163" ON "370" "370 - 163" ON "350" "350 - 163" ON "340" "340 - 163" ON "23A" "23A - 163" ON "235" "235 - 163" ON "230" "230 - 163" ON "170" "170 - 163" ON "315" "315 - 149" ON "626" "626 - 82" ON "624" "624 - 26" ON "175" "175 - 17" ON "140" "140 - 10" ON)
#array=($(k))
echo "array=(" > $1.array
echo ${array_c[@]} >> $1.array
echo ")" >> $1.array
source $1.array
#array=("cash3" "54311 May 25 10:10" OFF "cash10" "10475 May 28 18:23" OFF "cash14" "9905 May 27 15:5" OFF "cash13" "11780 May 29 09:32" OFF)

declare -a  arr

if [ -f "$1.exclude" ]; then
	arr=`cat $1.exclude`
fi

ret_array=()

for ((i=0;i< ${#array[@]} ;i+=3));
do
	if [[ " ${arr[*]} " == *"${array[i]}"* ]];
	then
		ret_array+=("${array[i]}" "${array[i+1]}" ON)
	else
		ret_array+=("${array[i]}" "${array[i+1]}" OFF)
	fi
done
#echo ${ret_array[@]}
#exit 0
TURNEDOFF=$(whiptail --title "Operations" --notags --checklist "Select Operation" 22 120 15 "${ret_array[@]}"  3>&1 1>&2 2>&3)

exitstatus=$?
echo $exitstatus

if [ $exitstatus = 0 ]; then
echo $TURNEDOFF > $1.exclude

repeat() while :; do cat "$1"; done

exp=`echo "${TURNEDOFF//\"}" | tr " " "\n" | awk '{ printf "\\\\s%s#|", $1 }'`
echo $exp
repeat $1 | grep -E "(${exp::-1})" | canplayer $2 
fi
#vcan0=$2
