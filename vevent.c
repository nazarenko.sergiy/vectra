#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>
#include <syslog.h>
#include <glob.h>
#include <errno.h>
#include <limits.h>
#include <string.h>

#define EVENT_DEVICE    "/dev/input/event1"
#define EVENT_TYPE      EV_ABS
#define EVENT_CODE_X    ABS_X
#define EVENT_CODE_Y    ABS_Y

/* TODO: Close fd on SIGINT (Ctrl-C), if it's open */


#ifndef  ULONG_BITS
#define  ULONG_BITS   (CHAR_BIT * sizeof (unsigned long))
#endif

static inline int  has_bit(const unsigned long  data[], const size_t  bit)
{
    return !!(data[bit / ULONG_BITS] & (1uL << (bit % ULONG_BITS)));
}

char *touchscreen_event_device(size_t skip)
{
    glob_t  files;
    int     result;

    result = glob("/dev/input/event*", 0, NULL, &files);
    if (result) {
        if (result == GLOB_NOSPACE) {
            errno = ENOMEM;
            return NULL;
        } else
        if (result == GLOB_NOMATCH) {
            errno = ENOENT;
            return NULL;
        } else {
            errno = EACCES;
            return NULL;
        }
    }

    for (size_t  i = 0;  i < files.gl_pathc;  i++) {
        int  fd = open(files.gl_pathv[i], O_RDONLY);
        if (fd != -1) {
            unsigned long  absbits[1 + ABS_MAX / ULONG_BITS] = { 0 };
            unsigned long  keybits[1 + KEY_MAX / ULONG_BITS] = { 0 };
            if (ioctl(fd, EVIOCGBIT(EV_ABS, ABS_MAX+1), &absbits) != -1 &&
                ioctl(fd, EVIOCGBIT(EV_KEY, KEY_MAX+1), &keybits) != -1) {
                if (has_bit(absbits, ABS_X) &&
                    has_bit(absbits, ABS_Y) &&
                    has_bit(keybits, BTN_TOUCH)) {
                    /* Device reports ABS_X, ABS_Y and BTN_TOUCH,
                       and therefore is a touchpad device. */
                    if (!skip) {
                        char *devpath = strdup(files.gl_pathv[i]);
                        close(fd);
                        globfree(&files);
                        if (!devpath)
                            errno = ENOMEM;
                        return devpath;
                    } else {
                        skip--;
                    }
                }
            }
            close(fd);
        }
    }
    globfree(&files);

    errno = ENOENT;
    return NULL;
}


struct pixel
{
    int x;
    int y;
};


// https://unix.stackexchange.com/questions/11953/make-a-log-file

int main(int argc, char *argv[])
{
    struct input_event ev;
    
    openlog("vevent", 0, LOG_USER);

    int fd;
    char name[256] = "Unknown";
    if(argc <= 1)
    {
        syslog(LOG_NOTICE, "no pid.. exit");
        closelog();
        exit(1);
    }

    size_t  i = 0;

    while (1) {
        char *devpath = touchscreen_event_device(i);
        if (!devpath) {
            if (i)
                break;
            fprintf(stderr, "No touchscreen input event devices found: %s.\n", strerror(errno));
            return EXIT_FAILURE;
        }
        printf("Found touchscreen input event device '%s'\n", devpath);
        free(devpath);
        i++;
    }

    int xterm_pid = atoi(argv[1]);
/*
    if ((getuid ()) != 0) {
        fprintf(stderr, "You are not root! This may not work...\n");
        return EXIT_SUCCESS;
    }
*/
    /* Open Device */
    fd = open(EVENT_DEVICE, O_RDONLY);
    if (fd == -1) {
        syslog(LOG_NOTICE, "%s is not a vaild device", EVENT_DEVICE);
        closelog();
        return EXIT_FAILURE;
    }
    
    /* Print Device Name */
    ioctl(fd, EVIOCGNAME(sizeof(name)), name);
    syslog(LOG_NOTICE, "Reading from: device file = %s device name = %s", EVENT_DEVICE,  name);
    struct pixel xy;
    xy.x = -1;
    xy.y = -1;

    for (;;) 
    {
        const size_t ev_size = sizeof(struct input_event);
        ssize_t size;

        /* TODO: use select() */

        size = read(fd, &ev, ev_size);
        if (size < ev_size) {
            fprintf(stderr, "Error size when reading\n");
            goto err;
        }

        if (ev.type == EVENT_TYPE)
        { 
            if (ev.code == EVENT_CODE_X)
            {
                    xy.x = ev.value;
                    xy.y = -1;
            }
            if (ev.code == EVENT_CODE_Y) 
                    xy.y = ev.value;
        }
        
        if (xy.x != -1 && xy.y != -1) 
        {
            syslog(LOG_NOTICE, "pid %u x=%d, y=%d", xterm_pid, xy.x, xy.y);
            if(xy.x > 700 && xy.y < 100)
            {
                kill(xterm_pid, SIGKILL);
                return EXIT_SUCCESS;
            }
            xy.x = -1;
            xy.y = -1;
        }
        
    }
    closelog();
    return EXIT_SUCCESS;

err:
    close(fd);
    return EXIT_FAILURE;
}
