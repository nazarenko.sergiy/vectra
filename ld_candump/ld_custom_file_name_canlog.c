#define _GNU_SOURCE

/*
CFLAGS += -std=c99 -Wall

log_file_accesses.so:
	gcc $(CFLAGS) -shared -fPIC path-mapping.c -o path-mapping.so -ldl

clean:
	rm *.so
*/

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <dlfcn.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdarg.h>
#include <malloc.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <libgen.h>


void __attribute__((constructor)) setup();
//void __attribute__((destructor)) calledLast();


static char filename[1024];
static char  filepath[1024];
#if 0
int main(int argc, char *argv[])
{
    
    return 0;
}
#endif

void to_upper(char* string)
{
    const char OFFSET = 'a' - 'A';
    while (*string)
    {
        *string = (*string >= 'a' && *string <= 'z') ? *string -= OFFSET : *string;
        string++;
    }
}

// This function is assigned to execute before
// main using __attribute__((constructor))
void setup()
{
    char *    can_dev = getenv("ODROID_CAN");
    if(can_dev != NULL)
    {
        to_upper(can_dev);
    }
    char *    can_name = getenv("CAN1_SW");
    if(can_name != NULL) 
    {
        to_upper(can_name);
    }
    time_t T = time(NULL);
    struct tm tm = *localtime(&T);
    sprintf(filename, "%s-%s_%04d-%02d-%02dT%02d%02d%02d.log", can_name, can_dev, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    printf("%s\n", filename);
}


static const char *fix_path(const char *path)
{
    char *fn;
    fn = dirname(path);
    strcpy(filepath , fn);
    strcpy(filepath, filename);
    return filepath;

}

typedef FILE *(*fopen_t)(const char* pathname, const char *mode);
fopen_t orig_func;

FILE *fopen64(const char *restrict pathname, const char *restrict mode)
{
    const char *new_path = fix_path(pathname);
    orig_func = (fopen_t)dlsym(RTLD_NEXT, "fopen64");
    return orig_func(new_path, mode);
}
