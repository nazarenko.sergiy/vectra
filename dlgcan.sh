#!/bin/bash
HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Backtitle here"
TITLE="Title here"
MENU="Choose one of the following options:"
hex="00"
kex="00"
pex="00"
CANCMD="130#00${hex}${kex}${pex}00"

OPTIONS=(1 "Option 1"
         2 "Option 2"
         3 "Option 3"
         4 "Quit")
CHOICE=1
while [ "$CHOICE" -ne 4 ]; do
    CHOICE=$(dialog --clear \
                    --backtitle "$BACKTITLE $CHOICE" \
                    --title "$TITLE" \
                    --menu "$MENU" \
                    $HEIGHT $WIDTH $CHOICE_HEIGHT \
                    "${OPTIONS[@]}" \
                    "Please enter the information" 12 40 4 "Name :" 1 1 "" 1 12 15 0 "Age: " 2 1 "" 2 12 15 0 "Mail id:" 3 1 "" 3 12 15 0 \
                    2>&1 >/dev/tty)

    clear
    case $CHOICE in
            1)
                sudo apt-get update
                ;;
            2)
                echo "You chose Option 2"
                ;;
            3)
                echo "You chose Option 3"
                ;;
    esac
done