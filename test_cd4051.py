import sys
sys.path.insert(0,'/mnt/pub/vectra/build/lib')

from cd4051bm import CD4051BM

cd40 = CD4051BM(3,7,23)

state =  cd40.get()


a = (state >> 0) & 1
b = (state >> 1) & 1
c = (state >> 2) & 1

print "%d%d%d" % (a,b,c)