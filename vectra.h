#pragma once

#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#define GUI_SEM_STR_NAME "gui_str_semafore"
#define GUI_MEM_STR_NAME "gui_str_memory"
#define GUI_MEM_STR_SIZE 50
#define GUI_SEM_ARR_NAME "gui_arr_semafore"
#define GUI_MEM_ARR_NAME "gui_arr_memory"
#define GUI_MEM_ARR_SIZE 50

#define CAN_SEM_STR_NAME "can_str_semafore"
#define CAN_MEM_STR_NAME "can_str_memory"
#define CAN_MEM_STR_SIZE 50
#define CAN_SEM_ARR_NAME "can_arr_semafore"
#define CAN_MEM_ARR_NAME "can_arr_memory"
#define CAN_MEM_ARR_SIZE 50

#define NRF_SEM_STR_NAME "nrf_str_semafore"
#define NRF_MEM_STR_NAME "nrf_str_memory"
#define NRF_MEM_STR_SIZE 50
#define NRF_SEM_ARR_NAME "nrf_arr_semafore"
#define NRF_MEM_ARR_NAME "nrf_arr_memory"
#define NRF_MEM_ARR_SIZE 50

// https://ru.wikipedia.org/wiki/OBD-II_PIDs

#define MAXLINE         1024

struct can_ret
{
    char* name;
    double min;
    double max;
    double ret;
};


double intake_air_temperature(can_ret&, unsigned char *);
double dummy_service(can_ret&, unsigned char *);
double odometer(can_ret&, unsigned char *);
double calculated_engine_load_value(can_ret&, unsigned char*);
double x_term_fuel(can_ret&, unsigned char*);
double fuel_pressure(can_ret&, unsigned char*);
double coolant_temperature(can_ret&, unsigned char*);
double intake_manifold_absolute_pressure(can_ret&, unsigned char*);
double rpm(can_ret&, unsigned char*);
double vehicle_speed(can_ret&, unsigned char*);
double MAF_air_flow_rate(can_ret&, unsigned char*);
double control_module_voltage(can_ret&, unsigned char*);

typedef double(*fptr)(can_ret&, unsigned char*);



int pipe_str_write (const std::string&, const std::string&, int, const char*);
int pipe_arr_write (const std::string&, const std::vector<int>&, int, const char*);
int pipe_arr_read(std::vector<int>&, const std::string&, int, const char*);
int pipe_str_read(std::string&,const std::string&, int, const char*);
extern "C" int pipe_arr_set_n_items(const int*, int, const std::string&, int, const char* );

std::vector<int> arr_read_can();
std::vector<int> arr_read_gui();
std::vector<int> arr_read_nrf();


int arr_set_nrf(const int*, int);

int arr_write_can(const std::vector<int>&);
int arr_set_can(const int*, int);
int arr_set_can(const std::vector<int>&);

int arr_write_gui(const std::vector<int>&);
int arr_write_nrf(const std::vector<int>&);

double get_can_current_value();
void set_can_current_data(int func_id, int frame_id, int a, int b);

int dhoLeftOff();
int dhoLeftOn();
int dhoRightOff();
int dhoRightOn();
int dhoOff();
int dhoOn();

enum ReadCan
{
     SUN 
    ,COOLANT
    ,RPM
    ,SPEED
    ,TMPR_BATTERY
    ,FLUID_METER
    ,FUEL
    ,BLOCKM_A
    ,BLOCKM_B
    ,BLOCKM_CALL_A
    ,BLOCKM_CALL_B
    ,LIGHT_SIGNALS
    ,REVERSE_TEST_VALUE
    ,REVERSE_TEST_CANID
    ,REVERSE_TEST_FUNCID
    ,REVERSE_TEST_CAN_FRAMES_A
    ,REVERSE_TEST_CAN_FRAMES_B
    ,TC_INST
    ,SPEED_MS
    ,TC_TIME
    ,INJ_RC
    ,TMPR_XU4_DISP
    ,TMPR_FRACTURE_XU4_DISP
    ,ODB_COOLANT
};

enum BlockM_RS
{
	 RS_FLOW_RESET
	,RS_LIGHTS
	,RS_RELAY_L_PORT
	,RS_RELAY_R_PORT
	,RESERVED4
	,RESERVED5
	,RESERVED6
	,RESERVED7
};

enum BlockM_RS_LIGTHS
{
	 RS_LIGHTS_ONOFF = 16
	,RS_LIGHTS_LEFT = 17
	,RS_LIGHTS_RIGHT = 18
};

enum BlockM
{
     FLOW_LO
    ,FLOW_HI
    ,TEMPER_LO
    ,TEMPER_HI
    ,RESERVED
    ,WIPES_LO
    ,WIPES_HI
    ,STATE
};
