#include "vectra.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>


#include <linux/can.h>

#define CAN_HELP 1
#define CAN_DRAIN  2
#define CAN_ONE_SHOT  3
#define CAN_CAN  4
#define CAN_CALL 5
#define CSR_HOME "\33[H"
#define CLR_SCREEN "\33[2J"

void usage(const char * s) {
    printf("Usage: %s <drain>\n", s);
}

int main (int argc, char ** argv) 
{

    int mode = 0;
    if ( argc > 1 ) 
    {
        if (!strcmp(argv[1], "help")) 
        {
            usage(argv[0]);
            return 1;
        }
        if (!strcmp(argv[1], "drain"))
            mode = CAN_DRAIN;
        if (!strcmp(argv[1], "can"))
            mode = CAN_CAN;
        if (!strcmp(argv[1], "call"))
            mode = CAN_CALL;
        if (!strcmp(argv[1], "shot"))
            mode = CAN_ONE_SHOT;
    }

    //printf("\033[H\033[J");
    //printf("\e[2J\e[H"); 
    // printf("%s%s", CLR_SCREEN, CSR_HOME);
    // printf("%s", CSR_HOME);
    std::vector<int> data_can;
    data_can.reserve(CAN_MEM_ARR_SIZE);
    data_can.assign(CAN_MEM_ARR_SIZE, 0);
    arr_write_can(data_can);
    if (mode == CAN_DRAIN)
    {
        while(true)
        {
            // std::vector<int> data_nrf = arr_read_nrf();
            // std::vector<int> data_gui = arr_read_gui();

            // int nrf0 = data_nrf[0];
            // if (nrf0 != -1)
            //     printf("CAN READ: Data[0] from nrf == %d\n", nrf0);

            // int gui0 = data_gui[0];
            // if (gui0 != -1)
            //     printf("CAN READ: Data[0] from gui == %d\n", gui0);
            
            pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    
            printf("CAN READ: == %d %d %d\n", data_can[BLOCKM_B], data_can[LIGHT_SIGNALS], data_can[2]);
            int res = 0x00;
            // int i = rand() % 1;
            // int j = rand() % 1;

            res |= (1<<0);

            // res |= (0 << 1);
            int data[4] = {BLOCKM_B, res, SUN, 1};

            int r = arr_set_can(data, 4);

            usleep(5000);
        }
    }
    else
    {
        int s;
        int nbytes;
        struct sockaddr_can addr;
        struct can_frame frame;
        struct ifreq ifr;

        // const char *ifname = "vcan0";
        const char *ifname = "can2";

        if((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
            perror("Error while opening socket");
            return -1;
        }

        strcpy(ifr.ifr_name, ifname);
        ioctl(s, SIOCGIFINDEX, &ifr);

        addr.can_family  = AF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;

        // printf("%s at index %d\n", ifname, ifr.ifr_ifindex);

        if(bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
            perror("Error in socket bind");
            return -2;
        }

        frame.can_id = 0x321;
        frame.can_dlc = 8;

        
        if (mode == CAN_CAN)
        {
            unsigned int lights = 0;
            int lights_data_can = 0;

            while( true )
            {
                pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);

                // lights_data_can = data_can[ReadCan::LIGHT_SIGNALS];
                
                // if (((lights_data_can >> 0) & 1 ) == 1)
                //     lights |= (1<<0); // ON
                // else
                //     lights &=~ (1<<0); // OFF

                // if (((lights_data_can >> 1) & 1 ) == 1)
                //     lights |= (1<<1); // ON
                // else
                //     lights &=~ (1<<1); // OFF

                // if (((lights_data_can >> 2) & 1 ) == 1)
                //     lights |= (1<<2); // ON
                // else
                //     lights &=~ (1<<2); // OFF

                // if (((lights_data_can >> 3) & 1 ) == 1)
                //     lights |= (1<<3); // ON
                // else
                //     lights &=~ (1<<3); // OFF

                //lights |= (1<<0);
                // left = lights 1
                // right = lights 2
                // alarm = lights 3
                frame.data[BlockM_RS::RS_FLOW_RESET] = data_can[BLOCKM_CALL_A] >> 24;
                frame.data[BlockM_RS::RS_LIGHTS] = data_can[LIGHT_SIGNALS];
                frame.data[BlockM_RS::RS_RELAY_L_PORT] = 0;//data_can[BLOCKM_CALL_A] >> 8;
                frame.data[BlockM_RS::RS_RELAY_R_PORT] = 0;//data_can[BLOCKM_CALL_A];
                frame.data[BlockM_RS::RESERVED4] = data_can[BLOCKM_CALL_B] >> 24;
                frame.data[BlockM_RS::RESERVED5] = data_can[BLOCKM_CALL_B] >> 16;
                frame.data[BlockM_RS::RESERVED6] = data_can[BLOCKM_CALL_B] >> 8;
                frame.data[BlockM_RS::RESERVED7] = data_can[BLOCKM_CALL_B];
                // int r = write(s, &frame, sizeof(struct can_frame));
                // frame.data[0] = 24;
                // frame.data[1] =  16;
                // frame.data[2] = 0;//data_can[BLOCKM_CALL_A] >> 8;
                // frame.data[3] = 0;//data_can[BLOCKM_CALL_A];
                // frame.data[4] = 24;
                // frame.data[5] = 16;
                // frame.data[6] = 8;
                // frame.data[7] = 0;
                int r = write(s, &frame, sizeof(struct can_frame));

                // printf("CAN WRITE: == %d\n", r);

                sleep(1);

            }
        }
        else if (mode == CAN_CALL)
        {
            while( true )
            {
                pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
                
                // left = lights 1
                // right = lights 2
                // alarm = lights 3
                frame.data[BlockM_RS::RS_FLOW_RESET] = data_can[BLOCKM_CALL_A] >> 24;
                frame.data[BlockM_RS::RS_LIGHTS] = data_can[BLOCKM_CALL_A] >> 16;
                frame.data[BlockM_RS::RS_RELAY_L_PORT] = 0;//data_can[BLOCKM_CALL_A] >> 8;
                frame.data[BlockM_RS::RS_RELAY_R_PORT] = 0;//data_can[BLOCKM_CALL_A];
                frame.data[BlockM_RS::RESERVED4] = data_can[BLOCKM_CALL_B] >> 24;
                frame.data[BlockM_RS::RESERVED5] = data_can[BLOCKM_CALL_B] >> 16;
                frame.data[BlockM_RS::RESERVED6] = data_can[BLOCKM_CALL_B] >> 8;
                frame.data[BlockM_RS::RESERVED7] = data_can[BLOCKM_CALL_B];
                // int r = write(s, &frame, sizeof(struct can_frame));
                // frame.data[0] = 24;
                // frame.data[1] =  16;
                // frame.data[2] = 0;//data_can[BLOCKM_CALL_A] >> 8;
                // frame.data[3] = 0;//data_can[BLOCKM_CALL_A];
                // frame.data[4] = 24;
                // frame.data[5] = 16;
                // frame.data[6] = 8;
                // frame.data[7] = 0;
                int r = write(s, &frame, sizeof(struct can_frame));

                // printf("CAN WRITE: == %d\n", r);

                if (mode == CAN_ONE_SHOT)
                    return 0;
                
                sleep(1);

            }
        }
    }
    return 0;
}

