#!/bin/bash

# https://wiki.odroid.com/odroid-c1/software/building_kernel
# https://releases.linaro.org/archive/14.09/components/toolchain/binaries/
# https://github.com/Illuminux/ct-ng-linaro-mac/blob/master/build.sh
# https://forum.odroid.com/viewtopic.php?t=38405

make -j4
make -j4 uImage modules
make dtbs
sudo make modules_install
sudo cp arch/arm/boot/uImage /media/boot/
sudo cp arch/arm/boot/dts/meson8b_odroidc.dtb /media/boot/

