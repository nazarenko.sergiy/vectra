#!/usr/bin/env python
# -*- coding: utf-8 -*-
import types
import sys
import os
import time
import subprocess
import math
import yaml
import sys
import traceback

from PIL import Image, ImageDraw
from subprocess import *

from time import strftime, localtime 
MENUDIR = os.environ.get('MENUDIR')
sys.path.append(MENUDIR)
import vectra_gui
import json

from datetime import datetime

import pygame
from pygame.locals import *

from collections import deque

import logging
from pygame import display
import signal
import time

import vectra_functions as vf

#cat candump-2021-11-23_072326.log | grep 130# |  cut -d "#" -f 2 > ../can3.txt

# def handler(signum, frame):
#   """Why is systemd sending sighups? I DON'T KNOW."""
#   logging.warning("Got a {} signal. Doing nothing".format(signum))

# signal.signal(signal.SIGHUP, handler)
# signal.signal(signal.SIGTERM, handler)
# signal.signal(signal.SIGCONT, handler)

# logging.warning("About to start display.")
# try:
#   display.init()   # hups
# except Exception as ex:
#   logging.warning("Got any exception: %s " % ex)

# logging.warning("Quitting in 60")
# time.sleep(60)

PI=math.pi

CONFIG_FILE = MENUDIR + os.sep + 'config.json'
with open(CONFIG_FILE) as json_file:
    config = json.load(json_file)


ODB2_FILE = MENUDIR + os.sep + 'odb2.yaml'
ODB2_SERVICES = {}
with open(ODB2_FILE, 'r') as file:
    odb2_service = yaml.safe_load(file)
    if odb2_service:
        for n in odb2_service:
            ODB2_SERVICES[n['service']] = n['name']


os.environ['VECTRA_CAN'] = config['can'].get('VECTRA_CAN', 'gray')
os.environ['CAN1_SW'] = config['can'].get('CAN1_SW', 'gray')
os.environ['ODROID_CAN'] = config['can'].get('ODROID_CAN', 'can0')
os.environ['ODROID_TEST_CANID'] = config['can'].get('TEST_CANID', '108')
speed_coefficient =  PI * float(config.get('diameter', 643)) / 1000000

font2_name = config['fonts']['label'].get('name', 'dejavusans') #k[font_index]
font_index = config['fonts']['label'].get('size', 12) #int(m)

CMDREADERRORS = MENUDIR + os.sep + 'read_dtc.sh'
CMDDUMPH = MENUDIR + os.sep + 'dump_hex.sh'
CMDDUMPL = MENUDIR + os.sep + 'dump_log.sh'
CMDSNIFB = MENUDIR + os.sep + 'sniffer_binary.sh'
CMDSNIFH = MENUDIR + os.sep + 'sniffer_hex.sh'
CMDPURPLEDUMP = MENUDIR + os.sep + 'dump_purple.sh'
CMDGRAYDUMP = MENUDIR + os.sep + 'dump_gray.sh'
CMDREDDUMP = MENUDIR + os.sep + 'dump_red.sh'
CMDODB2DUMP = MENUDIR + os.sep + 'dump_odb2.sh'
CMDBLOCKDUMP = MENUDIR + os.sep + 'dump_block.sh'
CMD_OFF_DUMP_START_TO_ALLSCAN = MENUDIR + os.sep + 'all_scan.sh'
CMDPLAYV = MENUDIR + os.sep + 'play_vcan.sh'
CMDTEST  = MENUDIR + os.sep + 'test_can.sh'
CMDAPSTART = 'sudo systemctl start create_ap'
CMDAPSTOP = 'sudo systemctl stop create_ap'
CMDRESETFLOWMETER  = MENUDIR + os.sep + 'reset_flowmeter.sh'

CMD500 = MENUDIR + os.sep + 'can500kbps.sh'
CMD95 = MENUDIR + os.sep + 'can95kbps.sh'
CMD33 = MENUDIR + os.sep + 'can33kbps.sh'

IMG_WINDSHIELD = MENUDIR + os.sep + 'windshield.png'
IMG_GAS = MENUDIR + os.sep + 'gas.png'
IMG_TURN_SIG = MENUDIR + os.sep + 'turn_signals_left.png'
IMG_BATTERY_TEMP = MENUDIR + os.sep + 'battery.png'


if os.environ.get('USER') == 'odroid':
    os.environ["SDL_FBDEV"] = "/dev/fb1"
    os.environ["SDL_MOUSEDEV"] = "/dev/input/touchscreen"
    os.environ["SDL_MOUSEDRV"] = "TSLIB"


# Initialize pygame modules individually (to avoid ALSA errors) and hide mouse
#pygame.init()
pygame.font.init()
pygame.display.init()

clock = pygame.time.Clock()

if os.environ.get('USER') == 'odroid':
    pygame.mouse.set_visible(0)



def run_cmd(cmd):
    process = Popen(cmd.split(), stdout=PIPE)
    output = process.communicate()[0]
    return output



class GlobalIndex(object):
    _index = 0
    def setIndex(self, val):
        self._index = val
    def index(self):
        return self._index



class GlobalCan(object):
    _data = []
    _cd4051bmstate = 0
    def refresh(self):
        self._data = vectra_gui.array_read_can()
    
    def getData(self):
        return self._data




class GlobalCanErrors(object):
    _data = []

    def refresh(self):
        self._data = vectra_gui.array_read_nrf()
    
    def getData(self):
        return self._data


    # def cd4051bmCheckState(self):
    #     self._cd4051bmstate = cd40.get()

    # def cd4051GetState(self):
    #     return self._cd4051bmstate



# colors    R    G    B
white    = (255, 255, 255)
tron_whi = (189, 254, 255)
red      = (255,   0,   0)
green    = (0, 255,   128)
blue     = (  0,   0, 255)
tron_blu = (  0, 219, 232)
black    = (  0,   0,   0)
cyan     = ( 50, 255, 255)
magenta  = (255,   0, 255)
yellow   = (255, 255,   0)
tron_yel = (255, 140, 0)
orange   = (255, 127,   0)
tron_ora = (255, 202,   0)

# Tron theme orange
tron_regular = tron_ora
tron_light   = tron_yel
tron_inverse = black


#set size of the screen
size = width, height = 800, 480
screen = pygame.display.set_mode(size)

globalcan = GlobalCan()


#m =  [8,12,16,20,26,40,58,61,70,77,79,81,86,90,92,96,101,102,112,117,128,131,133,141,150,151,157,161,165,173,174,177,179]

# k = pygame.font.get_fonts()
# with open('/tmp/pyfonts.txt','a') as f:
#     for d in k:
#         f.write('%s\n'%d)
# with open("/tmp/pyfonts.txt","r+") as f:
#    m = f.read()
#    i = int(m) + 1
#    if i > len(k) :
#        i = len(k) - 1
#    f.seek(0)
#    f.write(str(i))
#    f.truncate()


class VectraWidget(object):
    def __init__(self, colors=(tron_light, black
                , pygame.colordict.THECOLORS['sienna4'])):
        self._colors = colors
        self._pressed = False

    def render(self, inverse_color):
        self._color_index = inverse_color
        if self._pressed == True:
            self._color_index = 2

    def move(self, deltax, deltay):
        self.x += deltax
        self.y += deltay
        self.h += deltay
        self.w += deltax
    def set_inverse(self,val):
        self._inverse = val
    def get_text(self):
        return ''
    def set_pressed(self):
        pass
    def is_pressed(self):
        return self._pressed
    def set_released(self):
        pass
    def __contains__(self, touch_pos):
        return False


class Line(VectraWidget):
    def __init__(self, spos, epos, wide, colors=(tron_light, black)):
        super(Line, self).__init__()
        self.spos = spos
        self.epos = epos
        self._wide = wide

    def render(self, inverse_color=0):
        pygame.draw.line(screen, self._colors[inverse_color], self.spos, self.epos, self._wide)


class Label(VectraWidget):
    def __init__(self, pos, fontsize, font = None):
        super(Label, self).__init__()
        self.x, self.y = pos
        self._fontsize = fontsize
        self._font = font
        self.font = None
        if self._font == None:
            self.font=pygame.font.Font(self._font,self._fontsize)
        else:
            self.font=self._font


    def render(self, inverse_color=0):
        label=self.font.render(self.get_text(), 1, (self._colors[inverse_color]))
        background = int(not bool(inverse_color))
        pygame.draw.rect(screen, self._colors[background], (self.x,self.y,label.get_width(),label.get_height()),0)
        screen.blit(label,(self.x,self.y))


class LabelTransparent(VectraWidget):
    def __init__(self, pos, fontsize, font = None):
        super(LabelTransparent, self).__init__()
        self.x, self.y = pos
        self._fontsize = fontsize
        self._font = font
        self.font = None
        if self._font == None:
            self.font=pygame.font.Font(self._font,self._fontsize)
        else:
            self.font=self._font

    def render(self, inverse_color=0):

        label=self.font.render(self.get_text(), 1, (self._colors[inverse_color]))

        for i, w, color in self.get_rects():
            pygame.draw.rect(screen, pygame.colordict.THECOLORS[color], (self.x+i,self.y,w,label.get_height()),0)
        
        screen.blit(label,(self.x,self.y))

    def get_rects(self):
        return []


class Button(VectraWidget):
    def __init__(self, label, rect):
        super(Button, self).__init__()
        self._pressed = False
        self._label = label
        self._inverse = False
        self.x, self.y, self.h, self.w = rect
        self.x_min, self.x_max, self.y_min, self.y_max = self.x, self.x + self.w, self.y, self.y + self.h
        self.font=pygame.font.Font(None,42)

    def render(self, inverse_color=0):
        if self._inverse == True:
            self._color_index = int(not bool(inverse_color))
        super(Button, self).render(inverse_color)

        self._text_color_index = int(not bool(self._color_index))

        pygame.draw.rect(screen, self._colors[self._color_index], (self.x-9,self.y-8,self.w-1,self.h+1),0)
        
        label=self.font.render(str(self._label), 1, (self._colors[self._text_color_index]))
        screen.blit(label,(self.x,self.y))

    def click(self):
        pass

    def set_text(self, text):
        self._label = str(text)

    def set_pressed(self):
        self._pressed = True
    
    def is_pressed(self):
        return self._pressed 

    def set_released(self):
        self._pressed = False

    def __contains__(self, touch_pos):
        return (self.x_min <= touch_pos[0] <= self.x_max and self.y_min <= touch_pos[1] <= self.y_max)



class Screen(object):
    def __init__(self):
        self._objects = []

    def attach(self, *objs):
        for obj in objs:
            self._objects += [obj]

    def move(self, deltax, deltay):
        for n in self._objects:
            n.move(deltax, deltay)

    def render(self,v):
        for n in self._objects:
            n.render(v)

    def on_touch_down(self, touch_pos):
        for n in self._objects: 
            if touch_pos in n:
                n.set_pressed()
                return

    def on_touch_up(self, touch_pos):
        for n in self._objects: 
            if (touch_pos in n) and n.is_pressed():
                n.click()
            n.set_released()    


index = GlobalIndex()  


class BtnResetUI(Button):
    def __init__(self, rect):
        super(BtnResetUI, self).__init__(" Reset UI", rect)
    
    def click(self):
        pygame.quit()
        page=os.environ["MENUDIR"] + os.sep + "menu.py"
        os.execvp("python", ["python", page])
        sys.exit()


class BtnSnifferBin(Button):
    def __init__(self, rect):
        super(BtnSnifferBin, self).__init__(" Sniffer B", rect)
    
    def click(self):
        pygame.quit()
        run_cmd(CMDSNIFB)
        os.execv(__file__, sys.argv)


class BtnSnifferHex(Button):
    def __init__(self, rect):
        super(BtnSnifferHex, self).__init__(" Sniffer H", rect)
    
    def click(self):
        pygame.quit()
        run_cmd(CMDSNIFH)
        os.execv(__file__, sys.argv)


class BtnDumpLog(Button):
    def __init__(self, rect):
        super(BtnDumpLog, self).__init__(" Dump log ", rect)
    
    def click(self):
        pygame.quit()
        run_cmd(CMDDUMPL)
        os.execv(__file__, sys.argv)


class BtnDumpHex(Button):
    def __init__(self, rect):
        super(BtnDumpHex, self).__init__(" Dump hex ", rect)

    def click(self):
        pygame.quit()
        run_cmd(CMDDUMPH)
        os.execv(__file__, sys.argv)


class BtnRstFlowmeter(Button):
    def __init__(self, rect):
        super(BtnRstFlowmeter, self).__init__("Reset Flow", rect)

    def click(self):
         process = Popen(CMDRESETFLOWMETER.split(), stdout=PIPE)
         output = process.communicate()[0]
         return output

current_index = 0

class BtnNext(Button):
    def __init__(self, rect):
        super(BtnNext, self).__init__("  <<<", rect)

    def click(self):
        global current_index
        current_index += 1
        index.setIndex(current_index)


class BtnPrev(Button):
    def __init__(self, rect):
        super(BtnPrev, self).__init__("  >>>", rect)

    def click(self):
        global current_index
        current_index -= 1
        index.setIndex(current_index)


class BtnShutdown(Button):
    def __init__(self, rect):
        super(BtnShutdown, self).__init__(" Shutdown", rect)

    def click(self):
         command = "/usr/bin/sudo /sbin/shutdown -h now > /home/odroid/shutdown.$RANDOM 2>$1"
         process = Popen(command.split(), stdout=PIPE)
         output = process.communicate()[0]
         return output


class BtnReboot(Button):
    def __init__(self, rect):
        super(BtnReboot, self).__init__("   Reboot", rect)

    def click(self):
        command = "/bin/dmesg -F /home/odroid/dmesg.txt"
        process = Popen(command.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output


class BtnPlayVcan(Button):
    def __init__(self, rect):
        super(BtnPlayVcan, self).__init__("Play VCAN", rect)
    
    def click(self):
        pygame.quit()
        run_cmd(CMDPLAYV)
        os.execv(__file__, sys.argv)

class BtWIFIon(Button):
    def __init__(self, rect):
        super(BtWIFIon, self).__init__("WIFI on", rect)
    
    def click(self):
        process = Popen(CMDAPSTART.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output

class BtWIFIoff(Button):
    def __init__(self, rect):
        super(BtWIFIoff, self).__init__("WIFI off", rect)
    
    def click(self):
        process = Popen(CMDAPSTOP.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output



# 17-18 RED
# 19-20 YELLOW
# 22-23 PURPLE
# 25 GRAY
# 28 ORANGE


class BtnCan0(Button):
    def __init__(self, rect):
        super(BtnCan0, self).__init__("  GRAY  ", rect)
    
    def click(self):
        os.environ['ODROID_CAN']='can0'
        os.environ['VECTRA_CAN']='gray'
        os.environ['CAN1_SW'] = "gray"
        config['can']['ODROID_CAN']='can0'
        config['can']['VECTRA_CAN']='gray'
        config['can']['CAN1_SW']='gray'
        with open(CONFIG_FILE, "w") as json_file:
            json.dump(config, json_file, indent=4) 
        process = Popen(CMDGRAYDUMP.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output



class BtnCan1(Button):
    def __init__(self, rect):
        super(BtnCan1, self).__init__(" PURPLE ", rect)

    def click(self):
        os.environ['ODROID_CAN']='can0'
        config['can']['ODROID_CAN']='can0'
        os.environ['VECTRA_CAN']='blueviolet'
        os.environ['CAN1_SW'] = "purple"
        config['can']['VECTRA_CAN']='blueviolet'
        config['can']['CAN1_SW']='purple'
        with open(CONFIG_FILE, "w") as json_file:
            json.dump(config, json_file, indent=4)
        process = Popen(CMDPURPLEDUMP.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output


class BtnCan2(Button):
    def __init__(self, rect):
        super(BtnCan2, self).__init__("  ODB2  ", rect)

    def click(self):
        os.environ['ODROID_CAN']='can0'
        config['can']['ODROID_CAN']='can0'
        os.environ['VECTRA_CAN']='white'
        os.environ['CAN1_SW'] = "odb2"
        config['can']['VECTRA_CAN']='white'
        config['can']['CAN1_SW']='odb2'
        with open(CONFIG_FILE, "w") as json_file:
            json.dump(config, json_file, indent=4)
        process = Popen(CMDODB2DUMP.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output



class BtnCan3(Button):
    def __init__(self, rect):
        super(BtnCan3, self).__init__("  BLOCK ", rect)

    def click(self):
        os.environ['ODROID_CAN']='can0'
        config['can']['ODROID_CAN']='can0'        
        os.environ['VECTRA_CAN']='orange'
        os.environ['CAN1_SW'] = "block"
        config['can']['VECTRA_CAN']='orange'
        config['can']['CAN1_SW']='block'
        with open(CONFIG_FILE, "w") as json_file:
            json.dump(config, json_file, indent=4)
        process = Popen(CMDBLOCKDUMP.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output


class BtnCan4(Button):
    def __init__(self, rect):
        super(BtnCan4, self).__init__("   ALL  ", rect)

    def click(self):
        os.environ['ODROID_CAN']='can0'
        config['can']['ODROID_CAN']='can0'
        os.environ['VECTRA_CAN']='green'
        os.environ['CAN1_SW'] = "all"
        config['can']['VECTRA_CAN']='green'
        config['can']['CAN1_SW']='all'
        with open(CONFIG_FILE, "w") as json_file:
            json.dump(config, json_file, indent=4)
        process = Popen(CMD_OFF_DUMP_START_TO_ALLSCAN.split(), stdout=PIPE)
        output = process.communicate()[0]
        return output


class CanTest(Button):
    def __init__(self, rect):
        self._diameter = int(config.get('diameter', 643))
        super(CanTest, self).__init__("{: >10}".format(self._diameter), rect)
    
    def click(self):
        self._diameter += 1
        if self._diameter > 652:
            self._diameter = 643
        self.set_text("{: >10}".format(self._diameter))
        config['diameter'] = self._diameter
        with open(CONFIG_FILE, "w") as json_file:
            json.dump(config, json_file, indent=4)
        global speed_coefficient
        speed_coefficient = PI * float(self._diameter) / 1000000 


class BtnTurnOffDHO(Button):
    def __init__(self, rect):
        super(BtnTurnOffDHO, self).__init__(" DHO off ", rect)
    
    def click(self):
        i = vectra_gui.dhoOff()
        logging.warning("Got dhoOff: %s " % i)


class BtnTurnOnDHO(Button):
    def __init__(self, rect):
        super(BtnTurnOnDHO, self).__init__(" DHO on ", rect)
    
    def click(self):
        i = vectra_gui.dhoOn()
        logging.warning("Got dhoOn: %s " % i)
        

class BtnTurnOnLeftDHO(Button):
    def __init__(self, rect):
        super(BtnTurnOnLeftDHO, self).__init__(" Left on ", rect )
    
    def click(self):
        vectra_gui.dhoLeftOn()


class BtnTurnOffLeftDHO(Button):
    def __init__(self, rect):
        super(BtnTurnOffLeftDHO, self).__init__(" Left off ", rect)
    
    def click(self):
        vectra_gui.dhoLeftOff()
        
class BtnTurnOnRightDHO(Button):
    def __init__(self, rect):
        super(BtnTurnOnRightDHO, self).__init__(" Right on ", rect)
    
    def click(self):
        vectra_gui.dhoRightOn()


class BtnTurnOffRightDHO(Button):
    def __init__(self, rect):
        super(BtnTurnOffRightDHO, self).__init__(" Right off ", rect)
    
    def click(self):
        vectra_gui.dhoRightOff()
   

class LTemperature1(Label):
    def __init__(self, pos):
        super(LTemperature1, self).__init__(pos, 32
            , font=pygame.font.SysFont(font2_name,27))

    def get_text(self):
        return u"20ᵒ"


class ReadErrors(Button):
    def __init__(self, rect):
        super(ReadErrors, self).__init__(" Errors ", rect)
    
    def click(self):
        process = Popen(CMDREADERRORS.split(), stdout=PIPE)
        output = process.communicate()[0]
        #globalcanerrors.refresh()
        return output


    
class StatusBar(LabelTransparent):
    def __init__(self, pos):
        super(StatusBar, self).__init__(pos, 22
            , font=pygame.font.SysFont('freemono',20))
        self.params = {}
        self._x = 59

    def get_rects(self):
        ret = []
        ret += [(1,46,os.environ['VECTRA_CAN'])]
        ret += [(self._x,25,'greenyellow' if self.params['LF'] == 0 else 'red3')]
        ret += [(self._x + 37*1,25,'greenyellow' if self.params['LL'] == 0 else 'red3')]
        ret += [(self._x + 36*2,25,'greenyellow' if self.params['LR'] == 0 else 'red3')]
        ret += [(self._x + 36*3,25,'greenyellow' if self.params['LA'] == 0 else 'red3')]
        ret += [(self._x + 36*4,25,'greenyellow' if self.params['OF'] == 0 else 'red3')]
        ret += [(self._x + 36*5,25,'greenyellow' if self.params['DS'] == 0 else 'red3')]
        ret += [(self._x + 36*6,25,'greenyellow' if self.params['CL'] == 0 else 'red3')]
        return ret

    def get_text(self):
        data = globalcan.getData()
        disp_temp_frac = data[ int(vectra_gui.TMPR_FRACTURE_XU4_DISP) ]
        disp_temp = data[ int(vectra_gui.TMPR_XU4_DISP) ]
        blockm_state = data[ int(vectra_gui.BLOCKM_B) ]
        self.params['LF'] = (blockm_state >> 0) & 1
        self.params['LR'] = (blockm_state >> 1) & 1
        self.params['LL'] = (blockm_state >> 2) & 1
        self.params['LA'] = (blockm_state >> 3) & 1
        self.params['OF'] = (blockm_state >> 4) & 1
        self.params['DS'] = (blockm_state >> 5) & 1
        self.params['CL'] = (blockm_state >> 7) & 1
        return u"%s.%s NO LL LR LA OF DS CL" % ( disp_temp, disp_temp_frac ) # ( k[font_index], font_index)


class LDate(Label):
    def __init__(self, pos):
        super(LDate, self).__init__(pos, 58)

    def get_text(self):
        return strftime("%d.%m.%Y", localtime())


class LRange(Label):
    def __init__(self, pos):
        super(LRange, self).__init__(pos, 43, font=pygame.font.SysFont(font2_name,27))

    def get_text(self):
        return u"Range      %s km" % ('180'.rjust(4))


class LConsumption(Label):
    def __init__(self, pos):
        super(LConsumption, self).__init__(pos, 43, font=pygame.font.SysFont(font2_name,27))

    def get_text(self):
        return u"Ø-Consumption       %s Ltr/100 km" % ('14.1'.rjust(4))


class LInstConsumption(Label):
    def __init__(self, pos):
        super(LInstConsumption, self).__init__(pos, 43, font=pygame.font.SysFont(font2_name,27))

    def get_text(self):
        return u"Inst. consumpt.      %s %s" % ('1.2', 'Ltr./h') # ( k[font_index], font_index)


class LTime(Label):
    def __init__(self, pos):
        super(LTime, self).__init__(pos, 32, font=pygame.font.SysFont(font2_name,27))
    
    def get_text(self):
        return strftime("%H:%M:%S", localtime())


class TankInst(Label):
    def __init__(self, pos):
        super(TankInst, self).__init__(pos, 30
            , font=pygame.font.SysFont(font2_name,22))
    
    def get_text(self):
        v = str(globalcan.getData()[ int(vectra_gui.TC_INST) ]/10)
        return u'TC-inst: %s' % v.rjust(6)


class LCoolant(Label):
    def __init__(self, pos):
        super(LCoolant, self).__init__(pos, 30
            , font=pygame.font.SysFont(font2_name,22))
    
    def get_text(self):
        v = str(globalcan.getData()[ int(vectra_gui.COOLANT) ])
        return u'coolant: %sᵒ' % v.rjust(6)


class LRpm(Label):
    def __init__(self, pos):
        super(LRpm, self).__init__(pos, 30
            , font=pygame.font.SysFont(font2_name,22))

    def get_text(self):
        v = str(globalcan.getData()[ int(vectra_gui.RPM) ])
        return u'      rpm: %s' % v.rjust(5)


class LODB2_srv(Label):
    def __init__(self, pos):
        super(LODB2_srv, self).__init__(pos, 30
            , font=pygame.font.SysFont(font2_name,45))
        self._services = {}
    
    def render(self, inverse_color=0):
        srv_id = globalcan.getData()[ int(vectra_gui.REVERSE_TEST_CANID) ]
        srv_val = globalcan.getData()[ int(vectra_gui.REVERSE_TEST_VALUE) ]

        self._services[srv_id] = srv_val
        
        y_pos = self.y
        space = 20
        
        for k, name in ODB2_SERVICES.items():           
            paragraph = "%s: %s" % (name, self._services.get(k, 0.0))
            label=self.font.render(paragraph, 1, (self._colors[inverse_color]))
            screen.blit(label,(self.x,y_pos+space))
            y_pos += 55
            space = 20


class LErrors(Label):
    def __init__(self, pos):
        super(LErrors, self).__init__(pos, 30
            , font=pygame.font.SysFont(font2_name,22))
    
    def render(self, inverse_color=0):
        def grouped(itterable, n):
            return zip(*[iter(itterable)]*n)
        
        frame = vf.dtc_errors() 

        dtc, description = vf.parse_dtc(frame[1:3])
        
        text1 = dtc + ' (' + ' '.join(list(map(hex, frame))) + ')'
        y_pos = self.y
        space = 0
        
        text2 = description.split('\n')

        for i, paragraph in enumerate(([text1] + text2)):
            label=self.font.render(paragraph, 1, (self._colors[inverse_color]))
            screen.blit(label,(self.x,y_pos+space))
            y_pos += 20
            space = 10


class LMenuErrors(Label):
    def __init__(self, pos):
        super(LMenuErrors, self).__init__(pos, 30
            , font=pygame.font.SysFont(font2_name,22))
        self._stack_trace = []
    
    def set_error(self, ex):
        with open("/home/odroid/menu_error.%s.log" % datetime.now().strftime("%Y%m%d-%H%M%S"),"w") as f:
                traceback.print_exc(file=f)
        # Get current system exception
        ex_type, ex_value, ex_traceback = sys.exc_info()

        # Extract unformatter stack traces as tuples
        trace_back = traceback.extract_tb(ex_traceback)

        # Format stacktrace
        self._stack_trace = list()

        self._stack_trace.append("Exception type : %s" % ex_type.__name__)
        self._stack_trace.append("Exception message : %s" %ex_value)
        for trace in trace_back:
            self._stack_trace.append("File : %s , Line : %d, Func.Name : %s, Message : %s" % (trace[0], trace[1], trace[2], trace[3]))

    def render(self, inverse_color=0):
        y_pos = self.y
        space = 0
        for paragraph in self._stack_trace:
            label=self.font.render(paragraph, 1, (self._colors[inverse_color]))
            screen.blit(label,(self.x,y_pos+space))
            y_pos += 20
            space = 10


class BatteryTemperature(VectraWidget):
    def __init__(self, pos
            , font=pygame.font.SysFont(font2_name,22)):
        super(BatteryTemperature, self).__init__()
        self._font = font
        self.x, self.y = pos

        src = pygame.image.load(IMG_BATTERY_TEMP).convert_alpha()
        self._img_surf = pygame.transform.scale(src
            , (src.get_width()//17, src.get_height()//17))

        self._img_surf_invert = self._img_surf.copy()
        self._img_surf_invert.fill(self._colors[1], special_flags=pygame.BLEND_ADD)
        self._img_rect = self._img_surf.get_rect(center=(self.x-5, self.y+7))

    def render(self, inverse_color, *args, **kwargs):
        v = str(float(globalcan.getData()[ int(vectra_gui.TMPR_BATTERY) ])*0.0625)
        label=self._font.render(u'batt. temp: %sᵒC' % v.rjust(6), 1, self._colors[inverse_color])
        #img = self._img_surf if inverse_color == 1 else self._img_surf_invert
        #screen.blit(img, self._img_rect)
        screen.blit(label, (self.x + 20, self.y))


class ProgressFlowmeter(VectraWidget):
    def __init__(self, pos, fontsize, font = None):
        super(ProgressFlowmeter, self).__init__()
        self.x, self.y = pos
        self._fontsize = fontsize
        self._font = font

        self._pil_size = 80
        self._pil_radius = 42
        src = pygame.image.load(IMG_WINDSHIELD).convert_alpha()
        src.fill(pygame.colordict.THECOLORS['sienna4'], special_flags=pygame.BLEND_ADD)
        
        self._dog_surf = pygame.transform.scale(src, (src.get_width()//4, 
                                          src.get_height()//4))
        self._dog_x = self._dog_surf.get_width() // 2
        self._dog_y = self._dog_surf.get_height() // 2
        self._pil_image = Image.new("RGBA", (self._pil_size+1, self._pil_size+1))
        self._pil_draw = ImageDraw.Draw(self._pil_image)
        self._pil_half_size = self._pil_size // 2
        self._mode = self._pil_image.mode
        self._size = self._pil_image.size

    def render(self, inverse_color, *args, **kwargs):
        i = globalcan.getData()[ int(vectra_gui.FLUID_METER) ] + 0.1
        consump=360.0-(0.006923*i) # 360 / 52000
        background = inverse_color #int(not bool(inverse_color))
        #self._pil_draw.pieslice((0, 0, self._pil_size, self._pil_size), consump, 0, fill=(128, 128, 128))
        self._pil_draw.pieslice((0, 0, self._pil_size, self._pil_size)
            , 360, consump, fill=self._colors[background])

        pygame.draw.circle(screen, self._colors[background]
            , (self.x, self.y), self._pil_radius, 5)

        # - convert into PyGame image -
        data = self._pil_image.tobytes()
        self._image = pygame.image.fromstring(data, self._size, self._mode)
        self._image_rect = self._image.get_rect(center=(self.x, self.y))
    
        label=self._font.render(str(round(5.12-i/10000,2)) + " Ltr", 1, self._colors[inverse_color])
        x = label.get_width() // 2
        y = label.get_height() // 2
        screen.blit(self._image, self._image_rect) # <- display image

        screen.blit(self._dog_surf,
                             (self._image_rect[0] + self._pil_half_size - self._dog_x
                            , self._image_rect[1] + self._pil_half_size - self._dog_y))
        screen.blit(label,
                             (self._image_rect[0] + self._pil_half_size - x
                            , self._image_rect[1] + self._pil_size))


class ProgressGasmeter(VectraWidget):
    """docstring for ClockMeter"""
    def __init__(self, pos, fontsize, font = None):
        super(ProgressGasmeter, self).__init__()
        self.pos = pos
        self.size = 130.0
        self.font = font
        self.font2 = pygame.font.SysFont(font2_name,20)
        self.font2.set_bold(True)

        def roint(num):
            return int(round(num))

        self.hours = []
        self._half_size = self.size/2

        numbers = 10
        display = deque(range(numbers))
        cos_sin = deque()

        for hour in display:
            cos = math.cos(math.radians(360.0 / numbers * hour))
            sin = math.sin(math.radians(360.0 / numbers * hour))
            cos_sin.append( (cos,sin) )

        cos_sin.rotate(1) 

        for i , (cos, sin) in enumerate(cos_sin):
            x_h_0 = self.pos[0] - roint(self._half_size + self.size/13*cos*4) + self._half_size
            y_h_0 = self.pos[1] - roint(self._half_size + self.size/13*sin*4) + self._half_size
            x_h_1 = self.pos[0] - roint(self._half_size + self.size/3*cos) + self._half_size
            y_h_1 = self.pos[1] - roint(self._half_size + self.size/3*sin) + self._half_size
            x_h_2 = self.pos[0] - roint(self._half_size + self.size/18*cos*4) + self._half_size
            y_h_2 = self.pos[1] - roint(self._half_size + self.size/18*sin*4) + self._half_size

            self.hours += [ (i, x_h_0, y_h_0, x_h_1, y_h_1, x_h_2, y_h_2) ]
        self.hours = self.hours[:-3]

        self.PI_4_3 = 4*PI/3
        self.size_arrow_x =  6*self.size/16
        self.size_arrow_y =  6*self.size/16
        

    def render(self, inverse_color, *args, **kwargs):
        def roint(num):
            return int(round(num))
        background = self._colors[int(not bool(inverse_color))]
        foreground = self._colors[inverse_color]
        rect = pygame.draw.circle(screen, background, self.pos, roint(self.size/3)) 

        for hour in self.hours:
            i, x_h_0, y_h_0, x_h_1, y_h_1, x_h_2, y_h_2 = hour
            pygame.draw.line(screen, foreground,[x_h_0 ,y_h_0],[x_h_1 ,y_h_1],3)
            pygame.draw.line(screen, foreground,[x_h_0 ,y_h_0],[x_h_1 ,y_h_1],3)
            label=self.font.render(str(i), 1, foreground)
            text_rect = label.get_rect(center=(x_h_2,y_h_2))
            screen.blit(label,text_rect)
        
        # for minute in range(60):
        #     cos = math.cos(math.radians(360.0 / 60 * minute))
        #     sin = math.sin(math.radians(360.0 / 60 * minute))
        #     x_m_0 = pos[0] - roint(self.size/2 +self.size/13*cos*4) + self.size/2
        #     y_m_0 = pos[1] - roint(self.size/2 + self.size/13*sin*4) + self.size/2
        #     x_m_1 = pos[0] - roint(self.size/2 + self.size/3*cos) + self.size/2
        #     y_m_1 = pos[1] - roint(self.size/2 + self.size/3*sin) + self.size/2
        #     pygame.draw.line(screen, red,[x_m_0 ,y_m_0],[x_m_1 ,y_m_1],2)
        pygame.draw.arc(screen, foreground, rect.inflate(3,3), 0, self.PI_4_3, 3)
        pygame.draw.arc(screen, red, rect.inflate(3,3), PI, self.PI_4_3, 6)
        self.font.set_bold(True)

        second = float(datetime.now().second)

        #minute = float(datetime.now().minute)
        #hour = float(datetime.now().hour)
        # Second 
        #angle = 6 * second - 90
        #x = self.pos[0] + 6*self.size/16 * math.cos(math.radians(angle))
        #y = self.pos[1] + 6*self.size/16 * math.sin(math.radians(angle))
        #pygame.draw.line(screen, green,[ self.pos[0],self.pos[1] ],[x, y],4)
        # # Minute Hand

        i = float(globalcan.getData()[ int(vectra_gui.TC_INST) ]/10.0)
        j = float(globalcan.getData()[ int(vectra_gui.TC_TIME) ]/10.0)

        # 45   0 -220   60 0 -220
        radians = math.radians( i*3.66-220 )
        x = self.pos[0] + self.size_arrow_x * math.cos(radians)
        y = self.pos[1] + self.size_arrow_y * math.sin(radians)
        pygame.draw.line(screen, green,[ self.pos[0],self.pos[1] ],[x, y],4)
        # # Second Hand
        # angle = 30 * (hour%12) + 5*minute/10 - 90
        # x = self.pos[0] + 6*self.size/30 * math.cos(math.radians(angle))
        # y = self.pos[1] + 6*self.size/30 * math.sin(math.radians(angle))
        # pygame.draw.line(screen, white,[ self.pos[0],self.pos[1] ],[x, y],10)
        # The Clock
        # pos = (roint(self.size/2), roint(self.size/2))
        #(pos[0]-roint(self.size/2),pos[1]-roint(self.size/2),roint(self.size/2),roint(self.size/2))
        rect1 = rect.inflate(-30,-60).move(20,27)
        pygame.draw.rect(screen, black, rect1)
        label=self.font2.render(str(i), 1, green)
        text_rect = label.get_rect(center=rect1.center)
        screen.blit(label,text_rect)
        pygame.draw.rect(screen, (125,125,125), rect1, 1)

        rect2 = rect.inflate(-30,-60).move(20,53)
        pygame.draw.rect(screen, black, rect2)
        label2=self.font2.render(str(j), 1, green)
        text_rect2 = label2.get_rect(center=rect2.center)
        screen.blit(label2,text_rect2)
        pygame.draw.rect(screen, (125,125,125), rect2, 1)


class ProgressCoolantmeter(VectraWidget):
    """docstring for ClockMeter"""
    def __init__(self, pos, fontsize, font = None):
        super(ProgressCoolantmeter, self).__init__()
        self.pos = pos
        self.size = 130.0
        self.font = font
        self.font2 = pygame.font.SysFont(font2_name,20)
        self.font2.set_bold(True)

        def roint(num):
            return int(round(num))

        self.hours = []
        self._half_size = self.size/2

        numbers = 8
        display = deque(range(numbers))
        cos_sin = deque()

        for hour in display:
            cos = math.cos(math.radians(360.0 / numbers * hour))
            sin = math.sin(math.radians(360.0 / numbers * hour))
            cos_sin.append( (cos,sin) )

        cos_sin.rotate(1) 

        k = [-40,-10,20,50,80,110,90,110]

        for i , (cos, sin) in enumerate(cos_sin):
            x_h_0 = self.pos[0] - roint(self._half_size + self.size/13*cos*4) + self._half_size
            y_h_0 = self.pos[1] - roint(self._half_size + self.size/13*sin*4) + self._half_size
            x_h_1 = self.pos[0] - roint(self._half_size + self.size/3*cos) + self._half_size
            y_h_1 = self.pos[1] - roint(self._half_size + self.size/3*sin) + self._half_size
            x_h_2 = self.pos[0] - roint(self._half_size + self.size/18*cos*4) + self._half_size
            y_h_2 = self.pos[1] - roint(self._half_size + self.size/18*sin*4) + self._half_size

            self.hours += [ (k[i], x_h_0, y_h_0, x_h_1, y_h_1, x_h_2, y_h_2) ]
        self.hours = self.hours[:-2]

        self.PI_4_3 = 4*PI/3
        self.size_arrow_x =  6*self.size/16
        self.size_arrow_y =  6*self.size/16
        

    def render(self, inverse_color, *args, **kwargs):
        def roint(num):
            return int(round(num))
        background = self._colors[int(not bool(inverse_color))]
        foreground = self._colors[inverse_color]
        
        rect = pygame.draw.circle(screen, background, self.pos, roint(self.size/3)) 

        for hour in self.hours:
            i, x_h_0, y_h_0, x_h_1, y_h_1, x_h_2, y_h_2 = hour
            pygame.draw.line(screen, foreground,[x_h_0 ,y_h_0],[x_h_1 ,y_h_1],3)
            pygame.draw.line(screen, foreground,[x_h_0 ,y_h_0],[x_h_1 ,y_h_1],3)
            label=self.font.render(str(i), 1, foreground)
            text_rect = label.get_rect(center=(x_h_2,y_h_2))
            screen.blit(label,text_rect)
        
        pygame.draw.arc(screen, foreground, rect.inflate(3,3), 0, self.PI_4_3, 3)
        pygame.draw.arc(screen, red, rect.inflate(3,3), PI, self.PI_4_3, 6)
        self.font.set_bold(True)

        second = float(datetime.now().second)

        i = globalcan.getData()[ int(vectra_gui.COOLANT) ]

        # 45   0 -220   150 0
        radians = math.radians( i*1.46-220 )
        x = self.pos[0] + self.size_arrow_x * math.cos(radians)
        y = self.pos[1] + self.size_arrow_y * math.sin(radians)
        pygame.draw.line(screen, green,[ self.pos[0],self.pos[1] ],[x, y],4)
        
        rect = rect.inflate(-30,-60).move(20,27)
        pygame.draw.rect(screen, black, rect)
        label=self.font2.render(str(i-40), 1, green)
        text_rect = label.get_rect(center=rect.center)
        screen.blit(label,text_rect)
        pygame.draw.rect(screen, (125,125,125), rect, 1)


class ProgressGasmeterWithIcon(VectraWidget):
    def __init__(self, pos, fontsize, font = None):
        super(ProgressGasmeterWithIcon, self).__init__()
        self.x, self.y = pos
        self._fontsize = fontsize
        self._font = font
        self._font2 = font
        self._font.set_bold(True)
        self._font2.set_bold(False)
        self._pil_size = 80
        self._pil_radius = 42
        src = pygame.image.load(IMG_GAS).convert_alpha()
        src.fill(pygame.colordict.THECOLORS['sienna4'], special_flags=pygame.BLEND_ADD)
        
        self._dog_surf = pygame.transform.scale(src, (src.get_width()//4, 
                                          src.get_height()//4))
        self._dog_x = self._dog_surf.get_width() // 2
        self._dog_y = self._dog_surf.get_height() // 2
        self._pil_image = Image.new("RGBA", (self._pil_size+1, self._pil_size+1))
        self._pil_draw = ImageDraw.Draw(self._pil_image)
        self._pil_half_size = self._pil_size // 2
        self._mode = self._pil_image.mode
        self._size = self._pil_image.size

    def render(self, inverse_color, *args, **kwargs):
        i = float(globalcan.getData()[ int(vectra_gui.TC_INST) ]/10.0)
        consump=(6*i) # 360 / 60
        background = inverse_color 
        self._pil_draw.pieslice((0, 0, self._pil_size, self._pil_size)
            , 361, consump, fill=self._colors[background])

        pygame.draw.circle(screen, self._colors[background]
            , (self.x, self.y), self._pil_radius, 5)

        # - convert into PyGame image -
        data = self._pil_image.tobytes()
        self._image = pygame.image.fromstring(data, self._size, self._mode)
        self._image_rect = self._image.get_rect(center=(self.x, self.y))
    
        label=self._font.render(str(i), 1, self._colors[inverse_color])
        label2=self._font2.render(str(i), 1, pygame.colordict.THECOLORS['white'])
        x = label.get_width() // 2
        y = label.get_height() // 2
        screen.blit(self._image, self._image_rect) # <- display image

        screen.blit(self._dog_surf,
                             (self._image_rect[0] + self._pil_half_size - self._dog_x
                            , self._image_rect[1] + self._pil_half_size - self._dog_y))
        screen.blit(label,
                             (self._image_rect[0] + self._pil_half_size - x
                            , self._image_rect[1] + self._pil_size - 50))
        screen.blit(label2,
                             (self._image_rect[0] + self._pil_half_size - x
                            , self._image_rect[1] + self._pil_size - 50))

class LSpeed(Label):
    def __init__(self, pos):
        super(LSpeed, self).__init__(pos, 30, font=pygame.font.SysFont(font2_name,22))

    def get_text(self):
        v = float(globalcan.getData()[ int(vectra_gui.SPEED)]) * speed_coefficient 
        return u'  speed: {0:.4f} m/s'.format(v)

class KMSpeed(Label):
    def __init__(self, pos):
        super(KMSpeed, self).__init__(pos, 98)

    def get_text(self):
        v = float(globalcan.getData()[ int(vectra_gui.SPEED )]) * speed_coefficient * 3.6
        return u'{0:.2f} km/h'.format(v)

class TurnSignals(VectraWidget):
    def __init__(self, pos):
        super(TurnSignals, self).__init__()
        center_x, center_y = pos
        src_l = pygame.image.load(IMG_TURN_SIG).convert_alpha()

        self._left_surf = pygame.transform.scale(src_l, (src_l.get_width()//4, 
                                          src_l.get_height()//4))

        src_r = pygame.image.load(IMG_TURN_SIG).convert_alpha()
        src_rr = pygame.transform.scale(src_r, (src_r.get_width()//4, 
                                          src_r.get_height()//4))

        self._right_surf = pygame.transform.rotate(src_rr, 180)

        self._right_surf_on = self._right_surf.copy()
        self._left_surf_on = self._left_surf.copy()
        self._left_surf_on.fill((0, 255, 0, 0), special_flags=pygame.BLEND_ADD)
        self._right_surf_on.fill((0, 255, 0, 0), special_flags=pygame.BLEND_ADD)

        self._right_surf_inverse = self._right_surf.copy()
        self._left_surf_inverse = self._left_surf.copy()
        self._left_surf_inverse.fill(self._colors[0], special_flags=pygame.BLEND_ADD)
        self._right_surf_inverse.fill(self._colors[0], special_flags=pygame.BLEND_ADD)

        self._left_rect = self._left_surf.get_rect(center=(center_x-20, center_y))
        self._right_rect = self._right_surf.get_rect(center=(center_x+20, center_y))

    def render(self, inverse_color):
        value =  globalcan.getData()[ int(vectra_gui.LIGHT_SIGNALS) ]
        l_surf = self._left_surf
        r_surf = self._right_surf

        if inverse_color == 1:
            l_surf = self._left_surf_inverse
            r_surf = self._right_surf_inverse

        if ( ((value >> 1) & 1 ) == 1 ) :
            l_surf = self._left_surf_on
        if ( ((value >> 2) & 1 ) == 1 ) :
            r_surf = self._right_surf_on

        screen.blit(l_surf, self._left_rect)
        screen.blit(r_surf, self._right_rect)

xx = 50

s1 = Screen()
s1.attach(    LDate( (400-xx,90) )
            , BtnResetUI( (30, 70, 45, 170) )
            , ProgressGasmeter((620,70), 18, font=pygame.font.SysFont(font2_name,14))
            , BtnNext( (30, 430, 55, 110) )
            , LTime( (650,340) )
            , Line((230-xx,150),(750,150),4)
            , LRange( (390-xx,170) )
            , LConsumption( (280-xx,220) )
            , LInstConsumption( (280-xx,270) )
            , StatusBar( (10,10) )
            , Line((300-xx,330),(680-xx,330),4)
            , Line((680-xx,330),(680-xx,380),4)
            , Line((300-xx,330),(300-xx,380),4)
            , LTemperature1( (230-xx,340) )
            , LSpeed( (441,420) )
            , KMSpeed( (260,342) )
            , LRpm( (240,447) )
            , TankInst( (441,447) )
            , ProgressCoolantmeter( (70,280) , 18, font=pygame.font.SysFont(font2_name,14))
            , BatteryTemperature( (195,420) )
            , ProgressFlowmeter((730,70), 18, font=pygame.font.SysFont(font2_name,18))
            #, ProgressGasmeter((630,70), 18, font=pygame.font.SysFont(font2_name,18))
            , TurnSignals((720,430)))

s2 = Screen()
s2.attach(   BtnPrev( (30, 430, 55, 110) )
           , BtnNext( (180, 430, 55, 110) )
           , BtnShutdown( (30, 20, 45, 170) )
           , BtnReboot( (30, 80, 45, 170) )
           , BtnPlayVcan( (220, 380, 45, 170) )
           , BtnSnifferBin( (30, 180, 45, 170) )
           , BtnSnifferHex( (30, 240, 45, 170) )
           , BtnDumpLog( (30, 300, 45, 170) )
           , BtnDumpHex( (30, 360, 45, 170) )
           , BtnRstFlowmeter( (220, 20, 45, 170) )
           , BtnCan0( (220, 130, 45, 170) )
           , BtnCan1( (220, 190, 45, 170) )
           , BtnCan2( (220, 250, 45, 170) )
           , BtnCan3( (220, 310, 45, 170) )
           , BtnCan4( (410, 130, 45, 170) )
           , CanTest( (410, 370, 45, 170) )
           , BtWIFIon( (410, 430, 45, 170) )
           , BtnTurnOnDHO( (600, 130, 45, 170) )
           , BtnTurnOffDHO( (600, 70, 45, 170) )
           , BtnTurnOffRightDHO( (600, 370, 45, 170) )
           , BtnTurnOnRightDHO( (600, 310, 45, 170) )
           , BtnTurnOffLeftDHO( (600, 250, 45, 170) )
           , BtWIFIoff( (600, 430, 45, 170) )
           , BtnTurnOnLeftDHO( (600, 190, 45, 170) )   )

s3 = Screen()
s3.attach(
      BtnPrev( (30, 430, 55, 110) )
    , BtnNext( (180, 430, 55, 110) )
    , ReadErrors( (30, 20, 55, 110) )
    , LErrors( (30, 80) )

)

s4 = Screen()
s4.attach(
      BtnPrev( (30, 430, 55, 110) )
    , LODB2_srv( (30, 80) ) 
    , KMSpeed( (260,342) )
)


class Screens(object):
    _screens = []
    _error = None

    def __init__(self, index):
        self._index = index
        self._screen_error = Screen()
        self._error_widget = LMenuErrors( (30, 80) )
        self._screen_error.attach( 
                    BtnResetUI( (30, 70, 45, 170) ),
                    self._error_widget
        )

    def set_screens(self, lst):
        self._screens = lst
    
    def set_error(self, ex):
        self._error = ex
        self._error_widget.set_error(ex)
    
    def get_error(self):
        return self._error
    
    def item(self):
        if self._error:
            return self._screen_error
        return self._screens[index.index()]
    
    def __getitem__(self, row):
        return self._screens[row]

screens = Screens(index)
screens.set_screens( [s1, s2, s3, s4] )

background = (black, tron_light)
color_index = 1 

# CD4051BMCHECK = USEREVENT + 1
# pygame.time.set_timer(CD4051BMCHECK, 2500)

#While loop to manage touch screen inputs
while 1:
    globalcan.refresh()
    can_data = globalcan.getData()
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])
            screens.item().on_touch_down(pos)
        if event.type == pygame.MOUSEBUTTONUP:
            pos = (pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1])
            screens.item().on_touch_up(pos)

        #ensure there is always a safe way to end the program if the touch screen fails
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                sys.exit()
        # if event.type == CD4051BMCHECK:
        #     globalcan.cd4051bmCheckState()
    value = can_data[ int(vectra_gui.LIGHT_SIGNALS) ]
    color_index = not ((value >> 0) & 1 )
    
    try:
        screens.item().render(color_index)
    except Exception as e:
        screens.set_error(e)
        
    pygame.display.update()

    screen.fill( background[color_index] )
    clock.tick(24)


