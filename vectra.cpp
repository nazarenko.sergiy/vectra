#include "vectra.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include <semaphore.h>
#include <errno.h>
#include <unistd.h>

#define IDX_A 3
#define IDX_B 4
#define IDX_C 5
#define IDX_D 6


double dummy_service(can_ret& ret, unsigned char * frame)
{
    //strcpy (ret.name,"coolant");
    printf("Not implemented yet for 0x%02X  0x%02X  0x%02X  0x%02X  0x%02X  0x%02X  0x%02X  0x%02X\n",
           frame[0], frame[1], frame[2], frame[3], frame[4], frame[5], frame[6], frame[7]  );
    ret.min = 0.0;
    ret.max = 0.0;
    ret.ret = 0.0;
    return ret.ret;
}

double odometer(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    unsigned char B = frame[IDX_B];
    unsigned char C = frame[IDX_C];
    ret.min = 0;
    ret.max = 100;
    ret.ret = (A*16777216 + B*65536 + C*256) / 10;
    return ret.ret;
}

double intake_air_temperature(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    ret.min = -40.0;
    ret.max = 215.0;
    ret.ret = A-40.0 ;
    return ret.ret;
}


double calculated_engine_load_value(can_ret& ret, unsigned char * frame)
{
    //strcpy (ret.name,"engine load");
    unsigned char A = frame[IDX_A];
    ret.min = 0;
    ret.max = 100;
    ret.ret = A*100/255 ;
    return ret.ret;
}

double x_term_fuel(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    //strcpy (ret.name,"term fuel");
    ret.min = -100;
    ret.max = 99.22;
    ret.ret = (A-128) * 100/128;
    return ret.ret;
}

double fuel_pressure(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    //strcpy (ret.name,"fuel pressure");
    ret.min = 0;
    ret.max = 765;
    ret.ret = A * 3;
    return ret.ret;
}

double coolant_temperature(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    //strcpy (ret.name,"coolant");
    ret.min = -40;
    ret.max = 215;
    ret.ret = A - 40;
    return ret.ret;
}

double intake_manifold_absolute_pressure(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    //strcpy (ret.name,"intake pressure");
    ret.min = 0;
    ret.max = 255;
    ret.ret = A;
    return ret.ret;
}

double rpm(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    unsigned char B = frame[IDX_B];
    //strcpy (ret.name,"rpm");
    ret.min = 0;
    ret.max = 16383.75 ;
    ret.ret = ((A*256)+B)/4;
    return ret.ret;
}


double vehicle_speed(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    //strcpy (ret.name,"speed");
    ret.min = 0;
    ret.max = 255 ;
    ret.ret = A;
    return ret.ret;
}

double MAF_air_flow_rate(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    unsigned char B = frame[IDX_B];
    //strcpy (ret.name,"air flow");
    ret.min = 0;
    ret.max = 655.35 ;
    ret.ret = ((A*256)+B) / 100;
    return ret.ret;
}

double control_module_voltage(can_ret& ret, unsigned char * frame)
{
    unsigned char A = frame[IDX_A];
    unsigned char B = frame[IDX_B];
    //strcpy (ret.name,"module voltage");
    ret.min = 0;
    ret.max = 65.535 ;
    ret.ret = ((A*256)+B)/1000;
    return ret.ret;
}

double get_can_current_value()
{
    std::vector<int> ret;
    pipe_arr_read(ret, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);

    char trans[ sizeof( double ) ] = { 0 };

    int val = ret[ ReadCan::REVERSE_TEST_VALUE ];


    memcpy( trans , &val , sizeof( int ) );

    double rt;
    memcpy( &rt , trans , sizeof( double ) );
    return rt;
}


void set_can_current_data(int func_id, int frame_id, int a, int b)
{
    int val = int(  (0x00 << 24) | 
                    (0x00 << 16) |
                    (unsigned char)(a) << 8 |
                    (unsigned char)(b));

    int data[6] = {ReadCan::REVERSE_TEST_FUNCID, func_id
                    , ReadCan::REVERSE_TEST_CANID, frame_id
                    , ReadCan::REVERSE_TEST_CAN_FRAMES_A, val};

    arr_set_can(data, 6);
}

std::vector<int> 
arr_read_can()
{
    std::vector<int> ret;
    pipe_arr_read(ret, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    return ret;
}

std::vector<int> 
arr_read_gui()
{
    std::vector<int> ret;
    pipe_arr_read(ret, GUI_MEM_ARR_NAME, GUI_MEM_ARR_SIZE, GUI_SEM_ARR_NAME);
    return ret;
}

std::vector<int> 
arr_read_nrf()
{
    std::vector<int> ret;
    pipe_arr_read(ret, NRF_MEM_ARR_NAME, NRF_MEM_ARR_SIZE, NRF_SEM_ARR_NAME);
    return ret;
}

int arr_write_can(const std::vector<int>& array)
{
    return pipe_arr_write(CAN_MEM_ARR_NAME, array, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
}

int arr_write_gui(const std::vector<int>& array)
{
    return pipe_arr_write(GUI_MEM_ARR_NAME, array, GUI_MEM_ARR_SIZE, GUI_SEM_ARR_NAME);
}

int arr_write_nrf(const std::vector<int>& array)
{
    return pipe_arr_write(NRF_MEM_ARR_NAME, array, NRF_MEM_ARR_SIZE, NRF_SEM_ARR_NAME);
}


int 
pipe_str_write (const std::string& pipename
    , const std::string& data
    , int size
    , const char* semaphorename) 
{
    int shm, mode = 0;
    char *addr;
    int len = data.length();

    len = (len<=size)?len:size;
    mode = O_CREAT;

    sem_t *sem;

    if ( (sem = sem_open(semaphorename, O_CREAT, 0644, 1)) == SEM_FAILED ) {
        //perror("sem_open");
        return 1;
    }
    sem_wait(sem);

    if ( (shm = shm_open(pipename.c_str(), mode|O_RDWR, 0777)) == -1 ) {
        //perror("shm_open");
        return 1;
    }

    if ( ftruncate(shm, size+1) == -1 ) {
        //perror("ftruncate");
        return 1;
    }

    addr = (char*)mmap(0, size+1, PROT_WRITE|PROT_READ, MAP_SHARED, shm, 0);
    if ( addr == (char*)-1 ) {
        //perror("mmap");
        close(shm);
        return 1;
    }
    
    memcpy(addr, data.c_str(), len);
    addr[len] = '\0';
        
    munmap(addr, size);
    close(shm);

    int ret = 0;
    if ( sem_post(sem) < 0 )
        ret = -1;
        //perror("sem_post");

    if ( sem_close(sem) < 0 )
        ret = -1;
        //perror("sem_close");
    
    return ret;
}


int 
pipe_arr_write (const std::string& pipename
    , const std::vector<int>& data
    , int size
    , const char* semaforename) 
{
    sem_t *sem;

    if ( (sem = sem_open(semaforename, O_CREAT, 0644, 1)) == SEM_FAILED ) {
        //perror("sem_open");
        return 1;
    }
    sem_wait(sem);
    

    int shm, mode = 0;
    int *addr;
    int len = data.size();

    len = (len<=size)?len:size;
    mode = O_CREAT;

    if ( (shm = shm_open(pipename.c_str(), mode|O_RDWR, 0777)) == -1 ) {
        //perror("shm_open");
        sem_close(sem);
        return 1;
    }

    if ( ftruncate(shm, size+1) == -1 ) {
        //perror("ftruncate");
        sem_post(sem);
        return 1;
    }

    addr = (int*)mmap(0, size+1, PROT_WRITE|PROT_READ, MAP_SHARED, shm, 0);
    if ( addr == (int*)-1 ) {
        //perror("mmap");
        sem_post(sem);
        return 1;
    }
    
    memset (addr, -1, size);

    for(int i = 0; i < len; ++i)
        addr[i] = data[i];        
        
    munmap(addr, size);
    close(shm);
    
    if ( sem_post(sem) < 0 )
        //perror("sem_post");

    if ( sem_close(sem) < 0 )
        //perror("sem_close");
    
    return 0;
}

int
pipe_arr_read(std::vector<int>& ret
    , const std::string& pipename
    , int size
    , const char* semaphorename)
{
    int shm, len, mode = 0;
    int *addr;

    ret.reserve(size);
    ret.assign(size,-1);

    sem_t *sem;
    
    if ( (sem = sem_open(semaphorename, O_CREAT, 0644, 1)) == SEM_FAILED ) {
        //perror("sem_open");
        return 1;
    }
    sem_wait(sem);

    if ( (shm = shm_open(pipename.c_str(), O_RDWR, 0777)) == -1 ) {
        //perror("shm_open");
        sem_post(sem);
        return -1;
    }

    addr = (int *)mmap(0, size+1, PROT_WRITE|PROT_READ, MAP_SHARED, shm, 0);
    if ( addr == (int*)-1 ) {
        //perror("mmap");
        sem_post(sem);
        return -2;
    }

    for (int i = 0; i < size; ++i)
        ret[i] = addr[i];

    munmap(addr, size);
    close(shm);
    
    if ( sem_post(sem) < 0 )
        //perror("sem_post");

    if ( sem_close(sem) < 0 )
        //perror("sem_close");

    return 0;
}


int arr_set_nrf(const int* pos_items, int size)
{
    return pipe_arr_set_n_items(pos_items, size, NRF_MEM_ARR_NAME, NRF_MEM_ARR_SIZE, NRF_SEM_ARR_NAME);
}

int arr_set_can(const std::vector<int>& items)
{
    int size = items.size();
    const int *data = items.data();
    return pipe_arr_set_n_items(data, size, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
}


int arr_set_can(const int* pos_items, int size)
{
    return pipe_arr_set_n_items(pos_items, size, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
}


int
pipe_arr_set_n_items(const int *items
    , int items_size
    , const std::string& pipename
    , int size
    , const char* semaphorename)
{
    int shm, len, mode = 0;
    int *addr;
    sem_t *sem;
    
    if ( (sem = sem_open(semaphorename, O_CREAT, 0644, 1)) == SEM_FAILED ) {
        // perror("sem_open");
        return 1;
    }
    sem_wait(sem);

    if ( (shm = shm_open(pipename.c_str(), mode|O_RDWR, 0777)) == -1 ) {
        // perror("shm_open");
        sem_post(sem);
        return -1;
    }

    addr = (int *)mmap(0, size+1, PROT_WRITE|PROT_READ, MAP_SHARED, shm, 0);
    if ( addr == (int*)-1 ) {
        // perror("mmap");
        sem_post(sem);
        return -2;
    }

    for (int i = 0; i < items_size; i+=2)
    {
        int j = items[i];
        addr[j] = items[i+1];
    }

    munmap(addr, size);
    close(shm);
    
    if ( sem_post(sem) < 0 )
        //perror("sem_post");

    if ( sem_close(sem) < 0 )
        //perror("sem_close");

    return 0;
}

int
pipe_str_read(std::string& ret
    , const std::string& pipename
    , int size
    , const char* semaphorename) 
{
    int shm, len, cmd, mode = 0;
    char *addr;

    sem_t *sem;
    
    if ( (sem = sem_open(semaphorename, O_CREAT, 0644, 1)) == SEM_FAILED ) {
        //perror("sem_open");
        return 1;
    }
    sem_wait(sem);


    if ( (shm = shm_open(pipename.c_str(), mode|O_RDWR, 0777)) == -1 ) {
        //perror("shm_open");
        return -1;
    }

    addr = (char *)mmap(0, size+1, PROT_WRITE|PROT_READ, MAP_SHARED, shm, 0);
    if ( addr == (char*)-1 ) {
        //perror("mmap");
        return -1;
    }

    ret.assign(addr);
    munmap(addr, size);
    close(shm);

    int retval = 0;
    if ( sem_post(sem) < 0 )
        retval = -1;
        //perror("sem_post");

    if ( sem_close(sem) < 0 )
        retval = -1;
        //perror("sem_close");
    
    return retval;
}


int
dhoLeftOff()
{
    std::vector<int> data_can;
    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    int res = data_can[ReadCan::BLOCKM_CALL_A];
    res &=~ (1 << BlockM_RS_LIGTHS::RS_LIGHTS_LEFT);
    int data[2] = {
                          ReadCan::BLOCKM_CALL_A, res
                   };
    return arr_set_can(data, 2);
}

int
dhoLeftOn()
{
    std::vector<int> data_can;
    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    int res = data_can[ReadCan::BLOCKM_CALL_A];
    res |= (1 << BlockM_RS_LIGTHS::RS_LIGHTS_LEFT);
    int data[2] = {
                          ReadCan::BLOCKM_CALL_A, res
                   };
    return arr_set_can(data, 2);
}

int
dhoRightOff()
{
    std::vector<int> data_can;
    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    int res = data_can[ReadCan::BLOCKM_CALL_A];
    res &=~ (1 << BlockM_RS_LIGTHS::RS_LIGHTS_RIGHT);
    int data[2] = {
                          ReadCan::BLOCKM_CALL_A, res
                   };
    return arr_set_can(data, 2);
}

int
dhoRightOn()
{
    std::vector<int> data_can;
    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    int res = data_can[ReadCan::BLOCKM_CALL_A];
    res |= (1 << BlockM_RS_LIGTHS::RS_LIGHTS_RIGHT);
    int data[2] = {
                          ReadCan::BLOCKM_CALL_A, res
                   };
    return arr_set_can(data, 2);
}
int
dhoOff()
{
    std::vector<int> data_can;
    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    int res = data_can[ReadCan::BLOCKM_CALL_A];
    res |= (1 << BlockM_RS_LIGTHS::RS_LIGHTS_ONOFF);
    res &=~ (1 << BlockM_RS_LIGTHS::RS_LIGHTS_LEFT);
    res &=~ (1 << BlockM_RS_LIGTHS::RS_LIGHTS_RIGHT);
    int data[2] = {
                          ReadCan::BLOCKM_CALL_A, res
                   };
    return arr_set_can(data, 2);
}


int
dhoOn()
{
    std::vector<int> data_can;
    pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    int res = data_can[ReadCan::BLOCKM_CALL_A];
    res &=~ (1 << BlockM_RS_LIGTHS::RS_LIGHTS_ONOFF);
    res &=~ (1 << BlockM_RS_LIGTHS::RS_LIGHTS_LEFT);
    res &=~ (1 << BlockM_RS_LIGTHS::RS_LIGHTS_RIGHT);
    int data[2] = {
                          ReadCan::BLOCKM_CALL_A, res
                   };
    return arr_set_can(data, 2);
}