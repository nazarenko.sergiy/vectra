#include "vectra.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <cstdlib>

#include <errno.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <stdint.h>
#include <sys/timerfd.h>
#include <pthread.h>
#include <poll.h>

#include "vectra_timer.h"

#define CAN_HELP 1
#define CAN_DRAIN  2
#define CAN_DHO_ON 3
#define CAN_DHO_OFF 4
#define CAN_DHO_LON 5
#define CAN_DHO_LOFF 6
#define CAN_DHO_ROFF 7
#define CAN_DHO_RON 8
#define CSR_HOME "\33[H"
#define CLR_SCREEN "\33[2J"

void usage(const char * s) {
    printf("Usage: %s \n", s);
    printf("SUN: %d\n", ReadCan::SUN);
    printf("COOLANT: %d\n", ReadCan::COOLANT);
    printf("RPM: %d\n", ReadCan::RPM);
    printf("SPEED: %d\n", ReadCan::SPEED);
    printf("TMPR_BATTERY: %d\n", ReadCan::TMPR_BATTERY);
    printf("FLUID_METER: %d\n", ReadCan::FLUID_METER);
    printf("FUEL: %d\n", ReadCan::FUEL);
    printf("BLOCKM_A: %d\n", ReadCan::BLOCKM_A);
    printf("BLOCKM_B: %d\n", ReadCan::BLOCKM_B);
    printf("BLOCKM_CALL_A: %d\n", ReadCan::BLOCKM_CALL_A);
    printf("BLOCKM_CALL_B: %d\n", ReadCan::BLOCKM_CALL_B);
    printf("LIGHT_SIGNALS: %d\n", ReadCan::LIGHT_SIGNALS);
    printf("REVERSE_TEST_VALUE: %d\n", ReadCan::REVERSE_TEST_VALUE);
    printf("REVERSE_TEST_CANID: %d\n", ReadCan::REVERSE_TEST_CANID);
    printf("REVERSE_TEST_FUNCID: %d\n", ReadCan::REVERSE_TEST_FUNCID);
    printf("REVERSE_TEST_CAN_FRAMES_A: %d\n", ReadCan::REVERSE_TEST_CAN_FRAMES_A);
    printf("REVERSE_TEST_CAN_FRAMES_B: %d\n", ReadCan::REVERSE_TEST_CAN_FRAMES_B);
    printf("TC_INST: %d\n", ReadCan::TC_INST);
    printf("TC_TIME: %d\n", ReadCan::TC_TIME);
    printf("ODB_COOLANT: %d\n", ReadCan::ODB_COOLANT);
}

 
#define MAX_TIMER_COUNT 1000
 
void time_handler1(size_t timer_id, void * user_data)
{
    printf("Single shot timer expired.(%d)\n", timer_id);
}
 

static unsigned int speed_ms;

static unsigned int speed_ms_new;

void time_handler2(size_t timer_id, void * user_data)
{
    //printf("100 ms timer tick. (%d)\n", (speed_ms_new - speed_ms));
    //speed_ms = speed_ms_new;
    speed_ms_new++;
}
 
void time_handler3(size_t timer_id, void * user_data)
{
    printf("2000 ms timer expired. (%d)\n", timer_id);
}


int main (int argc, char ** argv) 
{
    std::vector<int> data_can;
    data_can.reserve(CAN_MEM_ARR_SIZE);
    data_can.assign(CAN_MEM_ARR_SIZE, 0);
    
    std::vector<int> data_nrf;
    data_nrf.reserve(NRF_MEM_ARR_SIZE);
    data_nrf.assign(NRF_MEM_ARR_SIZE, 0);

    int mode = 0;
    if ( argc > 1 ) 
    {
        if (strcmp(argv[1], "help") == 0) 
        {
            usage(argv[0]);

            size_t timer1, timer2, timer3;
 
            initialize();

            //  timer1 = start_timer(200, time_handler1, TIMER_SINGLE_SHOT, NULL);
            timer2 = start_timer(100, time_handler2, TIMER_PERIODIC, NULL);
            // timer3 = start_timer(200, time_handler3, TIMER_PERIODIC, NULL);
            speed_ms = 0;
            speed_ms_new = 0;
            for(int i = 0; i < 100; i++)
            {
                //speed_ms_new = i;
                printf("(%d)\n", (speed_ms_new));
                sleep(1);
            }

            //   stop_timer(timer1);
            stop_timer(timer2);
            //  stop_timer(timer3);

            finalize();

            return 1;
        } // IF HELP
        if (strcmp(argv[1], "init") == 0) 
        {
            if (strcmp(argv[2], "nrf") == 0) 
            {
                arr_write_nrf(data_nrf);
            }
            else
            {
                arr_write_can(data_can);
            }

            return 0;
        }
    }

    


            // std::vector<int> data_nrf = arr_read_nrf();
            // std::vector<int> data_gui = arr_read_gui();

            // int nrf0 = data_nrf[0];
            // if (nrf0 != -1)
            //     printf("CAN READ: Data[0] from nrf == %d\n", nrf0);

            // int gui0 = data_gui[0];
            // if (gui0 != -1)
            //     printf("CAN READ: Data[0] from gui == %d\n", gui0);
            
            //pipe_arr_read(&data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    /*
            printf("CAN READ TEST: == %d %d%d%d%d %d\n"
                , ((data_can[BLOCKM_B] >> 5)&1)
                , ((data_can[BLOCKM_B] >> 3)&1)
                , ((data_can[BLOCKM_B] >> 2)&1)
                , ((data_can[BLOCKM_B] >> 1)&1)
                , ((data_can[BLOCKM_B] >> 0)&1)
                , data_can[BLOCKM_CALL_A]);
            int res = 0x00; */
            // int i = rand() % 1;
            // int j = rand() % 1;

            /*res |= (1<<0);

            // res |= (0 << 1);*/
        if (argc >= 3)
        {
            if (strcmp(argv[1], "can") == 0) 
            {
                int pos = atoi(argv[2]);
                int res = atoi(argv[3]);
                int data[2] = {pos, res};
                arr_set_can(data, 2);
            }
            if (strcmp(argv[1], "nrf") == 0) 
            {
                unsigned long a1 = strtoul(argv[2], NULL, 16);
                unsigned long a2 = strtoul(argv[3], NULL, 16);
                unsigned long a3 = strtoul(argv[4], NULL, 16);
                unsigned long a4 = strtoul(argv[5], NULL, 16);

                unsigned long b1 = strtoul(argv[6], NULL, 16);
                unsigned long b2 = strtoul(argv[7], NULL, 16);
                unsigned long b3 = strtoul(argv[8], NULL, 16);
                unsigned long b4 = strtoul(argv[9], NULL, 16);

                int a = int((unsigned char)(a4) << 24 |
                                    (unsigned char)(a3) << 16 |
                                    (unsigned char)(a2) << 8 |
                                    (unsigned char)(a1));
                int b = int((unsigned char)(b4) << 24 |
                            (unsigned char)(b3) << 16 |
                            (unsigned char)(b2) << 8 |
                            (unsigned char)(b1));
                int data[4] = { 0, a, 1, b  }; 
             
                arr_set_nrf(data, 4);
                printf("NRF SET: %lu %lu %lu %lu %lu %lu %lu %lu\n", a1, a2, a3, a4, b1, b2, b3, b4);
            }
            //printf("CAN READ: ret %d\n", r);
        }

 
    return 0;
}

