import unittest
import vectra_gui as vg
from DTC_codes  import DTC

# https://github.com/brendan-w/python-OBD/blob/master/obd/obd.py
def bytes_to_hex(bs):
    h = ""
    for b in bs:
        bh = hex(b)[2:]
        h += ("0" * (2 - len(bh))) + bh
    return h

def to_bytes(n, length, endianess='little'):
            h = '%x' % n
            s = ('0'*(len(h) %2) + h ).zfill(length*2).decode('hex')
            return s if endianess == 'big' else s[::-1]



def parse_dtc(_bytes):
    """ converts 2 bytes into a DTC code """

    # check validity (also ignores padding that the ELM returns)
    if (len(_bytes) != 2) or (_bytes == (0, 0)):
        return None

    # BYTES: (16,      35      )
    # HEX:    4   1    2   3
    # BIN:    01000001 00100011
    #         [][][  in hex   ]
    #         | / /
    # DTC:    C0123

    dtc = ['P', 'B', 'C', 'U'][_bytes[0] >> 6]  # the last 2 bits of the first byte
    dtc += str((_bytes[0] >> 4) & 0b0011)  # the next pair of 2 bits. Mask off the bits we read above
    dtc += bytes_to_hex(_bytes)[1:4]

    # pull a description if we have one
    return (dtc, DTC.get(dtc, ""))


def dtc_errors():
    data = vg.array_read_nrf()
    n = data[0]
    m = to_bytes(n & 0xffffffff,4)
    h = data[1]
    d = to_bytes(h & 0xffffffff,4)
    ret = []
    ret += list(bytearray(m))
    ret += list(bytearray(d))
    return ret


class TestMod(unittest.TestCase):
    
    def test_dtc_errors(self):
        frame = dtc_errors()
        print( frame )
        print( parse_dtc(frame[1:2]) )
    
    def test_parse_dtc(self):
        print(parse_dtc([67,1]))
        print(parse_dtc([71,1]))


if __name__ == '__main__':
    unittest.main()

