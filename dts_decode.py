import sys
sys.path.append('C:/Users/SNA/Documents/Vectra/vectra')
from DTC_codes  import DTC

# https://github.com/brendan-w/python-OBD/blob/master/obd/obd.py
def bytes_to_hex(bs):
    h = ""
    for b in bs:
        bh = hex(b)[2:]
        h += ("0" * (2 - len(bh))) + bh
    return h


def parse_dtc(_bytes):
    """ converts 2 bytes into a DTC code """

    # check validity (also ignores padding that the ELM returns)
    if (len(_bytes) != 2) or (_bytes == (0, 0)):
        return None

    # BYTES: (16,      35      )
    # HEX:    4   1    2   3
    # BIN:    01000001 00100011
    #         [][][  in hex   ]
    #         | / /
    # DTC:    C0123

    dtc = ['P', 'B', 'C', 'U'][_bytes[0] >> 6]  # the last 2 bits of the first byte
    dtc += str((_bytes[0] >> 4) & 0b0011)  # the next pair of 2 bits. Mask off the bits we read above
    dtc += bytes_to_hex(_bytes)[1:4]

    # pull a description if we have one
    return (dtc, DTC.get(dtc, ""))

print(parse_dtc([67,1]))
print(parse_dtc([71,1]))


"""

03:
7E8	4	43	1	1	37	AA	AA	AA
https://www.engineobd2codes.com/c0301_opel-vectra.html
http://c0301.enginecodes.net/

07:
7E8	4	47	1	1	37	AA	AA	AA	
https://www.engineobd2codes.com/c0701_opel-vectra.html
http://c0701.enginecodes.net/

01:
7E8	3	7F	1	12	AA	AA	AA	AA

09:
7E8	6	49	0	54	0	0	0	AA	


04:
Q
7E8	1	44	AA	AA	AA	AA	AA	AA	




43	1	1	37

0100 0011 0000 0001 0000 0001 

P02 11 
"""