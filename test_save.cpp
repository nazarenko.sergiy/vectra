#include<iostream>
#include<fstream>
using namespace std;
struct TC_data {
   int roll_no;
   int k[16];
};
int main() {
   ofstream wf("test_tc_data.dat", ios::out | ios::binary);
   if(!wf) {
      cout << "Cannot open file!" << endl;
      return 1;
   }
   TC_data wstu[3];
   wstu[0].roll_no = 1;
   for (int i = 0; i < 16; ++i)
      wstu[0].k[i] = i;
   wstu[1].roll_no = 2;
   for (int i = 0; i < 16; ++i)
      wstu[1].k[i] = i;
   wstu[2].roll_no = 3;
   for (int i = 0; i < 16; ++i)
      wstu[2].k[i] = i;
   
   for(int i = 0; i < 3; i++)
      wf.write((char *) &wstu[i], sizeof(TC_data));
   wf.close();
   if(!wf.good()) {
      cout << "Error occurred at writing time!" << endl;
      return 1;
   }
   ifstream rf("student.dat", ios::out | ios::binary);
   if(!rf) {
      cout << "Cannot open file!" << endl;
      return 1;
   }
   TC_data rstu[3];
   for(int i = 0; i < 3; i++)
      rf.read((char *) &rstu[i], sizeof(TC_data));
   rf.close();
   if(!rf.good()) {
      cout << "Error occurred at reading time!" << endl;
      return 1;
   }
   cout<<"TC_data's Details:"<<endl;
   for(int i=0; i < 3; i++) {
      cout << "Roll No: " << wstu[i].roll_no << endl;
      cout << "K: " << wstu[i].k[15] << endl;
      cout << endl;
   }
   return 0;
}