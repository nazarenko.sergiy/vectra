#ifndef NMEA_PARSER_F124_H
#define NMEA_PARSER_F124_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>


typedef enum
{
	GPRMC_DATA_VALID = 0x0,
	GPRMC_DATA_INVALID
} GPRMC_status;


typedef struct GPRMC_time
{
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	uint8_t year;
	uint8_t months;
	uint8_t day;
} GPRMC_time;


#define RAW_MESSAGE_SZ 256

typedef struct GPRMC_Message
{
	char timestamp[16];
	GPRMC_time Time;
	GPRMC_status PosStatus;
	uint8_t FixStatus;
	float Latitude;
	float Longitude;
        
	char raw[RAW_MESSAGE_SZ];
} GPRMC_Message;


#define USER_UART2_BUF_SZ 256

typedef struct Message
{
	uint8_t id;
	char body[USER_UART2_BUF_SZ];
} Message;


typedef enum
{
	GPRMC = 38301,
	GPVTG = 40356,
	GPGGA = 34967,
	GPGSA = 34979,
	GPGSV = 40355,
	GPGLL = 37788,
	GLGSV = 40351,
	GNGSA = 34977,
	GNRMC = 38299,
	GNGLL = 37786
} NMEA_t;

uint8_t GxRMC_parser(GPRMC_Message * message, char * str);

#endif