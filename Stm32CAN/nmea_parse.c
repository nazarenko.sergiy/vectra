
#include <string.h> 
#include "nmea_parse.h"


static Message current_message;

char *strtok_fr (char *s, char delim, char **save_ptr)
{
    char *tail;
    char c;

    if (s == NULL) {
        s = *save_ptr;
    }
    tail = s;
    if ((c = *tail) == '\0') {
        s = NULL;
    }
    else {
        do {
            if (c == delim) {
                *tail++ = '\0';
                break;
           }
        }while ((c = *++tail) != '\0');
    }
    *save_ptr = tail;
    return s;
}


char *strtok_f (char *s, char delim)
{
    static char *save_ptr;
    return strtok_fr (s, delim, &save_ptr);
}


#define NMEA_END_CHAR_1 '\n'
#define NMEA_MAX_LENGTH 70


int nmea0183_checksum(char *nmea_data)
{
    int crc = 0;
    int i;
    int len = strlen(nmea_data) - 3;
    // the first $ sign and the last two bytes of original CRC + the * sign
    for (i = 1; i < len; i ++) {
        crc ^= nmea_data[i];
    }

    return crc;
}

uint8_t nmea_get_checksum(const char *sentence)
{
    const char *n = sentence + 1; // Plus one, skip '$'
    uint8_t chk = 0;

    /* While current char isn't '*' or sentence ending (newline) */
    while ('*' != *n && NMEA_END_CHAR_1 != *n) {
        if ('\0' == *n || n - sentence > NMEA_MAX_LENGTH) {
            /* Sentence too long or short */
            return 0;
        }
        chk ^= (uint8_t) *n;
        n++;
    }

    return chk;
}

#define CHECK_MSG(a) if (a == NULL) stat = GPRMC_DATA_INVALID;

uint8_t GxRMC_parser(GPRMC_Message * local_message, char * str)
{
	current_message.body[0] = '\0';
	strcpy(current_message.body, str);
	strcpy(local_message->raw, str);

	GPRMC_status stat = GPRMC_DATA_VALID;

	uint8_t checksum = nmea_get_checksum(str);
	uint8_t crc = nmea0183_checksum(str);

	uint8_t a, b, c;
	char buffer[40];
	memset(buffer, 0x00, sizeof(char) * 40);
	const char *tfmt = "%02d%02d%02d";
	char* Message_ID = strtok_f(str,','); CHECK_MSG(Message_ID);
	char* Time = strtok_f(NULL,','); CHECK_MSG(Time);
	strcpy(buffer, Time);
	sscanf(buffer, tfmt, &a, &b, &c);
	c++;
	c = c > 59 ? 0 : c;

	local_message->Time.hours = a;
	local_message->Time.minutes = b;
	local_message->Time.seconds = c;
	char* Data_Valid = strtok_f(NULL,','); CHECK_MSG(Data_Valid);
#ifndef TEST_GPS
	local_message->PosStatus = GPRMC_DATA_INVALID;
	if (checksum == crc)
	{
		local_message->PosStatus = Data_Valid[0] == 'A' ? GPRMC_DATA_VALID : GPRMC_DATA_INVALID;
	}
#else
	message->PosStatus =  Data_Valid[0] == 'A' ? GPRMC_DATA_INVALID : GPRMC_DATA_VALID;
#endif
	char* Raw_Latitude = strtok_f(NULL,','); CHECK_MSG(Raw_Latitude);
	local_message->Latitude = atof(Raw_Latitude);
	char* N_S = strtok_f(NULL,','); CHECK_MSG(N_S);

	char* Raw_Longitude = strtok_f(NULL,','); CHECK_MSG(Raw_Longitude);
	local_message->Longitude = atof(Raw_Longitude);
	char* E_W = strtok_f(NULL,','); CHECK_MSG(E_W);

	char* Speed = strtok_f(NULL,','); CHECK_MSG(Speed);
	char* COG = strtok_f(NULL,','); CHECK_MSG(COG);
	char* Date = strtok_f(NULL,','); CHECK_MSG(Date);
	memset(buffer, 0x00, sizeof(char) * 40);
	strcpy(buffer, Date);

	uint8_t x, y, z;
	sscanf(buffer, tfmt, &x, &y, &z);
	local_message->Time.day = x;
	local_message->Time.months = y;
	local_message->Time.year = z;

	snprintf(local_message->timestamp, sizeof(local_message->timestamp), "20%02d%02d%02dT%02d%02d%02d"
			, local_message->Time.year, local_message->Time.months, local_message->Time.day
			, local_message->Time.hours, local_message->Time.minutes, local_message->Time.seconds);

	char* Magnetic_Variation = strtok_f(NULL,',');  CHECK_MSG(Magnetic_Variation);
	char* M_E_W = strtok_f(NULL,',');  CHECK_MSG(M_E_W);
	char* Positioning_Mode = strtok_f(NULL,',');

        if (local_message->PosStatus == GPRMC_DATA_VALID && stat == GPRMC_DATA_VALID)
        {
          return 1;
        }
        else
        {
          return 0;
        }
}
