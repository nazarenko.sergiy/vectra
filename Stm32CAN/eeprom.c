
/* ---------------- Inclusions ----------------- */
#include <stdint.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/f1/nvic.h>

#include "eeprom.h"

#define PAGE_SIZE		0x100
#define PAGE_MASK		(PAGE_SIZE-1)

/* EEPROM address */
#define EEPROM_ADDRESS				0

/* Address byte to send */
#define ADDRESS_BYTE				((uint8_t)(0x50 | EEPROM_ADDRESS))


/* Function to write a byte at a specific address */
bool eeprom_write_byte(uint16_t address, uint8_t data)
{
	bool success = false;

	/* send START and wait for completion */
	i2c_send_start(I2C2);
	while ((I2C_SR1(I2C2) & I2C_SR1_SB) == 0);

	/* send device address, r/w request and wait for completion */
	i2c_send_7bit_address(I2C2, ADDRESS_BYTE, I2C_WRITE);
	while ((I2C_SR1(I2C2) & (I2C_SR1_ADDR|I2C_SR1_AF)) == 0);
	bool ack = (I2C_SR1(I2C2) & I2C_SR1_ADDR) != 0; /* has the slave responded?  */

	/* check SR2 and go on if OK */
	if (ack && (I2C_SR2(I2C2) & I2C_SR2_MSL)		/* master mode */
	        && (I2C_SR2(I2C2) & I2C_SR2_BUSY)) {	/* communication ongoing  */

		/* send memory address MSB */
		i2c_send_data(I2C2, ((uint8_t)(address >> 8)));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* send memory address LSB */
		i2c_send_data(I2C2, ((uint8_t)address));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* send data byte */
		i2c_send_data(I2C2, data);
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);
		success = true;
	}
	/* send stop */
	i2c_send_stop(I2C2);
	while ((I2C_SR2(I2C2) & (I2C_SR2_BUSY | I2C_SR2_MSL)) != 0);

	return success;
}



/* Function to write a byte at a specific address */
bool eeprom_read_byte(uint16_t address, uint8_t *byte_ptr)
{
	bool success = false;

	/* send START and wait for completion */
	i2c_send_start(I2C2);
	while ((I2C_SR1(I2C2) & I2C_SR1_SB) == 0);

	/* send device address, write request and wait for completion */
	i2c_send_7bit_address(I2C2, ADDRESS_BYTE, I2C_WRITE);
	while ((I2C_SR1(I2C2) & (I2C_SR1_ADDR|I2C_SR1_AF)) == 0);
	bool ack = (I2C_SR1(I2C2) & I2C_SR1_ADDR) != 0; /* has the slave responded?  */

	/* check SR2 and go on if OK */
	if (ack && (I2C_SR2(I2C2) & I2C_SR2_MSL)	/* master mode */
	        && (I2C_SR2(I2C2) & I2C_SR2_BUSY)) {	/* communication ongoing  */

		/* send memory address MSB */
		i2c_send_data(I2C2, ((uint8_t)(address >> 8)));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* send memory address LSB */
		i2c_send_data(I2C2, ((uint8_t)address));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* send START and wait for completion */
		i2c_send_start(I2C2);
		while ((I2C_SR1(I2C2) & I2C_SR1_SB) == 0);

		/* send device address, read request and wait for completion */
		i2c_send_7bit_address(I2C2, ADDRESS_BYTE, I2C_READ);
		while ((I2C_SR1(I2C2) & I2C_SR1_ADDR) == 0);

		/* if communication ongoing  */
		if (I2C_SR2(I2C2) & I2C_SR2_BUSY) {
			/* read received byte */
			while ((I2C_SR1(I2C2) & I2C_SR1_RxNE) == 0);
			*byte_ptr = i2c_get_data(I2C2);
			success = true;
		}
	}
	/* send stop */
	i2c_send_stop(I2C2);
	while ((I2C_SR2(I2C2) & (I2C_SR2_BUSY | I2C_SR2_MSL)) != 0);

	return success;
}


/* Function to write a page starting from a specific address */
bool eeprom_write_page(uint16_t address, uint8_t *data_ptr, uint16_t data_length)
{
	bool success = false;

	/* make sure we don't cross the page boundary */
	uint16_t start_of_next_page = (address & ~PAGE_MASK) + PAGE_SIZE;
	if( address + data_length > start_of_next_page )
		data_length = start_of_next_page - address;

	/* send START and wait for completion */
	i2c_send_start(I2C2);
	while ((I2C_SR1(I2C2) & I2C_SR1_SB) == 0);

	/* send device address, r/w request and wait for completion */
	i2c_send_7bit_address(I2C2, ADDRESS_BYTE, I2C_WRITE);
	while ((I2C_SR1(I2C2) & (I2C_SR1_ADDR|I2C_SR1_AF)) == 0);
	bool ack = (I2C_SR1(I2C2) & I2C_SR1_ADDR) != 0; /* has the slave responded?  */

	/* check SR2 and go on if OK */
	if (ack && (I2C_SR2(I2C2) & I2C_SR2_MSL)	/* master mode */
	        && (I2C_SR2(I2C2) & I2C_SR2_BUSY)) {	/* communication ongoing  */

		/* send memory address MSB */
		i2c_send_data(I2C2, ((uint8_t)(address >> 8)));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* send memory address LSB */
		i2c_send_data(I2C2, ((uint8_t)address));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* write all bytes */
		while (data_length > 0) {
			/* send next data byte */
			i2c_send_data(I2C2, *data_ptr);
			/* increment data buffer pointer and
			 * decrement data buffer length */
			data_ptr++;
			data_length--;
			while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);
		}
		success = true;
	}
	/* send stop */
	i2c_send_stop(I2C2);
	while ((I2C_SR2(I2C2) & (I2C_SR2_BUSY | I2C_SR2_MSL)) != 0);

	return success;
}

/* Function to read a page starting from a specific address */
bool eeprom_read_page(uint16_t address, uint8_t *byte_ptr, uint16_t data_length)
{
	bool success = false;

	/* make sure we don't cross the page boundary */
	uint16_t start_of_next_page = (address & ~PAGE_MASK) + PAGE_SIZE;
	if( address + data_length > start_of_next_page )
		data_length = start_of_next_page - address;

	/* send START and wait for completion */
	i2c_send_start(I2C2);
	while ((I2C_SR1(I2C2) & I2C_SR1_SB) == 0);

	/* send device address, write request and wait for completion */
	i2c_send_7bit_address(I2C2, ADDRESS_BYTE, I2C_WRITE);
	while ((I2C_SR1(I2C2) & (I2C_SR1_ADDR|I2C_SR1_AF)) == 0);
	bool ack = (I2C_SR1(I2C2) & I2C_SR1_ADDR) != 0; /* has the slave responded?  */
success = ack;
	/* check SR2 and go on if OK */
	if (ack && (I2C_SR2(I2C2) & I2C_SR2_MSL)	/* master mode */
	        && (I2C_SR2(I2C2) & I2C_SR2_BUSY)) {	/* communication ongoing  */

		/* send memory address MSB */
		i2c_send_data(I2C2, ((uint8_t)(address >> 8)));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* send memory address LSB */
		i2c_send_data(I2C2, ((uint8_t)address));
		while ((I2C_SR1(I2C2) & I2C_SR1_TxE) == 0);

		/* send START and wait for completion */
		i2c_send_start(I2C2);
		while ((I2C_SR1(I2C2) & I2C_SR1_SB) == 0);

		/* send device address, read request and wait for completion */
		i2c_send_7bit_address(I2C2, ADDRESS_BYTE, I2C_READ);
		while ((I2C_SR1(I2C2) & I2C_SR1_ADDR) == 0);

		/* if communication ongoing  */
		if (I2C_SR2(I2C2) & I2C_SR2_BUSY) {
			/* enable ACK */
			i2c_enable_ack(I2C2);
			/* read all bytes */
			while (data_length > 0) {
				/* read received byte */
				while ((I2C_SR1(I2C2) & I2C_SR1_RxNE) == 0);
				*byte_ptr = i2c_get_data(I2C2);
				/* increment data buffer pointer and
				 * decrement data buffer length */
				byte_ptr++;
				data_length--;
				/* if last byte is remaining */
				if (data_length == 1) {
					/* disable ACK */
					i2c_disable_ack(I2C2);
				}
			}
			success = true;
		}
	}
	/* send stop */
	i2c_send_stop(I2C2);
	while ((I2C_SR2(I2C2) & (I2C_SR2_BUSY | I2C_SR2_MSL)) != 0);

	return success;
}
