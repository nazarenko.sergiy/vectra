/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------
// https://github.com/libopencm3/libopencm3-examples/blob/master/examples/stm32/f4/stm32f429i-discovery/usart_console/usart_console.c
File    : main.c
Purpose : Generic application start
.. https://wiki.segger.com/Import_projects_from_STM32CubeMX_to_Embedded_Studio
*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
//#include "stm32f1xx.h"
//#include "stm32f10x.h"
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/f1/bkp.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "pn532_routine.h"
/*********************************************************************
*
*       main()
*
*  Function description
*   Application entry point.
*/
/*
PA1 -> MOD2

PB0 -> MOD1
PB1 -> MOD0
*/



void vApplicationMallocFailedHook(void);
void vApplicationMallocFailedHook(void) {
  taskDISABLE_INTERRUPTS();
  for( ;; );
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName) {
  ( void ) pcTaskName;
  ( void ) xTask;
  taskDISABLE_INTERRUPTS();
  for( ;; );
}


#undef  BLINK_EXAMPLE

#ifdef BLINK_EXAMPLE
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

static void toggle_task(void *args) 
{

  while (1) 
  {
        gpio_toggle(GPIOC, GPIO13);
        vTaskDelay(200);
  }
}


static TaskHandle_t h_Toggle_task;

int main(void) 
{
    rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
    // Enable clock GPIOC
    rcc_periph_clock_enable(RCC_GPIOC);
    //rcc_periph_clock_enable(RCC_GPIOA);

    // Configure GPIO13
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    //gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

    xTaskCreate(toggle_task, "Toggle", configMINIMAL_STACK_SIZE*3, (void*)NULL, configMAX_PRIORITIES-2, &h_Toggle_task);

    vTaskStartScheduler();

    while (1) 
    {

    }
}
#else

static TaskHandle_t  PN532_receive_task_handle;
static TaskHandle_t  PN532_transmit_task_handle;


#define PN532_receive_PRIORITY  (tskIDLE_PRIORITY + 3)
#define PN532_send_PRIORITY  (tskIDLE_PRIORITY + 3)

#define FALLING 0
#define RISING 1

static void gpio_setup(void)
{
      ///* Enable GPIOA clock. */
      rcc_periph_clock_enable(RCC_GPIOA); 

      rcc_periph_clock_enable(RCC_GPIOC);
      gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
      gpio_set(GPIOC, GPIO13);
      
      rcc_periph_clock_enable(RCC_AFIO);
      rcc_periph_clock_enable(RCC_USART1);
}

static void systick_setup(void)
{
	/* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	/* SysTick interrupt every N clock pulses: set reload to N-1 */
	systick_set_reload(8999);

	systick_interrupt_enable();

	/* Start counting. */
	systick_counter_enable();
}


static void PN532_receive_task(void *args) 
{
  uint32_t ulNotifiedValue;
  uint8_t ret ;
  uint8_t data[8] = {2, 3, 4, 5, 6, 7, 8, 9};
  

  while (1) 
  {  
     vTaskDelay(5);
     if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE )
      {
         gpio_toggle(GPIOC, GPIO13);
      }
  }

  vTaskDelete(NULL);
}



static void PN532_transmit_task(void *args) 
{

  while (1) 
  {  
    
     vTaskDelay(10);

  }
  
  vTaskDelete(NULL);
}


static uint16_t exti1_direction = FALLING;

static uint16_t exti0_direction = FALLING;

static void exti1_setup(void)
{
	/* Enable GPIOA clock. */
	// Enabled in CAN setup

	/* Enable AFIO clock. */
	//rcc_periph_clock_enable(RCC_AFIO);

	/* Enable EXTI1 interrupt. */
	nvic_enable_irq(NVIC_EXTI1_IRQ);
        
	nvic_set_priority(NVIC_EXTI1_IRQ, 200);

	/* Set GPIO0 (in GPIO port A) to 'input open-drain'. */
	gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO1);
        uint16_t data = gpio_port_read(GPIOA);
        //data |= 1UL << 12;
        gpio_port_write(GPIOA, data);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI1, GPIOA);
	//exti_direction = FALLING;
	exti_set_trigger(EXTI1, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI1);
}

static void exti0_setup(void)
{
	nvic_enable_irq(NVIC_EXTI0_IRQ);
        
	nvic_set_priority(NVIC_EXTI0_IRQ, 200);

	/* Set GPIO0 (in GPIO port A) to 'input open-drain'. */
	gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO0);
        uint16_t data = gpio_port_read(GPIOA);
        gpio_port_write(GPIOA, data);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI0, GPIOA);
	//exti_direction = FALLING;
	exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI0);
}

void exti1_isr(void)
{
	exti_reset_request(EXTI1);

        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (exti1_direction == FALLING) 
        {
            exti1_direction = RISING;
            exti_set_trigger(EXTI1, EXTI_TRIGGER_RISING);
	} 
        else 
        {
            exti1_direction = FALLING;
            exti_set_trigger(EXTI1, EXTI_TRIGGER_FALLING);
	}
}


void exti0_isr(void)
{
	exti_reset_request(EXTI0);

        BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if (exti0_direction == FALLING) 
        {
           vTaskNotifyGiveIndexedFromISR( PN532_receive_task_handle, 0, &xHigherPriorityTaskWoken );
            portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
            exti0_direction = RISING;
            exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
	} 
        else 
        {
            exti0_direction = FALLING;
            exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
	}
}



static void PN532_init(void *args)
{

    if (pn532_init() != 0)
    {
    }
    else
    {
      xTaskCreate(PN532_receive_task, "PN532 rx", configMINIMAL_STACK_SIZE, (void*)NULL, PN532_receive_PRIORITY, &PN532_receive_task_handle);
      xTaskCreate(PN532_transmit_task, "PN532 tx", configMINIMAL_STACK_SIZE, (void*)NULL, PN532_send_PRIORITY, &PN532_transmit_task_handle);
    }

    vTaskDelete(NULL);
}



int main(void)
{
      rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
      gpio_setup();
      systick_setup();
      exti0_setup();
      exti1_setup();

      xTaskCreate(PN532_init, "PN532 init", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      
      vTaskStartScheduler();

      while(1) 
      {

      }
      return 0;
}



#endif // BLINK_EXAMPLE
/*********************************************************************
*
*       vApplicationGetIdleTaskMemory()
*
*/
#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize ) {
  /* If the buffers to be provided to the Idle task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xIdleTaskTCB;
  static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

  /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
  state will be stored. */
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

  /* Pass out the array that will be used as the Idle task's stack. */
  *ppxIdleTaskStackBuffer = uxIdleTaskStack;

  /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
#endif

/*********************************************************************
*
*       vApplicationGetTimerTaskMemory()
*
*/
/*-----------------------------------------------------------*/

#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize ) {
  /* If the buffers to be provided to the Timer task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xTimerTaskTCB;
  static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

  /* Pass out a pointer to the StaticTask_t structure in which the Timer
  task's state will be stored. */
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

  /* Pass out the array that will be used as the Timer task's stack. */
  *ppxTimerTaskStackBuffer = uxTimerTaskStack;

  /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
#endif

//void HardFault_Handler(void) {

// _Continue = 0u;
// //
// // When stuck here, change the variable value to != 0 in order to step out
// //
// while (_Continue == 0u);
//}

/*************************** End of file ****************************/


