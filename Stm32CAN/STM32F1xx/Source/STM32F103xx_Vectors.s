/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*            (c) 2014 - 2020 SEGGER Microcontroller GmbH             *
*                                                                    *
*       www.segger.com     Support: support@segger.com               *
*                                                                    *
**********************************************************************
*                                                                    *
* All rights reserved.                                               *
*                                                                    *
* Redistribution and use in source and binary forms, with or         *
* without modification, are permitted provided that the following    *
* condition is met:                                                  *
*                                                                    *
* - Redistributions of source code must retain the above copyright   *
*   notice, this condition and the following disclaimer.             *
*                                                                    *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND             *
* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,        *
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF           *
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
* DISCLAIMED. IN NO EVENT SHALL SEGGER Microcontroller BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  *
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;    *
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF      *
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          *
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  *
* USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH   *
* DAMAGE.                                                            *
*                                                                    *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------

File      : STM32F103xx_Vectors.s
Purpose   : Exception and interrupt vectors for STM32F103xx devices.

Additional information:
  Preprocessor Definitions
    __NO_EXTERNAL_INTERRUPTS
      If defined,
        the vector table will contain only the internal exceptions
        and interrupts.
    __VECTORS_IN_RAM
      If defined,
        an area of RAM, large enough to store the vector table, 
        will be reserved.

    __OPTIMIZATION_SMALL
      If defined,
        all weak definitions of interrupt handlers will share the 
        same implementation.
      If not defined,
        all weak definitions of interrupt handlers will be defined
        with their own implementation.
*/
        .syntax unified

/*********************************************************************
*
*       Macros
*
**********************************************************************
*/

//
// Directly place a vector (word) in the vector table
//
.macro VECTOR Name=
        .section .vectors, "ax"
        .code 16
        .word \Name
.endm

//
// Declare an exception handler with a weak definition
//
.macro EXC_HANDLER Name=
        //
        // Insert vector in vector table
        //
        .section .vectors, "ax"
        .word \Name
        //
        // Insert dummy handler in init section
        //
        .section .init.\Name, "ax"
        .thumb_func
        .weak \Name
\Name:
        1: b 1b   // Endless loop
.endm

//
// Declare an interrupt handler with a weak definition
//
.macro ISR_HANDLER Name=
        //
        // Insert vector in vector table
        //
        .section .vectors, "ax"
        .word \Name
        //
        // Insert dummy handler in init section
        //
#if defined(__OPTIMIZATION_SMALL)
        .section .init, "ax"
        .weak \Name
        .thumb_set \Name,Dummy_Handler
#else
        .section .init.\Name, "ax"
        .thumb_func
        .weak \Name
\Name:
        1: b 1b   // Endless loop
#endif
.endm

//
// Place a reserved vector in vector table
//
.macro ISR_RESERVED
        .section .vectors, "ax"
        .word 0
.endm

//
// Place a reserved vector in vector table
//
.macro ISR_RESERVED_DUMMY
        .section .vectors, "ax"
        .word Dummy_Handler
.endm

/*********************************************************************
*
*       Externals
*
**********************************************************************
*/
        .extern __stack_end__
        .extern Reset_Handler
        .extern HardFault_Handler

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*  Setup of the vector table and weak definition of interrupt handlers
*
*/
        .section .vectors, "ax"
        .code 16
        .balign 512
        .global _vectors
_vectors:
        //
        // Internal exceptions and interrupts
        //
        VECTOR __stack_end__
        VECTOR Reset_Handler
        EXC_HANDLER NMI_Handler
        VECTOR HardFault_Handler
        ISR_RESERVED
        ISR_RESERVED
        ISR_RESERVED
        ISR_RESERVED
        ISR_RESERVED
        ISR_RESERVED
        ISR_RESERVED
        EXC_HANDLER sv_call_handler
        ISR_RESERVED
        ISR_RESERVED
        EXC_HANDLER pend_sv_handler
        EXC_HANDLER sys_tick_handler
        //
        // External interrupts
        //
#ifndef __NO_EXTERNAL_INTERRUPTS
        ISR_HANDLER wwdg_isr
        ISR_HANDLER pvd_isr
        ISR_HANDLER tamper_isr
        ISR_HANDLER rtc_isr
        ISR_HANDLER flash_isr
        ISR_HANDLER rcc_isr
        ISR_HANDLER exti0_isr
        ISR_HANDLER exti1_isr
        ISR_HANDLER exti2_isr
        ISR_HANDLER exti3_isr
        ISR_HANDLER exti4_isr
        ISR_HANDLER dma1_channel1_isr
        ISR_HANDLER dma1_channel2_isr
        ISR_HANDLER dma1_channel3_isr
        ISR_HANDLER dma1_channel4_isr
        ISR_HANDLER dma1_channel5_isr
        ISR_HANDLER dma1_channel6_isr
        ISR_HANDLER dma1_channel7_isr
        ISR_HANDLER adc1_2_isr
        ISR_HANDLER usb_hp_can_tx_isr
        ISR_HANDLER usb_lp_can_rx0_isr
        ISR_HANDLER can_rx1_isr
        ISR_HANDLER can_sce_isr
        ISR_HANDLER exti9_5_isr
        ISR_HANDLER tim1_brk_isr
        ISR_HANDLER tim1_up_isr
        ISR_HANDLER tim1_trg_com_isr
        ISR_HANDLER tim1_cc_isr
        ISR_HANDLER tim2_isr
        ISR_HANDLER tim3_isr
        ISR_HANDLER tim4_isr
        ISR_HANDLER i2c1_ev_isr
        ISR_HANDLER i2c1_er_isr
        ISR_HANDLER i2c2_ev_isr
        ISR_HANDLER i2c2_er_isr
        ISR_HANDLER spi1_isr
        ISR_HANDLER spi2_isr
        ISR_HANDLER usart1_isr
        ISR_HANDLER usart2_isr
        ISR_HANDLER usart3_isr
        ISR_HANDLER exti15_10_isr
        ISR_HANDLER rtc_alarm_isr
        ISR_HANDLER usb_wakeup_isr
        ISR_HANDLER tim8_brk_isr
        ISR_HANDLER tim8_up_isr
        ISR_HANDLER tim8_trg_com_isr
        ISR_HANDLER tim8_cc_isr
        ISR_HANDLER adc3_isr
        ISR_HANDLER fsmc_isr
        ISR_HANDLER sdio_isr
        ISR_HANDLER tim5_isr
        ISR_HANDLER spi3_isr
        ISR_HANDLER uart4_isr
        ISR_HANDLER uart5_isr
        ISR_HANDLER tim6_isr
        ISR_HANDLER tim7_isr
        ISR_HANDLER dma2_channel1_isr
        ISR_HANDLER dma2_channel2_isr
        ISR_HANDLER dma2_channel3_isr
        ISR_HANDLER dma2_channel4_5_isr
#endif
        //
        .section .vectors, "ax"
_vectors_end:

#ifdef __VECTORS_IN_RAM
        //
        // Reserve space with the size of the vector table
        // in the designated RAM section.
        //
        .section .vectors_ram, "ax"
        .balign 512
        .global _vectors_ram

_vectors_ram:
        .space _vectors_end - _vectors, 0
#endif

/*********************************************************************
*
*  Dummy handler to be used for reserved interrupt vectors
*  and weak implementation of interrupts.
*
*/
        .section .init.Dummy_Handler, "ax"
        .thumb_func
        .weak Dummy_Handler
Dummy_Handler:
        1: b 1b   // Endless loop


/*************************** End of file ****************************/
