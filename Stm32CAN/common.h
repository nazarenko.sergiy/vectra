#ifndef USER_COMMON_GPIO_H
#define USER_COMMON_GPIO_H


#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "mcp2515.h"
#include "gitcommit.h"



#define BUILD_YEAR_CH0 (__DATE__[ 7])
#define BUILD_YEAR_CH1 (__DATE__[ 8])
#define BUILD_YEAR_CH2 (__DATE__[ 9])
#define BUILD_YEAR_CH3 (__DATE__[10])


#define BUILD_MONTH_IS_JAN (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_FEB (__DATE__[0] == 'F')
#define BUILD_MONTH_IS_MAR (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
#define BUILD_MONTH_IS_APR (__DATE__[0] == 'A' && __DATE__[1] == 'p')
#define BUILD_MONTH_IS_MAY (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
#define BUILD_MONTH_IS_JUN (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_JUL (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
#define BUILD_MONTH_IS_AUG (__DATE__[0] == 'A' && __DATE__[1] == 'u')
#define BUILD_MONTH_IS_SEP (__DATE__[0] == 'S')
#define BUILD_MONTH_IS_OCT (__DATE__[0] == 'O')
#define BUILD_MONTH_IS_NOV (__DATE__[0] == 'N')
#define BUILD_MONTH_IS_DEC (__DATE__[0] == 'D')

#define VERSION_MAJOR 1
#define VERSION_MINOR 1

#define BUILD_MONTH_CH0 \
    ((BUILD_MONTH_IS_OCT || BUILD_MONTH_IS_NOV || BUILD_MONTH_IS_DEC) ? '1' : '0')

#define BUILD_MONTH_CH1 \
    ( \
        (BUILD_MONTH_IS_JAN) ? '1' : \
        (BUILD_MONTH_IS_FEB) ? '2' : \
        (BUILD_MONTH_IS_MAR) ? '3' : \
        (BUILD_MONTH_IS_APR) ? '4' : \
        (BUILD_MONTH_IS_MAY) ? '5' : \
        (BUILD_MONTH_IS_JUN) ? '6' : \
        (BUILD_MONTH_IS_JUL) ? '7' : \
        (BUILD_MONTH_IS_AUG) ? '8' : \
        (BUILD_MONTH_IS_SEP) ? '9' : \
        (BUILD_MONTH_IS_OCT) ? '0' : \
        (BUILD_MONTH_IS_NOV) ? '1' : \
        (BUILD_MONTH_IS_DEC) ? '2' : \
        /* error default */    '?' \
    )

#define BUILD_DAY_CH0 ((__DATE__[4] >= '0') ? (__DATE__[4]) : '0')
#define BUILD_DAY_CH1 (__DATE__[ 5])

// Example of __TIME__ string: "21:06:19"
//                              01234567

#define BUILD_HOUR_CH0 (__TIME__[0])
#define BUILD_HOUR_CH1 (__TIME__[1])

#define BUILD_MIN_CH0 (__TIME__[3])
#define BUILD_MIN_CH1 (__TIME__[4])

#define BUILD_SEC_CH0 (__TIME__[6])
#define BUILD_SEC_CH1 (__TIME__[7])



#if VERSION_MAJOR > 100

#define VERSION_MAJOR_INIT \
    ((VERSION_MAJOR / 100) + '0'), \
    (((VERSION_MAJOR % 100) / 10) + '0'), \
    ((VERSION_MAJOR % 10) + '0')

#elif VERSION_MAJOR > 10

#define VERSION_MAJOR_INIT \
    ((VERSION_MAJOR / 10) + '0'), \
    ((VERSION_MAJOR % 10) + '0')

#else

#define VERSION_MAJOR_INIT \
    (VERSION_MAJOR + '0')

#endif

#if VERSION_MINOR >= 100

#define VERSION_MINOR_INIT \
    ((VERSION_MINOR / 100) + '0'), \
    (((VERSION_MINOR % 100) / 10) + '0'), \
    ((VERSION_MINOR % 10) + '0')

#elif VERSION_MINOR >= 10

#define VERSION_MINOR_INIT \
    ((VERSION_MINOR / 10) + '0'),  ((VERSION_MINOR % 10) + '0')

#else

#define VERSION_MINOR_INIT \
    (VERSION_MINOR + '0')

#endif

const unsigned char completeVersion1[] =
{ BUILD_YEAR_CH2, BUILD_YEAR_CH3, BUILD_MONTH_CH0, BUILD_MONTH_CH1, BUILD_DAY_CH0, BUILD_DAY_CH1,'\0'  } ;
//-30030134_SmartSole_DV1_SW_v
const unsigned char completeVersion2[] =
{ VERSION_MAJOR_INIT, '.', VERSION_MINOR_INIT, '-','T',
BUILD_HOUR_CH0, BUILD_HOUR_CH1,
BUILD_MIN_CH0, BUILD_MIN_CH1,
BUILD_SEC_CH0, BUILD_SEC_CH1,'\0' };


const unsigned char dvVersion2[] = { VERSION_MAJOR_INIT, '.', VERSION_MINOR_INIT, '\0' };



#define MODE_BOOTLOADER_GIT_TS 0x01
#define MODE_FIRMWARE_GIT_TS 0x02
#define MODE_BOOTLOADER_GIT_HASH 0x03
#define MODE_FIRMWARE_GIT_HASH 0x04


// Some definitions for calculation
#define SEC_PER_MIN             60UL
#define SEC_PER_HOUR            3600UL
#define SEC_PER_DAY             86400UL
#define SEC_PER_YEAR            (SEC_PER_DAY*365)

// extracts 1..4 characters from a string and interprets it as a decimal value
#define CONV_STR2DEC_1(str, i)  (str[i]>'0'?str[i]-'0':0)
#define CONV_STR2DEC_2(str, i)  (CONV_STR2DEC_1(str, i)*10 + str[i+1]-'0')
#define CONV_STR2DEC_3(str, i)  (CONV_STR2DEC_2(str, i)*10 + str[i+2]-'0')
#define CONV_STR2DEC_4(str, i)  (CONV_STR2DEC_3(str, i)*10 + str[i+3]-'0')

// Custom "glue logic" to convert the month name to a usable number
#define GET_MONTH(str, i)      (str[i]=='J' && str[i+1]=='a' && str[i+2]=='n' ? 1 :     \
                                str[i]=='F' && str[i+1]=='e' && str[i+2]=='b' ? 2 :     \
                                str[i]=='M' && str[i+1]=='a' && str[i+2]=='r' ? 3 :     \
                                str[i]=='A' && str[i+1]=='p' && str[i+2]=='r' ? 4 :     \
                                str[i]=='M' && str[i+1]=='a' && str[i+2]=='y' ? 5 :     \
                                str[i]=='J' && str[i+1]=='u' && str[i+2]=='n' ? 6 :     \
                                str[i]=='J' && str[i+1]=='u' && str[i+2]=='l' ? 7 :     \
                                str[i]=='A' && str[i+1]=='u' && str[i+2]=='g' ? 8 :     \
                                str[i]=='S' && str[i+1]=='e' && str[i+2]=='p' ? 9 :     \
                                str[i]=='O' && str[i+1]=='c' && str[i+2]=='t' ? 10 :    \
                                str[i]=='N' && str[i+1]=='o' && str[i+2]=='v' ? 11 :    \
                                str[i]=='D' && str[i+1]=='e' && str[i+2]=='c' ? 12 : 0)

// extract the information from the time string given by __TIME__ and __DATE__
#define __TIME_SECONDS__        CONV_STR2DEC_2(__TIME__, 6)
#define __TIME_MINUTES__        CONV_STR2DEC_2(__TIME__, 3)
#define __TIME_HOURS__          CONV_STR2DEC_2(__TIME__, 0)
#define __TIME_DAYS__           CONV_STR2DEC_2(__DATE__, 4)
#define __TIME_MONTH__          GET_MONTH(__DATE__, 0)
#define __TIME_YEARS__          CONV_STR2DEC_4(__DATE__, 7)

// Days in February
#define _UNIX_TIMESTAMP_FDAY(year) \
    (((year) % 400) == 0UL ? 29UL : \
        (((year) % 100) == 0UL ? 28UL : \
            (((year) % 4) == 0UL ? 29UL : \
                28UL)))

// Days in the year
#define _UNIX_TIMESTAMP_YDAY(year, month, day) \
    ( \
        /* January */    day \
        /* February */ + (month >=  2 ? 31UL : 0UL) \
        /* March */    + (month >=  3 ? _UNIX_TIMESTAMP_FDAY(year) : 0UL) \
        /* April */    + (month >=  4 ? 31UL : 0UL) \
        /* May */      + (month >=  5 ? 30UL : 0UL) \
        /* June */     + (month >=  6 ? 31UL : 0UL) \
        /* July */     + (month >=  7 ? 30UL : 0UL) \
        /* August */   + (month >=  8 ? 31UL : 0UL) \
        /* September */+ (month >=  9 ? 31UL : 0UL) \
        /* October */  + (month >= 10 ? 30UL : 0UL) \
        /* November */ + (month >= 11 ? 31UL : 0UL) \
        /* December */ + (month >= 12 ? 30UL : 0UL) \
    )

// get the UNIX timestamp from a digits representation
#define _UNIX_TIMESTAMP(year, month, day, hour, minute, second) \
    ( /* time */ second \
                + minute * SEC_PER_MIN \
                + hour * SEC_PER_HOUR \
    + /* year day (month + day) */ (_UNIX_TIMESTAMP_YDAY(year, month, day) - 1) * SEC_PER_DAY \
    + /* year */ (year - 1970UL) * SEC_PER_YEAR \
                + ((year - 1969UL) / 4UL) * SEC_PER_DAY \
                - ((year - 1901UL) / 100UL) * SEC_PER_DAY \
                + ((year - 1601UL) / 400UL) * SEC_PER_DAY \
    )

// the UNIX timestamp
#define UNIX_TIMESTAMP (_UNIX_TIMESTAMP(__TIME_YEARS__, __TIME_MONTH__, __TIME_DAYS__, __TIME_HOURS__, __TIME_MINUTES__, __TIME_SECONDS__))



#define STR_HELPER(A) #A
#define STR(A) STR_HELPER(A)
#define STRINGIFY(...) STR_HELPER(__VA_ARGS__)
#define KEY STRINGIFY(GITCOMMIT)
#define KEY_TSMP STRINGIFY(GITTIMESTAMP)
#define HEXTON(c) (*(c) >= 'a' ? (*(c) -'a')+10 : (*(c)-'0'))
#define HEXTOB(c) (HEXTON(c)*16 + HEXTON(c+1))
#define HEXTOS(c) (HEXTON(c)*16 + HEXTON(c+1))



static const char git_hash_char[] = STR(GITCOMMIT);

uint8_t git_hash_data[] = {
        HEXTOB(KEY+0)
        ,HEXTOB(KEY+2)
        ,HEXTOB(KEY+4)
        ,HEXTOB(KEY+6)
        ,HEXTOB(KEY+8)
        ,HEXTOB(KEY+10)
        ,HEXTOB(KEY+12)
        ,HEXTOB(KEY+14) };


static const char git_timestamp_char[] = STR(GITTIMESTAMP);
uint8_t git_timestamp_data[] = {
  HEXTOB(KEY_TSMP+0)
  ,HEXTOB(KEY_TSMP+2)
  ,HEXTOB(KEY_TSMP+4)
  ,HEXTOB(KEY_TSMP+6)
  ,HEXTOB(KEY_TSMP+8)
};

enum CAN_state_t
{
  SKAN = 0,
  DUMP
} can_state;


union STM32_NAME
{
   struct {
   uint8_t name:5;
   uint8_t mode:3;
   } fields;
   uint8_t val;
} ;


union stm32_epoch_t {
  uint64_t big;
  uint8_t chunks[8];
} ;


#define USER_8_BUF_SZ 8

typedef struct CAN_Message
{
	uint16_t frame_id;
	uint8_t frame_lenght;
        uint8_t repeat;
        uint8_t delay;
	uint8_t body[USER_8_BUF_SZ];
} CAN_Message;


static TaskHandle_t  NMEA_task_handle;
static TaskHandle_t  CAN_transmit_task_handle;
static TaskHandle_t  CAN_scan_task_handle;
static TaskHandle_t  CAN_dump_task_handle;
static TaskHandle_t  CAN_receive_task_handle;
static TaskHandle_t XU4_transmit_task_handle;
static TaskHandle_t CAN33_scan_handle;
static TaskHandle_t CAN33_dump_handle;
static TaskHandle_t XU4_receive_task_handle;
static TaskHandle_t XU4_init_task_handle;
static TaskHandle_t Underhood_task_handle;
static TaskHandle_t Underhood_scan_handle;
static TaskHandle_t Underhood_dump_handle;
static TaskHandle_t Underhood_transmit_handle;
static TaskHandle_t Underhood_heartbeat_handle;
static TaskHandle_t DS18x20_task_handle;

xQueueHandle GNSS_parser_Queue_handle;
xQueueHandle CAN_Queue_handle;
xQueueHandle Underhood_Queue_handle;
xQueueHandle XU4_Queue_handle;

static TaskStatus_t xDumpTaskDetails;
static TaskStatus_t xScanTaskDetails;

#define MCP2515_send_PRIORITY  (tskIDLE_PRIORITY + 3) // configMAX_PRIORITIES-1
#define MCP2515_dump_PRIORITY  (tskIDLE_PRIORITY + 3) // configMAX_PRIORITIES-1
#define MCP2515_scan_PRIORITY  (tskIDLE_PRIORITY + 3)
#define MCP2515_receive_PRIORITY  (tskIDLE_PRIORITY + 3)
#define MCP2515_scan_PRIORITY  (tskIDLE_PRIORITY + 3)

#define CAN_transmit_PRIORITY  (tskIDLE_PRIORITY + 3)
#define CAN_receive_PRIORITY   (tskIDLE_PRIORITY + 3)
#define CAN_dump_PRIORITY  (tskIDLE_PRIORITY + 3)
#define CAN_scan_PRIORITY  (tskIDLE_PRIORITY + 3)
#define NMEA_PRIORITY  (configMAX_PRIORITIES-1) // // configMAX_PRIORITIES-2

#define CAN_rx_irq_PRIORITY 200
#define BLOCK_heartbeat_irq_PRIORITY 200

#define ID_BC_UNDERHOOD_CMD 0x321
#define ID_BC_UNDERHOOD_HEARTBEAT 0x7FD

#define ID_SPEED 0x7F0
#define ID_SUN_LIGHT 0x7F1
#define ID_COOLANT  0x7F2
#define ID_TC_INST_TIME  0x7F3
#define ID_LIGHT_SIGNALS  0x7F4
#define ID_DS18x20 0x7F5
#define ID_NMEA 0x7FA
#define ID_SHUTDOWN_XU4_ID 0x7FB
#define ID_BC_FLUID_TEMP 0x7F7
#define ID_BC_FLUID_RESET 0x7F6
#define ID_CMD 0x7FF
#define ID_CMD_RESPONSE 0x7F8
#define ID_VERSION_RESPONSE 0x7F9


#define PID_ERRORS_REQ   0x7DF
#define PID_ERRORS_REPLY 0x7E8

#define STM_BLOCK  0x01
#define STM_GRAY   0x02
#define STM_PURPLE 0x04
#define STM_RED    0x08
#define STM_ODB2   0x10

#define MODE_BOOTLOADER_GIT_TS 0x01
#define MODE_FIRMWARE_GIT_TS 0x02
#define MODE_BOOTLOADER_GIT_HASH 0x03
#define MODE_FIRMWARE_GIT_HASH 0x04
#define MODE_FIRMWARE_BUILD_TS 0x05


#define BIT_CMD_BLOCK(data)       (data[0] >> 0)  /* 0x7FF#01 */
#define BIT_CMD_GRAY(data)        (data[0] >> 1)  /* 0x7FF#02 */
#define BIT_CMD_PURPLE(data)      (data[0] >> 2)  /* 0x7FF#04 */
#define BIT_CMD_RED(data)         (data[0] >> 3)  /* 0x7FF#08 */
#define BIT_CMD_ODB2(data)        (data[0] >> 4)  /* 0x7FF#10 */
#define BIT_CMD_BROADCAST(data)   (data[0] >> 7)  /* 0x7FF#80 */

#define BIT_CMD_LINE_HI(data)     (data[1] >> 0)
#define BIT_CMD_LINE_LO(data)     (data[1] >> 1)
#define BIT_CMD_PRINT_GIT(data)   (data[1] >> 2)
#define BIT_CMD_BL_GIT(data)      (data[1] >> 3)
#define BIT_CMD_PRINT_GIT_TS(data) (data[1] >> 4)
#define BIT_CMD_PRINT_BUILD_TS(data) (data[1] >> 5)
#define BIT_CMD_LINE_RESET(data)  (data[1] >> 7)

#define BIT_CMD_CAN_DUMP(data)    (data[2] >> 0)
#define BIT_CMD_CAN_SCAN(data)    (data[2] >> 1)
#define BIT_CMD_CAN_SKIP(data)    (data[2] >> 2)
#define BIT_CMD_CAN_SPEED1(data)  (data[2] >> 3)
#define BIT_CMD_CAN_SPEED2(data)  (data[2] >> 4)

#define SCAN_BIT (1UL << 0)
#define DUMP_BIT (1UL << 1)

static uint8_t task_run_flag;

static uint32_t mcp_errors;

/* CPU speed in MHz */
#define CPU_SPEED 72000000

/* LED blink intervals for bootloader mode (1 second / 0.33 seconds) */
#define BOOTLOADER_BLINK_INTERVAL ((CPU_SPEED/2)/65536)

#undef TEST_CAN_TRANSMIT
#undef TEST_BLUEPILL_BL

#ifdef TEST_BLUEPILL_BL
#define TEST_BLUEPILL
#else 
#undef TEST_BLUEPILL
#endif


union STM32_NAME this_name;
union stm32_epoch_t epoch;

#endif