#ifndef STM32_MCP_H
#define STM32_MCP_H
// https://github.com/Seeed-Studio/Seeed_Arduino_CAN/blob/master/src/mcp2515_can.cpp
// https://github.com/libopencm3/libopencm3-examples/blob/master/examples/stm32/f1/lisa-m-2/spi/spi.c

#define DEBUG_EN 1

#include "mcp2515_def.h"
#define byte uint8_t


#define CAN_OK              (0)
#define CAN_FAILINIT        (1)
#define CAN_FAILTX          (2)
#define CAN_MSGAVAIL        (3)
#define CAN_NOMSG           (4)
#define CAN_CTRLERROR       (5)
#define CAN_GETTXBFTIMEOUT  (6)
#define CAN_SENDMSGTIMEOUT  (7)
#define CAN_FAIL            (0xff)

uint8_t mcp2515_setup(MCP_CLOCK_T clock, MCP_BITTIME_SETUP  bittime);
byte readMsgBuf(unsigned long * id, byte *len, byte *buf) ;
byte sendMsgBuf2(unsigned long id, byte len, const byte* buf, uint8_t wait_sent);
byte checkReceive(void);
void mcp2515_reset(void) ;
void mcp2515_rx_interrupt_clear(void);
void mcp2515_disable_interrupts(void);
void mcp2515_enable_interrupts(void);
#endif //STM32_MCP_H