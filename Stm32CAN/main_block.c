/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------

File    : main.c
Purpose : Generic application start

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "stm32f1xx.h"
#include "stm32f10x.h"
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/f1/bkp.h>
#include <libopencm3/stm32/i2c.h>

#include "mcp2515.h"
#include "nmea_parse.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "vectra_lib.h"
#include "eeprom.h"
#include "common.h"
/*********************************************************************
*
*       main()
*
*  Function description
*   Application entry point.
*/
/*
PA1 -> MOD2

PB0 -> MOD1
PB1 -> MOD0
*/

void vApplicationMallocFailedHook(void);
void vApplicationMallocFailedHook(void) {
  taskDISABLE_INTERRUPTS();
  
  printf("get vApplicationMallocFailedHook\r\n");
  for( ;; );
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName) {
  ( void ) xTask;
  
  printf("get vApplicationStackOverflowHook %s\r\n",  pcTaskName);
  taskDISABLE_INTERRUPTS();
  for( ;; );
}


static BaseType_t xHigherPriorityTaskWoken;

#define V_CAN_ALT


enum BlockM
{
     FLOW_LO
    ,FLOW_HI
    ,TEMPER_LO
    ,TEMPER_HI
    ,RESERVED
    ,WIPES_LO
    ,WIPES_HI
    ,STATE
};


#define BC_RESET_FLUID (1 << 0)
#define BC_LIGHT_SIGNAL (1 << 1)
#define CAN_TX_BIT (1 << 2)


#define	USER_DELIMITER_0	'\0'
#define	USER_DELIMITER_1	'\n'
#define	USER_DELIMITER_2	'\r'

static uint16_t nmea;

void get_data_from_uart1(uint8_t ch, BaseType_t * xHigherPriorityTaskWoken)
{
	static int i = 0;
        static int j = 0;
        Message msg;
        Message tst;
        /* check data length */
        if (i >= (USER_UART2_BUF_SZ - 1))
        {
            i = USER_UART2_BUF_SZ - 2;
        }if (j >= (USER_UART2_BUF_SZ - 1))
        {
            j = USER_UART2_BUF_SZ - 2;
        }

        if (ch == USER_DELIMITER_1 || ch == USER_DELIMITER_2)
        {
           if (j > 0)
           {
             tst.body[j++] = USER_DELIMITER_0;
             nmea = ( ((uint16_t)tst.body[5] << 8) | (uint16_t)tst.body[4] ) +
                          ( ((uint16_t)tst.body[3] << 8) | (uint16_t)tst.body[2] );

                switch(nmea)
                {
                        case GPGSV:
                     //       printf("%s\n", tst.body);
                            break;
                }
           }
           j = 0;
                      if (i > 0)
           {
                msg.body[i++] = USER_DELIMITER_0;
                
                nmea = ( ((uint16_t)msg.body[5] << 8) | (uint16_t)msg.body[4] ) +
                          ( ((uint16_t)msg.body[3] << 8) | (uint16_t)msg.body[2] );

                switch(nmea)
                {
                        case GPGGA:

                            break;
                        case GPRMC:
                        case GNRMC:;
                              if(xQueueSendToBackFromISR(GNSS_parser_Queue_handle, &msg, 0) == pdPASS)
                              {
        
                                 vTaskNotifyGiveIndexedFromISR( NMEA_task_handle, 0, xHigherPriorityTaskWoken );
                                  portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
                              }
                           break;
                         default:
                          break;
                }
           }
           i = 0;
        }
        else
        {
          msg.body[i++] = ch;
          tst.body[j++] = ch;
        }

}


static void i2c_setup(void)
{
	/* Disable the I2C before changing any configuration. */
	i2c_peripheral_disable(I2C2);
	/* Enable clocks for I2C2 and AFIO. */
	rcc_periph_clock_enable(RCC_I2C2);
	rcc_periph_clock_enable(RCC_AFIO);

	/* Set alternate functions for the SCL and SDA pins of I2C2. */
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
		      GPIO_I2C2_SCL | GPIO_I2C2_SDA);

	i2c_peripheral_disable(I2C2);

        i2c_reset(I2C2);
	/* standard mode */
	i2c_set_standard_mode(I2C2);
        
	i2c_set_speed( I2C2, i2c_speed_fm_400k, rcc_apb1_frequency / 1e6 );

	/* If everything is configured -> enable the peripheral. */
	i2c_peripheral_enable(I2C2);
}


static void exti1_setup(void)
{
	/* Enable GPIOA clock. */
	// Enabled in CAN setup

	/* Enable AFIO clock. */
	//rcc_periph_clock_enable(RCC_AFIO);

	/* Enable EXTI0 interrupt. */
	nvic_enable_irq(NVIC_EXTI1_IRQ);
        
	nvic_set_priority(NVIC_EXTI1_IRQ, 200);

	/* Set GPIO0 (in GPIO port A) to 'input open-drain'. */
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO5);
        uint16_t data = gpio_port_read(GPIOB);
        //data |= 1UL << 12;
        gpio_port_write(GPIOB, data);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI1, GPIOB);
	exti_direction = FALLING;
	exti_set_trigger(EXTI1, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI1);
}


void exti15_10_isr(void)
{
      mcp2515_disable_interrupts();
      exti_reset_request(EXTI12);
      gpio_set(GPIOB, GPIO12);

     if (Underhood_scan_handle != NULL && (task_run_flag & SCAN_BIT))
      {
        vTaskNotifyGiveFromISR( Underhood_scan_handle, &xHigherPriorityTaskWoken );
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
      }

      if (Underhood_dump_handle != NULL && (task_run_flag & DUMP_BIT))
      {
        vTaskNotifyGiveFromISR( Underhood_dump_handle, &xHigherPriorityTaskWoken );
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
      }
}


void exti1_isr(void)
{
	exti_reset_request(EXTI1);

        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        vTaskNotifyGiveIndexedFromISR( XU4_receive_task_handle, 0, &xHigherPriorityTaskWoken );
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

	if (exti_direction == FALLING) 
        {
            exti_direction = RISING;
            exti_set_trigger(EXTI1, EXTI_TRIGGER_RISING);
	} 
        else 
        {
            exti_direction = FALLING;
            exti_set_trigger(EXTI1, EXTI_TRIGGER_FALLING);
	}
}

void usart1_isr(void)
{
    static uint8_t data = 'A';
    BaseType_t xHigherPriorityTaskWoken;
    xHigherPriorityTaskWoken = pdFALSE;

    /* Check if we were called because of RXNE. */
    if (((USART_CR1(USART1) & USART_CR1_RXNEIE) != 0) &&
            ((USART_SR(USART1) & USART_SR_RXNE) != 0)) 
    {
            /* Indicate that we got data. */
          //  gpio_toggle(GPIOA, GPIO6);

            /* Retrieve the data from the peripheral. */
            data = usart_recv(USART1);

            get_data_from_uart1(data, &xHigherPriorityTaskWoken);

            /* Enable transmit interrupt so it sends back the data. */
         //  USART_CR1(USART1) |= USART_CR1_TXEIE;
    }

      //  if( xHigherPriorityTaskWoken == pdTRUE )
        {
            /* Actual macro used here is port specific. */
           portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
        //   taskYIELD();
        }
}


/* blink timer ISR */
void tim4_isr(void) 
{
  if(Underhood_heartbeat_handle != NULL)
  {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR( Underhood_heartbeat_handle, &xHigherPriorityTaskWoken );
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
  }

  TIM_SR(TIM4) &= ~TIM_SR_UIF;
}

/* set up LED blinking timer and pin */
static void setup_heartbeat(void) 
{
        rcc_periph_clock_enable(RCC_TIM4);

	RCC_APB1RSTR |= RCC_APB1RSTR_TIM4RST;
	RCC_APB1RSTR &= ~RCC_APB1RSTR_TIM4RST;

        // Set timer start value
        TIM_CNT(TIM4) = 1;

        // Set timer prescaler 72/Mhz/65536 => ~1099 counts per second
	TIM_PSC(TIM4) = 0xFFFF;

        // End timer value. If this reached an interrupt is generated
	TIM_ARR(TIM4) = 26000;

	TIM_DIER(TIM4) |= TIM_DIER_UIE;

	nvic_set_priority(NVIC_TIM4_IRQ, BLOCK_heartbeat_irq_PRIORITY);
	nvic_enable_irq(NVIC_TIM4_IRQ);

	TIM_CR1(TIM4) |= TIM_CR1_CEN;
}




static void gpio_setup(void)
{
      rcc_periph_clock_enable(RCC_GPIOA);
      rcc_periph_clock_enable(RCC_AFIO);
      rcc_periph_clock_enable(RCC_GPIOB);
#if 1
      gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO8);
#else
      gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO8);
      gpio_set(GPIOA, GPIO8);
#endif
      rcc_periph_clock_enable(RCC_GPIOC);
      gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
      gpio_set(GPIOC, GPIO13);
}


static void usart1_setup(void)
{
       rcc_periph_clock_enable(RCC_USART1);
       // NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );
       //scb_set_priority_grouping(SCB_AIRCR_PRIGROUP_GROUP16_NOSUB);
        nvic_set_priority(NVIC_USART1_IRQ, configKERNEL_INTERRUPT_PRIORITY);

	/* Enable the USART1 interrupt. */
	nvic_enable_irq(NVIC_USART1_IRQ);

	/* Enable USART1 pin software remapping. */
	AFIO_MAPR |= AFIO_MAPR_USART1_REMAP;

	/* Setup GPIO pin GPIO_USART1_RE_TX on GPIO port B for transmit. */
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_RE_TX);

	/* Setup GPIO pin GPIO_USART1_RE_RX on GPIO port B for receive. */
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_FLOAT, GPIO_USART1_RE_RX);

	/* Setup UART parameters. */
	usart_set_baudrate(USART1, 9600);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
	usart_set_mode(USART1, USART_MODE_TX_RX);

	/* Enable USART1 Receive interrupt. */
	USART_CR1(USART1) |= USART_CR1_RXNEIE;

	/* Finally enable the USART. */
	usart_enable(USART1);
}



static void systick_setup(void)
{
	/* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	/* SysTick interrupt every N clock pulses: set reload to N-1 */
	systick_set_reload(8999);

	systick_interrupt_enable();

	/* Start counting. */
	systick_counter_enable();
}


static void can_setup(void)
{
	/* Enable peripheral clocks. */
	rcc_periph_clock_enable(RCC_CAN1);

        AFIO_MAPR |= AFIO_MAPR_CAN1_REMAP_PORTB;
 
	/* Configure CAN pin: RX (input pull-up). */
	gpio_set_mode(GPIO_BANK_CAN1_PB_RX, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN1_PB_RX);
	gpio_set(GPIO_BANK_CAN1_PB_RX, GPIO_CAN1_PB_RX);

	/* Configure CAN pin: TX. */
	gpio_set_mode(GPIO_BANK_CAN1_PB_TX, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN1_PB_TX);

        
        nvic_set_priority(NVIC_USB_LP_CAN_RX0_IRQ, CAN_rx_irq_PRIORITY);
	nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);


	/* NVIC setup. */


	/* Reset CAN. */
	can_reset(CAN1);
// APB1 ? ??? ???????? ?? ??????? 36???,
  //????????????? ????????? ????????? ???????? ?????????
  //BaudRate= 1000  BRP=  4  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 500   BRP=  9  SJW = 1  BS1= 3 BS2= 4
  //BaudRate= 250   BRP= 16  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 125   BRP= 32  SJW = 1  BS1= 4 BS2= 4
	/* CAN cell init. */
	if (can_init(CAN1,
		     false,           /* TTCM: Time triggered comm mode? */
		     true,            /* ABOM: Automatic bus-off management? */
		     false,           /* AWUM: Automatic wakeup mode? */
		     false,           /* NART: No automatic retransmission? */
		     false,           /* RFLM: Receive FIFO locked mode? */
		     false,           /* TXFP: Transmit FIFO priority? */
		     CAN_BTR_SJW_1TQ,
		     CAN_BTR_TS1_3TQ,
		     CAN_BTR_TS2_4TQ,
		     9,
		     false,
		     false))             /* BRP+1: Baud rate prescaler */
	{
		/* Die because we failed to initialize. */
		while (1)
			__asm__("nop");
	}

	/* CAN filter 0 init. */
	can_filter_id_mask_32bit_init(
				0,     /* Filter ID */
				0,     /* CAN ID */
				0,     /* CAN ID mask */
				0,     /* FIFO assignment (here: FIFO0) */
				true); /* Enable the filter. */

	/* Enable CAN RX interrupt. */
	can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
}


static void XU4_transmit_task(void *args) 
{
  uint32_t ulNotifiedValue;
  CAN_Message current_message;

  while(1)
  {
    if (XU4_Queue_handle != NULL)
    {
          //while( xQueueReceive( XU4_Queue_handle, &current_message, portMAX_DELAY ) != pdPASS );
          if(xQueueReceive(XU4_Queue_handle, &current_message, 0) == pdPASS)
          {
            while(current_message.repeat > 0)
            {
              current_message.repeat--;
              gpio_toggle(GPIOC, GPIO13);
              can_transmit(CAN1,
  			 current_message.frame_id,     
  			 false, // IDE: CAN ID extended? 
  			 false, // RTR: Request transmit? 
  			 current_message.frame_lenght,     // DLC: Data length
  			 current_message.body);
              vTaskDelay(5);
            }
          }
        
    }//if
    
   }
}


static void Underhood_heartbeat_task(void *args) 
{
  uint32_t ulNotifiedValue;

  CAN_Message current_message;
  current_message.frame_id = ID_BC_UNDERHOOD_HEARTBEAT;
  current_message.frame_lenght = 2;
  current_message.repeat = 1;
  current_message.body[0] = 0x00;
  current_message.body[1] = 0x00;

  while (1) 
  {
     if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
     {
        if(xQueueSendToBack(Underhood_Queue_handle, &current_message, 0) == pdPASS)
        {
          // xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
        }
        if(xQueueSendToBack(XU4_Queue_handle, &current_message, portMAX_DELAY) == pdPASS)
        {
         // xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
           //    xSemaphoreGive(mutex);	
        }//*/
     }
  }

}


static void Underhood_scan_task(void *args) 
{
  unsigned char len = 0;
  unsigned char data[8];
  unsigned long frame_id;
  uint32_t ulNotifiedValue;
  
  while (1) 
  {
   if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
    {
        readMsgBuf(&frame_id, &len, data);
        
        switch (frame_id)
        {
            case 0x123:;
            {
              CAN_Message current_message;
              current_message.frame_id = ID_BC_FLUID_TEMP;
              current_message.frame_lenght = 8;
              current_message.repeat = 1;
              memcpy( current_message.body, data, 8);

              if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
              {
                // xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
              }
              
              break;
            }
            default:
              break;
        }
    }
    vTaskDelay(10);
  }
}

static void XU4_receive_task(void *args) 
{
  uint32_t id;
  bool ext, rtr;
  uint8_t fmi, length, data[8];
  uint32_t ulNotifiedValue;
  static uint8_t light_signals = 0x0;
  static uint8_t previous_light_signals = 0x0;
  uint8_t fluid_reset = 0x0;
  CAN_Message underhood_message;
  
  this_name.fields.name = STM_BLOCK;

  while (1) 
  { 
      if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE )
      {
	can_receive(CAN1, 0, false, &id, &ext, &rtr, &fmi, &length, data, NULL);
        switch(id)
        {
          case 0x23A:
          {
            if (!( (data[0] >> 2) & 1 ))
            {
                light_signals |= (1<<0);
                light_signals |= (1<<7);
            }
            else
            {
                light_signals &=~ (1<<7);
            }
            break;
          } 
          case 0x260:
          { 
              light_signals &=~ (1<<1);
              light_signals &=~ (1<<2);
              light_signals &=~ (1<<3);
              if ( data[0] == 0b00100101 )
                 light_signals |= (1<<1);
              if ( data[0] == 0b00111010 )
                 light_signals |= (1<<2);
              if ( data[0] == 0b00011111 )
                 light_signals |= (1<<3);
              break;
          } 
          case 0x350:
          { 
              if (!((light_signals >> 7) & 1 ))
              {
                if ( (data[0] >> 2) & 1 )
                {
                    light_signals |= (1<<0);
                }
                else
                {
                    light_signals &=~ (1<<0);
                }
              }
              break;
          } 
          case ID_BC_FLUID_RESET:
          { 
              underhood_message.frame_id = ID_BC_UNDERHOOD_CMD;
              underhood_message.frame_lenght = 8;
              underhood_message.repeat = 1;
              
              memset(underhood_message.body, 0x0, sizeof(uint8_t)*8);
              underhood_message.body[0] = 0x01;

              if(xQueueSendToBack(Underhood_Queue_handle, &underhood_message, 0) == pdPASS)
              {
                 
              }

              break;
          }
          case ID_CMD:
            if(( BIT_CMD_BLOCK(data) & 1UL )|| (BIT_CMD_BROADCAST(data) & 1UL))
            {
              uint8_t ret = 0x0;
              //gpio_toggle(GPIOC, GPIO13);
              // cansend 999#0101 0000 0000 0000
              if ( BIT_CMD_LINE_HI(data) & 1UL )
              {           
                gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO8);
                gpio_set(GPIOA, GPIO8);
                ret |= (1 << 0);
              }
              
              // cansend 999#0102 0000 0000 0000
              if ( BIT_CMD_LINE_LO(data) & 1UL )
              {          
                gpio_clear(GPIOA, GPIO8); 
                gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO8);
                ret |= (1 << 1);
              }
              // cansend 999#0180 0000 0000 0000
              if ( BIT_CMD_LINE_RESET(data) & 1UL )
              {
                scb_reset_system();
              }
              if ( BIT_CMD_PRINT_GIT(data) & 1UL )
                {
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;
                  uint8_t * body_ptr = current_message.body;

                  if (BIT_CMD_BROADCAST(data) & 1UL)
                  {
                    body_ptr++;
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                      epoch.big = GITTIMESTAMP;
                      this_name.fields.mode = MODE_FIRMWARE_GIT_TS;
                      current_message.body[0] = this_name.val;
                      body_ptr+=1;
                      memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                    }
                   
                    else
                    {
                      this_name.fields.mode = MODE_FIRMWARE_GIT_HASH;
                      current_message.body[0] = this_name.val;
                      memcpy(body_ptr, git_hash_data, sizeof(uint8_t)*7);
                    }
                  }
                  else
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                      epoch.big = GITTIMESTAMP;
                      this_name.fields.mode = MODE_FIRMWARE_GIT_TS;
                      current_message.body[0] = this_name.val;
                      body_ptr+=2;
                      memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                    }
                    else
                    {
                      memcpy(body_ptr, git_hash_data, sizeof(uint8_t)*8);
                    }
                  }

                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                     xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                  }
                }
               if (BIT_CMD_PRINT_BUILD_TS(data) & 1UL)
                {
                  epoch.big = UNIX_TIMESTAMP;
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;
                  
                  uint8_t * body_ptr = current_message.body;

                  this_name.fields.mode = MODE_FIRMWARE_BUILD_TS;
                  current_message.body[0] = this_name.val;
                  body_ptr+=2;
                  memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                     xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                  }
                }
                if ( BIT_CMD_BL_GIT(data) & 1UL )
                {
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;

                  if (BIT_CMD_BROADCAST(data) & 1UL)
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_TS;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = 0x0;
                        current_message.body[2] = BKP_DR7 >> 8;
                        current_message.body[3] = BKP_DR7 & 0x00FF;
                        current_message.body[4] = BKP_DR8 >> 8;
                        current_message.body[5] = BKP_DR8 & 0x00FF;
                        current_message.body[6] = BKP_DR9 >> 8;
                        current_message.body[7] = BKP_DR9 & 0x00FF;
                    }
                    else
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_HASH;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = BKP_DR3 >> 8;
                        current_message.body[2] = BKP_DR3 & 0x00FF;
                        current_message.body[3] = BKP_DR4 >> 8;
                        current_message.body[4] = BKP_DR4 & 0x00FF;
                        current_message.body[5] = BKP_DR5 >> 8;
                        current_message.body[6] = BKP_DR5 & 0x00FF;
                        current_message.body[7] = BKP_DR6 >> 8;
                    }
                  }
                  else
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_TS;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = 0x0;
                        current_message.body[2] = BKP_DR7 >> 8;
                        current_message.body[3] = BKP_DR7 & 0x00FF;
                        current_message.body[4] = BKP_DR8 >> 8;
                        current_message.body[5] = BKP_DR8 & 0x00FF;
                        current_message.body[6] = BKP_DR9 >> 8;
                        current_message.body[7] = BKP_DR9 & 0x00FF;
                    }
                    else
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_HASH;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = BKP_DR3 >> 8;
                        current_message.body[2] = BKP_DR3 & 0x00FF;
                        current_message.body[3] = BKP_DR4 >> 8;
                        current_message.body[4] = BKP_DR4 & 0x00FF;
                        current_message.body[5] = BKP_DR5 >> 8;
                        current_message.body[6] = BKP_DR5 & 0x00FF;
                        current_message.body[7] = BKP_DR6 >> 8;
                    }
                  }

                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                  //   xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
                  }
                }
              // cansend 999#0100 0100 0000 0000
              if ( BIT_CMD_CAN_DUMP(data) & 1UL )
              {
                  ret |= (1 << 2);
                  vTaskSuspend(Underhood_scan_handle);
                  task_run_flag &=~ SCAN_BIT;
                  task_run_flag |= DUMP_BIT;
                  vTaskDelay(1);
                  vTaskResume(Underhood_dump_handle);
                  vTaskResume(XU4_transmit_task_handle);
                  vTaskDelay(1);
              }
              // cansend 999#0100 0200 0000 0000
              if ( BIT_CMD_CAN_SCAN(data) & 1UL )
              {
                  ret |= (1 << 3);
                  vTaskSuspend(Underhood_dump_handle);
                  task_run_flag &=~ DUMP_BIT;
                  task_run_flag |= SCAN_BIT;
                  vTaskResume(Underhood_scan_handle);
                  vTaskResume(XU4_transmit_task_handle);
                  vTaskDelay(1);
              }
              // cansend 999#0100 0400 0000 0000
              if ( BIT_CMD_CAN_SKIP(data) & 1UL )
              {
                  task_run_flag = 0x00;
                  ret |= (1 << 4);
                  vTaskSuspend(Underhood_dump_handle);
                  vTaskSuspend(Underhood_scan_handle);
                  vTaskSuspend(XU4_transmit_task_handle);
                  vTaskDelay(1);
              }

              vTaskGetInfo( Underhood_scan_handle, &xDumpTaskDetails, pdTRUE, eInvalid );
              vTaskGetInfo( Underhood_dump_handle, &xScanTaskDetails, pdTRUE, eInvalid );

              CAN_Message current_message;
              current_message.frame_id = ID_CMD_RESPONSE;
              current_message.frame_lenght = 8;
              current_message.repeat = 5;
              current_message.body[0] = ret;
              current_message.body[1] = xScanTaskDetails.eCurrentState;
              current_message.body[2] = xDumpTaskDetails.eCurrentState;
              current_message.body[4] = ( mcp_errors & 0x000000ff );
              current_message.body[5] = ( mcp_errors & 0x0000ff00 ) >> 8;
              current_message.body[6] = ( mcp_errors & 0x00ff0000 ) >> 16;
              current_message.body[7] = ( mcp_errors & 0xff000000 ) >> 24;

              if(xQueueSendToBack(XU4_Queue_handle, &current_message, portMAX_DELAY) == pdPASS)
              {
               // xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
             //    xSemaphoreGive(mutex);	
              }//*/
            } 
            break;
          default:
            break;
        } // switch

	can_fifo_release(CAN1, 0);
        can_enable_irq(CAN1, CAN_IER_FMPIE0);

        if (previous_light_signals != light_signals)
        {
          previous_light_signals = light_signals;
          CAN_Message current_message;
          current_message.frame_id = ID_BC_UNDERHOOD_CMD;
          current_message.frame_lenght = 8;
          current_message.repeat = 1;
          memset(current_message.body, 0x0, sizeof(uint8_t)*8);
          current_message.body[1] = light_signals;

          if(xQueueSendToBack(Underhood_Queue_handle, &current_message, portMAX_DELAY) == pdPASS)
          {
            // xTaskNotify(Underhood_transmit_handle, BC_LIGHT_SIGNAL, eSetBits);
          }//*/

          if(xQueueSendToBack(XU4_Queue_handle, &current_message, portMAX_DELAY) == pdPASS)
          {
           // xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
             //    xSemaphoreGive(mutex);	
          }//*/
        }
      }// if 

     // */
      //vTaskDelay(2);
  } // while
}


static void Nmea_task(void *args) 
{
    Message msg;
    GPRMC_Message message;
    uint32_t ulNotifiedValue;
    while(1) 
    {
      if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE )
      {
        if(xQueueReceive(GNSS_parser_Queue_handle, &msg, 0) == pdTRUE) 
        { 
            if ( GxRMC_parser(&message, msg.body) == 1 )
            {
                CAN_Message current_message;
                current_message.frame_id = ID_NMEA;
                current_message.repeat = 1;
                current_message.frame_lenght = 8;
                uint8_t * body_ptr = current_message.body;

                *((float *)body_ptr) = message.Latitude;
                body_ptr += (sizeof(uint8_t) * 4);
                *((float *)body_ptr) = message.Longitude;

                if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) != pdPASS)
                {
                    //PRINTF("no queue space.\n");
                }
            
            }
                printf("get %s\r\n",message.raw);
          //  ;
        }
      } // xTaskNotifyWait
    }// while
}

void usb_lp_can_rx0_isr(void)
{
  if(XU4_receive_task_handle != NULL)
  {
    can_disable_irq(CAN1, CAN_IER_FMPIE0);
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR( XU4_receive_task_handle, &xHigherPriorityTaskWoken );
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
  }
}


static void Underhood_transmit_task(void *args) 
{
  CAN_Message current_message;

  while (1) 
  { 
      if(xQueueReceive(Underhood_Queue_handle, &current_message, 10) == pdPASS)
      {
        while(current_message.repeat > 0)
        {
          current_message.repeat--;
          sendMsgBuf2(current_message.frame_id, current_message.frame_lenght, current_message.body, 0);
          vTaskDelay(5);
        }
      }        
  }
}


static void Underhood_dump_task(void *args)
{
  unsigned char len = 0;
  unsigned char data[8];
  unsigned long frame_id;
  uint32_t ulNotifiedValue;

  while (1) 
  {
    if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
    {
        readMsgBuf(&frame_id, &len, data);
        mcp2515_enable_interrupts();

        CAN_Message current_message;
        current_message.frame_id = frame_id;
        current_message.repeat = 1;
        current_message.frame_lenght = len;

        memcpy(current_message.body, data, sizeof(uint8_t)*len);
        if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) != pdPASS)
        {
            //PRINTF("no queue space.\n");
        }
    }
    vTaskDelay(5);
  }
    vTaskDelete(NULL);
}


static void Underhood_init_task(void *args)
{
    while ( mcp2515_setup( MCP_8MHz, CAN_33KBPS ) != CAN_OK )
    {
      mcp_errors++;
      vTaskDelay(1000);
    }
    
    xTaskCreate(Underhood_scan_task, "UH scan", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_scan_PRIORITY, &Underhood_scan_handle);
    xTaskCreate(Underhood_heartbeat_task, "UH heartbeat", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_scan_PRIORITY, &Underhood_heartbeat_handle);
    xTaskCreate(Underhood_dump_task, "UH dump", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_dump_PRIORITY, &Underhood_dump_handle);
    xTaskCreate(Underhood_transmit_task, "UH send", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_send_PRIORITY, &Underhood_transmit_handle);

    vTaskSuspend(Underhood_dump_handle);
      
    vTaskDelete(NULL);
}


void tim3_isr(void);


int main(void)
{
      rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
      gpio_setup();
      i2c_setup();
      usart1_setup();
      mcp2515_exti_setup();
      exti1_setup();
      systick_setup();
      setup_heartbeat();
      can_setup();
      setup_tim3();

      gpio_clear(GPIOC, GPIO13);
      
      task_run_flag |= SCAN_BIT;

     //uint8_t test_buffer[22] = {'T','H','I','S','_','I','S','_',
     //                             'A','N','_','E','E','P','R','O','M',
     //                             '_','T','E','S','T'};
     //uint8_t test_buffer_back[22] = {0,0,0,0,0,0,0,0,0,0,0,0,
     //                                 0,0,0,0,0,0,0,0,0,0};
     //if ( eeprom_write_page(EEPROM_TEST_PAGE_START_ADD, test_buffer, 22) == true )
     //{
     //  if (eeprom_read_page(EEPROM_TEST_PAGE_START_ADD, test_buffer_back, 22))
     //     printf("--%s--\n", test_buffer_back);
     //}

      XU4_Queue_handle = xQueueCreate(5,sizeof(CAN_Message)); /* Create a queue */
      Underhood_Queue_handle = xQueueCreate(5,sizeof(CAN_Message)); /* Create a queue */
      GNSS_parser_Queue_handle = xQueueCreate(5,sizeof(Message)); /* Create a queue */

      xTaskCreate(XU4_transmit_task, "XU4 tx", configMINIMAL_STACK_SIZE, (void*)NULL, CAN_transmit_PRIORITY, &XU4_transmit_task_handle);
      xTaskCreate(XU4_receive_task, "XU4 rx", configMINIMAL_STACK_SIZE * 2, (void*)NULL, CAN_receive_PRIORITY,  &XU4_receive_task_handle);
      xTaskCreate(Underhood_init_task, "UH init", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      xTaskCreate(Nmea_task, "Nmea", configMINIMAL_STACK_SIZE*3, (void*)NULL, NMEA_PRIORITY, &NMEA_task_handle);

      vTaskStartScheduler();

      while(1) 
      {

      }
      return 0;
}

/*********************************************************************
*
*       vApplicationGetIdleTaskMemory()
*
*/
#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize ) {
  /* If the buffers to be provided to the Idle task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xIdleTaskTCB;
  static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

  /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
  state will be stored. */
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

  /* Pass out the array that will be used as the Idle task's stack. */
  *ppxIdleTaskStackBuffer = uxIdleTaskStack;

  /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
#endif

/*********************************************************************
*
*       vApplicationGetTimerTaskMemory()
*
*/
/*-----------------------------------------------------------*/

#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize ) {
  /* If the buffers to be provided to the Timer task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xTimerTaskTCB;
  static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

  /* Pass out a pointer to the StaticTask_t structure in which the Timer
  task's state will be stored. */
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

  /* Pass out the array that will be used as the Timer task's stack. */
  *ppxTimerTaskStackBuffer = uxTimerTaskStack;

  /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
#endif

//void HardFault_Handler(void) {

// _Continue = 0u;
// //
// // When stuck here, change the variable value to != 0 in order to step out
// //
// while (_Continue == 0u);
//}

/*************************** End of file ****************************/
