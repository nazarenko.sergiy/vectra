/* LBUS based bootloader
 *
 * Copyright (c) 2016 Hans-Werner Hilse <hwhilse@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <stdint.h>
#include <stddef.h>


#include "stm32f1xx.h"
#include "stm32f10x.h"

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/f1/bkp.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/crc.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities


#include "xmodem/config.h"
#include "xmodem/xmodem.h"
#include "xmodem/xtimer.h"

#include "common.h"

#include <stdio.h>
#include <errno.h>
//#include <unistd.h>

#define WATCHDOG_PERIOD 10000 /**< watchdog period in ms */

#define VERSION 1
#define FLASH_WRONG_DATA_WRITTEN 0x80

static uint32_t flash_program_data(uint32_t start_address, uint8_t *input_data, uint16_t num_elements);
static uint32_t flash_erase_47k(uint32_t start_address);
int _write(int file, char *ptr, int len);


int _write(int file, char *ptr, int len)
{
  int i;
  if (file == 1 )
  {
    for(i = 0; i < len; i++)
      usart_send_blocking(USART1, ptr[i]);
    return i;
  }
  //errno = EIO;
  return -1;
}

/* boot firmware */
static void run_firmware(void) 
{
	/* shut down all devices we might have used in bootloader mode */
	RCC_AHBENR &= ~(RCC_AHBENR_CRCEN );
	RCC_APB2ENR &= ~(RCC_APB2ENR_USART1EN | RCC_APB2ENR_SPI1EN | RCC_APB2ENR_TIM1EN | RCC_APB2ENR_ADC2EN | RCC_APB2ENR_ADC1EN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN);
	RCC_APB1ENR &= ~(RCC_APB1ENR_USART3EN | RCC_APB1ENR_USART2EN | RCC_APB1ENR_SPI3EN | RCC_APB1ENR_SPI2EN | RCC_APB1ENR_WWDGEN | RCC_APB1ENR_TIM4EN | RCC_APB1ENR_TIM3EN | RCC_APB1ENR_TIM2EN);
	RCC_AHBENR &= ~(RCC_AHBENR_CRCEN);
	/* send reset to all devices */
	RCC_APB2RSTR |= (RCC_APB2RSTR_USART1RST | RCC_APB2RSTR_SPI1RST | RCC_APB2RSTR_TIM1RST | RCC_APB2RSTR_ADC2RST | RCC_APB2RSTR_ADC1RST | RCC_APB2RSTR_IOPCRST | RCC_APB2RSTR_IOPBRST | RCC_APB2RSTR_IOPARST | RCC_APB2RSTR_AFIORST);
	RCC_APB1RSTR |= (RCC_APB1RSTR_USART3RST | RCC_APB1RSTR_USART2RST | RCC_APB1RSTR_SPI3RST | RCC_APB1RSTR_SPI2RST | RCC_APB1RSTR_WWDGRST | RCC_APB1RSTR_TIM4RST | RCC_APB1RSTR_TIM3RST | RCC_APB1RSTR_TIM2RST);
	RCC_APB2RSTR = 0;
	RCC_APB1RSTR = 0;
	/* disable all interrupts and priorities */
	NVIC_ICER(0) = 0xFFFFFFFF;
	NVIC_ICER(1) = 0xFFFFFFFF;
	NVIC_ICER(2) = 0xFFFFFFFF;
	NVIC_ICPR(0) = 0xFFFFFFFF;
	NVIC_ICPR(1) = 0xFFFFFFFF;
	NVIC_ICPR(2) = 0xFFFFFFFF;

	/* store bootloader version in BKP_DR2,3 */
	//BKP_DR2 = VERSION & 0xFFFF;

        pwr_disable_backup_domain_write_protect();
	BKP_DR3 = ( git_hash_data[0] << 8 ) + git_hash_data[1];
	BKP_DR4 = ( git_hash_data[2] << 8 ) + git_hash_data[3];
	BKP_DR5 = ( git_hash_data[4] << 8 ) + git_hash_data[5];
	BKP_DR6 = ( git_hash_data[6] << 8 ) + git_hash_data[7];
	BKP_DR7 = ( epoch.chunks[0] << 8 ) + epoch.chunks[1];
	BKP_DR8 = ( epoch.chunks[2] << 8 ) + epoch.chunks[3];
	BKP_DR9 = epoch.chunks[4];
        pwr_enable_backup_domain_write_protect();

	/* Set vector table base address. */
	SCB_VTOR = FW_ADDRESS & 0xFFFF;

        SysTick->CTRL = 0;
        SysTick->LOAD = 0;
        SysTick->VAL = 0;
        
	/* Initialise master stack pointer. */
	asm volatile("msr msp, %0"::"r" (*(volatile uint32_t *)FW_ADDRESS) );
	/* Jump to application. */
	(*(void (**)())(FW_ADDRESS + 4))();
}



static int32_t cli_xmodem_recv_to_file_cb(uint8_t *data, uint32_t len, uint32_t offset) 
{
        iwdg_reset();
	//struct sffs_file *f = (struct sffs_file *)ctx;
	/* We are ignoring the offset. */
	//sffs_write(f, data, len);
        uint32_t res = flash_program_data(FW_ADDRESS+offset, data, len);
        //printf("%d offset: %d len: %d\n", res, offset, len);
	return XMODEM_RECV_CB_OK;
}

void bootloader_mode(void)
{
    uint16_t data;
    int i, j = 0, c = 0;

    struct xmodem xmdm;
    xmodem_init(&xmdm, USART1);
    xmodem_set_recv_callback(&xmdm, cli_xmodem_recv_to_file_cb);
#if 1
    flash_unlock();
    flash_erase_47k(FW_ADDRESS);
    int32_t res = xmodem_recv(&xmdm);
    flash_lock();
    if (res == XMODEM_RECV_EOT)
#endif
    {
      run_firmware();
    }

    while(1)
    {
       usart_wait_recv_ready(USART1);
        /*Receive command from host*/
       data = usart_recv(USART1);
       gpio_set(GPIOC, GPIO13);

      /*
      usart_send_blocking(USART2, c + '0');	
		c = (c == 9) ? 0 : c + 1;	
		if ((j++ % 80) == 0) {		
			usart_send_blocking(USART2, '\r');
			usart_send_blocking(USART2, '\n');
		}
                */
		for (i = 0; i < 100000; i++)	
			__asm__("nop");

        switch(data)
        {
          case BL_PRINT_TIMESTAMP:
                for (uint8_t i = 0; i < 7; i++)
                {
                  usart_send(USART1,git_timestamp_char[i]);
                  usart_wait_send_ready(USART1);
                  printf("%c",git_timestamp_char[i]);
                }
                usart_send(USART1, '\r');
                usart_wait_send_ready(USART1);
                usart_send(USART1, '\n');
                printf("\n");
                data = 0;
                break;
          case BL_PRINT_VERSION:
        	for (uint8_t i = 0; i < 8; i++)
                {
                  usart_send(USART1,git_hash_char[i]);
                  usart_wait_send_ready(USART1);
                  printf("%c",git_hash_char[i]);
                }
                usart_send(USART1, '\r');
                usart_wait_send_ready(USART1);
                usart_send(USART1, '\n');
                printf("\n");
                data = 0;
        	break;
          case BL_GOTO_FIRMWARE:
                run_firmware();
        	break;
          case BL_WRITE_BIN_TO_FLASH:
          
              usart_wait_recv_ready(USART1);
              flash_unlock();
              int32_t res = xmodem_recv(&xmdm);
              flash_lock();
              if (res == XMODEM_RECV_EOT)
              {
                usart_send(USART1, 0x30);
              }
              else
              {
                if (res == XMODEM_RECV_TIMEOUT)
                {
                  usart_send(USART1, 0x32);
                }
                else if (res == XMODEM_RECV_FAILED)
                {
                  usart_send(USART1, 0x31);
                }
                else if (res == XMODEM_RECV_CANCEL)
                {
                  usart_send(USART1, 0x33);
                }
                else 
                {
                  usart_send(USART1, 0x34);
                }
                usart_send(USART1, '\r');
                usart_wait_send_ready(USART1);
                usart_send(USART1, '\n');
              }
              break;
          case BL_ERASE_47K_FLASH:
              flash_unlock();
              flash_erase_47k(FW_ADDRESS);
              flash_lock();
              usart_send(USART1, 0x30);
            break;
          default:
              break;;
        }//*/
    }
}



static void usart_setup(void)
{
	rcc_periph_clock_enable(RCC_USART1);

        //nvic_enable_irq(NVIC_USART1_IRQ);
	/* Setup GPIO6 (in GPIO port A) to 'output push-pull' for LED use. */
	//gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO2);

	//AFIO_MAPR |= AFIO_MAPR_USART2_REMAP;

	/* Setup GPIO pin GPIO_USART3_TX/GPIO10 on GPIO port B for transmit. */
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);

        gpio_set_mode(GPIO_BANK_USART1_RX, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_FLOAT, GPIO_USART1_RX);

	/* Setup UART parameters. */
	usart_set_baudrate(USART1, 115200);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX_RX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

        USART_CR1(USART1) |= USART_CR1_RXNEIE;

	/* Finally enable the USART. */
	usart_enable(USART1);
}

#if 0

void usart1_isr(void)
{
	static uint8_t data = 'A';

	/* Check if we were called because of RXNE. */
	if (((USART_CR1(USART1) & USART_CR1_RXNEIE) != 0) &&
	    ((USART_SR(USART1) & USART_SR_RXNE) != 0)) {

		/* Indicate that we got data. */
		gpio_toggle(GPIOC, GPIO13);

		/* Retrieve the data from the peripheral. */
		data = usart_recv(USART1);

		/* Enable transmit interrupt so it sends back the data. */
		USART_CR1(USART1) |= USART_CR1_TXEIE;
	}

	/* Check if we were called because of TXE. */
	if (((USART_CR1(USART1) & USART_CR1_TXEIE) != 0) &&
	    ((USART_SR(USART1) & USART_SR_TXE) != 0)) {

		/* Indicate that we are sending out data. */
		//gpio_toggle(GPIOC, GPIO13);

		/* Put data into the transmit register. */
		usart_send(USART1, data);

		/* Disable the TXE interrupt as we don't need it anymore. */
		USART_CR1(USART1) &= ~USART_CR1_TXEIE;
	}
}

#endif

int main(void) 
{
        epoch.big = UNIX_TIMESTAMP;

	//rcc_clock_setup_in_hse_8mhz_out_72mhz();
        rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);

	/* enable devices */
	RCC_APB2ENR |= (RCC_APB2ENR_USART1EN | RCC_APB2ENR_TIM1EN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN);
	RCC_APB1ENR |= (RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN | RCC_APB1ENR_USART3EN | RCC_APB1ENR_USART2EN | RCC_APB1ENR_TIM4EN | RCC_APB1ENR_TIM3EN | RCC_APB1ENR_TIM2EN);
	RCC_AHBENR |= (RCC_AHBENR_CRCEN);

        rcc_periph_clock_enable(RCC_GPIOC);

	rcc_periph_clock_enable(RCC_AFIO);
	rcc_periph_clock_enable(RCC_BKP);
	rcc_periph_clock_enable(RCC_PWR);
        rcc_periph_clock_enable(RCC_GPIOA);
        gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO8);
        gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
        gpio_set(GPIOC, GPIO13);
        
        timer_init();
        usart_setup();

        
        int j = 50;

        while(j > 0)
        {
            gpio_toggle(GPIOC, GPIO13);

            for (int i = 0; i < 100000; i++) 
            {	/* Wait a bit. */
               __asm__("nop");
            }
            j--;
        }
        
        // setup watchdog to reset in case we get stuck (i.e. when an error occurred)
	iwdg_set_period_ms(WATCHDOG_PERIOD); // set independent watchdog period
	iwdg_start(); // start independent watchdog


       // while(1) {};

	/* disable backup domain write protection */
	//PWR_CR |= PWR_CR_DBP;
        uint16_t val = gpio_get(GPIOA, GPIO8);
        if(val == 256)
        {
	  bootloader_mode();
        }
        else
        {
          run_firmware();
        }

}

static uint32_t flash_erase_47k(uint32_t start_address)
{
	uint32_t page_address = start_address;

	uint32_t flash_status = 0;
	/*calculate current page address*/
	if(start_address % FLASH_PAGE_SIZE)
		page_address -= (start_address % FLASH_PAGE_SIZE);

        printf("PA: 0x%x\n", page_address);
	/*Erasing page*/
        uint8_t page = 47;
        while(page--)
        {
          flash_erase_page(page_address);
          flash_status = flash_get_status_flags();
          if(flash_status != FLASH_SR_EOP)
		return flash_status;
          page_address += FLASH_PAGE_SIZE;
        }
        return 0;
}

static uint32_t flash_program_data(uint32_t start_address, uint8_t *input_data, uint16_t num_elements)
{
	uint16_t iter;
	uint32_t current_address = start_address;
	uint32_t flash_status = 0;

	/*programming flash memory*/
	for(iter=0; iter<num_elements; iter += 4)
	{
                iwdg_reset(); // start independent watchdog
		/*programming word data*/
                printf("0x%x:", current_address+iter);
		flash_program_word(current_address+iter, *((uint32_t*)(input_data + iter)));
		flash_status = flash_get_status_flags();
		if(flash_status != FLASH_SR_EOP)
			return flash_status;

		/*verify if correct data is programmed*/
		if(*((uint32_t*)(current_address+iter)) != *((uint32_t*)(input_data + iter)))
			return FLASH_WRONG_DATA_WRITTEN;
	}
        printf("\n");

	return 0;
}
