#ifndef STM32_SPI_H
#define STM32_SPI_H

#include "stm32f1xx.h"
#include "stm32f10x.h"

uint8_t SPI1_RX(void);
void SPI1_TX(uint8_t val);
void PeripheralInit_SPI1_Master(void);
void spi_setup(void);
void spi_select(void);
void spi_unselect(void);
#endif //STM32_SPI_H