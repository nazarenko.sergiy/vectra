#include "spi_routine.h"
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"

#include "pn532_defs.h"
#include "pn532_routine.h"

#define STATUS_READ 2
#define DATA_WRITE 1
#define DATA_READ 3


#ifdef DEBUG_ARDUINO
#define DMSG(args...) PN532_DEBUG_SERIAL.print(args)
#define DMSG_STR(str) PN532_DEBUG_SERIAL.println(str)
#define DMSG_HEX(num)                                 \
    PN532_DEBUG_SERIAL.print(' ');                    \
    PN532_DEBUG_SERIAL.print((num >> 4) & 0x0F, HEX); \
    PN532_DEBUG_SERIAL.print(num & 0x0F, HEX)
#define DMSG_INT(num)              \
    PN532_DEBUG_SERIAL.print(' '); \
    PN532_DEBUG_SERIAL.print(num)
#else
#define DMSG(args...)
#define DMSG_STR(str)
#define DMSG_HEX(num)
#define DMSG_INT(num)
#endif

#define PN532_PREAMBLE                (0x00)
#define PN532_STARTCODE1              (0x00)
#define PN532_STARTCODE2              (0xFF)
#define PN532_POSTAMBLE               (0x00)

#define PN532_HOSTTOPN532             (0xD4)
#define PN532_PN532TOHOST             (0xD5)

#define PN532_ACK_WAIT_TIME           (10)  // ms, timeout of waiting for ACK

#define PN532_INVALID_ACK             (-1)
#define PN532_TIMEOUT                 (-2)
#define PN532_INVALID_FRAME           (-3)
#define PN532_NO_SPACE                (-4)

#define REVERSE_BITS_ORDER(b)         b = (b & 0xF0) >> 4 | (b & 0x0F) << 4; \
                                      b = (b & 0xCC) >> 2 | (b & 0x33) << 2; \
                                      b = (b & 0xAA) >> 1 | (b & 0x55) << 1

void PN532_wakeup(void)
{
    spi_select();
    vTaskDelay(2);
    spi_unselect();
    vTaskDelay(2);
}


uint8_t PN532_isReady(void)
{
    spi_select();
    SPI1_TX(STATUS_READ);
    uint8_t status =  SPI1_RX() & 1;
    spi_unselect();
    return status;
}


int8_t PN532_readAckFrame(void)
{
    const uint8_t PN532_ACK[] = {0, 0, 0xFF, 0, 0xFF, 0};

    uint8_t ackBuf[sizeof(PN532_ACK)];

    spi_select();
    vTaskDelay(1);
    SPI1_TX(DATA_READ);

    for (uint8_t i = 0; i < sizeof(PN532_ACK); i++)
    {
        ackBuf[i] = SPI1_RX();
    }

    spi_unselect();

    return memcmp(ackBuf, PN532_ACK, sizeof(PN532_ACK));
}


void PN532_writeFrame(const uint8_t *header, uint8_t hlen, const uint8_t *body, uint8_t blen)
{
    spi_select();
    vTaskDelay(2); // wake up PN532

    SPI1_TX(DATA_WRITE);
    SPI1_TX(PN532_PREAMBLE);
    SPI1_TX(PN532_STARTCODE1);
    SPI1_TX(PN532_STARTCODE2);

    uint8_t length = hlen + blen + 1; // length of data field: TFI + DATA
    SPI1_TX(length);
    SPI1_TX(~length + 1); // checksum of length

    SPI1_TX(PN532_HOSTTOPN532);
    uint8_t sum = PN532_HOSTTOPN532; // sum of TFI + DATA

    DMSG("write: ");

    for (uint8_t i = 0; i < hlen; i++)
    {
        SPI1_TX(header[i]);
        sum += header[i];

        DMSG_HEX(header[i]);
    }
    for (uint8_t i = 0; i < blen; i++)
    {
        SPI1_TX(body[i]);
        sum += body[i];

        DMSG_HEX(body[i]);
    }

    uint8_t checksum = ~sum + 1; // checksum of TFI + DATA
    SPI1_TX(checksum);
    SPI1_TX(PN532_POSTAMBLE);

    spi_unselect();

    DMSG('\n');
}

uint8_t command;

int8_t PN532_writeCommand(const uint8_t *header, uint8_t hlen, const uint8_t *body, uint8_t blen)
{
    command = header[0];
    PN532_writeFrame(header, hlen, body, blen);

    uint8_t timeout = PN532_ACK_WAIT_TIME;
    while (!PN532_isReady())
    {
        vTaskDelay(1);
        timeout--;
        if (0 == timeout)
        {
            DMSG("Time out when waiting for ACK\n");
            return -2;
        }
    }
    if (PN532_readAckFrame())
    {
        DMSG("Invalid ACK\n");
        return PN532_INVALID_ACK;
    }
    return 0;
}


int16_t PN532_readResponse(uint8_t buf[], uint8_t len, uint16_t timeout)
{
    uint16_t time = 0;
    while (!PN532_isReady())
    {
        vTaskDelay(1);
        time++;
        if (time > timeout)
        {
            return PN532_TIMEOUT;
        }
    }

    spi_select();
    vTaskDelay(2);

    int16_t result;
    do
    {
        SPI1_TX(DATA_READ);

        if (0x00 != SPI1_RX() || // PREAMBLE
            0x00 != SPI1_RX() || // STARTCODE1
            0xFF != SPI1_RX()    // STARTCODE2
        )
        {

            result = PN532_INVALID_FRAME;
            break;
        }

        uint8_t length = SPI1_RX();
        if (0 != (uint8_t)(length + SPI1_RX()))
        { // checksum of length
            result = PN532_INVALID_FRAME;
            break;
        }

        uint8_t cmd = command + 1; // response command
        if (PN532_PN532TOHOST != SPI1_RX() || (cmd) != SPI1_RX())
        {
            result = PN532_INVALID_FRAME;
            break;
        }

        DMSG("read:  ");
        DMSG_HEX(cmd);

        length -= 2;
        if (length > len)
        {
            for (uint8_t i = 0; i < length; i++)
            {
                DMSG_HEX(read()); // dump message
            }
            DMSG("\nNot enough space\n");
            SPI1_RX();
            SPI1_RX();
            result = PN532_NO_SPACE; // not enough space
            break;
        }

        uint8_t sum = PN532_PN532TOHOST + cmd;
        for (uint8_t i = 0; i < length; i++)
        {
            buf[i] = SPI1_RX();
            sum += buf[i];

            DMSG_HEX(buf[i]);
        }
        DMSG('\n');

        uint8_t checksum = SPI1_RX();
        if (0 != (uint8_t)(sum + checksum))
        {
            DMSG("checksum is not ok\n");
            result = PN532_INVALID_FRAME;
            break;
        }
        SPI1_RX(); // POSTAMBLE

        result = length;
    } while (0);

    spi_unselect();
    vTaskDelay(2);

    return result;
}


uint8_t pn532_init(void)
{
  command = 0;
  spi_setup() ;
  PN532_wakeup();

  PN532_SAMConfig();
 // {
 //   return 3;
  //}

  uint32_t fw = PN532_getFirmwareVersion();

  if (!fw)
  {
    return 1;
  }
  



  if(!PN532_setPassiveActivationRetries(0xFF))
  {
    return 2;
  }

  PN532_startPassiveTargetIDDetection(PN532_MIFARE_ISO14443A);

  

  return 0;

}


uint8_t pn532_packetbuffer[64];

uint32_t PN532_getFirmwareVersion(void)
{
    uint32_t response;

    pn532_packetbuffer[0] = PN532_COMMAND_GETFIRMWAREVERSION;

    if (PN532_writeCommand(pn532_packetbuffer, 1, 0, 0))
    {
        return 0;
    }

    //// read data packet
    int16_t status = PN532_readResponse(pn532_packetbuffer, sizeof(pn532_packetbuffer), 1);
    if (0 > status)
    {
        return 0;
    }

    response = pn532_packetbuffer[0];
    response <<= 8;
    response |= pn532_packetbuffer[1];
    response <<= 8;
    response |= pn532_packetbuffer[2];
    response <<= 8;
    response |= pn532_packetbuffer[3];

    return response; // 32010607
}



uint8_t PN532_setPassiveActivationRetries(uint8_t maxRetries)
{
	// compose message to PN532
	pn532_packetbuffer[0] = PN532_COMMAND_RFCONFIGURATION;
	pn532_packetbuffer[1] = 5;    // Config item 5 (MaxRetries)
	pn532_packetbuffer[2] = 0xFF; // MxRtyATR (default = 0xFF)
	pn532_packetbuffer[3] = 0x01; // MxRtyPSL (default = 0x01)
	pn532_packetbuffer[4] = maxRetries;

	// send configuration message to PN532
	//if (! sendCommandCheckAck(_pn532_packetbuffer, 5))
	//{
	//	return false;  // because return value is not successful return false
	//}

        if (PN532_writeCommand(pn532_packetbuffer, 5, 0, 0) != 0)
        {
            return 0;
        }

	return 1;
}
/*
int8_t Adafruit_PN532_sendCommandCheckAck(uint8_t *cmd, uint8_t cmdlen,
                                         uint16_t timeout) {


  // write the command
  writecommand(cmd, cmdlen);

    vTaskDelay(2);

uint16_t time = 0;
    while (!PN532_isReady())
    {
        vTaskDelay(1);
        time++;
        if (time > timeout)
        {
            return PN532_TIMEOUT;
        }
    }



  // read acknowledgement
  if (!readack()) {
    return -1;
  }

    vTaskDelay(2);

  time = 0;
    while (!PN532_isReady())
    {
        vTaskDelay(1);
        time++;
        if (time > timeout)
        {
            return PN532_TIMEOUT;
        }
    }
*/


uint8_t PN532_startPassiveTargetIDDetection(uint8_t cardbaudrate)
{
    pn532_packetbuffer[0] = PN532_COMMAND_INLISTPASSIVETARGET;
    pn532_packetbuffer[1] = 1; // max 1 cards at once (we can set this to 2 later)
    pn532_packetbuffer[2] = cardbaudrate;

    if (PN532_writeCommand(pn532_packetbuffer, 3, 0, 0))
    {
        return 0x0; // command failed
    }
    return 1;
}

uint8_t PN532_SAMConfig(void)
{
	// Compose message
	pn532_packetbuffer[0] = PN532_COMMAND_SAMCONFIGURATION;
	pn532_packetbuffer[1] = 0x01; // normal mode;
	pn532_packetbuffer[2] = 0x14; // timeout 50ms * 20 = 1 second
	pn532_packetbuffer[3] = 0x01; // use IRQ pin!

	// send message
	if (PN532_writeCommand(pn532_packetbuffer, 4, 0, 0) != 0)
	{
            return 0;
	}
	// read data packet
	int16_t status = PN532_readResponse(pn532_packetbuffer, 8, 1);
        if (0 > status)
        {
            return 0;
        }

	int offset = 6;
	// check if data at position 5 is equal 0x15 and return outcome
	return (pn532_packetbuffer[offset] == 0x15);
}

uint8_t PN532_powerDownMode(void)
{
    pn532_packetbuffer[0] = PN532_COMMAND_POWERDOWN;
    pn532_packetbuffer[1] = 0xC0; // I2C or SPI Wakeup
    pn532_packetbuffer[2] = 0x00; // no IRQ

    DMSG("POWERDOWN\n");

    if (PN532_writeCommand(pn532_packetbuffer, 4, 0, 0))
        return 1;

    return (0 < PN532_readResponse(pn532_packetbuffer, sizeof(pn532_packetbuffer), 1));
}

