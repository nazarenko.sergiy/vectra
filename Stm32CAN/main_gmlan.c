/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------

File    : main.c
Purpose : Generic application start

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "stm32f1xx.h"
#include "stm32f10x.h"
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/exti.h>

#include "FreeRTOS.h"
#include "task.h"
#include "mcp2515.h"

#define ONEWIRE_USART3
#include "OneWire.h"

/*********************************************************************
*
*       main()
*
*  Function description
*   Application entry point.
*/
/*
PA1 -> MOD2

PB0 -> MOD1
PB1 -> MOD0
*/

void vApplicationMallocFailedHook(void);
void vApplicationMallocFailedHook(void) {
  taskDISABLE_INTERRUPTS();
  for( ;; );
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName) {
  ( void ) pcTaskName;
  ( void ) xTask;
  taskDISABLE_INTERRUPTS();
  for( ;; );
}


#define V_CAN_ALT

static TaskHandle_t  MCP2515_task_handle;

enum CAN_data_t
{
  DATA_WAIT = 0,
  DATA_READY
} can_data;



struct can_tx_msg {
	uint32_t std_id;
	uint32_t ext_id;
	uint8_t ide;
	uint8_t rtr;
	uint8_t dlc;
	uint8_t data[8];
};

struct can_rx_msg {
	uint32_t std_id;
	uint32_t ext_id;
	uint8_t ide;
	uint8_t rtr;
	uint8_t dlc;
	uint8_t data[8];
	uint8_t fmi;
};

struct can_tx_msg can_tx_msg;
struct can_rx_msg can_rx_msg;

#define FALLING 0
#define RISING 1
static uint16_t exti_direction = FALLING;

static void exti_setup(void)
{
	/* Enable GPIOA clock. */
	rcc_periph_clock_enable(RCC_GPIOA);

	/* Enable AFIO clock. */
	//rcc_periph_clock_enable(RCC_AFIO);

	/* Enable EXTI0 interrupt. */
	nvic_enable_irq(NVIC_EXTI0_IRQ);

	/* Set GPIO0 (in GPIO port A) to 'input open-drain'. */
	gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO0);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI0, GPIOA);
	exti_direction = FALLING;
	exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI0);
}


void exti0_isr(void)
{
	exti_reset_request(EXTI0);

        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        vTaskNotifyGiveIndexedFromISR( MCP2515_task_handle, 0, &xHigherPriorityTaskWoken );
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

	if (exti_direction == FALLING) {

		exti_direction = RISING;
		exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
	} else {

		exti_direction = FALLING;
		exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
	}
}

static void gpio_setup(void)
{
	///* Enable GPIOA clock. */
	//rcc_periph_clock_enable(RCC_GPIOA);

	///* Enable GPIOB clock. */
	//rcc_periph_clock_enable(RCC_GPIOB);

	//gpio_set(GPIOA, GPIO6); /* LED0 off */
	//gpio_set(GPIOA, GPIO7); /* LED1 off */
	//gpio_set(GPIOB, GPIO0); /* LED2 off */
	//gpio_set(GPIOB, GPIO1); /* LED3 off */

	///* Set GPIO6/7 (in GPIO port A) to 'output push-pull' for the LEDs. */
	//gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO6);
	//gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO7);

	///* Set GPIO0/1 (in GPIO port B) to 'output push-pull' for the LEDs. */
	//gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO0);
	//gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

      rcc_periph_clock_enable(RCC_GPIOC);
      gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
      gpio_set(GPIOC, GPIO13);
}


static void dallas_setup(void)
{
      rcc_periph_clock_enable(RCC_USART3);
      gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_10_MHZ,
                  GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_USART3_TX | GPIO_USART3_RX);
    //  AFIO_MAPR |= AFIO_MAPR_SWJ_CFG_FULL_SWJ_NO_JNTRST;
 
}


static void systick_setup(void)
{
	/* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	/* SysTick interrupt every N clock pulses: set reload to N-1 */
	systick_set_reload(8999);

	systick_interrupt_enable();

	/* Start counting. */
	systick_counter_enable();
}

static void can_setup(void)
{
	/* Enable peripheral clocks. */
	rcc_periph_clock_enable(RCC_AFIO);

#ifdef 	V_CAN_ALT
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_CAN1);
#else
      	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_CAN);
#endif	
      
#ifdef 	V_CAN_ALT
        AFIO_MAPR |= AFIO_MAPR_CAN1_REMAP_PORTB;
 
	/* Configure CAN pin: RX (input pull-up). */
	gpio_set_mode(GPIO_BANK_CAN1_PB_RX, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN1_PB_RX);
	gpio_set(GPIO_BANK_CAN1_PB_RX, GPIO_CAN1_PB_RX);

	/* Configure CAN pin: TX. */
	gpio_set_mode(GPIO_BANK_CAN1_PB_TX, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN1_PB_TX);


        /* Normal mode */
        gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
                              GPIO_CNF_OUTPUT_PUSHPULL, GPIO0);
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
                  	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);
        gpio_clear(GPIOB, GPIO0); /* MODE 0 -> 1 */
	gpio_clear(GPIOB, GPIO1); /* MODE 1 -> 1 */
#else
	gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN_RX);
	gpio_set(GPIOA, GPIO_CAN_RX);
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN_TX);
#endif	

	/* NVIC setup. */
	nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);
	nvic_set_priority(NVIC_USB_LP_CAN_RX0_IRQ, 1);

	/* Reset CAN. */
	can_reset(CAN1);
// APB1 ? ??? ???????? ?? ??????? 36???,
  //????????????? ????????? ????????? ???????? ?????????
  //BaudRate= 1000  BRP=  4  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 500   BRP=  9  SJW = 1  BS1= 3 BS2= 4
  //BaudRate= 250   BRP= 16  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 125   BRP= 32  SJW = 1  BS1= 4 BS2= 4
	/* CAN cell init. */
	if (can_init(CAN1,
		     false,           /* TTCM: Time triggered comm mode? */
		     true,            /* ABOM: Automatic bus-off management? */
		     false,           /* AWUM: Automatic wakeup mode? */
		     false,           /* NART: No automatic retransmission? */
		     false,           /* RFLM: Receive FIFO locked mode? */
		     false,           /* TXFP: Transmit FIFO priority? */
		     CAN_BTR_SJW_1TQ,
		     CAN_BTR_TS1_10TQ,
		     CAN_BTR_TS2_7TQ,
		     60,
		     false,
		     false))             /* BRP+1: Baud rate prescaler */
	{
		//gpio_set(GPIOA, GPIO6);		/* LED0 off */
		//gpio_set(GPIOA, GPIO7);		/* LED1 off */
		//gpio_set(GPIOB, GPIO0);		/* LED2 off */
		//gpio_clear(GPIOB, GPIO1);	/* LED3 on */

		/* Die because we failed to initialize. */
		while (1)
			__asm__("nop");
	}

	/* CAN filter 0 init. */
	can_filter_id_mask_32bit_init(
				0,     /* Filter ID */
				0,     /* CAN ID */
				0,     /* CAN ID mask */
				0,     /* FIFO assignment (here: FIFO0) */
				true); /* Enable the filter. */

	/* Enable CAN RX interrupt. */
	can_enable_irq(CAN1, CAN_IER_FMPIE0);
}

typedef enum {
  DS18x20_DATA_READY = 0,
  DS18x20_DATA_WAIT
} DS18x20_CAN_t;

DS18x20_CAN_t DS18x20_data_state;

typedef enum {
  DS18x20_START =0,
  DS18x20_PROLOG,
  DS18x20_READ,
  DS18x20_ERROR
} DS18x20_t;

DS18x20_t DS18x20_state;

#define DS18x20_DATA_CAN_LENGTH 4

static uint8_t DS18x20_data[DS18x20_DATA_CAN_LENGTH] = { 0, };

OneWire ow;

void USART3_IRQHandler() {
  owReadHandler(USART3);
}

static void DS18x20_task(void *args) 
{
  DS18x20_state = DS18x20_PROLOG;
  ow.usart = USART3;
/*

  ow.usart = USART3;

  uint32_t pDelay = 300, i;

  while (1) {
    if (owResetCmd(&ow) != ONEWIRE_NOBODY) {    // is anybody on the bus?
      int devices = owSearchCmd(&ow);           // ???????? ROMid ???? ???????? ?? ???? ??? ??????? ??? ??????
      if (devices <= 0) {
        printf("\n\rError has happened!");
        pDelay = 8000000;
        gpio_toggle(GPIOC, GPIO13);   
        for (i = 0; i < pDelay * 4; i++)   
            __asm__("nop");
        continue;
      }
      printf("\n\rfound %d devices on 1-wire bus", devices);
      if (devices < 1)
        continue; // ???????? ???-?? ????? "??????????" ?? ????????
      i = 0;
      for (; i < devices; i++) {
        RomCode *r = &ow.ids[i];
        Temperature t;
        uint8_t crc = owCRC8(r);
        char *crcOK = (crc == r->crc)?"CRC OK":"CRC ERROR!";
        printf("\n\rdevice %d (SN: %02X/%02X%02X%02X%02X%02X%02X/%02X) ", i, r->family, r->code[5], r->code[4], r->code[3],
               r->code[2], r->code[1], r->code[0], r->crc);
        printf(crcOK);
        if (crc != r->crc) {
          printf("\n\r can't read cause CNC error");
          continue;
        }
        switch (r->family) {
          case DS18B20:
            // ????? ?????????? ???????? ??????????? ?????????!
            t = readTemperature(&ow, &ow.ids[i], true);
            printf("\n\rDS18B20 , Temp: %3d.%dC", t.inCelsus, t.frac);
            break;
          case DS18S20:
            t = readTemperature(&ow, &ow.ids[i], true);
            printf("\n\rDS18S20 , Temp: %3d.%dC", t.inCelsus, t.frac);
            break;
          case 0x00:
            break;
          default:
            printf("\n\rUNKNOWN Family:%x (SN: %x%x%x%x%x%x)", r->family, r->code[0], r->code[1], r->code[2],
                   r->code[3], r->code[4], r->code[5]);
            break;
        }
        pDelay = 8000000;
      }
      printf("\n\r...");
    } else {
      printf("there is no device on the bus");
      pDelay = 8000000;
    }
    gpio_toggle(GPIOC, GPIO13);   
    for (i = 0; i < pDelay * 4; i++)   
        __asm__("nop");
  }

*/
uint8_t error_retries = 0;
uint8_t devices;
uint8_t current_device;
RomCode *r;
Temperature t;
uint8_t static rx_data;



  while(error_retries < 60)
  {
      switch(DS18x20_state)
      {
          case DS18x20_PROLOG:
          {
            if (owResetCmd(&ow) != ONEWIRE_NOBODY)
            {
              devices = owSearchCmd(&ow);
              if (devices <= 0) 
              {
                 DS18x20_state = DS18x20_PROLOG;
                 if ((error_retries++) > 60)
                 {
                  DS18x20_state = DS18x20_ERROR;
                 }
                 else
                 {
                  vTaskDelay(1000);
                 }
              }
              else
              {
                DS18x20_state = DS18x20_START;
                error_retries = 0;
                current_device = 0;
              }
            }
            else
            {
              DS18x20_state = DS18x20_PROLOG;
              vTaskDelay(1000);
            }
            break;
          }
          case DS18x20_START:
          {
                current_device++;
          	if (current_device >= devices)
                {
                  current_device = 0;
                }

                r = &ow.ids[current_device];
                uint8_t crc = owCRC8(r);

                //char *crcOK = (crc == r->crc)?"CRC OK":"CRC ERROR!";
                //printf("\n\rdevice %d (SN: %02X/%02X%02X%02X%02X%02X%02X/%02X) ", i, r->family, r->code[5], r->code[4], r->code[3],
                //       r->code[2], r->code[1], r->code[0], r->crc);
                //printf(crcOK);
                if (crc == r->crc) 
                {
                   DS18x20_state = DS18x20_READ;
                }
                break;
          }
          case DS18x20_READ:
          {
                 t = readTemperature(&ow, &ow.ids[current_device], true);
          //       printf("\n\rDS18B20 , Temp: %3d.%dC", t.inCelsus, t.frac);
                 DS18x20_data[0] = current_device;
                 DS18x20_data[1] = r->family;
                 DS18x20_data[2] = t.inCelsus;
                 DS18x20_data[3] = t.frac;
          	 DS18x20_state = DS18x20_START;
                 DS18x20_data_state = DS18x20_DATA_READY;
                 break;
          }
          case DS18x20_ERROR:
          {
            break;
          }
          vTaskDelay(80);
      }//switch
  }//while

  vTaskDelete(NULL);
}

static uint8_t rx_data;

static void CAN_tansmit_task(void *args) 
{
   uint8_t data[2] = { 1, 5 };
   uint8_t temp32 = 0;
   uint8_t trr = 0;
   rx_data = 0;
   while (1) 
   {
       gpio_toggle(GPIOC, GPIO13);
	/* Transmit CAN frame. */
	data[1] = rx_data;
        temp32++ ;
        if (temp32 >= 3)
        {
          temp32 = 0;
          if (trr == 0)
          {
          trr = 1;
          data[0] = 5;
          data[1] = 1;
          }
          else
          {
          trr = 0;
          data[0] = 1;
          data[1] = 5;
          }

        }

        if (DS18x20_data_state == DS18x20_DATA_READY)
        {
            DS18x20_data_state = DS18x20_DATA_WAIT;
            if (can_transmit(CAN1,
                             0x123, /* (EX/ST)ID: CAN ID */
                             false, /* IDE: CAN ID extended? */
                             false, /* RTR: Request transmit? */
                             DS18x20_DATA_CAN_LENGTH,     /* DLC: Data length */
                             DS18x20_data) == -1)
            {
                    //gpio_set(GPIOA, GPIO6);		/* LED0 off */
                    //gpio_set(GPIOA, GPIO7);		/* LED1 off */
                    //gpio_clear(GPIOB, GPIO0);	/* LED2 on */
                    //gpio_set(GPIOB, GPIO1);		/* LED3 off */
            }
        }
        else
        {
          if (can_transmit(CAN1,
                             0x122, /* (EX/ST)ID: CAN ID */
                             false, /* IDE: CAN ID extended? */
                             false, /* RTR: Request transmit? */
                             2,     /* DLC: Data length */
                             data) == -1)
            {
                    //gpio_set(GPIOA, GPIO6);		/* LED0 off */
                    //gpio_set(GPIOA, GPIO7);		/* LED1 off */
                    //gpio_clear(GPIOB, GPIO0);	/* LED2 on */
                    //gpio_set(GPIOB, GPIO1);		/* LED3 off */
            }
        }

    vTaskDelay(1000);
  }// while
}


static void MCP2515_task(void *args) 
{
  uint32_t ulNotifiedValue;
  unsigned char len = 0;
  unsigned char buf[8];
  uint32_t id;

  while (1) 
  {
    if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
    {
        readMsgBuf(&id, &len, buf);
    }
  }
}

static void CAN_receive_task(void *args) {
  uint32_t id;
  bool ext, rtr;
  uint8_t fmi, length, data[8];
  can_data = DATA_WAIT;
  while (1) 
  {
      if (can_data == DATA_READY )
      {
        gpio_toggle(GPIOC, GPIO13);
	can_receive(CAN1, 0, false, &id, &ext, &rtr, &fmi, &length, data, NULL);
        rx_data = data[0]; 
	//if (data[0] & 1)
	//	gpio_clear(GPIOA, GPIO6);
	//else
	//	gpio_set(GPIOA, GPIO6);

	//if (data[0] & 2)
	//	gpio_clear(GPIOA, GPIO7);
	//else
	//	gpio_set(GPIOA, GPIO7);

	//if (data[0] & 4)
	//	gpio_clear(GPIOB, GPIO0);
	//else
	//	gpio_set(GPIOB, GPIO0);

	//if (data[0] & 8)
	//	gpio_clear(GPIOB, GPIO1);
	//else
	//	gpio_set(GPIOB, GPIO1);

	can_fifo_release(CAN1, 0);

        can_data = DATA_WAIT;
        can_enable_irq(CAN1, CAN_IER_FMPIE0);
      }// if 
      vTaskDelay(10);
  } // while
}




void CAN1_RX0_IRQHandler(void)
{
  can_disable_irq(CAN1, CAN_IER_FMPIE0);
  can_data = DATA_READY;
}


int main(void)
{
      rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
      gpio_setup();
      can_setup();
      systick_setup();
      dallas_setup();
      exti_setup();

      mcp2515_setup( MCP_16MHz, CAN_500KBPS );

      xTaskCreate(CAN_tansmit_task, "CAN_TX", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      xTaskCreate(CAN_receive_task, "CAN_RX", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      xTaskCreate(DS18x20_task, "DS18", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-3, NULL);
      xTaskCreate(MCP2515_task, "MCP", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, &MCP2515_task_handle);
      vTaskStartScheduler();

      while(1) 
      {

      }
      return 0;
}


/*********************************************************************
*
*       vApplicationGetIdleTaskMemory()
*
*/
#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize ) {
  /* If the buffers to be provided to the Idle task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xIdleTaskTCB;
  static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

  /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
  state will be stored. */
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

  /* Pass out the array that will be used as the Idle task's stack. */
  *ppxIdleTaskStackBuffer = uxIdleTaskStack;

  /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
#endif

/*********************************************************************
*
*       vApplicationGetTimerTaskMemory()
*
*/
/*-----------------------------------------------------------*/

#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize ) {
  /* If the buffers to be provided to the Timer task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xTimerTaskTCB;
  static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

  /* Pass out a pointer to the StaticTask_t structure in which the Timer
  task's state will be stored. */
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

  /* Pass out the array that will be used as the Timer task's stack. */
  *ppxTimerTaskStackBuffer = uxTimerTaskStack;

  /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
#endif

//void HardFault_Handler(void) {

// _Continue = 0u;
// //
// // When stuck here, change the variable value to != 0 in order to step out
// //
// while (_Continue == 0u);
//}

/*************************** End of file ****************************/
