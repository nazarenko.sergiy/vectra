

#define EEPROM_TEST_PAGE_START_ADD			(0x0004)

//extern void eeprom_init(void);
extern bool eeprom_write_byte(uint16_t, uint8_t);
extern bool eeprom_write_page(uint16_t, uint8_t *, uint16_t);
//extern bool eeprom_write_block(uint16_t, uint8_t *, uint16_t);
extern bool eeprom_read_byte(uint16_t, uint8_t *);
extern bool eeprom_read_page(uint16_t, uint8_t *, uint16_t);
//extern bool eeprom_read_block(uint16_t, uint8_t *, uint16_t);
