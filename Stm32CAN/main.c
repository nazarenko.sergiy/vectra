/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------
// https://github.com/libopencm3/libopencm3-examples/blob/master/examples/stm32/f4/stm32f429i-discovery/usart_console/usart_console.c
File    : main.c
Purpose : Generic application start
.. https://wiki.segger.com/Import_projects_from_STM32CubeMX_to_Embedded_Studio
*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

//#include "stm32f1xx.h"
//#include "stm32f10x.h"
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities

#include "vectra_lib.h"
#include "common.h"

#undef BLINK_EXAMPLE


#include "mcp2515.h"
#include "nmea_parse.h"
#define ONEWIRE_USART3 
#include "OneWire.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
/*********************************************************************
*
*       main()
*
*  Function description
*   Application entry point.
*/
/*
PA1 -> MOD2

PB0 -> MOD1
PB1 -> MOD0
*/
// https://interrupt.memfault.com/blog/noinit-memory


void vApplicationMallocFailedHook(void);
void vApplicationMallocFailedHook(void) {
  taskDISABLE_INTERRUPTS();
  for( ;; );
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName) {
  ( void ) pcTaskName;
  ( void ) xTask;
  taskDISABLE_INTERRUPTS();
  for( ;; );
}


#ifdef BLINK_EXAMPLE
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

static void toggle_task(void *args) 
{

  while (1) 
  {
        gpio_toggle(GPIOC, GPIO13);
        vTaskDelay(200);
  }
}


static TaskHandle_t h_Toggle_task;

int main(void) 
{
    rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
    // Enable clock GPIOC
    rcc_periph_clock_enable(RCC_GPIOC);
    //rcc_periph_clock_enable(RCC_GPIOA);

    // Configure GPIO13
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    //gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

    xTaskCreate(toggle_task, "Toggle", configMINIMAL_STACK_SIZE*3, (void*)NULL, configMAX_PRIORITIES-2, &h_Toggle_task);

    vTaskStartScheduler();

    while (1) 
    {

    }
}
#else

#define DALLAS_TASK

#define CAN_DS18x20_GET_BIT (1 << 0)
#define CAN_DS18x20_WAIT_BIT (1 << 1)
#define CAN_DS18x20_READY_BIT (1 << 2)


#define DS18x20_DATA_CAN_LENGTH 4

static uint8_t DS18x20_data[DS18x20_DATA_CAN_LENGTH] = { 0, };

static TaskHandle_t h_DS18x20_task;
static TaskHandle_t h_MCP2515_task;
static TaskHandle_t h_NMEA_task;

xQueueHandle GNSS_parser_Queue_handle;

#define	USER_DELIMITER_0	'\0'
#define	USER_DELIMITER_1	'\n'
#define	USER_DELIMITER_2	'\r'


#define V_CAN_ALT

enum CAN_data_t
{
  DATA_WAIT = 0,
  DATA_READY
} can_data;



struct can_tx_msg {
	uint32_t std_id;
	uint32_t ext_id;
	uint8_t ide;
	uint8_t rtr;
	uint8_t dlc;
	uint8_t data[8];
};

struct can_rx_msg {
	uint32_t std_id;
	uint32_t ext_id;
	uint8_t ide;
	uint8_t rtr;
	uint8_t dlc;
	uint8_t data[8];
	uint8_t fmi;
};

struct can_tx_msg can_tx_msg;
struct can_rx_msg can_rx_msg;

static TaskHandle_t  MCP2515_task_handle;


static void exti_setup(void)
{
	/* Enable GPIOA clock. */
	// Enabled in CAN setup

	/* Enable AFIO clock. */
	//rcc_periph_clock_enable(RCC_AFIO);

	/* Enable EXTI0 interrupt. */
	nvic_enable_irq(NVIC_EXTI9_5_IRQ);

	/* Set GPIO0 (in GPIO port A) to 'input open-drain'. */
	//gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO12);
        uint16_t data = gpio_port_read(GPIOA);
        data &= ~( 1UL << 8 );
        gpio_port_write(GPIOA, data);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI8, GPIOA);
	exti_direction = RISING;
	exti_set_trigger(EXTI8, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI8);
}

void exti9_5_isr(void)
{
	exti_reset_request(EXTI8);

        //    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
        //    vTaskNotifyGiveIndexedFromISR( MCP2515_scan_handle, 0, &xHigherPriorityTaskWoken );
        //    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

	if (exti_direction == FALLING) 
        {
            exti_direction = RISING;
            exti_set_trigger(EXTI8, EXTI_TRIGGER_RISING);
	} 
        else 
        {
            exti_direction = FALLING;
            exti_set_trigger(EXTI8, EXTI_TRIGGER_FALLING);
            
            uint16_t port = gpio_port_read(GPIOA);
            if ( (port >> 8) & 1UL)
            {
              //printf("got!\n");
              scb_reset_system();
             // NVIC_SystemReset();
            }
	}
}

#ifdef  DALLAS_TASK
typedef enum {
  DS18x20_START =0,
  DS18x20_PROLOG,
  DS18x20_READ,
  DS18x20_ERROR
} DS18x20_t;

DS18x20_t DS18x20_state;


OneWire ow;

void USART3_IRQHandler() 
{
  owReadHandler(USART3);
}



static void DS18x20_task(void *args) 
{
  DS18x20_state = DS18x20_PROLOG;
  ow.usart = USART3;

  uint8_t error_retries = 0;
  uint8_t devices;
  uint8_t current_device;
  RomCode *r;
  Temperature t;
  uint8_t static rx_data;
  uint32_t ulNotifiedValue;


  while(1)
  {
    WAIT:
    if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
    {
      while(error_retries < 60)
      {
          switch(DS18x20_state)
          {
              case DS18x20_PROLOG:
              {
                if (owResetCmd(&ow) != ONEWIRE_NOBODY)
                {
                  devices = owSearchCmd(&ow);
                  if (devices <= 0) 
                  {
                     DS18x20_state = DS18x20_PROLOG;
                     if ((error_retries++) > 60)
                     {
                      DS18x20_state = DS18x20_ERROR;
                     }
                     else
                     {
                      vTaskDelay(1000);
                     }
                  }
                  else
                  {
                    DS18x20_state = DS18x20_START;
                    error_retries = 0;
                    current_device = 0;
                  }
                }
                else
                {
                  DS18x20_state = DS18x20_PROLOG;
                  vTaskDelay(1000);
                }
                break;
              }
              case DS18x20_START:
              {
                    current_device++;
              	if (current_device >= devices)
                    {
                      current_device = 0;
                    }

                    r = &ow.ids[current_device];
                    uint8_t crc = owCRC8(r);

                    if (crc == r->crc) 
                    {
                       DS18x20_state = DS18x20_READ;
                    }
                    break;
              }
              case DS18x20_READ:
              {
                     t = readTemperature(&ow, &ow.ids[current_device], true);
                     //printf("DS18B20 , Temp: %d.%d C\r\n", t.inCelsus, t.frac);
                     DS18x20_data[0] = current_device;
                     DS18x20_data[1] = r->family;
                     DS18x20_data[2] = t.inCelsus;
                     DS18x20_data[3] = t.frac;
                     DS18x20_state = DS18x20_START;
                     xTaskNotify( h_MCP2515_task , CAN_DS18x20_READY_BIT, eSetBits);
                     goto WAIT;
                     break;
              }
              case DS18x20_ERROR:
              {
                break;
              }
              vTaskDelay(1000);
          }//switch
      }//while
    } // TaskWait
  } // while
  vTaskDelete(NULL);
}


static void dallas_setup(void)
{
      rcc_periph_clock_enable(RCC_GPIOB);
      rcc_periph_clock_enable(RCC_USART3);
      gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_10_MHZ,
                  GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_USART3_TX | GPIO_USART3_RX);
    //  AFIO_MAPR |= AFIO_MAPR_SWJ_CFG_FULL_SWJ_NO_JNTRST;
}

#endif

#define GPIO_8_MODE_INPUT GPIO_CNF_INPUT_PULL_UPDOWN

static void gpio_setup(void)
{
	///* Enable GPIOA clock. */
	rcc_periph_clock_enable(RCC_GPIOA); 

        
      rcc_periph_clock_enable(RCC_GPIOA);
      gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_8_MODE_INPUT, GPIO8);
   
	///* Enable GPIOB clock. */
	//rcc_periph_clock_enable(RCC_GPIOB);

	//gpio_set(GPIOA, GPIO6); /* LED0 off */
	//gpio_set(GPIOA, GPIO7); /* LED1 off */
	//gpio_set(GPIOB, GPIO0); /* LED2 off */
	//gpio_set(GPIOB, GPIO1); /* LED3 off */

	///* Set GPIO6/7 (in GPIO port A) to 'output push-pull' for the LEDs. */
	//gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO6);
	//gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO7);

	///* Set GPIO0/1 (in GPIO port B) to 'output push-pull' for the LEDs. */
	//gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO0);
	//gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
	//	      GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

      rcc_periph_clock_enable(RCC_GPIOC);
      gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
      gpio_set(GPIOC, GPIO13);

      
      rcc_periph_clock_enable(RCC_GPIOB);
      rcc_periph_clock_enable(RCC_AFIO);
      rcc_periph_clock_enable(RCC_USART1);
}

static void systick_setup(void)
{
	/* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	/* SysTick interrupt every N clock pulses: set reload to N-1 */
	systick_set_reload(8999);

	systick_interrupt_enable();

	/* Start counting. */
	systick_counter_enable();
}

static uint16_t nmea;

void get_data_from_uart1(uint8_t ch, BaseType_t * xHigherPriorityTaskWoken)
{
	static int i = 0;
        Message uart_buffer;
   
        /* check data length */
        if (i >= (USER_UART2_BUF_SZ - 1))
        {
            i = USER_UART2_BUF_SZ - 2;
        }

        if (ch == USER_DELIMITER_1 || ch == USER_DELIMITER_2)
        {
           if (i > 0)
           {
                uart_buffer.body[i++] = USER_DELIMITER_0;
 
                nmea = ( ((uint16_t)uart_buffer.body[5] << 8) | (uint16_t)uart_buffer.body[4] ) +
                          ( ((uint16_t)uart_buffer.body[3] << 8) | (uint16_t)uart_buffer.body[2] );

                switch(nmea)
                {
                        case GPRMC:
                        case GNRMC:
                        {
                            if(xQueueSendFromISR(GNSS_parser_Queue_handle, &uart_buffer, xHigherPriorityTaskWoken) != pdPASS)
                            {
                               //PRINTF("no queue space.\n");
    
                            }
                          break;
                         }
                         default:
                          break;
                }
           }
           i = 0;
        }
        else
        {
                uart_buffer.body[i++] = ch;
        }

}


void USART1_IRQHandler(void)
{
    static uint8_t data = 'A';
        BaseType_t xHigherPriorityTaskWoken;
        xHigherPriorityTaskWoken = pdFALSE;


    /* Check if we were called because of RXNE. */
    if (((USART_CR1(USART1) & USART_CR1_RXNEIE) != 0) &&
            ((USART_SR(USART1) & USART_SR_RXNE) != 0)) 
    {
            /* Indicate that we got data. */
          //  gpio_toggle(GPIOA, GPIO6);

            /* Retrieve the data from the peripheral. */
            data = usart_recv(USART1);

            get_data_from_uart1(data, &xHigherPriorityTaskWoken);

            /* Enable transmit interrupt so it sends back the data. */
         //  USART_CR1(USART1) |= USART_CR1_TXEIE;
    }

      //  if( xHigherPriorityTaskWoken == pdTRUE )
        {
            /* Actual macro used here is port specific. */
           portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
        //   taskYIELD();
        }

    /*
    // Check if we were called because of TXE. 
    if (((USART_CR1(USART1) & USART_CR1_TXEIE) != 0) &&
            ((USART_SR(USART1) & USART_SR_TXE) != 0)) {
            // Indicate that we are sending out data. 
            gpio_toggle(GPIOA, GPIO7);

            // Put data into the transmit register. 
            usart_send(USART1, data);

            // Disable the TXE interrupt as we don't need it anymore. 
            USART_CR1(USART1) &= ~USART_CR1_TXEIE;
    }
    */
}




static void usart1_setup(void)
{
       // NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );
       scb_set_priority_grouping(SCB_AIRCR_PRIGROUP_GROUP16_NOSUB);
       nvic_set_priority(NVIC_USART1_IRQ, configKERNEL_INTERRUPT_PRIORITY);

	/* Enable the USART1 interrupt. */
	nvic_enable_irq(NVIC_USART1_IRQ);

	/* Enable USART1 pin software remapping. */
	AFIO_MAPR |= AFIO_MAPR_USART1_REMAP;

	/* Setup GPIO pin GPIO_USART1_RE_TX on GPIO port B for transmit. */
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_RE_TX);

	/* Setup GPIO pin GPIO_USART1_RE_RX on GPIO port B for receive. */
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_FLOAT, GPIO_USART1_RE_RX);

	/* Setup UART parameters. */
	usart_set_baudrate(USART1, 9600);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
	usart_set_mode(USART1, USART_MODE_TX_RX);

	/* Enable USART1 Receive interrupt. */
	USART_CR1(USART1) |= USART_CR1_RXNEIE;

	/* Finally enable the USART. */
	usart_enable(USART1);
}


static void can_setup(void)
{
	/* Enable peripheral clocks. */
	rcc_periph_clock_enable(RCC_AFIO);

#ifdef 	V_CAN_ALT
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_CAN1);
#else
      	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_CAN);
#endif	
      
#ifdef 	V_CAN_ALT
        AFIO_MAPR |= AFIO_MAPR_CAN1_REMAP_PORTB;
 
	/* Configure CAN pin: RX (input pull-up). */
	gpio_set_mode(GPIO_BANK_CAN1_PB_RX, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN1_PB_RX);
	gpio_set(GPIO_BANK_CAN1_PB_RX, GPIO_CAN1_PB_RX);

	/* Configure CAN pin: TX. */
	gpio_set_mode(GPIO_BANK_CAN1_PB_TX, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN1_PB_TX);
#else
	gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN_RX);
	gpio_set(GPIOA, GPIO_CAN_RX);
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN_TX);
#endif	

	/* NVIC setup. */
	nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);
	nvic_set_priority(NVIC_USB_LP_CAN_RX0_IRQ, 1);

	/* Reset CAN. */
	can_reset(CAN1);
// APB1 ? ??? ???????? ?? ??????? 36???,
  //????????????? ????????? ????????? ???????? ?????????
  //BaudRate= 1000  BRP=  4  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 500   BRP=  9  SJW = 1  BS1= 3 BS2= 4
  //BaudRate= 250   BRP= 16  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 125   BRP= 32  SJW = 1  BS1= 4 BS2= 4
	/* CAN cell init. */
	if (can_init(CAN1,
		     false,           /* TTCM: Time triggered comm mode? */
		     true,            /* ABOM: Automatic bus-off management? */
		     false,           /* AWUM: Automatic wakeup mode? */
		     false,           /* NART: No automatic retransmission? */
		     false,           /* RFLM: Receive FIFO locked mode? */
		     false,           /* TXFP: Transmit FIFO priority? */
		     CAN_BTR_SJW_1TQ,
		     CAN_BTR_TS1_3TQ,
		     CAN_BTR_TS2_4TQ,
		     9,
		     false,
		     false))             /* BRP+1: Baud rate prescaler */
	{
		//gpio_set(GPIOA, GPIO6);		/* LED0 off */
		//gpio_set(GPIOA, GPIO7);		/* LED1 off */
		//gpio_set(GPIOB, GPIO0);		/* LED2 off */
		//gpio_clear(GPIOB, GPIO1);	/* LED3 on */

		/* Die because we failed to initialize. */
		while (1)
			__asm__("nop");
	}

	/* CAN filter 0 init. */
	can_filter_id_mask_32bit_init(
				0,     /* Filter ID */
				0,     /* CAN ID */
				0,     /* CAN ID mask */
				0,     /* FIFO assignment (here: FIFO0) */
				true); /* Enable the filter. */

	/* Enable CAN RX interrupt. */
	can_enable_irq(CAN1, CAN_IER_FMPIE0);
}

static void Nmea_task(void *args) 
{
    Message msg;
    GPRMC_Message message;
    GNSS_parser_Queue_handle = xQueueCreate(2,sizeof(Message)); /* Create a queue */
    while(1) 
    {
        if(xQueueReceive(GNSS_parser_Queue_handle, &msg, 0) == pdTRUE) 
        {
           // vTaskDelay(5);
          //  
            GxRMC_parser(&message, msg.body);
            printf("get %d\r\n", message.PosStatus);
          //  ;
        }
        vTaskDelay(10);
    }
}

uint8_t static rx_data;

static void CAN_tansmit_task(void *args) 
{
   uint8_t data[3] = { 1, 5, 0 };
   uint8_t temp32 = 0;
   uint8_t trr = 0;
   rx_data = 0;
   while (1) 
   {
       /* Transmit CAN frame. */
	data[2] = rx_data;
        temp32++ ;
        if (temp32 >= 3)
        {
          temp32 = 0;
          if (trr == 0)
          {
          trr = 1;
          data[0] = 5;
          data[1] = 1;
          }
          else
          {
          trr = 0;
          data[0] = 1;
          data[1] = 5;
          }
        
        }
        int8_t res = can_transmit(CAN1,
			 0x123,     /* (EX/ST)ID: CAN ID */
			 false, /* IDE: CAN ID extended? */
			 false, /* RTR: Request transmit? */
			 3,     /* DLC: Data length */
			 data);
	if (res == -1)
	{
		//gpio_set(GPIOA, GPIO6);		/* LED0 off */
		//gpio_set(GPIOA, GPIO7);		/* LED1 off */
		//gpio_clear(GPIOB, GPIO0);	/* LED2 on */
		//gpio_set(GPIOB, GPIO1);		/* LED3 off */ 
	
	}
        else if (res == 0)
        {
          gpio_toggle(GPIOC, GPIO13);
        }

    vTaskDelay(500);
  }// while
}

static void CAN_receive_task(void *args) {
  uint32_t id;
  bool ext, rtr;
  uint8_t fmi, length, data[8];
  can_data = DATA_WAIT;
  while (1) 
  {
      if (can_data == DATA_READY )
      {
        //gpio_toggle(GPIOC, GPIO13);
	can_receive(CAN1, 0, false, &id, &ext, &rtr, &fmi, &length, data, NULL);
        rx_data = data[0]; 

	can_fifo_release(CAN1, 0);

        can_data = DATA_WAIT;
        can_enable_irq(CAN1, CAN_IER_FMPIE0);
      }// if 
      vTaskDelay(10);
  } // while
}



static void MCP2515_task(void *args) 
{
  uint32_t ulNotifiedValue;
  uint8_t ret ;
  byte len = 0;
  unsigned long can_id = 0;
  byte data[8] = {2, 3, 4, 5, 6, 7, 8, 9};

  mcp2515_setup( MCP_16MHz, CAN_33KBPS ); // ?

#ifdef  DALLAS_TASK
  xTaskNotify(h_DS18x20_task, CAN_DS18x20_GET_BIT, eSetBits);
#endif

  while (1) 
  {

    
    //ret = sendMsgBuf2(0x123, 8, buf, 0);
    //printf(" SPI Sent Byte: %i\r\n", ret);
    vTaskDelay(100);
#ifdef DALLAS_TASK
    if(xTaskNotifyWait(0xffffffff, 0xffffffff, &ulNotifiedValue, 0) == pdTRUE)
    {
        if (ulNotifiedValue & CAN_DS18x20_READY_BIT)
        {
            ret = sendMsgBuf2(0x124, 4, DS18x20_data, 0);
            xTaskNotify(h_DS18x20_task, CAN_DS18x20_GET_BIT, eSetBits);
        }
    }
#else
if(xTaskNotifyWait(0xffffffff, 0xffffffff, &ulNotifiedValue, 0) == pdTRUE)
    {
        readMsgBuf(&can_id, &len, data);
        rx_data = data[0];
    }
#endif

  }
}


void usb_lp_can_rx0_isr(void)
{
  can_disable_irq(CAN1, CAN_IER_FMPIE0);
  can_data = DATA_READY;
}

#undef  SPI_TEST

#ifdef SPI_TEST
#include <libopencm3/stm32/spi.h>



static void spi1_test_task(void *args) 
{
  uint32_t ulNotifiedValue;
  int counter = 0;
  uint16_t rx_value = 0x42;

  while (1) 
  {
    printf("Counter: %i  SPI Sent Byte: %i", counter, (uint8_t) counter);
    /* blocking send of the byte out SPI1 */
    spi_send(SPI1, (uint8_t) counter);

    /* Read the byte that just came in (use a loopback between MISO and MOSI
     * to get the same byte back)
     */
    rx_value = spi_read(SPI1);
    /* printf the byte just received */
    printf("    SPI Received Byte: %i\r\n", rx_value);
    vTaskDelay(100);
    counter++;
  }
}



static void clock_setup1(void)
{
	rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);

	/* Enable GPIOA, GPIOB, GPIOC clock. */
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOC);

	/* Enable clocks for GPIO port A (for GPIO_USART2_TX) and USART2. */
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_AFIO);
	rcc_periph_clock_enable(RCC_USART1);
	rcc_periph_clock_enable(RCC_USART2);

	/* Enable SPI1 Periph and gpio clocks */
	rcc_periph_clock_enable(RCC_SPI1);

}

static void spi_setup1(void) {

  /* Configure GPIOs: SS=PA4, SCK=PA5, MISO=PA6 and MOSI=PA7 */
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
            GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO4 |
            				    GPIO5 |
                                            GPIO7 );

  gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT,
          GPIO6);


   gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO4);

  /* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
  spi_reset(SPI1);

  /* Set up SPI in Master mode with:
   * Clock baud rate: 1/64 of peripheral clock frequency
   * Clock polarity: Idle High
   * Clock phase: Data valid on 2nd clock pulse
   * Data frame format: 8-bit
   * Frame format: MSB First
   */
  spi_init_master(SPI1, SPI_CR1_BAUDRATE_FPCLK_DIV_64, SPI_CR1_CPOL_CLK_TO_1_WHEN_IDLE,
                  SPI_CR1_CPHA_CLK_TRANSITION_2, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);

  /*
   * Set NSS management to software.
   *
   * Note:
   * Setting nss high is very important, even if we are controlling the GPIO
   * ourselves this bit needs to be at least set to 1, otherwise the spi
   * peripheral will not send any data out.
   */
  spi_enable_software_slave_management(SPI1);
  spi_set_nss_high(SPI1);

  /* Enable SPI1 periph. */
  spi_enable(SPI1);
}



int main(void)
{
      clock_setup1();
      // spi_setup1();
      usart1_setup();
#ifdef  DALLAS_TASK
      dallas_setup();
#endif
      xTaskCreate(MCP2515_task, "MCP", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, &h_MCP2515_task);
#ifdef  DALLAS_TASK
      xTaskCreate(DS18x20_task, "DS18", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-3, &h_DS18x20_task);
#endif

      xTaskCreate(Nmea_task, "Nmea", configMINIMAL_STACK_SIZE*3, (void*)NULL, configMAX_PRIORITIES-2, &h_NMEA_task);

      vTaskStartScheduler();

      while(1) 
      {

      }

      return 0;
}


#else


/* variable blinking interval for LED when in bootloader mode */
uint16_t blink_interval = BOOTLOADER_BLINK_INTERVAL;

/* blink timer ISR */
void tim4_isr(void) 
{
	gpio_toggle(GPIOC, GPIO13);
	TIM_SR(TIM4) &= ~TIM_SR_UIF;
}

/* set up LED blinking timer and pin */
static void setup_blink(void) 
{
        rcc_periph_clock_enable(RCC_TIM4);
	RCC_APB1RSTR |= RCC_APB1RSTR_TIM4RST;
	RCC_APB1RSTR &= ~RCC_APB1RSTR_TIM4RST;
	TIM_PSC(TIM4) = 0xFFFF;
	TIM_ARR(TIM4) = blink_interval;
	TIM_DIER(TIM4) |= TIM_DIER_UIE;

	nvic_enable_irq(NVIC_TIM4_IRQ);

	TIM_CR1(TIM4) |= TIM_CR1_CEN;
}

void tim3_isr(void);

static void toggle_task(void *args) 
{

  while (1) 
  {
        gpio_toggle(GPIOC, GPIO13);
        vTaskDelay(200);
  }
}



int main(void)
{
      rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);

      rcc_periph_clock_enable(RCC_GPIOA);
      gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_8_MODE_INPUT, GPIO8);
      rcc_periph_clock_enable(RCC_GPIOC);
      gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
      gpio_set(GPIOC, GPIO13);
      exti_setup();
      setup_blink();
      setup_tim3();

      xTaskCreate(toggle_task, "Led", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, &h_DS18x20_task);

      vTaskStartScheduler();


        while(1)
        {
        /*
                gpio_toggle(GPIOC, GPIO13);

                for (int i = 0; i < 150000; i++) {	
        			__asm__("nop");
        		}
  */
        }

        /*
      gpio_setup();
      can_setup();
      systick_setup();
      exti_setup();
       #ifdef  DALLAS_TASK
      dallas_setup();
#endif
      xTaskCreate(CAN_tansmit_task, "CAN_TX", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      xTaskCreate(CAN_receive_task, "CAN_RX", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      xTaskCreate(MCP2515_task, "MCP", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, &MCP2515_task_handle);
 #ifdef  DALLAS_TASK
      xTaskCreate(DS18x20_task, "DS18", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-3, &h_DS18x20_task);
#endif
      vTaskStartScheduler();
*/
      //while(1) 
      //{

      //}
      return 0;
}

#endif

#endif // BLINK_EXAMPLE
/*********************************************************************
*
*       vApplicationGetIdleTaskMemory()
*
*/
#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize ) {
  /* If the buffers to be provided to the Idle task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xIdleTaskTCB;
  static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

  /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
  state will be stored. */
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

  /* Pass out the array that will be used as the Idle task's stack. */
  *ppxIdleTaskStackBuffer = uxIdleTaskStack;

  /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
#endif

/*********************************************************************
*
*       vApplicationGetTimerTaskMemory()
*
*/
/*-----------------------------------------------------------*/

#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize ) {
  /* If the buffers to be provided to the Timer task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xTimerTaskTCB;
  static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

  /* Pass out a pointer to the StaticTask_t structure in which the Timer
  task's state will be stored. */
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

  /* Pass out the array that will be used as the Timer task's stack. */
  *ppxTimerTaskStackBuffer = uxTimerTaskStack;

  /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
#endif

//void HardFault_Handler(void) {

// _Continue = 0u;
// //
// // When stuck here, change the variable value to != 0 in order to step out
// //
// while (_Continue == 0u);
//}

/*************************** End of file ****************************/


