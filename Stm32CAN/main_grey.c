/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------

File    : main.c
Purpose : Generic application start

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include <time.h>

#include "stm32f1xx.h"
#include "stm32f10x.h"
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/f1/bkp.h>
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities
#include "common.h"
#include "vectra_lib.h"

/*********************************************************************
*
*       main()
*
*  Function description
*   Application entry point.
*/
/*
PA1 -> MOD2

PB0 -> MOD1
PB1 -> MOD0
*/

void vApplicationMallocFailedHook(void);
void vApplicationMallocFailedHook(void) {
  taskDISABLE_INTERRUPTS();
  for( ;; );
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName) {
  ( void ) pcTaskName;
  ( void ) xTask;
  taskDISABLE_INTERRUPTS();
  for( ;; );
}


#define V_CAN_ALT


static BaseType_t xHigherPriorityTaskCan1;

uint16_t  tc_inst[] = {600,600,600,600,600,600,600,600,600
    ,600,600,600,600,600,600,600,600,600,600,600
    ,600,600,600,600,600,600,600,600,600,600,600
    ,600,600,600,600,600,600,600,600,600,600,560
    ,520,514,508,502,496,490,485,479,473,467,461
    ,455,450,444,439,433,428,422,417,412,406,401
    ,395,390,385,379,344,368,363,357,352,347,341
    ,336,330,325,320,313,307,301,295,289,283,277
    ,271,265,258,252,246,240,234,228,222,216,210
    ,204,198,192,186,180,175,169,163,157,151,145
    ,140,133,127,120,114,107,101,95,88,82,75,69
    ,62,56,50,42,35,28,21,14,7};

#define N (256)

typedef struct TC_Data 
{
    int in;
    int out;
    int summa;
    int b_summa;
    int buffer[N];
} TC_Data;

static TC_Data tc_data;

int enqueue(int value) 
{ 
    int k = (tc_data.in++) & (N-1);
    int all = ((tc_data.b_summa == 1) ? N : ((k == 0) ? 1 : k)); 

    tc_data.summa -= tc_data.buffer[ k ];
    tc_data.buffer[ k ] = value;
    
    if (tc_data.in == N) 
         tc_data.b_summa = 1;
 
    tc_data.summa += value;
    return tc_data.summa/all;
}


#define GPIO_8_MODE_INPUT GPIO_CNF_INPUT_FLOAT



static BaseType_t xHigherPriorityTaskWoken;

void exti15_10_isr(void)
{
      mcp2515_disable_interrupts();
      exti_reset_request(EXTI12);
      gpio_set(GPIOB, GPIO12);

  if (CAN33_scan_handle != NULL && (task_run_flag & SCAN_BIT))
  {
      vTaskNotifyGiveFromISR( CAN33_scan_handle, &xHigherPriorityTaskWoken );
      portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
  }

  if (CAN33_dump_handle != NULL && (task_run_flag & DUMP_BIT))
  {
      vTaskNotifyGiveFromISR( CAN33_dump_handle, &xHigherPriorityTaskWoken );
      portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
  }
}


void tim4_isr(void) 
{
  gpio_clear(GPIOB, GPIO14);
  timer_disable_counter(TIM4);
  TIM_SR(TIM4) &= ~TIM_SR_UIF;
}

/* set up LED blinking timer and pin */
static void setup_shutdown_xu4(void) 
{
        rcc_periph_clock_enable(RCC_TIM4);

	RCC_APB1RSTR |= RCC_APB1RSTR_TIM4RST;
	RCC_APB1RSTR &= ~RCC_APB1RSTR_TIM4RST;

        // Set timer start value
        TIM_CNT(TIM4) = 1;

        // Set timer prescaler 72/Mhz/65536 => ~1099 counts per second
	TIM_PSC(TIM4) = 0xFFFF;

        // End timer value. If this reached an interrupt is generated
	TIM_ARR(TIM4) = 10000;

	TIM_DIER(TIM4) |= TIM_DIER_UIE;

	nvic_set_priority(NVIC_TIM4_IRQ, BLOCK_heartbeat_irq_PRIORITY);
	nvic_enable_irq(NVIC_TIM4_IRQ);

}

static void gpio_setup(void)
{
      rcc_periph_clock_enable(RCC_GPIOA);
      gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_8_MODE_INPUT, GPIO8);
      gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);
      rcc_periph_clock_enable(RCC_GPIOC);
      gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
      gpio_set(GPIOC, GPIO13);

      rcc_periph_clock_enable(RCC_GPIOB);
      gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO14);

      gpio_set(GPIOB, GPIO14); 
}


static void systick_setup(void)
{
	/* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	/* SysTick interrupt every N clock pulses: set reload to N-1 */
	systick_set_reload(8999);

	systick_interrupt_enable();

	/* Start counting. */
	systick_counter_enable();
}


static void can_setup(void)
{
	/* Enable peripheral clocks. */
	rcc_periph_clock_enable(RCC_AFIO);

	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_CAN1);

        AFIO_MAPR |= AFIO_MAPR_CAN1_REMAP_PORTB;
 
	/* Configure CAN pin: RX (input pull-up). */
	gpio_set_mode(GPIO_BANK_CAN1_PB_RX, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN1_PB_RX);
	gpio_set(GPIO_BANK_CAN1_PB_RX, GPIO_CAN1_PB_RX);

	/* Configure CAN pin: TX. */
	gpio_set_mode(GPIO_BANK_CAN1_PB_TX, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN1_PB_TX);

	/* NVIC setup. */
	nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);
	nvic_set_priority(NVIC_USB_LP_CAN_RX0_IRQ, CAN_rx_irq_PRIORITY);

	/* Reset CAN. */
	can_reset(CAN1);
// APB1 ? ??? ???????? ?? ??????? 36???,
  //????????????? ????????? ????????? ???????? ?????????
  //BaudRate= 1000  BRP=  4  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 500   BRP=  9  SJW = 1  BS1= 3 BS2= 4
  //BaudRate= 250   BRP= 16  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 125   BRP= 32  SJW = 1  BS1= 4 BS2= 4
	/* CAN cell init. */
	if (can_init(CAN1,
		     false,           /* TTCM: Time triggered comm mode? */
		     true,            /* ABOM: Automatic bus-off management? */
		     false,           /* AWUM: Automatic wakeup mode? */
		     false,           /* NART: No automatic retransmission? */
		     false,           /* RFLM: Receive FIFO locked mode? */
		     false,           /* TXFP: Transmit FIFO priority? */
		     CAN_BTR_SJW_1TQ,
		     CAN_BTR_TS1_3TQ,
		     CAN_BTR_TS2_4TQ,
		     9,
		     false,
		     false))             /* BRP+1: Baud rate prescaler */
	{
		/* Die because we failed to initialize. */
		while (1)
			__asm__("nop");
	}

	/* CAN filter 0 init. */
	can_filter_id_mask_32bit_init(
				0,     /* Filter ID */
				0,     /* CAN ID */
				0,     /* CAN ID mask */
				0,     /* FIFO assignment (here: FIFO0) */
				true); /* Enable the filter. */

	/* Enable CAN RX interrupt. */
	can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
}



static void CAN33_scan_task(void *args) 
{
  uint32_t ulNotifiedValue;
  unsigned char len = 0;
  unsigned char data[8];
  unsigned long frame_id;

  while (1) 
  {
    if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
    {
        readMsgBuf(&frame_id, &len, data);
        mcp2515_enable_interrupts();
        switch (frame_id)
        {
            case 0x350:
            case 0x23A:
            case 0x145:
            case 0x260:
            case 0x108:
            {                
                CAN_Message current_message;
                current_message.frame_id = frame_id;
                current_message.frame_lenght = len;
                current_message.repeat = 1;
                memcpy( current_message.body, data, len);

                if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                {
                  // xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                }
                
                break;
            }
            case 0x130:
            {
                int val = 0;
                uint8_t i = data[3];
                if (i < 0x81) val = tc_inst[ i ];

                int val2 = enqueue(val);
                
                CAN_Message current_message;
                current_message.frame_id = ID_TC_INST_TIME;
                current_message.frame_lenght = 4;
                current_message.repeat = 1;
                current_message.body[0] = val & 0xFF;
                current_message.body[1] = ( val >> 8 ) & 0xFF;
                current_message.body[2] = val2 & 0xFF;
                current_message.body[3] = ( val2 >> 8 ) & 0xFF;
                
                if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                {
                   xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                }

                break;
            }
        } // switch
    }
  }
  vTaskDelete(NULL);
}


static void CAN33_dump_task(void *args)
{
  unsigned char len = 0;
  unsigned char data[8];
  unsigned long frame_id;
  uint32_t ulNotifiedValue;

  while (1) 
  {
    if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
    {
        readMsgBuf(&frame_id, &len, data);
        mcp2515_enable_interrupts();

        CAN_Message current_message;
        current_message.frame_id = frame_id;
        current_message.repeat = 1;
        current_message.frame_lenght = len;

        memcpy(current_message.body, data, sizeof(uint8_t)*len);
        if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
        {
           xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
        }
    }
  }
  vTaskDelete(NULL);
}


static void XU4_receive_task(void *args) 
{
    uint32_t id;
    bool ext, rtr;
    uint8_t fmi, length, data[8];
    uint32_t ulNotifiedValue;

    this_name.fields.name = STM_GRAY;

    while (1) 
    { 
       if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
       {
          can_receive(CAN1, 0, false, &id, &ext, &rtr, &fmi, &length, data, NULL);
  
          switch(id)
          {
            case ID_SHUTDOWN_XU4_ID:
                if ( (data[0] >> 0) & 1UL )
                {   
                  timer_enable_counter(TIM4);
                }
              break;
            case ID_CMD:
              if(( BIT_CMD_GRAY(data) & 1UL ) || (BIT_CMD_BROADCAST(data) & 1UL))
              {
                uint8_t ret = 0x0;
                if ( BIT_CMD_LINE_HI(data) & 1UL )
                {           
                  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO8);
                  gpio_set(GPIOA, GPIO8);
                  ret |= (1 << 0);
                }
                if ( BIT_CMD_LINE_LO(data) & 1UL )
                {          
                  gpio_clear(GPIOA, GPIO8); 
                  gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO8);
                  ret |= (1 << 1);
                }
                if ( BIT_CMD_LINE_RESET(data) & 1UL )
                {
                  scb_reset_system();
                }
                if ( BIT_CMD_PRINT_GIT(data) & 1UL )
                {
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;
                  uint8_t * body_ptr = current_message.body;

                  if (BIT_CMD_BROADCAST(data) & 1UL)
                  {
                    body_ptr++;
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                      epoch.big = GITTIMESTAMP;
                      this_name.fields.mode = MODE_FIRMWARE_GIT_TS;
                      current_message.body[0] = this_name.val;
                      body_ptr+=1;
                      memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                    }
                   
                    else
                    {
                      this_name.fields.mode = MODE_FIRMWARE_GIT_HASH;
                      current_message.body[0] = this_name.val;
                      memcpy(body_ptr, git_hash_data, sizeof(uint8_t)*7);
                    }
                  }
                  else
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                      epoch.big = GITTIMESTAMP;
                      this_name.fields.mode = MODE_FIRMWARE_GIT_TS;
                      current_message.body[0] = this_name.val;
                      body_ptr+=2;
                      memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                    }
                    else
                    {
                      memcpy(body_ptr, git_hash_data, sizeof(uint8_t)*8);
                    }
                  }

                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                     xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                  }
                }
               if (BIT_CMD_PRINT_BUILD_TS(data) & 1UL)
                {
                  epoch.big = UNIX_TIMESTAMP;
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;
                  
                  uint8_t * body_ptr = current_message.body;

                  this_name.fields.mode = MODE_FIRMWARE_BUILD_TS;
                  current_message.body[0] = this_name.val;
                  body_ptr+=2;
                  memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                     xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                  }
                }
                if ( BIT_CMD_BL_GIT(data) & 1UL )
                {
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;

                  if (BIT_CMD_BROADCAST(data) & 1UL)
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_TS;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = 0x0;
                        current_message.body[2] = BKP_DR7 >> 8;
                        current_message.body[3] = BKP_DR7 & 0x00FF;
                        current_message.body[4] = BKP_DR8 >> 8;
                        current_message.body[5] = BKP_DR8 & 0x00FF;
                        current_message.body[6] = BKP_DR9 >> 8;
                        current_message.body[7] = BKP_DR9 & 0x00FF;
                    }
                    else
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_HASH;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = BKP_DR3 >> 8;
                        current_message.body[2] = BKP_DR3 & 0x00FF;
                        current_message.body[3] = BKP_DR4 >> 8;
                        current_message.body[4] = BKP_DR4 & 0x00FF;
                        current_message.body[5] = BKP_DR5 >> 8;
                        current_message.body[6] = BKP_DR5 & 0x00FF;
                        current_message.body[7] = BKP_DR6 >> 8;
                    }
                  }
                  else
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_TS;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = 0x0;
                        current_message.body[2] = BKP_DR7 >> 8;
                        current_message.body[3] = BKP_DR7 & 0x00FF;
                        current_message.body[4] = BKP_DR8 >> 8;
                        current_message.body[5] = BKP_DR8 & 0x00FF;
                        current_message.body[6] = BKP_DR9 >> 8;
                        current_message.body[7] = BKP_DR9 & 0x00FF;
                    }
                    else
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_HASH;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = BKP_DR3 >> 8;
                        current_message.body[2] = BKP_DR3 & 0x00FF;
                        current_message.body[3] = BKP_DR4 >> 8;
                        current_message.body[4] = BKP_DR4 & 0x00FF;
                        current_message.body[5] = BKP_DR5 >> 8;
                        current_message.body[6] = BKP_DR5 & 0x00FF;
                        current_message.body[7] = BKP_DR6 >> 8;
                    }
                  }

                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                  //   xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
                  }
                }
                if ( BIT_CMD_CAN_DUMP(data) & 1UL )
                {
                    ret |= (1 << 2);
                    vTaskSuspend(CAN33_scan_handle);
                    task_run_flag &=~ SCAN_BIT;
                    task_run_flag |= DUMP_BIT;
                    vTaskDelay(1);
                    vTaskResume(CAN33_dump_handle);
                }
                if ( BIT_CMD_CAN_SCAN(data) & 1UL )
                {
                    ret |= (1 << 3);

                    vTaskSuspend(CAN33_dump_handle);
                    task_run_flag &=~ DUMP_BIT;
                    task_run_flag |= SCAN_BIT;
                    vTaskDelay(1);
                    vTaskResume(CAN33_scan_handle);
                   
                }
                if ( BIT_CMD_CAN_SKIP(data) & 1UL )
                {
                    ret |= (1 << 4);
                    task_run_flag = 0x00;
                    vTaskSuspend(CAN33_dump_handle);
                    vTaskSuspend(CAN33_scan_handle);
                    vTaskDelay(1);
                }

                if ( (data[7] >> 0) & 1UL )
                {
                    ret |= (1 << 5);
                    gpio_set(GPIOA, GPIO1); 
                }

                if ( (data[7] >> 1) & 1UL )
                {
                    ret &= ~(1 << 5);
                    gpio_clear(GPIOA, GPIO1); 
                }

                vTaskGetInfo( CAN33_scan_handle, &xDumpTaskDetails, pdTRUE, eInvalid );
                vTaskGetInfo( CAN33_dump_handle, &xScanTaskDetails, pdTRUE, eInvalid );

                CAN_Message current_message;
                current_message.frame_id = ID_CMD_RESPONSE;
                current_message.frame_lenght = 8;
                current_message.repeat = 5;
                current_message.body[0] = ret;
                current_message.body[1] = xScanTaskDetails.eCurrentState;
                current_message.body[2] = xDumpTaskDetails.eCurrentState;
                current_message.body[4] = ( mcp_errors & 0x000000ff );
                current_message.body[5] = ( mcp_errors & 0x0000ff00 ) >> 8;
                current_message.body[6] = ( mcp_errors & 0x00ff0000 ) >> 16;
                current_message.body[7] = ( mcp_errors & 0xff000000 ) >> 24;

                if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                {
                  // xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                }
              } 
              break;
            default:
              break;
        } // switch
          
	can_fifo_release(CAN1, 0);
        can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
      }
    }
    vTaskDelete(NULL);
}


static void XU4_transmit_task(void *args) 
{
  uint32_t ulNotifiedValue;
  CAN_Message current_message;
  
  while (1) 
  {  
    if (XU4_Queue_handle != NULL)
    {
        if(xQueueReceive(XU4_Queue_handle, &current_message, 10) == pdPASS)
        {
            while(current_message.repeat > 0)
            {
              current_message.repeat--;
              can_transmit(CAN1,
                       current_message.frame_id,     /* (EX/ST)ID: CAN ID */
                       false, /* IDE: CAN ID extended? */
                       false, /* RTR: Request transmit? */
                       current_message.frame_lenght,     /* DLC: Data length */
                       current_message.body);
              vTaskDelay(1);
            }
      }
    }
  }//while
}


void usb_lp_can_rx0_isr(void)
{
  can_disable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
  vTaskNotifyGiveFromISR( XU4_receive_task_handle, &xHigherPriorityTaskCan1 );
  portYIELD_FROM_ISR( xHigherPriorityTaskCan1 );
}


static void CAN33_init(void *args)
{
    while ( mcp2515_setup( MCP_8MHz, CAN_33KBPS ) != CAN_OK )
    {
      mcp_errors++;
      vTaskDelay(1000);
    }
    
    xTaskCreate(CAN33_scan_task, "CAN33 scan", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_scan_PRIORITY, &CAN33_scan_handle);
    xTaskCreate(CAN33_dump_task, "CAN33 dump", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_dump_PRIORITY, &CAN33_dump_handle);

    vTaskSuspend(CAN33_dump_handle);
      
    vTaskDelete(NULL);
}


void tim3_isr(void);


int main(void)
{
      rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
      gpio_setup();
      can_setup();
      systick_setup();
      setup_shutdown_xu4();
      mcp2515_exti_setup();
      setup_tim3();

      task_run_flag |= SCAN_BIT;

      XU4_Queue_handle = xQueueCreate(2,sizeof(CAN_Message)); /* Create a queue */

      xTaskCreate(XU4_transmit_task, "XU4 tx", configMINIMAL_STACK_SIZE, (void*)NULL, CAN_transmit_PRIORITY, &XU4_transmit_task_handle);
      xTaskCreate(XU4_receive_task, "XU4 rx", configMINIMAL_STACK_SIZE, (void*)NULL, CAN_receive_PRIORITY, &XU4_receive_task_handle);
      xTaskCreate(CAN33_init, "CAN33 init", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      vTaskStartScheduler();

      while(1) 
      {

      }
      return 0;
}


/*********************************************************************
*
*       vApplicationGetIdleTaskMemory()
*
*/
#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize ) {
  /* If the buffers to be provided to the Idle task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xIdleTaskTCB;
  static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

  /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
  state will be stored. */
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

  /* Pass out the array that will be used as the Idle task's stack. */
  *ppxIdleTaskStackBuffer = uxIdleTaskStack;

  /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
#endif

/*********************************************************************
*
*       vApplicationGetTimerTaskMemory()
*
*/
/*-----------------------------------------------------------*/

#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize ) {
  /* If the buffers to be provided to the Timer task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xTimerTaskTCB;
  static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

  /* Pass out a pointer to the StaticTask_t structure in which the Timer
  task's state will be stored. */
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

  /* Pass out the array that will be used as the Timer task's stack. */
  *ppxTimerTaskStackBuffer = uxTimerTaskStack;

  /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
#endif

//void HardFault_Handler(void) {

// _Continue = 0u;
// //
// // When stuck here, change the variable value to != 0 in order to step out
// //
// while (_Continue == 0u);
//}

/*************************** End of file ****************************/
