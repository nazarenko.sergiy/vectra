/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------
// https://github.com/libopencm3/libopencm3-examples/blob/master/examples/stm32/f4/stm32f429i-discovery/usart_console/usart_console.c
File    : main.c
Purpose : Generic application start
.. https://wiki.segger.com/Import_projects_from_STM32CubeMX_to_Embedded_Studio
*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
//#include "stm32f1xx.h"
//#include "stm32f10x.h"
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/f1/bkp.h>
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities

#include "vectra_lib.h"
#include "mcp2515.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "common.h"
/*********************************************************************
*
*       main()
*
*  Function description
*   Application entry point.
*/
/*
PA1 -> MOD2

PB0 -> MOD1
PB1 -> MOD0
*/




void vApplicationMallocFailedHook(void);
void vApplicationMallocFailedHook(void) {
  taskDISABLE_INTERRUPTS();
  for( ;; );
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName) {
  ( void ) pcTaskName;
  ( void ) xTask;
  taskDISABLE_INTERRUPTS();
  for( ;; );
}


#undef  BLINK_EXAMPLE

#ifdef BLINK_EXAMPLE
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

static void toggle_task(void *args) 
{

  while (1) 
  {
        gpio_toggle(GPIOC, GPIO13);
        vTaskDelay(200);
  }
}


static TaskHandle_t h_Toggle_task;

int main(void) 
{
    rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
    // Enable clock GPIOC
    rcc_periph_clock_enable(RCC_GPIOC);
    //rcc_periph_clock_enable(RCC_GPIOA);

    // Configure GPIO13
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    //gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

    xTaskCreate(toggle_task, "Toggle", configMINIMAL_STACK_SIZE*3, (void*)NULL, configMAX_PRIORITIES-2, &h_Toggle_task);

    vTaskStartScheduler();

    while (1) 
    {

    }
}
#else

static BaseType_t xHigherPriorityTaskCan1;


#define V_CAN_ALT

static uint8_t rx_id;
uint8_t static rx_data;


xQueueHandle CAN_Queue_handle;
xQueueHandle XU4_Queue_handle;


void usb_lp_can_rx0_isr(void)
{
  can_disable_irq(CAN1, CAN_IER_FMPIE0);

  if (CAN_scan_task_handle != NULL && (task_run_flag & SCAN_BIT))
  {
    vTaskNotifyGiveFromISR( CAN_scan_task_handle, &xHigherPriorityTaskCan1 );
    portYIELD_FROM_ISR( xHigherPriorityTaskCan1 );
  }

  if (CAN_dump_task_handle != NULL && (task_run_flag & DUMP_BIT))
  {
    vTaskNotifyGiveFromISR( CAN_dump_task_handle, &xHigherPriorityTaskCan1 );
    portYIELD_FROM_ISR( xHigherPriorityTaskCan1 );
  }
}

static BaseType_t xHigherPriorityTaskWoken;

void exti15_10_isr(void)
{
      mcp2515_disable_interrupts();
      exti_reset_request(EXTI12);
      gpio_set(GPIOB, GPIO12);

      vTaskNotifyGiveFromISR( XU4_receive_task_handle, &xHigherPriorityTaskWoken );
      portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

static void exti_setup(void)
{
	/* Enable GPIOA clock. */
	//rcc_periph_clock_enable(RCC_GPIOA);

	/* Enable AFIO clock. */
	//rcc_periph_clock_enable(RCC_AFIO);

	/* Enable EXTI0 interrupt. */
	nvic_enable_irq(NVIC_EXTI0_IRQ);
   
         scb_set_priority_grouping(SCB_AIRCR_PRIGROUP_GROUP16_NOSUB);
         nvic_set_priority(NVIC_EXTI0_IRQ, configKERNEL_INTERRUPT_PRIORITY);

	/* Set GPIO0 (in GPIO port A) to 'input open-drain'. */
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO12);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI0, GPIOB);
	exti_direction = FALLING;
	exti_set_trigger(EXTI0, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI0);

        
	nvic_enable_irq(NVIC_EXTI0_IRQ);
}



static void gpio_setup(void)
{
      ///* Enable GPIOA clock. */
      rcc_periph_clock_enable(RCC_GPIOA); 

      rcc_periph_clock_enable(RCC_GPIOC);
      gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
      gpio_set(GPIOC, GPIO13);
      
      rcc_periph_clock_enable(RCC_GPIOB);
      rcc_periph_clock_enable(RCC_AFIO);
      rcc_periph_clock_enable(RCC_USART1);
}

static void systick_setup(void)
{
	/* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	/* SysTick interrupt every N clock pulses: set reload to N-1 */
	systick_set_reload(8999);

	systick_interrupt_enable();

	/* Start counting. */
	systick_counter_enable();
}

static void can_setup(void)
{
	/* Enable peripheral clocks. */
	rcc_periph_clock_enable(RCC_AFIO);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_CAN1);

        AFIO_MAPR |= AFIO_MAPR_CAN1_REMAP_PORTB;
 
	/* Configure CAN pin: RX (input pull-up). */
	gpio_set_mode(GPIO_BANK_CAN1_PB_RX, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN1_PB_RX);
	gpio_set(GPIO_BANK_CAN1_PB_RX, GPIO_CAN1_PB_RX);

	/* Configure CAN pin: TX. */
	gpio_set_mode(GPIO_BANK_CAN1_PB_TX, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN1_PB_TX);

	/* NVIC setup. */
        nvic_set_priority(NVIC_USB_LP_CAN_RX0_IRQ, CAN_rx_irq_PRIORITY);
	nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);

	/* Reset CAN. */
	can_reset(CAN1);
// APB1 ? ??? ???????? ?? ??????? 36???,
  //????????????? ????????? ????????? ???????? ?????????
  //BaudRate= 1000  BRP=  4  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 500   BRP=  9  SJW = 1  BS1= 3 BS2= 4
  //BaudRate= 250   BRP= 16  SJW = 1  BS1= 4 BS2= 4
  //BaudRate= 125   BRP= 32  SJW = 1  BS1= 4 BS2= 4
	/* CAN cell init. */
	if (can_init(CAN1,
		     false,           /* TTCM: Time triggered comm mode? */
		     true,            /* ABOM: Automatic bus-off management? */
		     false,           /* AWUM: Automatic wakeup mode? */
		     false,           /* NART: No automatic retransmission? */
		     false,           /* RFLM: Receive FIFO locked mode? */
		     false,           /* TXFP: Transmit FIFO priority? */
		     CAN_BTR_SJW_1TQ,
		     CAN_BTR_TS1_3TQ,
		     CAN_BTR_TS2_4TQ,
		     9,
		     false,
		     false))             /* BRP+1: Baud rate prescaler */
	{
		/* Die because we failed to initialize. */
		while (1)
			__asm__("nop");
	}

	/* CAN filter 0 init. */
	can_filter_id_mask_32bit_init(
				0,     /* Filter ID */
				0,     /* CAN ID */
				0,     /* CAN ID mask */
				0,     /* FIFO assignment (here: FIFO0) */
				true); /* Enable the filter. */

	/* Enable CAN RX interrupt. */
	can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
}


static void CAN_tansmit_task(void *args) 
{
  uint32_t ulNotifiedValue;
  CAN_Message current_message;
  
#ifdef TEST_CAN_TRANSMIT
   uint8_t data[2] = { 1, 5 };

  while (1) 
  {  
    if (can_transmit(CAN1,
                             0x122, /* (EX/ST)ID: CAN ID */
                             false, /* IDE: CAN ID extended? */
                             false, /* RTR: Request transmit? */
                             2,     /* DLC: Data length */
                             data) == -1)
            {
                   gpio_toggle(GPIOC, GPIO13);
            }
            vTaskDelay(50);
    }
#else
  while (1) 
  {  
    if (CAN_Queue_handle != NULL)
    {
        //if(xTaskNotifyWait(0xffffffff, 0xffffffff, &ulNotifiedValue, 0) == pdTRUE) CAN_TX_BIT
        //if(xQueueReceive(CAN_Queue_handle, &current_message, 10) == pdPASS)
      //  if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE )
        {
          if(xQueueReceive(CAN_Queue_handle, &current_message, 10) == pdPASS)
          {
              while(current_message.repeat > 0)
              {
                current_message.repeat--;
                gpio_toggle(GPIOC, GPIO13);
                can_transmit(CAN1,
    			 current_message.frame_id,     /* (EX/ST)ID: CAN ID */
    			 false, /* IDE: CAN ID extended? */
    			 false, /* RTR: Request transmit? */
    			 current_message.frame_lenght,     /* DLC: Data length */
    			 current_message.body);
                vTaskDelay(1);
              }
          }
        }
    }
  }//while
#endif
}


char head[4] = {'P','C','B','U'};

static void CAN_scan_task(void *args) 
{
  uint32_t id;
  bool ext, rtr;
  uint8_t fmi, length, data[8];
  uint32_t ulNotifiedValue;
  
  while (1) 
  {
      if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
      {
	can_receive(CAN1, 0, false, &id, &ext, &rtr, &fmi, &length, data, NULL);

        switch (id)
        {
          case 0x450: // sun
          case 0x510: // coolant
          case 0x110: // rpm
          case 0x128: // fuel
          case PID_ERRORS_REPLY:
          {
              CAN_Message current_message;
              current_message.frame_id = id;
              current_message.frame_lenght = length;
              current_message.repeat = 1;
              memcpy( current_message.body, data, length);

              if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
              {
                // xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
              }
              break;
          }
          /*          case PID_ERRORS_REPLY:;

            uint8_t a = data[1];
            uint8_t b = a>>6;
            uint8_t c = (a >> 4) & 0b0011;
            uint8_t d = (a) & 0b00001111;
            uint8_t e =  data[2];
            unsigned int f = (d << 8) | e;

            CAN_Message current_message;
            current_message.frame_id = PID_ERRORS_REPLY;
            current_message.frame_lenght = 6;
            current_message.repeat = 1;
            current_message.body[0] = head[b];
            current_message.body[1] = c;
            current_message.body[2] = ( f & 0x000000ff );
            current_message.body[3] = ( f & 0x0000ff00 ) >> 8;
            current_message.body[4] = ( f & 0x00ff0000 ) >> 16;
            current_message.body[5] = ( f & 0xff000000 ) >> 24;

            if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
            {
              // xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
            }

            break;*/
          default:
            break;
        } // switch

	can_fifo_release(CAN1, 0);
	can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
      }// if 
  } // while
}


static void CAN_dump_task(void *args) 
{
  uint32_t id;
  bool ext, rtr;
  uint8_t fmi, length, data[8];

  uint32_t ulNotifiedValue;
  while (1) 
  { 
     if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
     {
      can_receive(CAN1, 0, false, &id, &ext, &rtr, &fmi, &length, data, NULL);
      CAN_Message current_message;
      current_message.frame_id = id;
      current_message.repeat = 1;
      current_message.frame_lenght = length;
      memcpy(current_message.body, data, sizeof(uint8_t)*length);
      if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
      {
      //    xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
      }
      
      can_fifo_release(CAN1, 0);
      can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
    }
  }
  vTaskDelete(NULL);
}


static void XU4_receive_task(void *args) 
{
  uint32_t ulNotifiedValue;
  uint8_t ret ;
  byte len = 0;
  unsigned long can_id = 0;
  byte data[8] = {2, 3, 4, 5, 6, 7, 8, 9};
  
  this_name.fields.name = STM_ODB2;

  while (1) 
  {  
     vTaskDelay(5);
     if (CAN_MSGAVAIL == checkReceive()) 
     //if( xTaskNotifyWait(0, 0, &ulNotifiedValue, portMAX_DELAY) == pdTRUE)
     {
          readMsgBuf(&can_id, &len, data);
          
           // mcp2515_rx_interrupt_clear(); 
           // mcp2515_enable_interrupts();
  
          switch(can_id)
          {
            case PID_ERRORS_REQ:
            {
                CAN_Message tx_message;
                tx_message.frame_id = PID_ERRORS_REQ;
                tx_message.frame_lenght = len;
                tx_message.repeat = 1;
                memcpy(tx_message.body, data, sizeof(uint8_t)*len);

                if(xQueueSendToBack(CAN_Queue_handle, &tx_message, portMAX_DELAY) == pdPASS)
                {
                 // xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
               //    xSemaphoreGive(mutex);	
                }//*/
              break;
            }
            case ID_CMD:
              if( (BIT_CMD_ODB2(data) & 1UL ) || (BIT_CMD_BROADCAST(data) & 1UL))
              {
                uint8_t ret = 0x0;
                gpio_toggle(GPIOC, GPIO13);
                // cansend 999#0101 0000 0000 0000
                if ( BIT_CMD_LINE_HI(data) & 1UL )
                {           
                  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO8);
                  gpio_set(GPIOA, GPIO8);
                  ret |= (1 << 0);
                }
              
                // cansend 999#0102 0000 0000 0000
                if ( BIT_CMD_LINE_LO(data) & 1UL )
                {          
                  gpio_clear(GPIOA, GPIO8); 
                  gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO8);
                  ret |= (1 << 1);
                }
                // cansend 999#0180 0000 0000 0000
                if ( BIT_CMD_LINE_RESET(data) & 1UL )
                {
                  scb_reset_system();
                }
                if ( BIT_CMD_PRINT_GIT(data) & 1UL )
                {
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;
                  uint8_t * body_ptr = current_message.body;

                  if (BIT_CMD_BROADCAST(data) & 1UL)
                  {
                    body_ptr++;
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                      epoch.big = GITTIMESTAMP;
                      this_name.fields.mode = MODE_FIRMWARE_GIT_TS;
                      current_message.body[0] = this_name.val;
                      body_ptr+=1;
                      memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                    }
                   
                    else
                    {
                      this_name.fields.mode = MODE_FIRMWARE_GIT_HASH;
                      current_message.body[0] = this_name.val;
                      memcpy(body_ptr, git_hash_data, sizeof(uint8_t)*7);
                    }
                  }
                  else
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                      epoch.big = GITTIMESTAMP;
                      this_name.fields.mode = MODE_FIRMWARE_GIT_TS;
                      current_message.body[0] = this_name.val;
                      body_ptr+=2;
                      memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                    }
                    else
                    {
                      memcpy(body_ptr, git_hash_data, sizeof(uint8_t)*8);
                    }
                  }

                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                     xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                  }
                }
               if (BIT_CMD_PRINT_BUILD_TS(data) & 1UL)
                {
                  epoch.big = UNIX_TIMESTAMP;
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;
                  
                  uint8_t * body_ptr = current_message.body;

                  this_name.fields.mode = MODE_FIRMWARE_BUILD_TS;
                  current_message.body[0] = this_name.val;
                  body_ptr+=2;
                  memcpy(body_ptr, epoch.chunks, sizeof(uint8_t)*6);
                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                     xTaskNotifyGiveIndexed( XU4_transmit_task_handle, 0 );
                  }
                }
                if ( BIT_CMD_BL_GIT(data) & 1UL )
                {
                  CAN_Message current_message;
                  current_message.frame_id = ID_VERSION_RESPONSE;
                  current_message.frame_lenght = 8;
                  current_message.repeat = 1;

                  if (BIT_CMD_BROADCAST(data) & 1UL)
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_TS;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = 0x0;
                        current_message.body[2] = BKP_DR7 >> 8;
                        current_message.body[3] = BKP_DR7 & 0x00FF;
                        current_message.body[4] = BKP_DR8 >> 8;
                        current_message.body[5] = BKP_DR8 & 0x00FF;
                        current_message.body[6] = BKP_DR9 >> 8;
                        current_message.body[7] = BKP_DR9 & 0x00FF;
                    }
                    else
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_HASH;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = BKP_DR3 >> 8;
                        current_message.body[2] = BKP_DR3 & 0x00FF;
                        current_message.body[3] = BKP_DR4 >> 8;
                        current_message.body[4] = BKP_DR4 & 0x00FF;
                        current_message.body[5] = BKP_DR5 >> 8;
                        current_message.body[6] = BKP_DR5 & 0x00FF;
                        current_message.body[7] = BKP_DR6 >> 8;
                    }
                  }
                  else
                  {
                    if (BIT_CMD_PRINT_GIT_TS(data) & 1UL)
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_TS;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = 0x0;
                        current_message.body[2] = BKP_DR7 >> 8;
                        current_message.body[3] = BKP_DR7 & 0x00FF;
                        current_message.body[4] = BKP_DR8 >> 8;
                        current_message.body[5] = BKP_DR8 & 0x00FF;
                        current_message.body[6] = BKP_DR9 >> 8;
                        current_message.body[7] = BKP_DR9 & 0x00FF;
                    }
                    else
                    {
                        this_name.fields.mode = MODE_BOOTLOADER_GIT_HASH;
                        current_message.body[0] = this_name.val;
                        current_message.body[1] = BKP_DR3 >> 8;
                        current_message.body[2] = BKP_DR3 & 0x00FF;
                        current_message.body[3] = BKP_DR4 >> 8;
                        current_message.body[4] = BKP_DR4 & 0x00FF;
                        current_message.body[5] = BKP_DR5 >> 8;
                        current_message.body[6] = BKP_DR5 & 0x00FF;
                        current_message.body[7] = BKP_DR6 >> 8;
                    }
                  }

                  if(xQueueSendToBack(XU4_Queue_handle, &current_message, 0) == pdPASS)
                  {
                  //   xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
                  }
                }
                // cansend 999#0100 0100 0000 0000
                if ( BIT_CMD_CAN_DUMP(data) & 1UL )
                {
                    can_disable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
                    ret |= (1 << 2);
                    vTaskSuspend(CAN_scan_task_handle);
                    vTaskDelay(1);
                    task_run_flag &=~ SCAN_BIT;
                    task_run_flag |= DUMP_BIT;
                    vTaskResume(CAN_dump_task_handle);
                    can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
                }
                // cansend 999#0100 0200 0000 0000
                if ( BIT_CMD_CAN_SCAN(data) & 1UL )
                {
                    can_disable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
                    ret |= (1 << 3);
                    vTaskSuspend(CAN_dump_task_handle);
                    vTaskDelay(1);
                    task_run_flag &=~ DUMP_BIT;
                    task_run_flag |= SCAN_BIT;
                    vTaskResume(CAN_scan_task_handle);
                    can_enable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
                }
                // cansend 999#0100 0400 0000 0000
                if ( BIT_CMD_CAN_SKIP(data) & 1UL )
                {
                    can_disable_irq(CAN1, CAN_IER_FMPIE0 | CAN_IER_FMPIE1);
                    task_run_flag = 0x00;
                    ret |= (1 << 4);
                    vTaskSuspend(CAN_dump_task_handle);
                    vTaskSuspend(CAN_scan_task_handle);
                    vTaskDelay(1);
                }

                vTaskGetInfo( CAN_scan_task_handle, &xDumpTaskDetails, pdTRUE, eInvalid );
                vTaskGetInfo( CAN_dump_task_handle, &xScanTaskDetails, pdTRUE, eInvalid );

                CAN_Message current_message;
                current_message.frame_id = ID_CMD_RESPONSE;
                current_message.frame_lenght = 8;
                current_message.repeat = 5;
                current_message.body[0] = ret;
                current_message.body[1] = xScanTaskDetails.eCurrentState;
                current_message.body[2] = xDumpTaskDetails.eCurrentState;
                current_message.body[4] = ( mcp_errors & 0x000000ff );
                current_message.body[5] = ( mcp_errors & 0x0000ff00 ) >> 8;
                current_message.body[6] = ( mcp_errors & 0x00ff0000 ) >> 16;
                current_message.body[7] = ( mcp_errors & 0xff000000 ) >> 24;

                if(xQueueSendToBack(XU4_Queue_handle, &current_message, portMAX_DELAY) == pdPASS)
                {
                 // xTaskNotifyGiveIndexed( XU4_send_handle, 0 );
               //    xSemaphoreGive(mutex);	
                }//*/
              } 
              break;
            default:
                break;
          } // switch
     }
  }
  vTaskDelete(NULL);
}



static void XU4_transmit_task(void *args) 
{
  CAN_Message current_message;

  while (1) 
  {  
    if (XU4_Queue_handle != NULL)
    {
        if(xQueueReceive(XU4_Queue_handle, &current_message, 10) == pdPASS)
        {
            while(current_message.repeat > 0)
            {
                current_message.repeat--;
                sendMsgBuf2(current_message.frame_id, current_message.frame_lenght, current_message.body, 0);
                vTaskDelay(1);
            }
        }
    }
  }
  
  vTaskDelete(NULL);
}


static void XU4_init(void *args)
{
    while ( mcp2515_setup( MCP_8MHz, CAN_500KBPS ) != CAN_OK )
    {
      mcp_errors++;
      vTaskDelay(1000);
    }

    xTaskCreate(XU4_receive_task, "XU4 rx", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_receive_PRIORITY, &XU4_receive_task_handle);
    xTaskCreate(XU4_transmit_task, "XU4 tx", configMINIMAL_STACK_SIZE, (void*)NULL, MCP2515_send_PRIORITY, &XU4_transmit_task_handle);

    vTaskDelete(NULL);
}


/* blink timer ISR */
void tim3_isr(void);


int main(void)
{
      rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
      gpio_setup();
      can_setup();
      systick_setup();
      mcp2515_exti_setup();
      setup_tim3();

      CAN_Queue_handle = xQueueCreate(10,sizeof(CAN_Message));
      XU4_Queue_handle = xQueueCreate(10,sizeof(CAN_Message)); /* Create a queue */

      task_run_flag |= SCAN_BIT;

      xTaskCreate(CAN_tansmit_task, "CAN_TX", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, &CAN_transmit_task_handle);
      xTaskCreate(CAN_scan_task, "CAN scan", configMINIMAL_STACK_SIZE, (void*)NULL, CAN_scan_PRIORITY, &CAN_scan_task_handle);
      xTaskCreate(CAN_dump_task, "CAN dump", configMINIMAL_STACK_SIZE, (void*)NULL, CAN_dump_PRIORITY, &CAN_dump_task_handle);
      xTaskCreate(XU4_init, "XU4 init", configMINIMAL_STACK_SIZE, (void*)NULL, configMAX_PRIORITIES-1, NULL);
      
      vTaskSuspend(CAN_dump_task_handle);
      
      vTaskStartScheduler();

      while(1) 
      {

      }
      return 0;
}



#endif // BLINK_EXAMPLE
/*********************************************************************
*
*       vApplicationGetIdleTaskMemory()
*
*/
#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize ) {
  /* If the buffers to be provided to the Idle task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xIdleTaskTCB;
  static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

  /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
  state will be stored. */
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

  /* Pass out the array that will be used as the Idle task's stack. */
  *ppxIdleTaskStackBuffer = uxIdleTaskStack;

  /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
#endif

/*********************************************************************
*
*       vApplicationGetTimerTaskMemory()
*
*/
/*-----------------------------------------------------------*/

#if configSUPPORT_STATIC_ALLOCATION == 1
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize ) {
  /* If the buffers to be provided to the Timer task are declared inside this
  function then they must be declared static - otherwise they will be allocated on
  the stack and so not exists after this function exits. */
  static StaticTask_t xTimerTaskTCB;
  static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

  /* Pass out a pointer to the StaticTask_t structure in which the Timer
  task's state will be stored. */
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

  /* Pass out the array that will be used as the Timer task's stack. */
  *ppxTimerTaskStackBuffer = uxTimerTaskStack;

  /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
#endif

//void HardFault_Handler(void) {

// _Continue = 0u;
// //
// // When stuck here, change the variable value to != 0 in order to step out
// //
// while (_Continue == 0u);
//}

/*************************** End of file ****************************/


