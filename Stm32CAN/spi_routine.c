#include "stm32f1xx.h"
#include "stm32f10x.h"

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"

int _write(int file, char *ptr, int len);

// https://learnbuildshare.wordpress.com/about/stm32/using-spi-as-master/


// Setup SPI1 pins A4/SS, A5/SCK, A6/MISO, A7/MOSI on port A
// Hardware slave selectis not used. That is indicated in the settings
void PeripheralInit_SPI1_Master(void)
{
    GPIO_InitTypeDef GPIO_InitDef;
    SPI_InitTypeDef SPI_InitDef;
 
    // initialize init structs
    GPIO_StructInit(&GPIO_InitDef);
    SPI_StructInit(&SPI_InitDef);

    SPI_I2S_DeInit(SPI1);
 
    // initialize clocks
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 | RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA, ENABLE);
 
    // do not initialize A4/SS because a software SS will be used
    GPIO_InitDef.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitDef.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitDef.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOA, &GPIO_InitDef);

    // initialize A5/SCK alternate function push-pull (50 MHz)
    GPIO_InitDef.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitDef.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitDef.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitDef);
 
    // initialize A6/MISO input pull-up (50MHz)
    GPIO_InitDef.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitDef.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitDef.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitDef);
 
    // initialize A7/MOSI alternate function push-pull (50 MHz)
    GPIO_InitDef.GPIO_Pin = GPIO_Pin_7;
    GPIO_InitDef.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitDef.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitDef);
 #ifdef LEGO

 // 							      IRQ
	//GPIO_InitDef.GPIO_Pin = GPIO_Pin_8;
	//GPIO_InitDef.GPIO_Mode = GPIO_Mode_IN;
	//GPIO_InitDef.GPIO_PuPd = GPIO_PuPd_NOPULL;
	//GPIO_Init(GPIOF, &GPIO_InitDef);

        SPI_InitDef.SPI_Mode = SPI_Mode_Master;
	SPI_InitDef.SPI_Direction = SPI_Direction_2Lines_FullDuplex; // Half duplex mode not possible so full duplex
	SPI_InitDef.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitDef.SPI_CPOL = SPI_CPOL_High; // clock is Mode 0: SCK is active high.
	SPI_InitDef.SPI_CPHA = SPI_CPHA_1Edge; // Data is sampled on the first clock edge of SCK
	SPI_InitDef.SPI_NSS = SPI_NSS_Soft;
	SPI_InitDef.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128; //64; // Maximum 5 MHz set to 2,8 MHz
	SPI_InitDef.SPI_FirstBit = SPI_FirstBit_LSB; // The data order used is LSB first.
#else
    // initialize SPI master
    // for slave, no need to define SPI_BaudRatePrescaler
    SPI_InitDef.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitDef.SPI_Mode = SPI_Mode_Master;
    SPI_InitDef.SPI_DataSize = SPI_DataSize_8b;     // 8-bit transactions
    SPI_InitDef.SPI_FirstBit = SPI_FirstBit_MSB;    // MSB first
    SPI_InitDef.SPI_CPOL = SPI_CPOL_Low;            // CPOL = 0, clock idle low
    SPI_InitDef.SPI_CPHA = SPI_CPHA_1Edge;          // CPHA = 1
    SPI_InitDef.SPI_NSS = SPI_NSS_Soft;             // use hardware  SS
    // SPI_InitDef.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64; // APB2 72/64 = 1.125 MHz
    // SPI_InitDef.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256; // APB2 72/256 = 0.28 MHz
    SPI_InitDef.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32; // APB2 72/16 = 4.5 MHz
    SPI_InitDef.SPI_CRCPolynomial = 7;
#endif
    SPI_Init(SPI1, &SPI_InitDef);
 
    SPI_Cmd(SPI1, ENABLE);
 
}


void spi_setup(void) 
{

 PeripheralInit_SPI1_Master();

}

// Transfer a byte over SPI1  A4/SS, A5/SCK, A6/MISO, A7/MOSI


uint8_t SPI1_RX(void) 
{
//    while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
    SPI_I2S_SendData(SPI1, 0x00); // send
    while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));
    return SPI_I2S_ReceiveData(SPI1); // read received
}


void SPI1_TX(uint8_t val) 
{
    // Approach 1, from Brown's book
    // SPI_I2S_SendData(SPI1, outByte); // send
    // while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
    // return SPI_I2S_ReceiveData(SPI1); // read received
 
    // Approach 2,
    // from http://www.lxtronic.com/index.php/basic-spi-simple-read-write
    while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
    SPI_I2S_SendData(SPI1, val); // send
    while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));
    SPI_I2S_ReceiveData(SPI1); // read received
}

void spi_select(void)
{
    GPIO_ResetBits(GPIOA, GPIO_Pin_4);
}

void spi_unselect(void)
{
    GPIO_SetBits(GPIOA, GPIO_Pin_4);
}
