/**
 * uBLoad configuration
 *
 * Copyright (C) 2015, Marek Koza, qyx@krtko.org
 *
 * This file is part of uMesh node firmware (http://qyx.krtko.org/projects/umesh)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdint.h>
#include <stdbool.h>

#define CONFIG_FIRMWARE_ID_LEN 32

/**
 * This file contains all configuration variables which are supported by uBLoad.
 * Config options are initialized during the bootloader initialization and can
 * be later overriden with values from configuration memory (eg. EEPROM).
 *
 * Not all uBLoad ports read and use all provided values.
 */

/* start address of the flash memory */
#define FLASH_START	0x08000000

/* size of reserved space for bootloader */
#define BOOTLOADER_SIZE	0x00004400

/* start address of main firmware */
#define FW_ADDRESS	(FLASH_START + BOOTLOADER_SIZE)

/* address of main firmware's config information section */
#define FW_CONFIG_ADDRESS (FW_ADDRESS + sizeof(vector_table_t))

/* address of configuration data space in flash */
#define CONFIG_ADDRESS	0x0800C000

/* size of configuration data space */
#define CONFIG_SIZE	0x00004000

/* maximum size for main firmware */
#define FW_MAX_SIZE	(CONFIG_ADDRESS - FW_ADDRESS)

/* size of a single page in flash */
#define FLASH_PAGE_SIZE 0x400


/* flags for BKP_DR1 to specify boot mode */
#define BOOTFLAG_NORMAL_BOOT 0
#define BOOTFLAG_GOTO_BOOTLOADER 1
#define BOOTFLAG_ENFORCE_NORMAL_BOOT 2


#define BL_ERASE_47K_FLASH 0x32
#define BL_WRITE_BIN_TO_FLASH 0x31
#define BL_GOTO_FIRMWARE 0x30
#define BL_PRINT_VERSION 0x33
#define BL_PRINT_TIMESTAMP 0x34

#define FLASH_PAGE_NUM_MAX 127


/**
 * Mode of onboard status/diagnostic LEDs.
 */
enum ubload_config_led_mode {
	/**
	 * All status LEDs are off during bootloader code execution
	 */
	LED_MODE_OFF,
	/**
	 * Status LED is lit during the whole bootloading process. It is
	 * switched off when the bootloader jumps to the user application.
	 */
	LED_MODE_STILL_ON,
	/**
	 * Status LED(s) blinks according to actual bootloading phase.
	 * Recommended blink sequences are provided in the led.h file.
	 */
	LED_MODE_BASIC,
	/**
	 * Multiple status LEDs are initialized (if the board has them) and
	 * used to provide boot process feedback to the user. Usually all
	 * available LEDs with different colors (or multicolor LEDs) are used.
	 */
	LED_MODE_DIAG
};


struct ubload_config {

	/* Console serial port settings */
	bool serial_enabled;
	uint32_t serial_speed;

	/* Diagnostic LED settings */
	enum ubload_config_led_mode led_mode;

	/* Bootloader enter setup */
	bool cli_enabled;
	uint32_t wait_time;
	char enter_key;
	char skip_key;

	/* CLI inactivity time setup */
	uint32_t idle_time;

	/* System identification */
	char host[32];

	/* Independend watchdog */
	bool watchdog_enabled;

	/* XMODEM configuration. */
	uint32_t xmodem_retry_count;
	uint32_t xmodem_timeout;

	char fw_working[CONFIG_FIRMWARE_ID_LEN];
	char fw_request[CONFIG_FIRMWARE_ID_LEN];
};


extern const struct ubload_config default_config;
extern struct ubload_config running_config;

#endif


