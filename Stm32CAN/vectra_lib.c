
//#include "stm32f1xx.h"
//#include "stm32f10x.h"
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/f1/bkp.h>
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities


#include "mcp2515.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"


#include "vectra_lib.h"

#define GPIO12_rx_irq_PRIORITY 200
#define IWDG_irq_PRIORITY 200


void setup_tim3(void) 
{
        rcc_periph_clock_enable(RCC_TIM3);
	RCC_APB1RSTR |= RCC_APB1RSTR_TIM3RST;
	RCC_APB1RSTR &= ~RCC_APB1RSTR_TIM3RST;
        TIM_CNT(TIM3) = 1;
	TIM_PSC(TIM3) = 0xFFFF;
	TIM_ARR(TIM3) = IWDG_RESET_INTERVAL;
	TIM_DIER(TIM3) |= TIM_DIER_UIE;

	nvic_set_priority(NVIC_TIM3_IRQ, IWDG_irq_PRIORITY);
	nvic_enable_irq(NVIC_TIM3_IRQ);

	TIM_CR1(TIM3) |= TIM_CR1_CEN;
}



void tim3_isr(void) 
{
        iwdg_reset();
	TIM_SR(TIM3) &= ~TIM_SR_UIF;
}


void mcp2515_exti_setup(void)
{
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_AFIO);

    ///* Enable EXTI0 interrupt. */
    //nvic_enable_irq(NVIC_EXTI15_10_IRQ);

    ///* Set GPIO12 (in GPIO port B) to input  */
    //gpio_set_mode(GPIOB, GPIO_MODE_INPUT,GPIO_CNF_INPUT_FLOAT, GPIO12);

    ///* Configure the EXTI subsystem. */
    //exti_select_source(EXTI12,GPIOB);
    //exti_set_trigger(EXTI12, EXTI_TRIGGER_BOTH);
    //exti_enable_request(EXTI12);

	/* Enable GPIOA clock. */
	//rcc_periph_clock_enable(RCC_GPIOA);

	/* Enable AFIO clock. */
	//rcc_periph_clock_enable(RCC_AFIO);

	/* Enable EXTI0 interrupt. */
   

	/* Set GPIO0 (in GPIO port A) to 'input open-drain'. */
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO12);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI12, GPIOB);
	exti_direction = FALLING;
	exti_set_trigger(EXTI12, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI12);
        
        
        nvic_set_priority(NVIC_EXTI15_10_IRQ, GPIO12_rx_irq_PRIORITY);
	nvic_enable_irq(NVIC_EXTI15_10_IRQ);
}
