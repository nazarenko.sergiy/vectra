#ifndef PN532_SPI_H
#define PN532_SPI_H

#include <stdint.h>

uint8_t pn532_init(void);
uint32_t PN532_getFirmwareVersion(void);
uint8_t PN532_setPassiveActivationRetries(uint8_t maxRetries);
uint8_t PN532_SAMConfig(void);
uint8_t PN532_powerDownMode(void);
uint8_t PN532_startPassiveTargetIDDetection(uint8_t cardbaudrate);

#endif