
#ifndef VECTRA_LIB_H
#define VECTRA_LIB_H


#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#define FALLING 0
#define RISING 1

static uint16_t exti_direction = FALLING;

#define IWDG_RESET_INTERVAL 6000

void mcp2515_exti_setup(void);
void setup_tim3(void);

#endif


