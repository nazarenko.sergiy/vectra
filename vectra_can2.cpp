#include "vectra.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>


#define CAN_HELP 1
#define CAN_DRAIN  2
#define CAN_DHO_ON 3
#define CAN_DHO_OFF 4
#define CAN_DHO_LON 5
#define CAN_DHO_LOFF 6
#define CAN_DHO_ROFF 7
#define CAN_DHO_RON 8
#define CSR_HOME "\33[H"
#define CLR_SCREEN "\33[2J"

void usage(const char * s) {
    printf("Usage: %s <drain>\n", s);
}

int main (int argc, char ** argv) 
{
    int mode = 0;
    if ( argc > 1 ) 
    {
        if (!strcmp(argv[1], "help")) 
        {
            usage(argv[0]);
            return 1;
        }
        if (!strcmp(argv[1], "drain"))
            mode = CAN_DRAIN;
        if (!strcmp(argv[1], "dhoon"))
            mode = CAN_DHO_ON;
        if (!strcmp(argv[1], "dhooff"))
            mode = CAN_DHO_OFF;
        if (!strcmp(argv[1], "dhoron"))
            mode = CAN_DHO_RON;
        if (!strcmp(argv[1], "dhoroff"))
            mode = CAN_DHO_ROFF;
        if (!strcmp(argv[1], "dholon"))
            mode = CAN_DHO_LON;
        if (!strcmp(argv[1], "dholoff"))
            mode = CAN_DHO_LOFF;
    }

    //printf("\033[H\033[J");
    //printf("\e[2J\e[H"); 
    // printf("%s%s", CLR_SCREEN, CSR_HOME);
    // printf("%s", CSR_HOME);

    switch (mode)
    {
        case CAN_DHO_ON:
            dhoOn();
            return 0; 
    
        case CAN_DHO_LOFF:
            dhoLeftOff();
            return 0;

        case CAN_DHO_LON:
            dhoLeftOn();
            return 0;

        case CAN_DHO_ROFF:
            dhoRightOff();
            return 0;

        case CAN_DHO_RON:
            dhoRightOn();
            return 0;

        case CAN_DHO_OFF:
            dhoOff();
            return 0;

        default:
            break;
    }

    std::vector<int> data_can;
        data_can.reserve(CAN_MEM_ARR_SIZE);
        data_can.assign(CAN_MEM_ARR_SIZE, 0);

    if (mode == CAN_DRAIN)
    {
        while(true)
        {
            // std::vector<int> data_nrf = arr_read_nrf();
            // std::vector<int> data_gui = arr_read_gui();

            // int nrf0 = data_nrf[0];
            // if (nrf0 != -1)
            //     printf("CAN READ: Data[0] from nrf == %d\n", nrf0);

            // int gui0 = data_gui[0];
            // if (gui0 != -1)
            //     printf("CAN READ: Data[0] from gui == %d\n", gui0);
            
            pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    
            printf("CAN READ TEST: == %d %d%d%d%d %d\n"
                , ((data_can[BLOCKM_B] >> 5)&1)
                , ((data_can[BLOCKM_B] >> 3)&1)
                , ((data_can[BLOCKM_B] >> 2)&1)
                , ((data_can[BLOCKM_B] >> 1)&1)
                , ((data_can[BLOCKM_B] >> 0)&1)
                , data_can[BLOCKM_CALL_A]);
            int res = 0x00;
            // int i = rand() % 1;
            // int j = rand() % 1;

            /*res |= (1<<0);

            // res |= (0 << 1);
            int data[2] = {BLOCKM_B, res};

            int r = arr_set_can(data, 2);
*/
            //printf("CAN READ: ret %d\n", r);
            usleep(100000);
        }
    }
    else
    {
        arr_write_can(data_can);

        struct ifreq ifr;
        struct sockaddr_can addr;
     
        struct can_frame frame;
        int s;

        memset(&ifr, 0x0, sizeof(ifr));
        memset(&addr, 0x0, sizeof(addr));
        memset(&frame, 0x0, sizeof(frame));

        /* open CAN_RAW socket */
        s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

        /* convert interface sting "can0" into interface index */ 
        strcpy(ifr.ifr_name, "can2");
        ioctl(s, SIOCGIFINDEX, &ifr);

        /* setup address for bind */
        addr.can_ifindex = ifr.ifr_ifindex;
        addr.can_family = PF_CAN;

        /* bind socket to the can0 interface */
        bind(s, (struct sockaddr *)&addr, sizeof(addr));

        int n = 0;
        int log_i = 0;

        while( (n = read(s, &frame, sizeof(frame)) > 0))
        {
            switch (frame.can_id)
            {
                case 0x123:
                {
                    int a = int((unsigned char)(frame.data[BlockM::FLOW_HI]) << 24 |
                                (unsigned char)(frame.data[BlockM::FLOW_LO]) << 16 |
                                (unsigned char)(frame.data[BlockM::TEMPER_HI]) << 8 |
                                (unsigned char)(frame.data[BlockM::TEMPER_LO]));
                    int b = int((0x00 << 24) |
                                (unsigned char)(frame.data[BlockM::WIPES_HI]) << 16 |
                                (unsigned char)(frame.data[BlockM::WIPES_LO]) << 8 |
                                (unsigned char)(frame.data[BlockM::STATE]));

                    // if (log_i < 110)
                    // {
                    // printf("CAN LOG STATE: == %d %d%d%d%d\n"
                    //     , ((b >> 5)&1)
                    //     , ((b >> 3)&1)
                    //     , ((b >> 2)&1)
                    //     , ((b >> 1)&1)
                    //     , ((b >> 0)&1));
                    // log_i=0;
                    // }
                    // else
                    // {
                    //     log_i++;
                    // }
                    short ds_data = short( (unsigned char)(frame.data[BlockM::TEMPER_HI])<<8 | 
                                            (unsigned char)(frame.data[BlockM::TEMPER_LO]) );

                    if ((ds_data >> 15) & 1UL )
                        ds_data = (~(ds_data-1));
                           
                    int battery = int((0x00 << 24) | (0x00 << 16) | ds_data ); //*0.0625;

                    int fluid = int((0x00 << 24) | (0x00 << 16) |
                                            (unsigned char)(frame.data[BlockM::FLOW_HI])<<8 |
                                            (unsigned char)(frame.data[BlockM::FLOW_LO])); 
                    int data[8] = {
                          ReadCan::FLUID_METER, fluid
                        , ReadCan::TMPR_BATTERY, battery
                        , ReadCan::BLOCKM_A, a
                        , ReadCan::BLOCKM_B, b};
                    arr_set_can(data, 8);
                    break;
                }
            }
            usleep(10000);
        }
    }
    return 0;
}

