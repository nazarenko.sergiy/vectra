#include <stdio.h>

#define AA "\033["
#define AB ";0H"

#define PPCAT_NX(A, B)  AA #A AB B
#define PPCAT(A, B) PPCAT_NX(A, B)
#define ROW1
#define FORMAT_PRINTF "\033[1m%-10.3f %-10d %-10d %-10d %-10.3f\t%-10.3f\t%-10.3f\033[0m"
#define FORMAT_HEADER "\e[4m\e[31m\e[1m\033[47m%-10s %-10s %-10s %-10s %-10s\t%-10s\t%-10s\e[24m"
#define FORMAT_H "\033[1m%-10.3f %-10d %-10d\033[0m"

inline float term_fuel(unsigned char A) { return (float)( (A-128) * 100/128 ); }
inline unsigned char coolant(unsigned char A) { return  (A-40); }
inline unsigned char speed(unsigned char A) { return  (A); }
inline float rpm(unsigned char A, unsigned char B) { return (float)( ((A*256)+B)/4 ); }
inline float module_voltage(unsigned char A, unsigned char B) { return (float)( ((A*256)+B)/1000); }
inline float fuel_rate(unsigned char A, unsigned char B) { return (float)( ((A*256)+B)*0.05); }
inline float lambda(unsigned char A, unsigned char B) { return (float)( ((A*256)+B)/32768 ); }
int main()
{
	unsigned char data[6] = { 0b01000110, 0b00000010,0b10100100,
	 0b11001011, 0b00000000, 0b00000000 };

	unsigned char a0 = data[0];
    unsigned char a1 = data[1];
    unsigned char a2 = data[2];
    unsigned char a3 = data[3];
    unsigned char a4 = data[4];
    unsigned char a5 = data[5];  
	
    printf(PPCAT(ROW1, FORMAT_PRINTF), term_fuel( a0 )
            , coolant( a0 ), speed( a0 ), rpm(a0, a1)
            , module_voltage(a0, a1)
            , fuel_rate(a0, a1), lambda(a0, a1) );
	return 0;
}

