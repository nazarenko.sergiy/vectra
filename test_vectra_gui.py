import sys
#sys.path.insert(0, "/home/nazarenko/workspace/vectra")
sys.path.insert(0,'./build/lib')


import vectra_gui
import time

while True:
    print('CAN:', vectra_gui.array_read_can())
    print('NRF:', vectra_gui.array_read_nrf())
    print('GUI:', vectra_gui.array_read_gui())
    time.sleep(1)
    vectra_gui.array_write_gui([567,99])
