#include <dispatch/dispatch.h>
#include <stdio.h>
#include <stdlib.h>

int i = 0;
dispatch_queue_t queue;
dispatch_source_t timer1;
dispatch_source_t timer2;

#include <memory>

#define N (128)
int buffer[N];
int in = 0, out = 0;




void enqueue(int value) { /* Add one item to the front of the queue*/
  buffer[in] = value;
  in++; /* Advance the index for next time */
  if (in == 160) {
        in = 0; /* Wrap around! */
    int res = 0;
    for (int i = 0 ; i < 160; ++i)
            res+=buffer[i];
    printf("Mid: %d\n", res/160);
  }
}



void sigtrap(int sig)
{
    dispatch_source_cancel(timer1);
    dispatch_source_cancel(timer2);
    printf("CTRL-C received, exiting program\n");
    exit(EXIT_SUCCESS);
}

void vector1(dispatch_source_t timer)
{
        //printf("a: %d\n", i);
        i++;
        if (i >= 500) 
        {
            dispatch_source_cancel(timer);
        }   
}

static int summa ;
static int b_summa;
void vector2(dispatch_source_t timer)
{
    printf("b: %d\n", i);
    i++; 
   // in++; /* Advance the index for next time */
    int k = (in++) & (N-1);
    int all = ((b_summa == 1) ? N  : ((k == 0) ? 1 : k)); 

    summa -= buffer[ k ];

    buffer[ k ] = 10;
    
    if (in == N) 
         b_summa = 1; /* Wrap around! */
    
    //int res = 0;
    // for (int j = 0 ; j < i; ++j)
    //     res+=buffer[j];
 
    summa += 10;
    printf("Mid: %d\n", summa/all);
      
    if (i >= 500)  //at 20 count cancel the 
    {
        dispatch_source_cancel(timer);
    }   

}

// 0 - 20 +20 = 20
// 1 - 40 +40 = 60
// 2 - 60 +60 = 120
// 3 - 80 +80 = 200

// ~4 - 100 +100 = 300



int main(int argc, const char* argv[]) {
b_summa = 0;
    for (int j = 0 ; j < N; ++j)
        buffer[j] = 0;

    signal(SIGINT, &sigtrap);   //catch the cntl-c
    queue = dispatch_queue_create("timerQueue", 0);

    // Create dispatch timer source
    timer1 = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    timer2 = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);

    // Set block for dispatch source when catched events
    dispatch_source_set_event_handler(timer1, ^{vector1(timer1);});
    dispatch_source_set_event_handler(timer2, ^{vector2(timer2);});

    // Set block for dispatch source when cancel source
    dispatch_source_set_cancel_handler(timer1, ^{
        dispatch_release(timer1);
        dispatch_release(queue);
        printf("end\n");
        exit(0);
    });
    dispatch_source_set_cancel_handler(timer2, ^{
        dispatch_release(timer2);
        dispatch_release(queue);
        printf("end\n");
        exit(0);
    }); 

    dispatch_time_t start = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC); // after 1 sec

    // Set timer
    dispatch_source_set_timer(timer1, start, NSEC_PER_SEC / 5, 0);  // 0.2 sec
    dispatch_source_set_timer(timer2, start, NSEC_PER_SEC / 2, 0);  // 0.5 sec
    printf("start\n");    
    dispatch_resume(timer1);
    dispatch_resume(timer2);
    while(1)
    {
        ;;
    }


    return 0;
}