//------------------------------------------------------------------------------------------------------------
//
// ODROID-C GPIO(LED) / ADC Test Application.
//
// Defined port number is wiringPi port number.
//
// Compile : gcc -o <create excute file name> <source file name> -lwiringPi -lwiringPiDev -lpthread
// Run : sudo ./<created excute file name>
//
//------------------------------------------------------------------------------------------------------------
#include <stdint.h>

#include <linux/version.h>
#include <linux/input.h>
#include <linux/reboot.h>
#include <sys/reboot.h>
#include <grp.h>
#include <pwd.h>
#include <error.h>
#include <errno.h>

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#ifndef EV_SYN
#define EV_SYN 0
#endif


#define BITS_PER_LONG (sizeof(long) * 8)
#define NBITS(x) ((((x)-1)/BITS_PER_LONG)+1)


//------------------------------------------------------------------------------------------------------------
//
// Start Program
//
//------------------------------------------------------------------------------------------------------------
int main (int argc, char *argv[])
{
    int fd, rd, i, j, k;
	struct input_event ev[64];
	int version;
	unsigned short id[4];
	unsigned long bit[EV_MAX][NBITS(KEY_MAX)];
	char name[256] = "Unknown";
    char can0_str[]="can0";

    struct ifreq ifr;
    struct sockaddr_can addr;
    int required_mtu;
    struct can_frame frame;
    int s;

    memset(&ifr, 0x0, sizeof(ifr));
    memset(&addr, 0x0, sizeof(addr));
    memset(&frame, 0x0, sizeof(frame));

    /* open CAN_RAW socket */
    s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    /* convert interface sting "can0" into interface index */ 
    strcpy(ifr.ifr_name, can0_str);
    ioctl(s, SIOCGIFINDEX, &ifr);

    /* setup address for bind */
    addr.can_ifindex = ifr.ifr_ifindex;
    addr.can_family = PF_CAN;

    /* bind socket to the can0 interface */
    bind(s, (struct sockaddr *)&addr, sizeof(addr));
    int n = 0;

    if ((fd = open("/dev/input/event0", O_RDONLY)) < 0) {
		perror("evtest");
		return 1;
	}

    ioctl(fd, EVIOCGID, id);
	printf("Input device ID: bus 0x%x vendor 0x%x product 0x%x version 0x%x\n",
		id[ID_BUS], id[ID_VENDOR], id[ID_PRODUCT], id[ID_VERSION]);

	ioctl(fd, EVIOCGNAME(sizeof(name)), name);
	printf("Input device name: \"%s\"\n", name);

    while (1) 
    {
		rd = read(fd, ev, sizeof(struct input_event) * 64);

		if (rd < (int) sizeof(struct input_event)) {
			perror("\nevtest: error reading");
			return 1;
		}

		for (i = 0; i < rd / sizeof(struct input_event); i++)
        {
            printf("GOT event %d %d\n", ev[i].code, ev[i].value);
            if (ev[i].code == 116 && ev[i].value == 1)
            {
                frame.can_id = 0x7FB;
                frame.can_dlc = 5;
                frame.data[0] = 0x1;
                frame.data[1] = 0x0;
                frame.data[2] = 0x0;
                frame.data[3] = 0x0;
                frame.data[4] = 0x0;

                int j = 2; 
                while(j--)
                {
                    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame))
                    {
                        perror("bind");
                        return 1;
                    }
                    usleep(15000);
                }

                sync();
                setuid(0);
                struct passwd *pw;
                pw = getpwuid(getuid());
                if(pw == NULL) error(1, errno, "failed to get username\n");
                //if(!reboot_allowed()) error(1, 0, "Permission denied\n");
                reboot(LINUX_REBOOT_CMD_POWER_OFF);
                
                return(0);
            }
        }
	}
}
 
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
