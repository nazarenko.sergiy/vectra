#!/bin/bash

/usr/bin/sudo /sbin/ifconfig can1 down
/usr/bin/sudo /sbin/ip link set can1 type can bitrate 95238 triple-sampling on restart-ms 100
/usr/bin/sudo /sbin/ifconfig can1 up
/sbin/ip -details link show can1

