// demo: CAN-BUS Shield, receive data with check mode
// send data coming to fast, such as less than 10ms, you can use this way
// loovee, 2014-6-13
#include <SPI.h>

#define CAN_2515
// #define CAN_2518FD

// Set SPI CS Pin according to your hardware

#if defined(SEEED_WIO_TERMINAL) && defined(CAN_2518FD)
// For Wio Terminal w/ MCP2518FD RPi Hat：
// Channel 0 SPI_CS Pin: BCM 8
// Channel 1 SPI_CS Pin: BCM 7
// Interupt Pin: BCM25
const int SPI_CS_PIN  = BCM8;
const int CAN_INT_PIN = BCM25;
#else

// For Arduino MCP2515 Hat:
// the cs pin of the version after v1.1 is default to D9
// v0.9b and v1.0 is default D10
const int SPI_CS_PIN = 10;
const int CAN_INT_PIN = 2;
const int CLR_INT_PIN = 3;
const int READ_INT_PIN = 4;
#endif

#define PID_REQ 0x7DF
#define PID_REPLY 0x7E8

#ifdef CAN_2518FD
#include "mcp2518fd_can.h"
mcp2518fd CAN(SPI_CS_PIN); // Set CS pin
#endif

#ifdef CAN_2515
#include "mcp2515_can.h"
mcp2515_can CAN(SPI_CS_PIN); // Set CS pin
#endif                           

void setup() {
    SERIAL_PORT_MONITOR.begin(115200);

    while (CAN_OK != CAN.begin(CAN_500KBPS, MCP_8MHz)) {             // init can bus : baudrate = 500k
        SERIAL_PORT_MONITOR.println("CAN init fail, retry...");
        delay(100);
    }
    pinMode(CLR_INT_PIN, INPUT);
    pinMode(READ_INT_PIN, INPUT);
    SERIAL_PORT_MONITOR.println("CAN init ok!");
}


unsigned char rst_tmp[8] = {0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
unsigned char set_tmp[8] = {0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
unsigned char read_tmp[2] = {0x01, 0x03};
//unsigned char read_tmp[2] = {0x01, 0x07};
//unsigned char read_tmp[2] = {0x01, 0x01};
//unsigned char read_tmp[2] = {0x01, 0x09};
// unsigned char read_tmp[2] = {0x01, 0x04};
//unsigned char read_tmp[3] = {0x02, 0x01, 0x0d};


char head[4] = {'P','C','B','U'};
/*
#include <iostream>
#include <bitset>

char s [4] = {'P','C','B','U'};

int main() {
    unsigned short a = 0x47, b = a>>6, c = (a >> 4) & 0b0011, d = (a) & 0b00001111, e = 1;
    unsigned int f = (d << 8) | e;

    std::cout << "a = " << std::bitset<8>(a)  << std::endl;
    std::cout << "b = " << std::bitset<8>(b)  << " " << s[b] << c << std::endl;
    std::cout << "c = " << std::bitset<8>(c) << std::endl;
    std::cout << "d = " << std::bitset<8>(d) << std::endl;
    std::cout << "f = " << std::bitset<16>(f) << " " << s[b] << c << std::hex << f << std::endl;
}
*/

void loop() {
    unsigned char len = 0;
    unsigned char buf[8];
    static char ctmp;
    static char rtmp;
    char clr_btn = digitalRead(CLR_INT_PIN);
    char read_btn = digitalRead(READ_INT_PIN);


    if (rtmp == 0)
    {
      SERIAL_PORT_MONITOR.println("Q");
      for (int i = 0; i < 1; i++)
      {
                     delay(10);

           CAN.sendMsgBuf(PID_REQ, 0, 2, read_tmp);
      //       CAN.sendMsgBuf(0x7e0, 0, 8, set_tmp);
       //     delay(1000);
      }
      rtmp = 1;
    }
    /*
    if (clr_btn != ctmp)
    {
      //SERIAL_PORT_MONITOR.print(clr_btn, HEX);
      ctmp = clr_btn;
      if (ctmp == 1)
      {
          CAN.sendMsgBuf(PID_REQ, 0, 8, rst_tmp);
          //    SERIAL_PORT_MONITOR.println("04");

      }  
    }
    
    if (read_btn != rtmp)
    {
      //SERIAL_PORT_MONITOR.print(read_btn, HEX);
      rtmp = read_btn;
      if (rtmp == 1)
      {
       CAN.sendMsgBuf(PID_REQ, 0, 8, read_tmp);
       delay(10);
       CAN.sendMsgBuf(0x7e0, 0, 8, set_tmp);
       delay(1000);
       //SERIAL_PORT_MONITOR.println("03");
      }  
    }
*/
    if (CAN_MSGAVAIL == CAN.checkReceive()) 
    {         // check if data coming
        CAN.readMsgBuf(&len, buf);    // read data,  len: data length, buf: data buf

        unsigned long canId = CAN.getCanId();
  if (canId == PID_REPLY)
  {
        SERIAL_PORT_MONITOR.print(canId, HEX);
            SERIAL_PORT_MONITOR.print("\t");

        for (int i = 0; i < len; i++) 
        { // print the data
            SERIAL_PORT_MONITOR.print(buf[i], HEX);
            SERIAL_PORT_MONITOR.print("\t");
        }

        SERIAL_PORT_MONITOR.println();
        unsigned short a = buf[1], b = a>>6, c = (a >> 4) & 0b0011, d = (a) & 0b00001111, e =  buf[2];
        unsigned int f = (d << 8) | e;
        SERIAL_PORT_MONITOR.print(head[b]);
        SERIAL_PORT_MONITOR.print(c);
        SERIAL_PORT_MONITOR.print(f, HEX);
        SERIAL_PORT_MONITOR.println();
    }}
}

/*********************************************************************************************************
    END FILE
*********************************************************************************************************/
