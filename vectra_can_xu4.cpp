#include "vectra.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>


#define CAN_HELP 1
#define CAN_DRAIN  2
#define CAN_REVERSE_TEST 3
#define CSR_HOME "\33[H"
#define CLR_SCREEN "\33[2J"

void usage(const char * s) {
    printf("Usage: %s <drain>\n", s);
}



int main (int argc, char ** argv) 
{
    std::vector<fptr> canfunctions;
    canfunctions.reserve(7);
    canfunctions[0] = &calculated_engine_load_value;
    canfunctions[1] = &x_term_fuel;
    canfunctions[2] = &fuel_pressure;
    canfunctions[3] = &coolant_temperature;
    canfunctions[4] = &intake_manifold_absolute_pressure;
    canfunctions[5] = &rpm;
    canfunctions[6] = &vehicle_speed;

    int mode = 0;
    int mode2 = 0;
    char can0_str[]="can0";
    if ( argc > 1 ) 
    {
        if (!strcmp(argv[1], "help")) 
        {
            usage(argv[0]);
            return 1;
        }
        if (!strcmp(argv[1], "drain"))
            mode = CAN_DRAIN;
        else
            strcpy (can0_str,argv[1]);

        if (argc > 2)
        {
            if (!strcmp(argv[1], "test"))
                mode2 = CAN_REVERSE_TEST;
        }
    }

    //printf("\033[H\033[J");
    //printf("\e[2J\e[H"); 
    // printf("%s%s", CLR_SCREEN, CSR_HOME);
    // printf("%s", CSR_HOME);
    std::vector<int> data_can;
    data_can.reserve(CAN_MEM_ARR_SIZE);
    data_can.assign(CAN_MEM_ARR_SIZE, 0);
    arr_write_can(data_can);

    if (mode == CAN_DRAIN)
    {
        while(true)
        {
            std::vector<int> data_nrf = arr_read_nrf();
            std::vector<int> data_gui = arr_read_gui();

            int nrf0 = data_nrf[0];
            if (nrf0 != -1)
                printf("CAN READ: Data[0] from nrf == %d\n", nrf0);

            int gui0 = data_gui[0];
            if (gui0 != -1)
                printf("CAN READ: Data[0] from gui == %d\n", gui0);

            pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
    
            printf("CAN READ: == %d %d %d\n", data_can[ReadCan::REVERSE_TEST_VALUE]
                , data_can[ReadCan::REVERSE_TEST_CANID], data_can[ReadCan::REVERSE_TEST_CAN_FRAMES_A]);
            
            int b = int((0x00 << 24) |
                                (0x00 << 16) |
                                (6 << 8) |
                                7);

            //data_can[0] = rand() % 800;
            //arr_write_can(data_can);
            // int i = rand() % 800;
            int data[6] = {ReadCan::REVERSE_TEST_CANID, 108
                , ReadCan::REVERSE_TEST_CAN_FRAMES_A, b
                , ReadCan::REVERSE_TEST_FUNCID, };
            int r = arr_set_can(data, 6);
            
            usleep(5000);
        }
    }
    else
    {
        struct ifreq ifr;
        struct sockaddr_can addr;
     
        struct can_frame frame;
        int s;

        memset(&ifr, 0x0, sizeof(ifr));
        memset(&addr, 0x0, sizeof(addr));
        memset(&frame, 0x0, sizeof(frame));

        /* open CAN_RAW socket */
        s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

        /* convert interface sting "can0" into interface index */ 
        strcpy(ifr.ifr_name, can0_str);
        ioctl(s, SIOCGIFINDEX, &ifr);

        /* setup address for bind */
        addr.can_ifindex = ifr.ifr_ifindex;
        addr.can_family = PF_CAN;

        /* bind socket to the can0 interface */
        bind(s, (struct sockaddr *)&addr, sizeof(addr));
        int n = 0;


        if (mode2 == CAN_REVERSE_TEST)
        {
            can_ret can_r;
            char trans[ sizeof( double ) ] = { 0 };

            if ( sizeof( double )  != sizeof( int ) )
                printf("types not equal!");

            while( (n = read(s, &frame, sizeof(frame)) > 0))
            {
                pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
                if ( frame.can_id == data_can[ ReadCan::REVERSE_TEST_CANID ] )
                {
                    int f = data_can[ ReadCan::REVERSE_TEST_CAN_FRAMES_A ];
                    int o_one = f  & 0xff;
                    int o_two = (f >> 8)  & 0xff;

                    int func_id = data_can[ ReadCan::REVERSE_TEST_FUNCID ];
                    
                    canfunctions[ func_id ]( can_r, frame.data[o_one], frame.data[o_two] );

                    memcpy( trans , &can_r.ret , sizeof( double ) );

                    int rt;
                    memcpy( &rt , trans , sizeof( int ) );

                    int data[2] = {ReadCan::REVERSE_TEST_VALUE, rt};
                    arr_set_can(data, 2);
                }
                usleep(1000);
            } // while
        } // if
        else
        {
           while( (n = read(s, &frame, sizeof(frame)) > 0))
           {
                switch (frame.can_id)
                {
                    case 0x450:
                    { 
                        int sun = 1;
                        if (frame.data[3]==0x47)
                            sun = 0; 
                        int data[2] = {ReadCan::SUN, sun};
                        arr_set_can(data, 2);
                        break;
                    }
                    case 0x510:
                    { 
                        int col = frame.data[1]-40;
                        //printf("\033[%d;%dHcoolant: %d   ", 10,40,col);
                        int data[2] = {ReadCan::COOLANT,col};
                        arr_set_can(data, 2);
                        break;            
                    }
                    case 0x110:
                    {
                        int rpm = (frame.data[1] * 256 + frame.data[2])/4;
                        //printf("\033[%d;%dHrpm: %d   ",10,80, rpm);
                        int data[2] = {ReadCan::RPM, rpm};
                        arr_set_can(data, 2);
                        break;            
                    }
                    case 0x128:
                    {
                        int rp = (256*frame.data[0] + frame.data[1])/100;
                        //printf("\033[%d;%dHfuel:     %d    ",10,80, rp);                
                        int data[2] = {ReadCan::FUEL, rp};
                        arr_set_can(data, 2);
                        break;
                    }
                    case 0x7F7:
                    {
                        int a = int((unsigned char)(frame.data[BlockM::FLOW_HI]) << 24 |
                                    (unsigned char)(frame.data[BlockM::FLOW_LO]) << 16 |
                                    (unsigned char)(frame.data[BlockM::TEMPER_HI]) << 8 |
                                    (unsigned char)(frame.data[BlockM::TEMPER_LO]));
                        int b = int((0x00 << 24) |
                                    (unsigned char)(frame.data[BlockM::WIPES_HI]) << 16 |
                                    (unsigned char)(frame.data[BlockM::WIPES_LO]) << 8 |
                                    (unsigned char)(frame.data[BlockM::STATE]));

                        short ds_data = short( (unsigned char)(frame.data[BlockM::TEMPER_HI])<<8 | 
                                                (unsigned char)(frame.data[BlockM::TEMPER_LO]) );

                        if ((ds_data >> 15) & 1UL )
                            ds_data = (~(ds_data-1));
                            
                        int battery = int((0x00 << 24) | (0x00 << 16) | ds_data ); //*0.0625;

                        int fluid = int((0x00 << 24) | (0x00 << 16) |
                                                (unsigned char)(frame.data[BlockM::FLOW_HI])<<8 |
                                                (unsigned char)(frame.data[BlockM::FLOW_LO])); 
                        int data[8] = {
                            ReadCan::FLUID_METER, fluid
                            , ReadCan::TMPR_BATTERY, battery
                            , ReadCan::BLOCKM_A, a
                            , ReadCan::BLOCKM_B, b};
                        arr_set_can(data, 8);
                        break;
                    }
                    case 0x108:
                    {
                        int a = int((unsigned char)(frame.data[4]) << 8 |
                                    (unsigned char)(frame.data[5]));
                        int b = int((unsigned char)(frame.data[6]) << 8 |
                                    (unsigned char)(frame.data[7]));
                        int data[4] = { ReadCan::SPEED , a
                                        ,ReadCan::SPEED_MS, b  }; 
                        arr_set_can(data, 4);
                        break;
                    }
                    case 0x350:
                    { 
                        pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);
                        if ( (frame.data[0] >> 2) & 1 )
                            data_can[ReadCan::LIGHT_SIGNALS] |= (1<<0);
                        else
                            data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<0);
                        
                        int data[2] = {ReadCan::LIGHT_SIGNALS
                                        , data_can[ReadCan::LIGHT_SIGNALS]};
                        arr_set_can(data, 2);
                        break;
                    }
                    case 0x145:
                    {
                        int data[2] = { ReadCan::COOLANT 
                                        , frame.data[3] }; // -40
                        arr_set_can(data, 2);
                        break;       
                    }
                    case 0x7F3:
                    {
                        int val = int((unsigned char)(frame.data[1]) << 8 |
                                    (unsigned char)(frame.data[0]));
                        int val2 = int((unsigned char)(frame.data[3]) << 8 |
                                    (unsigned char)(frame.data[2]));

                        int data[4] = { ReadCan::TC_INST, val 
                                         , ReadCan::TC_TIME, val2};
                        arr_set_can(data, 4); 
                        break;
                    }
                    case 0x7F5:
                    {
                        int val = int((unsigned char)frame.data[6]);
                        int val2 = int((unsigned char)frame.data[7]);

                        int data[4] = { ReadCan::TMPR_XU4_DISP, val 
                                         , ReadCan::TMPR_FRACTURE_XU4_DISP, val2};
                        arr_set_can(data, 4); 
                        break;
                    }
                    case 0x130:
                    {
                        // int val = 0;
                        // unsigned char i = frame.data[3];
                        // if (i < 0x81) val = tc_inst[ i ];

                        // int val2 = enqueue(val);
                        
                        // int data[4] = { ReadCan::TC_INST, val 
                        //                 , ReadCan::TC_TIME, val2};
                        // arr_set_can(data, 4);   
                        break;
                    }
                    case 0x260:
                    { 
                        pipe_arr_read(data_can, CAN_MEM_ARR_NAME, CAN_MEM_ARR_SIZE, CAN_SEM_ARR_NAME);

                        data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<1);
                        data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<2);
                        data_can[ReadCan::LIGHT_SIGNALS] &=~ (1<<3);
                        if ( frame.data[0] == 0b00100101 )
                            data_can[ReadCan::LIGHT_SIGNALS] |= (1<<1);
                        if ( frame.data[0] == 0b00111010 )
                            data_can[ReadCan::LIGHT_SIGNALS] |= (1<<2);
                        if ( frame.data[0] == 0b00011111 )
                            data_can[ReadCan::LIGHT_SIGNALS] |= (1<<3);
                        
                        arr_write_can(data_can);
                        break;
                    }
                    
                }
                usleep(1000);
            } // while
        } // else
    }
    return 0;
}

