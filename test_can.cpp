#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <math.h>

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
 // https://stackoverflow.com/questions/17006262/clearing-output-of-a-terminal-program-in-linux-c
#define CAN_HELP 1
#define CAN_DRAIN  2
#define CAN_REVERSE_TEST 3
#define CSR_HOME "\33[H"
#define CLR_SCREEN "\33[2J"


#define ROW1 2
#define ROW2 3
#define ROW3 4
#define ROW4 5
#define ROW5 6
#define ROW6 7
#define ROW7 8
#define ROW8 9
#define ROW9 10


void usage(const char * s) {
    printf("Usage: %s <drain>\n", s);
}

#define AA "\033["
#define AB ";0H"

#define PPCAT_NX(A, B)  AA #A AB B
#define PPCAT(A, B) PPCAT_NX(A, B)

#define FORMAT_PRINTF "\033[1m%-10.3F %-10d %-10d %-10d %-10.3f\t%-10.3F\t%-10.3F\033[0m"
#define FORMAT_HEADER "\e[4m\e[31m\e[1m\033[47m%-10s %-10s %-10s %-10s %-10s\t%-10s\t%-10s\e[24m"
#define FORMAT_H "\033[1m%-10.3f %-10d %-10d\033[0m"

#define getmin(a) ( (a) < 10000 ? (0) : (a) )

inline float term_fuel(unsigned char A) { return (float)( (A-128) * 100/128 ); }
inline unsigned char coolant(unsigned char A) { return  (A-40); }
inline unsigned char speed(unsigned char A) { return  (A); }
inline float rpm(unsigned char A, unsigned char B) { return (float)( ((A*256)+B)/4 ); }
inline float module_voltage(unsigned char A, unsigned char B) { return (float)( ((A*256)+B)/1000); }
inline float fuel_rate(unsigned char A, unsigned char B) 
{ 
    float a = (float)( (float)((A*256)+B)*0.05); 
    if ((a > 10000 ) || (a < 10000 ))
        return INFINITY;
    return a;
}
inline float lambda(unsigned char A, unsigned char B) 
{ float a =  (float)( (float)((A*256)+B)/32768 );  
    if ((a > 10000 ) || (a < 10000 ))
        return INFINITY;
    return a;
}
#define accelerator(A) A*100/255

#define engine_load(A) A*100/255
#define fuel_pressure(A) A * 3
#define abs_pressure(A) A
#define air_flow(A,B) ((A*256)+B) / 100
#define oxygen(A) (A-128)*100/128

int main (int argc, char ** argv) 
{
    int mode = 0;
    int mode2 = 0;
    char can0_str[]="can0";
    if ( argc > 1 ) 
    {
        if (!strcmp(argv[1], "help")) 
        {
            usage(argv[0]);
            return 1;
        }
        if (!strcmp(argv[1], "drain"))
            mode = CAN_DRAIN;
        else
            strcpy (can0_str,argv[1]);
    }

    // printf("\033[H\033[J");
    // printf("\e[2J\e[H"); 
    printf("\033[2J");
    printf("\033[H"); 
    // printf("%s%s", CLR_SCREEN, CSR_HOME);
    // printf("%s", CSR_HOME);
    

    if (mode == CAN_DRAIN)
    {
        return 0;
    }
    else
    {
        struct ifreq ifr;
        struct sockaddr_can addr;
     
        struct can_frame frame;
        int s;

        memset(&ifr, 0x0, sizeof(ifr));
        memset(&addr, 0x0, sizeof(addr));
        memset(&frame, 0x0, sizeof(frame));

        /* open CAN_RAW socket */
        s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

        /* convert interface sting "can0" into interface index */ 
        strcpy(ifr.ifr_name, can0_str);
        ioctl(s, SIOCGIFINDEX, &ifr);

        /* setup address for bind */
        addr.can_ifindex = ifr.ifr_ifindex;
        addr.can_family = PF_CAN;

        /* bind socket to the can0 interface */
        bind(s, (struct sockaddr *)&addr, sizeof(addr));
        int n = 0, m = 0;

        //printf("%s%s", CLR_SCREEN, CSR_HOME);

        printf(PPCAT(1, FORMAT_HEADER), "term fuel"
                               , "coolant", "speed", "rpm", "voltage", "fuel rate", "lambda");
        
        while( (n = read(s, &frame, sizeof(frame)) > 0))
        {
            switch (frame.can_id)
            {
                case VECTRA_CAN_ID1: // 1,2 4,5 6,7
                { 
#if VECTRA_CAN_DLC1==8
                            printf(PPCAT(ROW1, FORMAT_PRINTF), term_fuel(  frame.data[0] ), coolant(  frame.data[0] ), speed(  frame.data[0] )
                                    , rpm( frame.data[0], frame.data[1]), module_voltage( frame.data[0],  frame.data[1]), fuel_rate( frame.data[0],  frame.data[1]), lambda( frame.data[0],  frame.data[1]) );
                            printf(PPCAT(ROW2, FORMAT_PRINTF), term_fuel(  frame.data[1] )
                                    , coolant( frame.data[1] ), speed( frame.data[1] ), rpm(frame.data[1], frame.data[2])
                                    , module_voltage(frame.data[1], frame.data[2])
                                    , fuel_rate(frame.data[1], frame.data[2]), lambda(frame.data[1], frame.data[2])  );
                            printf(PPCAT(ROW3, FORMAT_PRINTF), term_fuel(  frame.data[2] )
                                    , coolant( frame.data[2] ), speed( frame.data[2] ), rpm(frame.data[2], frame.data[3])
                                    , module_voltage(frame.data[2], frame.data[3])
                                    , fuel_rate(frame.data[2], frame.data[3]), lambda(frame.data[2], frame.data[3])  );
                            printf(PPCAT(ROW4, FORMAT_PRINTF), term_fuel(  frame.data[3] )
                                    , coolant( frame.data[3] ), speed( frame.data[3] ), rpm(frame.data[3], frame.data[4])
                                    , module_voltage(frame.data[3], frame.data[4])
                                    , fuel_rate(frame.data[3], frame.data[4]), lambda(frame.data[3], frame.data[4])  );
                            printf(PPCAT(ROW5, FORMAT_PRINTF), term_fuel(  frame.data[4] )
                                    , coolant( frame.data[4] ), speed( frame.data[4] ), rpm(frame.data[4], frame.data[5])
                                    , module_voltage(frame.data[4], frame.data[5])
                                    , fuel_rate(frame.data[4], frame.data[5]), lambda(frame.data[4], frame.data[5])  );
                            printf(PPCAT(ROW6, FORMAT_PRINTF), term_fuel(  frame.data[5] )
                                    , coolant( frame.data[5] ), speed( frame.data[5] ), rpm(frame.data[5], frame.data[6])
                                    , module_voltage(frame.data[5], frame.data[6])
                                    , fuel_rate(frame.data[5], frame.data[6]), lambda(frame.data[5], frame.data[6])  );
                            printf(PPCAT(ROW7, FORMAT_PRINTF), term_fuel(  frame.data[6] )
                                    , coolant( frame.data[6] ), speed( frame.data[6] ), rpm(frame.data[6], frame.data[7])
                                    , module_voltage(frame.data[6], frame.data[7])
                                    , fuel_rate(frame.data[6], frame.data[7]), lambda(frame.data[6], frame.data[7])  );
                            printf(PPCAT(ROW8, FORMAT_H), term_fuel(  frame.data[7] )
                                    , coolant( frame.data[7] ), speed( frame.data[7] ) );
                            printf(PPCAT(ROW9,"%c%c%c%c%c%c%c%c"), frame.data[0], frame.data[1], frame.data[2], frame.data[3]
                                                       , frame.data[4], frame.data[5], frame.data[6], frame.data[7]);
#elif VECTRA_CAN_DLC1==7
                            printf(PPCAT(ROW1, FORMAT_PRINTF), term_fuel(  frame.data[0] )
                                    , coolant( frame.data[0] ), speed( frame.data[0] ), rpm(frame.data[0], frame.data[1])
                                    , module_voltage(frame.data[0], frame.data[1])
                                    , fuel_rate(frame.data[0], frame.data[1]), lambda(frame.data[0], frame.data[1]) );
                            printf(PPCAT(ROW2, FORMAT_PRINTF), term_fuel(  frame.data[1] )
                                    , coolant( frame.data[1] ), speed( frame.data[1] ), rpm(frame.data[1], frame.data[2])
                                    , module_voltage(frame.data[1], frame.data[2])
                                    , fuel_rate(frame.data[1], frame.data[2]), lambda(frame.data[1], frame.data[2])  );
                            printf(PPCAT(ROW3, FORMAT_PRINTF), term_fuel(  frame.data[2] )
                                    , coolant( frame.data[2] ), speed( frame.data[2] ), rpm(frame.data[2], frame.data[3])
                                    , module_voltage(frame.data[2], frame.data[3])
                                    , fuel_rate(frame.data[2], frame.data[3]), lambda(frame.data[2], frame.data[3])  );
                            printf(PPCAT(ROW4, FORMAT_PRINTF), term_fuel(  frame.data[3] )
                                    , coolant( frame.data[3] ), speed( frame.data[3] ), rpm(frame.data[3], frame.data[4])
                                    , module_voltage(frame.data[3], frame.data[4])
                                    , fuel_rate(frame.data[3], frame.data[4]), lambda(frame.data[3], frame.data[4])  );
                            printf(PPCAT(ROW5, FORMAT_PRINTF), term_fuel(  frame.data[4] )
                                    , coolant( frame.data[4] ), speed( frame.data[4] ), rpm(frame.data[4], frame.data[5])
                                    , module_voltage(frame.data[4], frame.data[5])
                                    , fuel_rate(frame.data[4], frame.data[5]), lambda(frame.data[4], frame.data[5])  );
                            printf(PPCAT(ROW6, FORMAT_PRINTF), term_fuel(  frame.data[5] )
                                    , coolant( frame.data[5] ), speed( frame.data[5] ), rpm(frame.data[5], frame.data[6])
                                    , module_voltage(frame.data[5], frame.data[6])
                                    , fuel_rate(frame.data[5], frame.data[6]), lambda(frame.data[5], frame.data[6])  );
                            printf(PPCAT(ROW7, FORMAT_H), term_fuel(  frame.data[6] )
                                    , coolant( frame.data[6] ), speed( frame.data[6] ) );
                            printf(PPCAT(ROW8,"%c%c%c%c%c%c%c"), frame.data[0], frame.data[1], frame.data[2], frame.data[3]
                                                       , frame.data[4], frame.data[5], frame.data[6]);
#elif VECTRA_CAN_DLC1==6                                       
                            printf(PPCAT(ROW1, FORMAT_PRINTF), term_fuel(  frame.data[0] )
                                    , coolant( frame.data[0] ), frame.data[0], rpm(frame.data[0], frame.data[1])
                                    , module_voltage(frame.data[0], frame.data[1])
                                    , fuel_rate(frame.data[0], frame.data[1]), lambda(frame.data[0], frame.data[1]) );
                            printf(PPCAT(ROW2, FORMAT_PRINTF), term_fuel(  frame.data[1] )
                                    , coolant( frame.data[1] ), speed( frame.data[1] ), rpm(frame.data[1], frame.data[2])
                                    , module_voltage(frame.data[1], frame.data[2])
                                    , fuel_rate(frame.data[1], frame.data[2]), lambda(frame.data[1], frame.data[2])  );
                            printf(PPCAT(ROW3, FORMAT_PRINTF), term_fuel(  frame.data[2] )
                                    , coolant( frame.data[2] ), speed( frame.data[2] ), rpm(frame.data[2], frame.data[3])
                                    , module_voltage(frame.data[2], frame.data[3])
                                    , fuel_rate(frame.data[2], frame.data[3]), lambda(frame.data[2], frame.data[3])  );
                            printf(PPCAT(ROW4, FORMAT_PRINTF), term_fuel(  frame.data[3] )
                                    , coolant( frame.data[3] ), speed( frame.data[3] ), rpm(frame.data[3], frame.data[4])
                                    , module_voltage(frame.data[3], frame.data[4])
                                    , fuel_rate(frame.data[3], frame.data[4]), lambda(frame.data[3], frame.data[4])  );
                            printf(PPCAT(ROW5, FORMAT_PRINTF), term_fuel(  frame.data[4] )
                                    , coolant( frame.data[4] ), speed( frame.data[4] ), rpm(frame.data[4], frame.data[5])
                                    , module_voltage(frame.data[4], frame.data[5])
                                    , fuel_rate(frame.data[4], frame.data[5]), lambda(frame.data[4], frame.data[5])  );
                            printf(PPCAT(ROW6, FORMAT_H), term_fuel(  frame.data[5] )
                                    , coolant( frame.data[5] ), speed( frame.data[5] ) );
                            printf(PPCAT(ROW7, "%c%c%c%c%c%c"), frame.data[0], frame.data[1], frame.data[2], frame.data[3]
                                                       , frame.data[4], frame.data[5]);
#elif VECTRA_CAN_DLC1==5                       
                            printf(PPCAT(1, FORMAT_PRINTF), term_fuel(  frame.data[0] )
                                    , coolant( frame.data[0] ), speed( frame.data[0] ), rpm(frame.data[0], frame.data[1])
                                    , module_voltage(frame.data[0], frame.data[1])
                                    , fuel_rate(frame.data[0], frame.data[1]), lambda(frame.data[0], frame.data[1]) );
                            printf(PPCAT(2, FORMAT_PRINTF), term_fuel(  frame.data[1] )
                                    , coolant( frame.data[1] ), speed( frame.data[1] ), rpm(frame.data[1], frame.data[2])
                                    , module_voltage(frame.data[1], frame.data[2])
                                    , fuel_rate(frame.data[1], frame.data[2]), lambda(frame.data[1], frame.data[2])  );
                            printf(PPCAT(3, FORMAT_PRINTF), term_fuel(  frame.data[2] )
                                    , coolant( frame.data[2] ), speed( frame.data[2] ), rpm(frame.data[2], frame.data[3])
                                    , module_voltage(frame.data[2], frame.data[3])
                                    , fuel_rate(frame.data[2], frame.data[3]), lambda(frame.data[2], frame.data[3])  );
                            printf(PPCAT(4, FORMAT_PRINTF), term_fuel(  frame.data[3] )
                                    , coolant( frame.data[3] ), speed( frame.data[3] ), rpm(frame.data[3], frame.data[4])
                                    , module_voltage(frame.data[3], frame.data[4])
                                    , fuel_rate(frame.data[3], frame.data[4]), lambda(frame.data[3], frame.data[4])  );
                            printf(PPCAT(5, FORMAT_H), term_fuel(  frame.data[4] )
                                    , coolant( frame.data[4] ), speed( frame.data[4] ) );
                            printf(PPCAT(6, "%c%c%c%c%c"), frame.data[0], frame.data[1], frame.data[2], frame.data[3]
                                                       , frame.data[4]);
#elif VECTRA_CAN_DLC1==4     
                            printf(PPCAT(1, FORMAT_PRINTF), term_fuel(  frame.data[0] )
                                    , coolant( frame.data[0] ), speed( frame.data[0] ), rpm(frame.data[0], frame.data[1])
                                    , module_voltage(frame.data[0], frame.data[1])
                                    , fuel_rate(frame.data[0], frame.data[1]), lambda(frame.data[0], frame.data[1]) );
                            printf(PPCAT(2, FORMAT_PRINTF), term_fuel(  frame.data[1] )
                                    , coolant( frame.data[1] ), speed( frame.data[1] ), rpm(frame.data[1], frame.data[2])
                                    , module_voltage(frame.data[1], frame.data[2])
                                    , fuel_rate(frame.data[1], frame.data[2]), lambda(frame.data[1], frame.data[2])  );
                            printf(PPCAT(3, FORMAT_PRINTF), term_fuel(  frame.data[2] )
                                    , coolant( frame.data[2] ), speed( frame.data[2] ), rpm(frame.data[2], frame.data[3])
                                    , module_voltage(frame.data[2], frame.data[3])
                                    , fuel_rate(frame.data[2], frame.data[3]), lambda(frame.data[2], frame.data[3])  );
                            printf(PPCAT(4, FORMAT_H), term_fuel(  frame.data[3] )
                                    , coolant( frame.data[3] ), speed( frame.data[3] ) );
                            printf(PPCAT(5,"%c%c%c%c"), frame.data[0], frame.data[1], frame.data[2], frame.data[3]);
#elif VECTRA_CAN_DLC1==3
                            printf(PPCAT(1, FORMAT_PRINTF), term_fuel(  frame.data[0] )
                                    , coolant( frame.data[0] ), speed( frame.data[0] ), rpm(frame.data[0], frame.data[1])
                                    , module_voltage(frame.data[0], frame.data[1])
                                    , fuel_rate(frame.data[0], frame.data[1]), lambda(frame.data[0], frame.data[1]) );
                            printf(PPCAT(2, FORMAT_PRINTF), term_fuel(  frame.data[1] )
                                    , coolant( frame.data[1] ), speed( frame.data[1] ), rpm(frame.data[1], frame.data[2])
                                    , module_voltage(frame.data[1], frame.data[2])
                                    , fuel_rate(frame.data[1], frame.data[2]), lambda(frame.data[1], frame.data[2])  );
                            printf(PPCAT(3, FORMAT_H), term_fuel(  frame.data[2] )
                                    , coolant( frame.data[2] ), speed( frame.data[2] ) );
                            printf(PPCAT(4, "%c%c%c"), frame.data[0], frame.data[1], frame.data[2]);
#else                    
                            printf(PPCAT(1, FORMAT_PRINTF), term_fuel(  frame.data[0] )
                                    , coolant( frame.data[0] ), speed( frame.data[0] ), rpm(frame.data[0], frame.data[1])
                                    , module_voltage(frame.data[0], frame.data[1])
                                    , fuel_rate(frame.data[0], frame.data[1]), lambda(frame.data[0], frame.data[1]) );
                            printf(PPCAT(2, FORMAT_H), term_fuel(  frame.data[1] )
                                    , coolant( frame.data[1] ), speed( frame.data[1] ) );
                            printf(PPCAT(3,"%c%c"), frame.data[0], frame.data[1]);
#endif
                    break;
                }
           
            } // switch
            usleep(1000);
        } // while
    } //else
    return 0;
}

