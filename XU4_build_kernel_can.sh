#/bin/bash

sudo apt install git gcc g++ build-essential
git clone --depth 1 https://github.com/hardkernel/linux -b odroidxu4-4.14.y
cd linux

https://github.com/hardkernel/linux/blob/odroidxu4-4.14.y/drivers/net/can/spi/mcp251x.c#L1046

freq=freq = 8000000;

https://wiki.odroid.com/odroid-xu4/software/building_kernel

make odroidxu4_defconfig
$ make -j8
$ sudo make modules_install
$ sudo cp -f arch/arm/boot/zImage /media/boot
$ sudo cp -f arch/arm/boot/dts/exynos5422-odroidxu3.dtb /media/boot
$ sudo cp -f arch/arm/boot/dts/exynos5422-odroidxu4.dtb /media/boot
$ sudo cp -f arch/arm/boot/dts/exynos5422-odroidxu3-lite.dtb /media/boot
$ sync
$ sudo cp .config /boot/config-`make kernelrelease`
$ sudo update-initramfs -c -k `make kernelrelease`
$ sudo mkimage -A arm -O linux -T ramdisk -C none -a 0 -e 0 -n uInitrd -d /boot/initrd.img-`make kernelrelease` /boot/uInitrd-`make kernelrelease`
$ sudo cp /boot/uInitrd-`make kernelrelease` /media/boot/uInitrd
$ sync
$ sudo sync
$ sudo reboot