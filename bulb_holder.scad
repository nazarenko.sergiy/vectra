

include <NopSCADlib/utils/core/core.scad>
use <NopSCADlib/utils/thread.scad>

//$fn=256;

module prism(l, w, h){
       polyhedron(
               points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
               faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
               );

 }

module koko(female, dia, height)
{
    pitch = 2;
    starts = 4;

    profile = thread_profile(pitch / 2, pitch * 0.366, 30);

    length = height ; //female ? 24  : 30;
    dia = female ? dia : dia - pitch;
    brass = [1,0,0.5, 0.5];
    silver = [1,0,0.5, 0.5];
    colour = female ? brass : silver;

    thread(dia, starts * pitch, length, profile, starts = starts, top = 45, bot = 45, female = female, colour = colour);
}


hole_koko_dia = 19;
hole_small_dia = 10;
thread_dia = hole_koko_dia -.4;



module lamp(){
color(c=[1,0,0.5, 0.5])
union() {
    cylinder(8, d=14.8, false);
    translate([0, 0, 4]) cylinder(40, d=19, false);
}
}


module tmp_holder() {
difference()  {
    union()
    {
        translate([0, 0, 5]) cylinder(14.8, d=27, false);
       // translate([0, 0, -19])  cylinder(24, d=18.3, false);
       translate([0, 0, -5])  cylinder(24, d=18.3, false);
       // koko(false, thread_dia-0.3, 38);
    }
translate([0, 0, -25]) cylinder(50, d=13);
}
}

module holder() {
    color([0,1,0.3])
    difference()  {
        translate([0, 0, 60])
        tmp_holder();
        translate([0, 0, 70]) 
        lamp();
    }
}

translate([0, 0, -18]) holder();

color([0.8,0.9,1])/*
union() {
//translate([0, 0, 4.7]) koko(true, thread_dia+0.3, 8);
difference()  {
    difference()  {
        union() {
            translate([0, 0, -4]) cylinder(8, d=39.2, false); // base
            //cylinder(23.5, d=27, false); //main
            translate([0, 0, 8.7-1.5]) cylinder(1.5, d=30.1, false); //middle
            cylinder(8.7, d=29.1, false);
            }
            translate([0, 0, -1]) cylinder(50, d=hole_koko_dia+0.3);
            
        }
        translate([0, 0, 0]) cylinder(100, d=hole_small_dia);
}
translate([-3, 9.5, 8]) rotate([0,0,0]) cube([4.8,2.8,4.8]);
translate([-3, -12.2, 8]) rotate([0,0,0]) cube([4.8,2.8,4.8]);

knob_height = 17;
knob_width = 5;
translate([-knob_width/2, -18, -knob_height-2]) cube([knob_width,36,knob_height]);
}//union

sc = 0.2;
pum_height = 4;
q_size = 30;
//color([0,0.5,1])
translate([0, -34, 8.7])
difference() 
{
difference() 
{
    union () {
        cylinder(14.8, d=27.2, false); //main
        difference() 
        {
            translate([0, 0, 12.3]) cylinder(2.5, d=30.5, false); //skirt
            union() 
            {
                color("orange") translate([-7, 3.2, 10])    cube([30, 20, 6]);
                color("violet") translate([-3.3, -33.5, 10])  cube([30, 30, 6]);
                color("olive") translate([-33.7 - pum_height, -35, 10]) cube([30, 30, 6]);
                color("gray") translate([-37 - pum_height, -30, 10]) cube([30, 60, 6]);
            }//union
        }//diff
    }//union
    translate([0, 0, -10]) cylinder(50, d=hole_koko_dia);
}//diff



union() {
translate([-3.05, 7.7, -1]) rotate([0,0,0]) cube([4.9,5.9,5.3]); // holder1
translate([-3.0, -14.7, -1]) rotate([0,0,0]) cube([4.9,5.9,5.3]); // holder2
translate([-21.1, -3.1, -8]) 
{ 
    union()
    {
        cube([10,6.5,40]);   
        translate([10,-1,60]) 
        rotate([-45,90,0]) 
        scale([sc,sc,sc])
        color("gray") 
            prism(600, 30, 30);};
     }; // VCC
    
}
 union(){
      translate([11.1, 2.0, -6]) 
      rotate([0,0,26]) 
        {
            cube([10,6.5,40]);
            translate([0,7.5,60]) 
            rotate([-225,90,0]) 
            scale([sc,sc,sc])
            color("violet") 
                prism(600, 30, 30);
        }
     }; // GND
}//diff

*/


union() {
translate([0, 0, 23]) {
    difference()
        {
            union (){
        cylinder(14, d=14.8, false);
               translate([0, 0, 6.5])  cylinder(h=15, r1=0, r2=9.17, center=true);
            }
        translate([0, 0, -1]) difference()
        {
            
            cylinder(d=12, h=20);
             
            translate([0,-52,0])
            cube(100, center=true);
                   

        }
    }
}
}