#!/bin/bash

sec_args="reboot|silent|reset|firmware|bootloader|timestamp|dump|scan"

# Function to convert the primary argument to a numeric value
get_numeric_value() {
    case "$1" in
        gray)
            echo "02"
            ;;
        odb2)
            echo "10"
            ;;
        block)
            echo "01"
            ;;
        purple)
            echo "04"
            ;;
        red)
            echo "08"
            ;;
        all)
            echo "80"
            ;;
        *)
            echo "Invalid primary argument. Usage: $0 <gray|odb2|block|purple|red|all> <$sec_args> "
            exit 1
            ;;
    esac
}

# Function to print a secondary action based on the secondary argument
get_secondary_cmd() {
    case "$1" in
        reboot)
            echo "Rebooting $2"
            ;;	
        silent)
	    echo "$20004"
            ;;
    	firmware)
	    echo "$20400"
            ;;
        bootloader)
	    echo "$20800"
            ;;
        timestamp)
	    echo "$21600"
	    ;;	
        dump)
	    echo "$20001"
            ;;
        scan)
	    echo "$20002"
            ;;
    	reset)
            echo "$28000"
            ;;
        *)
            echo "Invalid secondary argument for $2"
            exit 1
            ;;
    esac
}

# Check if the script has either two primary arguments or a primary argument and "all"
#  && [ $# -ne 3 ]
if [ $# -ne 2 ]; then
    echo "Usage: $0 <gray|odb2|block|all> <$sec_args>"
    exit 1
fi

# If "all" is provided as the second argument, execute the secondary action for all primary arguments
if [ "$1" = "all" ]; then
    primary_arg="$1"
    numeric_value=$(get_numeric_value "$primary_arg")
    cmd=$(get_secondary_cmd "$2" "$numeric_value")
    $(cansend can0 7FF#"$cmd"0000000000)
    #for primary_arg in "gray" "odb2" "block"; do
    #    numeric_value=$(get_numeric_value "$primary_arg")
    #    greet_secondary "$2" "$numeric_value"
    #done
else
    # Get the numeric value for the primary argument
    primary_arg="$1"
    numeric_value=$(get_numeric_value "$primary_arg")

    # Parse the secondary argument
    cmd=$(get_secondary_cmd "$2" "$numeric_value")
    $(cansend can0 7FF#"$cmd"0000000000)
fi


